/**
 * ConfigModule
 *   name: string = The name of the config module
 *   setup: function? = A synchronous setup function run after
 *      the config property for the module has been created.
 *   asyncSetup: async function? = An async setup function run when the
 *       asyncSetup function export is called.
 *
 *  All the state of the module should be stored on `config.{mod.name}`
 *      state stored in the global scope maybe invalid if the config is
 *      re-initialized after for a different app on the same domain.
 */
function setup(...modules) {
    globalThis.config = {
        // eslint-disable-next-line no-undef
        dev: process.env.NODE_ENV !== 'production',
        store_debug: false,
        __modules: modules,
        ...globalThis.__config ?? {},
    };

    for (const mod of config.__modules) {
        Object.defineProperty(config, mod.name, {
            writable: false,
            value: {},
            configurable: false,
        });

        mod.setup?.();
    }
}


async function asyncSetup() {
    for (const mod of config.__modules) {
        await mod.asyncSetup?.();
    }
}

export default {
    asyncSetup,
    setup,
};
