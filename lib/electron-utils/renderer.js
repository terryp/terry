import { ipcRenderer } from 'electron';

export const main = new Proxy({}, {
    get(target, key, receiver) {
        if (key === 'on') {
            return function(...args) {
                return ipcRenderer.on(...args);
            };
        }

        return async function(...args) {
            return await ipcRenderer.invoke(key, ...args);
        };
    },
});


export function setupRenderer() {
    globalThis.main = main;
    return undo;
}

function undo() {
    delete globalThis.main;
}
