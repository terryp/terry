import EventEmitter from 'node:events';
import { createReadStream, promises as fs } from 'node:fs';
import { resolve, dirname } from 'node:path';
import { defaultsDeep as mergeDefaults, set, get } from 'lodash-es';
import { ipcMain, app } from 'electron';
import toml from '@iarna/toml';


export function expose(channel, handler) {
    ipcMain.handle(channel, handler);
    return () => ipcMain.removeHandler(channel);
}

export class Settings extends EventEmitter {
    constructor(values, defaults, path, lock) {
        super();
        this.values = values;
        this.defaults = defaults;
        this.path = path;
        this.nextWrite = null;
        this.writing = false;
        this.lock = lock;
    }

    startWrite() {
        if (!this.writing && this.nextWrite) {
            this.write();
        }
    }

    set(path, value) {
        set(this.values, path, value);
        this.change();
        this.nextWrite = this.values;
        this.startWrite();
    }

    get(path = false) {
        if (path) {
            return get(this.values, path);
        }
        return this.values;
    }


    async write(lock = false) {
        this.writing = true;

        if (this.lock && !lock) {
            return await this.lock.sharedLock(this.write.bind(this), true);
        }

        try {
            const value = toml.stringify(this.nextWrite);
            this.nextWrite = null;
            await fs.writeFile(this.path, value, {
                encoding: 'utf8',
            });
        } catch (err) {
            this.emit('error', err);
        } finally {
            if (this.nextWrite) {
                this.write();
            } else {
                this.writing = false;
            }
        }
    }

    change() {
        this.emit('change', this.values);
    }


    static async load(defaults, lock) {
        const path = resolve(app.getPath('userData'), 'settings.toml');
        await fs.mkdir(dirname(path), { recursive: true });
        // Open the file to create it if not present
        const handle = await fs.open(path, 'a');
        await handle.close();
        const stream = createReadStream(path);
        const values = await toml.parse.stream(stream);
        mergeDefaults(values, defaults);
        return new this(values, defaults, path, lock);
    }
}

