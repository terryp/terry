// should this be a global symbol to coordinate between versions?
import micromatch from 'micromatch';
const cacheSymbol = Symbol('The instance cache for this class.');


export class PurePath {
    constructor(path, pathImpl) {
        if (path instanceof PurePath) {
            path = String(path);
        }

        const normalized = pathImpl.normalize(path);

        const Class = new.target;

        if (!Object.hasOwn(Class, cacheSymbol)) {
            Class[cacheSymbol] = new Map();
        }

        const cache = Class[cacheSymbol];

        const cachedRef = cache.get(normalized);
        if (cachedRef) {
            const defRefed = cachedRef.deref();
            if (defRefed) {
                return defRefed;
            }
        }

        const ref = new WeakRef(this);
        cache.set(normalized, ref);

        const parsed = pathImpl.parse(normalized);

        const atRoot = parsed.root === parsed.dir;
        const atEnd = parsed.dir === '';

        if (!atRoot && !atEnd) {
            this.parent = new this.constructor(parsed.dir);

            this.parents = this.parent.parents
                ? this.parent.parents.concat(this.parent)
                : [this.parent];
        } else {
            this.parent = null;
            this.parents = [];
        }


        Object.freeze(this.parents);


        // remove the dot
        this.suffix = parsed.ext.slice(1);

        this.ext = parsed.ext;
        this.base = parsed.base;
        this.name = parsed.name;
        this.dir = parsed.dir;
        this.root = parsed.root;
        this.string = pathImpl.format(this);
        this.segments = this.string.split(pathImpl.sep);
        this.__pathImpl = pathImpl;

        Object.freeze(this);
    }

    toPath() {
        return this.string;
    }

    toString() {
        return this.string;
    }

    toUrl() {
        return `file://${this.string}`;
    }

    isAbs() {
        return this.__pathImpl.isAbsolute(this.string);
    }

    join(...others) {
        const joined = this.__pathImpl.join(this.string, ...others.map(
            (other) => other.toString(),
        ));
        return new this.constructor(joined);
    }

    resolve(...paths) {
        const strPaths = paths.map((path) => String(path));
        const resolvedString = this.__pathImpl.resolve(
            this.string,
            ...strPaths,
        );
        return new this.constructor(resolvedString);
    }

    relativeTo(from) {
        const relativeString = this.__pathImpl.relative(
            String(from),
            String(this),
        );
        return new this.constructor(relativeString);
    }

    withExt(ext) {
        const self = this;
        const formatObject = {
            ...self,
            // base also has the ext, and format will use that ext if present
            base: null,
            ext,
        };

        const newString = this.__pathImpl.format(formatObject);

        return new this.constructor(newString);
    }

    match(...patterns) {
        const maybeOptions = patterns.at(-1);
        if (patterns.length > 1 && typeof maybeOptions === 'object') {
            patterns = patterns.slice(0, -1);
        }

        return micromatch.isMatch(this.string, patterns, maybeOptions);
    }

    [Symbol.for('nodejs.util.inspect.custom')]() {
        const cls_name = this.constructor.name;
        return `${cls_name}(${this.string})`;
    }

    get length() {
        return this.parents.length + 1;
    }
}
