import pathFuncs from 'node:path';
import { promises as fs, createReadStream, createWriteStream } from 'node:fs';
import { PurePath } from './base.js';


export class NodePath extends PurePath {
    constructor(path) {
        super(path, pathFuncs);
    }

    async isDir() {
        try {
            const stat = await fs.stat(this.toString());
            return await stat.isDirectory();
        } catch (err) {
            if (err.errno === -2) {
                return false;
            }
            throw err;
        }
    }

    async isFile() {
        try {
            const stat = await fs.stat(this.toString());
            return await stat.isFile();
        } catch (err) {
            if (err.errno === -2) {
                return false;
            }
            throw err;
        }
    }

    async isSymLink() {
        try {
            const stat = await fs.lstat(this.toString());
            return await stat.isSymbolicLink();
        } catch (err) {
            if (err.errno === -2) {
                return false;
            }
            throw err;
        }
    }

    async *opendir(options = {}) {
        options.encode = options.encoding ?? 'utf8';
        for await (const entry of await fs.opendir(this.string, options)) {
            yield this.join(entry.name);
        }
    }

    async findUp(test) {
        if (!this.isAbs()) {
            return await this.resolve().findUp(test);
        }

        if (await this.isDir()) {
            const found = await test(this);
            if (found) {
                return this;
            }
        }

        if (this.parent) {
            return await this.parent.findUp(test);
        }

        return null;
    }

    async readFile(options = {}) {
        options.encoding = options.encoding ?? 'utf8';

        return await fs.readFile(this.toString(), options);
    }

    async writeFile(data, options = {}) {
        return await fs.writeFile(this.toString(), data, options);
    }

    async *[Symbol.asyncIterator]() {
        if (await this.isFile()) {
            for await (const chunk of this.readStream()) {
                yield chunk;
            }
        } else if (await this.isDir()) {
            for await (const entry of this.opendir()) {
                yield entry;
            }
        }
    }

    async *glob(patterns, options = {}) {
        for await (const file of fs.glob(patterns, { cwd: String(this) })) {
            yield this.join(file);
        }
    }

    readStream(options = {}) {
        options.encoding ??= 'utf8';
        return createReadStream(this.toString(), options);
    }

    writeStream(options = {}) {
        options.encoding ??= 'utf8';

        return createWriteStream(this.toString(), options);
    }

    async mkdir({ exist_ok = false, ...options }) {
        try {
            return await fs.mkdir(this.toString(), options);
        } catch (err) {
            if (exist_ok && err.code === 'EEXIST') {
                return;
            } else {
                throw err;
            }
        }
    }

    async open(flags, mode) {
        return await fs.open(this.toString(), flags, mode);
    }

    async writeJSON(obj, { indent = 4, replacer = null, ...options } = {}) {
        const data = JSON.stringify(obj, replacer, indent);

        return await this.writeFile(data, options);
    }

    async readJSON(options) {
        const str = await this.readFile(options);
        return JSON.parse(str);
    }

    async unlink() {
        return await fs.unlink(this.toString());
    }

    static cwd() {
        // eslint-disable-next-line no-undef
        return new this(process.cwd());
    }

    [Symbol.for('nodejs.util.inspect.custom')]() {
        const cls_name = this.constructor.name;
        return `${cls_name}(${this.string})`;
    }
}

export { NodePath as Path };
