import pathFuncs from 'path-browserify';
import { PurePath } from './base.js';

class StreamAsyncIter {
    constructor(stream) {
        this.reader = stream.getReader();
        this.decoder = new TextDecoder();
    }

    async next() {
        const result = await this.reader.read();
        if (result.value !== undefined) {
            result.value = this.decoder.decode(result.value);
        }

        return result;
    }

    return() {
        this.reader.releaseLock();
        return {};
    }

    [Symbol.asyncIterator]() {
        return this;
    }
}

export class WebPath extends PurePath {
    constructor(path) {
        super(path, pathFuncs);
    }

    async *[Symbol.asyncIterator]() {
        const resp = await fetch(this.toString());

        for await (const chunk of new StreamAsyncIter(resp.body)) {
            yield chunk;
        }
    }
}

export { WebPath as Path };
