import test from 'ava';

import { Path } from '@omniblack/pathlib';

test('Normalize paths', (t) => {
    const path = new Path('./test//test.js');
    const actualString = path.toString();

    const expectedString = 'test/test.js';

    t.is(actualString, expectedString, 'Remove multiple path separators.');

    const dottyPath = new Path('./test/../test.js');
    const actualDottyString = dottyPath.toString();

    const expectedDottyString = 'test.js';

    // TODO: This messages sucks, What would a better one be
    t.is(actualDottyString, expectedDottyString, 'Resolve internal path ..');
});

test('Path caching', (t) => {
    const mainPath = new Path('./cached.js');

    const sameLiteral = new Path('./cached.js');

    t.is(mainPath, sameLiteral, 'Caches identical paths');

    const normalizesToSame = new Path('.//cached.js');

    t.is(mainPath, normalizesToSame, 'Caches normalized path');
});

test('Subclassing', (t) => {
    class Subclass extends Path {}

    const subInstance = new Subclass('/home/user/test.js');

    const resolved = subInstance.resolve();

    t.assert(
        resolved instanceof Subclass,
        'Correctly creates subclass instances',
    );
});
