from contextlib import ExitStack, contextmanager
from importlib.metadata import entry_points
from os import environ

from public import public
from requests import Session

from omniblack.logging import Verbosity
from omniblack.model import Data, Model
from omniblack.utils import load_plugins

try:
    from packaging.version import Version
except ImportError:
    Version = str

from .app import App
from .load_options import load_app_config
from .modes import get_mode_env
from .storage import Storage


def is_set(env_name):
    value = environ.get(env_name, '')
    return bool(value.strip())


def enable_color() -> bool:
    match environ.get('OMNIBLACK_COLORS', ''):
        case '0':
            return False
        case '1':
            return True

    match environ.get('PYTHON_COLORS', ''):
        case '0':
            return False
        case '1':
            return True

    if is_set('NO_COLOR'):
        return False

    if is_set('FORCE_COLOR'):
        return True

    return environ.get('TERM', 'dumb') != 'dumb'


NO_DEFAULT = object()


@public
class Factory:
    def __init__(self, func):
        self.__func = func

    def __repr__(self):
        cls_name = self.__class__.__name__
        return f'{cls_name}({self.__func})'

    def __rich_repr__(self):
        yield self.__func

    def __call__(self):
        return self.__func


class app_property:  # noqa: N801
    def __init__(
        self,
        *,
        default=NO_DEFAULT,
    ):
        self.default = default

    def __set_name__(self, owner, name):
        self._attr_name = name
        self.__objclass__ = owner
        self._storage_name = f'_{name}'

    def __set__(self, instance, value):
        setattr(instance, self._storage_name, value)

    def __delete__(self, instance):
        delattr(instance, self._storage_name)

    def __get__(self, instance, owner=None):
        if instance is None:
            return self

        try:
            return getattr(instance, self._storage_name)
        except AttributeError:
            return getattr(instance.app, self._attr_name)


def call_setup_funcs():
    eps = entry_points(group='omniblack.app', name='setup')

    for ep in eps:
        func = ep.load()
        func()


def flatten_option_structs(option_structs, plugins):
    option_structs = list(option_structs or ())

    for plugin in plugins:
        option_structs.extend(plugin.option_structs)

    return tuple(option_structs)


@public
class Config(Data):
    app: App
    color: bool
    verbosity: Verbosity
    model: Model = app_property()
    version: Version = app_property()
    storage: Storage = app_property()

    def __init__(self):
        self.__stack = ExitStack()

        super().__init__({
            'mode': get_mode_env(),
            'verbosity': Verbosity.DEBUG,
            'color': enable_color(),
        })

    def load_app(
        self,
        dist_name,
        module,
        storage=None,
        model=None,
        option_structs=None,
        plugins=(),
    ):
        app = App(
            dist_name,
            storage=storage,
            model=model,
            module=module,
        )
        self.plugins = load_plugins(plugins, self._init_plugin)

        self._data['dist_name'] = dist_name
        self._data['app'] = app

        option_structs = flatten_option_structs(option_structs, self.plugins)

        if option_structs:
            for key, value in load_app_config(app.name, option_structs):
                self._data[key] = value

    @contextmanager
    def task(self):
        try:
            self.__stack = ExitStack()
            self.http = self.enter_context(Session())
            yield self
        finally:
            self.__stack.close()

    def enter_context(self, context_manager):
        return self.__stack.enter_context(context_manager)

    def finish_setup(self, storage=None):
        self.app.storage = storage
        call_setup_funcs()

    def _init_plugin(self, plugin_def):
        if isinstance(plugin_def, list | tuple):
            plugin_cls, plugin_args = plugin_def
            return plugin_cls(*plugin_args, server=self)
        else:
            return plugin_def(server=self)


config = Config()

public(config=config)
