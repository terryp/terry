from enum import auto
from os import environ

from public import public

from omniblack.utils import Enum


def all_parents(member, final_class):
    seen = set()
    parent_names = list(reversed(tree[member.name]))

    while parent_names:
        parent_name = parent_names.pop()
        if parent_name in seen:
            continue

        parent_member = final_class[parent_name]
        seen.add(parent_name)
        parent_names.extend(tree[parent_member.name])
        yield parent_member


@public
class Modes(Enum):
    def __contains__(self, other):
        return other in self._parents

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        cls = self.__class__
        if not isinstance(other, cls):
            return NotImplemented

        return self.name == other.name

    def __repr__(self):
        cls_name = self.__class__.__name__
        return f'<{cls_name}.{self.name}>'

    default = auto()
    interactive = auto()

    headless = auto()
    container = auto()
    docker = auto()
    podman = auto()

    systemd = auto()
    supervisord = auto()


tree = {
    'default': (),
    'interactive': ('default', ),

    'headless': ('default',),
    'container': ('headless',),
    'docker': ('container',),
    'podman': ('container',),

    'systemd': ('headless',),
    'supervisord': ('headless',),
}


for member in Modes.__members__.values():
    member._parents = set(all_parents(member, Modes))


def get_mode_env():
    current_mode = environ.get('OMNIBLACK_MODE', 'default')
    current_mode = current_mode.lower()

    return Modes[current_mode]
