from omniblack.model import Model
from omniblack.utils import improve_module

model = Model('omniblack.app', struct_packages=['omniblack.app.structs'])

improve_module(__name__)
