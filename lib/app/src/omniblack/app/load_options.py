from os import environ, path, scandir

from ruamel.yaml import YAML

from omniblack.string_case import Cases


def expand(path_str):
    return path.expanduser(path.expandvars(path_str))


def get_config_file(app_name, app_name_upper):
    env_var = f'{app_name_upper}_CONFIG_PATH'

    if file_path := environ.get(env_var, '').strip():
        return expand(file_path)
    else:
        return f'/etc/{app_name}.yaml'


def from_dir(yaml, directory):
    if not path.exists(directory) or not path.isdir(directory):
        return

    for entry in scandir(directory):
        if entry.is_file() and entry.name.endswith('.yaml'):
            yield load_file(yaml, entry)


def get_config_dir(yaml, config_path, app_name_upper):
    if dir_paths := environ.get(f'{app_name_upper}_CONFIG_DIRS', '').strip():
        for dir_path in dir_paths.split(':'):
            dir_path = expand(dir_path.strip())
            yield from from_dir(yaml, dir_path)
    else:
        root, _ext = path.splitext(config_path)
        dir_path = root + '.d'
        yield from from_dir(yaml, dir_path)


class Env:
    def __init__(self, app_name_upper):
        self.__app_name_upper = app_name_upper
        self.__var_prefix = app_name_upper + '_'
        self.__data = {
            key: value
            for key, value in environ.items()
            if key.startswith(app_name_upper + '_')
        }

    def __get_key(self, key):
        return self.__var_prefix + Cases.CapSnake.to(key)

    def __getitem__(self, key):
        return self.__data[self.__get_key(key)]

    def __contains__(self, key):
        return self.__get_key(key) in self.__data

    def get(self, key, default=None):
        return self.__data.get(self.__get_key(key), default)

    def __rich_repr__(self):
        yield self.__data


def load_file(yaml, file_path):
    with open(file_path) as file:
        data = yaml.load(file)

    return ('yaml', data)


def load_app_config(app_name, option_structs):
    data_sources = get_data_sources(app_name)

    for struct in option_structs:
        rec = {}
        struct_model = struct.meta.model
        for field in struct.meta.fields:
            for data_format, source in data_sources:
                if value := source.get(field.name):
                    rec[field.name] = struct_model.types.from_format(
                        data_format,
                        value,
                        field,
                    )

        yield struct.meta.name, struct.hydrate(rec)


def get_data_sources(app_name):
    app_name_upper = Cases.CapSnake.to(app_name)

    data_sources = [('env', Env(app_name_upper))]
    yaml = YAML()

    config_file = get_config_file(app_name, app_name_upper)

    if path.exists(config_file) and path.isfile(config_file):
        data_sources.append(load_file(yaml, config_file))

    data_sources.extend(
        get_config_dir(yaml, config_file, app_name_upper),
    )

    return data_sources
