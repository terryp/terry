from dataclasses import dataclass
from importlib.metadata import Distribution, distribution
from json import loads

from public import public

try:
    from packaging.version import Version
except ImportError:
    Version = str

from omniblack.model import Model, coerce_from

from . import package_config
from .storage import Storage


@public
@dataclass
class App:
    dist_name: str
    version: Version
    dist: Distribution
    storage: Storage | None
    model: Model | None
    package_config: dict | None
    config: object | None = None
    module: str | None = None

    def __init__(
        self,
        dist_name,
        storage=None,
        model=None,
        module=None,
    ):
        self.dist_name = dist_name
        self.dist = distribution(self.dist_name)
        self.version = Version(self.dist.version)
        self.model = model
        self.module = module
        self.storage = storage

        for file in self.dist.files:
            if file.name == 'omniblack_package.json':
                rec = loads(file.read_text())
                self.package_config = coerce_from(
                    package_config.meta.model,
                    rec,
                    'json',
                    'package_config',
                )
                break
        else:
            self.package_config = package_config()

        self.name = self.package_config.get('name', self.dist_name)
        self.display_name = self.package_config.get('product_name', self.name)


class AppProxy:
    def __init__(self):
        self._app = None

    @property
    def __class__(self):
        return App

    def __getattr__(self, name):
        if self._app is None:
            msg = 'No app is currently active.'
            raise AttributeError(msg)
        else:
            return getattr(self._app, name)


app = AppProxy()
public(app=app)
