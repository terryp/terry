from omniblack.model import ModelType

from .modes import Modes


class Mode(ModelType):
    def from_string(self, value):
        value_lower = value.lower()

        try:
            return Modes[value_lower]
        except KeyError:
            msg = f'{value} is not a valid mode.'
            raise ValueError(msg, value)

    def to_string(self, value):
        return str(value)
