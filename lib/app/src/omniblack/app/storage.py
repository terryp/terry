from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import datetime
from enum import StrEnum

from public import public

from omniblack.model import Model, StructBase


class SaveStatus(StrEnum):
    created = 'created'
    modified = 'modified'
    unmodified = 'unmodified'


type SaveResult = StructBase | None | tuple[SaveStatus, StructBase | None]


@dataclass
class RecordMetadata:
    checksum: str
    last_modified: datetime


type ReadResult = StructBase | None | tuple[StructBase | None, RecordMetadata]


@public
class Storage(ABC):
    _model: Model

    @abstractmethod
    def save(
        self,
        indiv: StructBase,
        *,
        return_saved=False,
        return_metadata=False,
        exclusive_creation=False,
        exclusive_update=False,
    ) -> SaveResult:
        pass

    @abstractmethod
    def read(
        self,
        id: str,
        struct_cls: type,
        checksum: str | None,
        last_modified: datetime | None,
    ) -> ReadResult:
        pass

    @abstractmethod
    def search(self, **params) -> list[StructBase]:
        pass

    @abstractmethod
    def delete(self, id: str, struct_cls: type) -> bool:
        pass
