from json import JSONDecodeError, dump, load
from logging import getLogger
from operator import attrgetter
from os import PathLike, path

from requests import Session

from omniblack.model import coerce_from

from .model import model as build_model
from .unicode import ensure_id_start

log = getLogger(__name__)


def ensure_valid_id(raw_id: str) -> str:
    replaced_id = raw_id.translate(replacements)

    return ensure_id_start(replaced_id)


replacements = str.maketrans({
    '-': '_',
    '.': '_',
    '+': '_plus',
    '(': '',
    ')': '',
})

check_url = 'https://api.github.com/repos/spdx/license-list-data/git/refs/heads/main'  # noqa E501
url = 'https://raw.githubusercontent.com/spdx/license-list-data/main/json/licenses.json'  # noqa E501


UNIT_SEPARATOR = '\u001F'


def update_needed(s, built_file: PathLike):
    with s.get(check_url) as resp:
        raw_obj = resp.json()

    main_ref = coerce_from(build_model, raw_obj, 'json', 'git_ref')

    try:
        with open(built_file) as file_obj:
            raw_current = load(file_obj)
    except (JSONDecodeError, FileNotFoundError):
        return main_ref.object.sha

    if raw_current.get('__version') != main_ref.object.sha:
        return main_ref.object.sha

    return None


def update_file(
    session: Session,
    new_version: str,
    built_file: PathLike,
    template_file: PathLike,
):
    with session.get(url, stream=True) as resp:
        raw_obj = resp.json()

    parsed = coerce_from(build_model, raw_obj, 'json', 'license_list')

    enum_members = [
        {
            'internal': ensure_valid_id(license.licenseId),
            'name': license.licenseId,
            'display': {'en': license.name},
        }
        for license in sorted(parsed.licenses, key=attrgetter('licenseId'))
    ]

    with open(template_file) as file:
        template = load(file)

    id_field = template['fields'][1]

    id_field['is_a']['choice_attrs']['choices'] = enum_members

    template['__version'] = new_version

    with open(built_file, mode='w') as file:
        dump(template, file)


with Session() as s:
    dev_tools_dir = path.dirname(__file__)
    template_file = path.join(dev_tools_dir, 'license.json.template')
    built_file = path.join(
        path.dirname(dev_tools_dir),
        'src/omniblack/app/structs/license.json',
    )

    if new_version := update_needed(s, built_file):
        update_file(s, new_version, built_file, template_file)
        log.info('[green]license.json has been rebuilt.[/]')
    else:
        log.info('[blue]No update needed[/]')
