from contextlib import ExitStack
from json import load
from pathlib import Path

from omniblack.model import Model

struct_files = Path(__file__).parent.glob('*.json')

with ExitStack() as stack:
    struct_file_objs = (
        stack.enter_context(open(struct_file))
        for struct_file in struct_files
    )

    struct_defs = [
        load(struct_file_obj)
        for struct_file_obj in struct_file_objs
    ]


model = Model(
    'lib/app build model',
    struct_defs=struct_defs,
)
