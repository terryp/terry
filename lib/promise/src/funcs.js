import mapLimit from 'promise-map-limit';

export async function map(limit, iter, func) {
    return await mapLimit(iter, limit, func);
}

// Run Promise.all(array.filter(fn)) while limiting concurrency for an iterable
export async function filter(limit, iter, fn) {
    const results = await mapLimit(iter, limit, createWrapper(fn));
    return results.filter(({ passed }) => passed).map(({ value }) => value);
}

function createWrapper(fn) {
    return async function wrapper(value, ...args) {
        const passed = await fn(value, ...args);
        return { passed, value };
    };
}
