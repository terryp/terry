/* eslint-env shared-node-browser */

/**
 * A promise with the reject/resolve exposed.
 *
 * @typedef {Promise} Deferred
 * @property {resolve} Resolve
 * @property {reject} reject
 */

/**
 * Resolve the promise/deferred.
 * @callback resolve
 * @param {*} result - The result of the promise.
 * @returns {Promise}
 */

/**
 * Reject the promise/deferred.
 * @callback reject
 * @param {Error} reason - The reason to reject the promise.
 * @returns {Promise}
 */

/**
 * Creates a Promise with the `reject` and `resolve` functions
 * placed as methods on the promise object itself. It allows you to do:
 * @returns {Deferred}
 */
export function deferred() {
    let methods;
    // eslint-disable-next-line promise/avoid-new
    const promise = new Promise((resolve, reject) => {
        methods = { resolve, reject };
    });
    return Object.assign(promise, methods);
}

/**
 * Returns a promise that resolves after `ms` milliseconds,
 *  unless `abortSignal` aborts the timeout.
 *
 * @returns {Promise}
 */
export function wait(ms, value, { signal } = {}) {
    const prom = deferred();

    const id = setTimeout(() => prom.resolve(value), ms);

    signal?.addEventListener('abort', () => {
        clearTimeout(id);
        prom.reject(signal?.reason);
    });

    return prom;
}
