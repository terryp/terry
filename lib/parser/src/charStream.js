export class CharStream {
    constructor(characters) {
        this.chars = characters;
        this.nextIndex = 0;
    }

    get currentIndex() {
        return this.nextIndex - 1;
    }

    get currentChar() {
        return this.chars.at(this.currentIndex);
    }

    get atEnd() {
        return this.nextIndex === this.chars.length;
    }

    peek(numberOfValues = 1) {
        const chars = this.chars.slice(
            this.nextIndex,
            this.nextIndex + numberOfValues,
        );

        return chars;
    }

    [Symbol.iterator]() {
        return this;
    }

    next() {
        const value = this.chars[this.nextIndex];
        if (value === undefined) {
            return { done: true };
        }
        this.nextIndex += 1;

        return { done: false, value };
    }

    back(numberOfValues = 1) {
        this.nextIndex -= numberOfValues;
    }

    consume(numberOfValues = 1) {
        const chars = this.chars.slice(
            this.nextIndex,
            this.nextIndex + numberOfValues,
        );
        this.nextIndex += chars.length;

        return chars;
    }

    discard(str) {
        if (this.currentChar === str[0]) {
            if (this.peek(str.length - 1) === str.slice(1)) {
                this.nextIndex += str.length;
                return true;
            }
        }
        return false;
    }

    pop() {
        const [char] = this.consume();
        return char;
    }

    at(index) {
        return this.chars.at(index);
    }

    match(regex) {
        if (!regex.sticky) {
            throw new TypeError('Match only accepts sticky regexes');
        }

        regex.lastIndex = this.currentIndex;
        const match = regex.exec(this.chars);
        if (match === null) {
            return null;
        }

        const [matchingText] = match;

        this.nextIndex = regex.lastIndex;

        return matchingText;
    }

    checkToken(token) {
        if (this.currentChar !== token[0]) {
            return false;
        }

        const extraNeeded = token.length - 1;
        const tail = token.slice(1);

        return tail === this.peek(extraNeeded);
    }

    checkTokens(...tokens) {
        return tokens.some((token) => this.checkToken(token));
    }

    consumeUtil(...endTokens) {
        let consumed = '';
        for (const char of this) {
            if (this.checkTokens(...endTokens)) {
                this.back();
                return consumed;
            }
            consumed += char;
        }

        return consumed;
    }

    reset() {
        this.nextIndex = 0;
    }
}
