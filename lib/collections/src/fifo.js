export class Fifo {
    constructor(max) {
        this.max = max;
        this.length = 0;
    }

    push(item) {
        const node = { item };

        if (this.last) {
            this.last.next = node;
            this.last = node;
        } else {
            this.first = node;
            this.last = node;
        }

        this.length += 1;
        if (this.max && this.length > this.max) {
            this.pop();
        }
    }

    pop() {
        const node = this.first;
        if (node) {
            if (node.next) {
                this.first = node.next;
            } else {
                this.first = undefined;
                this.last = undefined;
            }

            this.length -= 1;
        }

        return node?.item;
    }

    *[Symbol.iterator]() {
        while (this.length > 0) {
            yield this.pop();
        }
    }

    get empty() {
        return this.length === 0;
    }

    *zip(...others) {
        const all = [this, ...others];

        while (all.every((fifo) => !fifo.empty)) {
            yield all.map((fifo) => fifo.pop());
        }
    }
}
