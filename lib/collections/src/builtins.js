import { imap, ifilter } from 'itertools';

const BuiltinMap = globalThis.Map;

export class Map extends BuiltinMap {
    pop(key) {
        const val = this.get(key);
        this.delete(key);
        return val;
    }

    map(cb) {
        return new this.constructor(imap(this, (entry) => cb(entry)));
    }

    filter(cb) {
        return new this.constructor(ifilter(this, (entry) => cb(entry)));
    }
}

const BuiltinSet = globalThis.Set;

export class Set extends BuiltinSet {
    map(cb) {
        return new this.constructor(imap(this, (entry) => cb(entry)));
    }

    filter(cb) {
        return new this.constructor(ifilter(this, (entry) => cb(entry)));
    }

    extend(iter) {
        for (const item of iter) {
            this.add(item);
        }
    }
}

export class IntSet {
    constructor(ints = []) {
        this.values = Array.from({ length: 128 }).fill(false, 0);

        for (const int of ints) {
            this.add(int);
        }
    }

    has(int) {
        return this.values[int];
    }

    add(int) {
        this.values[int] = true;
        return this;
    }

    delete(int) {
        this.values[int] = false;
    }

    addRange(start, end) {
        this.values.fill(true, start, end);
    }

    clear() {
        this.values.fill(false);
    }

    copy() {
        const newSet = new IntSet();
        newSet.values = this.values.slice();
        return newSet;
    }

    extend(iter) {
        for (const num of iter) {
            this.add(num);
        }
    }

    extended(iter) {
        const copy = this.copy();
        copy.extend(iter);
        return copy;
    }
}
