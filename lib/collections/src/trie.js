import { Safe } from '@omniblack/utils';

class StringRef {
    constructor(str, index) {
        this.index = index;
        this.str = str;
    }

    get atEnd() {
        return this.index === this.str.length;
    }

    get char() {
        return this.str[this.index];
    }

    get next() {
        if (this.atEnd) {
            return null;
        }

        return new this.constructor(this.str, this.index + 1);
    }

    static from(str) {
        return new this(str, 0);
    }
}

const MISSING_VALUE = Symbol('A missing value');

class Node {
    constructor(char) {
        this.char = char;
        this.children = new Safe();
        this.freq = 0;
    }

    set(ref, value) {
        this.freq += 1;

        if (ref.atEnd) {
            this.value = value;
        } else {
            if (!this.children.has(ref.char)) {
                this.children[ref.char] = new Node(ref.char);
            }
            const node = this.children[ref.char];
            node.set(ref.next, value);
        }

        if (this.freq === 1) {
            this.value = value;
            this.word = ref.str;
        } else {
            delete this.value;
            delete this.word;
        }
    }

    get(ref) {
        if (ref.atEnd) {
            return this.value;
        } else if (this.children.has(ref.char)) {
            return this.children[ref.char].get(ref.next);
        } else {
            return MISSING_VALUE;
        }
    }

    *findPrefixes() {
        if (this.freq === 1) {
            yield this.value;
        } else {
            for (const node of this.children.values()) {
                yield* node.findPrefixes();
            }
        }
    }
}


export class Trie {
    constructor() {
        this.children = {};
    }

    static from(entries) {
        // eslint-disable-next-line consistent-this
        const self = new this();

        for (const [key, value] of entries) {
            self.set(key, value);
        }

        return self;
    }

    *findPrefixes() {
        for (const node of this.children.values()) {
            yield node.findPrefixes();
        }
    }

    set(key, value) {
        const ref = StringRef.from(key);

        if (!Object.hasOwn(this.children, ref.char)) {
            this.children[ref.char] = new Node(ref.char);
        }

        const node = this.children[ref.char];

        node.set(ref.next, value);
    }

    get(...args) {
        const [key, defaultValue] = args;
        const ref = StringRef.from(key);

        let value = MISSING_VALUE;
        if (this.children.has(ref.char)) {
            value = this.children[ref.char].get(ref.next);
        }

        if (value === MISSING_VALUE) {
            if (args.length <= 1) {
                throw new Error(key);
            } else {
                return defaultValue;
            }
        }

        return value;
    }
}

