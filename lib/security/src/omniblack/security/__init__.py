import contextlib

from datetime import datetime
from logging import getLogger

from public import public as export
from werkzeug.exceptions import BadGateway, Unauthorized

from omniblack.app import config
from omniblack.secret import Secret
from omniblack.server import ServerPlugin, current_request, url_for

from .model import model

log = getLogger(__name__)


@export
def public(func=None):
    if func is None:
        return public

    func.public = True

    return func


def check_public(handler):
    if handler.public:
        config.user = None
        config.session = None
        return
    else:
        msg = 'You must login to access this page'
        raise Unauthorized(msg)


def check_user_auth(handler, current_token):
    User = config.model.structs.User

    users = tuple(config.storage.search(
        User,
        mongo_filter={
            'sessions.token': current_token.reveal(),
        },
    ))

    if len(users) == 0:
        return check_public(handler)

    assert len(users) == 1

    user, = users

    for session in user.sessions:
        if session.token.reveal() == current_token.reveal():
            current_session = session
            break

    else:
        msg = 'Session not found in session array'
        raise RuntimeError(msg, user.sessions)

    config.user = user
    config.session = current_session

    if current_session.expiration < datetime.now():
        # The session expired
        msg = 'Your session has expired.'
        raise Unauthorized(msg)

    return None


def call_auth_server(current_token):
    resp = config.http.get(
        url_for('/verify_user', package='auth'),
        cookies={'omniblack-session': current_token.reveal()},
    )

    if 500 <= resp.status_code < 600:
        raise BadGateway

    if resp.status_code == 401:
        return None
    else:
        return resp.json()


def check_user_other(handler, current_token):
    raw_user = call_auth_server(current_token)

    if not raw_user:
        return check_public(handler)

    user = model.structs.UserInfo(raw_user)

    config.user = user

    return None


@export
def authenticate(handler, server):  # noqa: ARG001
    current_token = Secret(current_request.cookies.get('omniblack-session'))

    with contextlib.suppress(KeyError):
        del current_request.cookies['omniblack-session']

    if not current_token:
        return check_public(handler)

    if config.app.name == 'auth':
        check_user_auth(handler, current_token)
    else:
        check_user_other(handler, current_token)

    return None


@export
class SecurityPlugin(ServerPlugin):
    @property
    def middlewares(self):
        return (authenticate,)
