from omniblack.model import Model

model = Model(
    'omniblack.security',
    struct_packages=['omniblack.security.model'],
)
