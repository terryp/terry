/* eslint-env node */
import { homedir } from 'node:os';

let regexMatch;
export function expandVars(path = '') {
    if (path.startsWith('~')) {
        path = path.replace('~', homedir());
    }

    if (!path.includes('$')) {
        return path;
    }

    if (!regexMatch) {
        regexMatch = /\$(\w+|\{[^}]*\})/g;
    }

    const { env } = process;

    path = path.replace(regexMatch, (fullMatch, varName) => {
        if (varName.startsWith('{') && varName.endsWith('}')) {
            varName = varName.slice(1, -1);
        }

        if (varName in env) {
            return env[varName];
        } else {
            return fullMatch;
        }
    });
    return path;
}


