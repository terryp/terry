import { fileURLToPath } from 'node:url';
import { dirname } from 'node:path';

export function modPath(meta) {
    const fileName = fileURLToPath(meta.url);
    return fileName;
}
export function modDir(meta) {
    const filePath = modPath(meta);
    return dirname(filePath);
}

export function isMain(meta) {
    const fileName = modPath(meta);
    return fileName === process.argv[1];
}

