import contextlib
import os
import sys

from abc import ABC, abstractmethod
from argparse import (
    ArgumentParser,
    BooleanOptionalAction,
    RawDescriptionHelpFormatter,
)
from functools import partial
from logging import getLogger
from sys import stderr
from tempfile import NamedTemporaryFile

from public import public

from omniblack.app import config
from omniblack.entry import entry
from omniblack.logging import Verbosity
from omniblack.model import Field, Model

log = getLogger(__name__)


def ensure_del(obj, name):
    with contextlib.suppress(AttributeError):
        delattr(obj, name)


class ModelCliType:
    def __init__(self, field: Field):
        self.__field = field
        self.__model = self.__field.model

    def __repr__(self):
        return self.__field.type.name

    def __call__(self, value: str):
        is_a = self.__field.is_a

        # This converter is called once per arg
        # instead of once for the whole list of items
        if is_a.type == 'list':
            is_a = is_a.list_attrs.item

        return self.__model.types.from_format(
            'string',
            value,
            self.__field,
            is_a,
        )


internal_errors = (
    ImportError,
    NotImplementedError,
    NameError,
    SyntaxError,
    IndentationError,
    SystemError,
)

stderr_print = partial(print, file=stderr)


class HasExitCode(ABC):
    @abstractmethod
    def exit_code(self) -> int:
        ...


def add_config_args(parser: ArgumentParser):
    if config is None:
        return
    else:
        return None

    parser.add_argument(
        '--color',
        action=BooleanOptionalAction,
        help='Should color be used for output?',
        dest='color',
        default=None,
    )

    parser.add_argument(
        '-v', '--verbose',
        action='count',
        dest='verbose',
        help='Set the logging verbosity.',
        default=Verbosity.WARNING,
    )


@public
class CLI:
    """
    A command-line interface application.

    :param distribution: The distribution this application is shipped in.
    :type distribution: :type:`str`
    """

    def __init__(self, distribution, model: Model = None, module: str = None):
        self.functions = []
        self.distribution = distribution
        self.module = module
        self.model = model

    def _create_root_parser(self):
        meta = config.app.dist.metadata
        root_parser = ArgumentParser(
            description=meta.get('Summary', ''),
            epilog=meta.get('Description', ''),
        )

        if config.version:
            root_parser.add_argument(
                '--version',
                action='version',
                version=str(config.version),
            )

        add_config_args(root_parser)

        return root_parser

    def print_all_help(self, root_parser, sub_parsers, exit_code=0):
        blobs = []
        blobs.append(root_parser.format_help())

        for sub_parser in sub_parsers:
            blobs.append(sub_parser.format_help())

        full_text = hr().join(blobs)

        print(full_text)
        sys.exit(exit_code)

    def _get_parser(self):
        root_parser = self._create_root_parser()
        functions = self._get_entries()

        if len(functions) == 1:
            name, = tuple(functions)
            func_entry = functions[name]

            root_parser.set_defaults(entry=func_entry)

            sub_parsers = []
            for field in func_entry.fields:
                add_field(root_parser, field)
        else:
            sub_parsers = self._add_subparsers(root_parser, functions)

        return root_parser, sub_parsers

    def get_parser(self):
        """
        Return the parser :type:`argparse.ArgumentParser`
        """

        root_parser, _sub_parsers = self._get_parser()
        return root_parser

    def __call__(self, *args):
        """Parse the command line and run the associated handler.

        Args:
            *args: The arguments to parse.
                If not provided, :type:`sys.argv` will be used.
        """
        with config.task():
            config.load_app(
                self.distribution,
                model=self.model,
                module=self.module,
            )
            root_parser, sub_parsers = self._get_parser()
            args = root_parser.parse_args()
            app_args = {}

            if getattr(args, 'help_all', False):
                self.print_all_help(root_parser, sub_parsers)

            ensure_del(args, 'help_all')
            config.finish_setup(**app_args)

            try:
                target_entry = args.entry
            except AttributeError:
                root_parser.print_usage()
                sys.exit(os.EX_USAGE)
            else:
                del args.entry
                self.__call_entry(target_entry, args)

    def __call_entry(self, entry, args):
        try:
            ret = entry.func(**vars(args))
        except KeyboardInterrupt:
            sys.exit(os.EX_OK)
        except OSError as err:
            log.error(err.strerror)
            if err.filename:
                log.error(err.filename)
            if err.filename2:
                log.error(err.filename2)
            sys.exit(err.errno)
        except EOFError as err:
            for arg in err.args:
                log.error(arg)
            sys.exit(os.EX_DATAERR)
        except PermissionError as err:
            for arg in err.args:
                log.error(arg)
            sys.exit(os.EX_NOPERM)
        except internal_errors as err:
            log.error('An internal error occured.')
            with NamedTemporaryFile('x', delete=False) as file:
                log.error(f'See {file.name} for details')
                print(*err.args, file=file)

            sys.exit(os.EX_SOFTWARE)
        else:
            if ret is None:
                sys.exit(os.EX_OK)
            elif isinstance(ret, int):
                sys.exit(ret)
            elif isinstance(ret, HasExitCode):
                if hasattr(ret, 'exit_message'):
                    log.error(ret.exit_message())

                sys.exit(ret.exit_code())

            else:
                try:
                    exit_code = int(ret)
                except Exception:  # noqa: BLE001
                    # the return could not be coerced so we will exit normally
                    sys.exit(os.EX_OK)
                else:
                    sys.exit(exit_code)

    def _add_subparsers(self, root_parser, functions):
        root_parser.add_argument(
            '--help-all',
            action='store_true',
            dest='help_all',
            help='Print help for all subcommands.',
        )

        sub_parsers = root_parser.add_subparsers(
            title='subcommand',
            description='Subcommands available in %(prog)s',
        )

        all_sub_parsers = []
        for func_name, func_entry in functions.items():
            sub_parser = sub_parsers.add_parser(
                func_name.replace('_', '-'),
                formatter_class=RawDescriptionHelpFormatter,
                help=func_entry.short_desc,
                description=func_entry.short_desc,
                epilog=func_entry.long_desc,
            )

            all_sub_parsers.append(sub_parser)

            sub_parser.set_defaults(entry=func_entry)

            for field in func_entry.fields:
                add_field(sub_parser, field)

        return all_sub_parsers

    def command(self, func):
        """
        Add a command to the application.

        :param func: The function that implements the command.
            :type:`omniblack.entry.entry` will be used to
            analzye the function to create the argument parser.
        :type func: :type:`callable`
        """
        self.functions.append(func)

        return func

    def _get_entries(self):
        entries = [
            entry(config.model, func)
            for func in self.functions
        ]

        return {
            entry.name: entry
            for entry in entries
        }


def add_field(parser: ArgumentParser, field: Field):
    name = field.meta.name
    pos_name = name.replace('_', '-')
    is_a = field.is_a

    extra_args = {}

    required = field.meta.required
    if not required:
        if 'assumed' in field.meta:
            extra_args['default'] = field.meta.assumed
        else:
            extra_args['default'] = None

    if 'desc' in field.meta:
        extra_args['help'] = field.meta.desc.en

    if is_a.type == 'list':
        extra_args['nargs'] = '*' if required else '+'

    if is_a.type == 'boolean':
        extra_args.setdefault('default', False)

        parser.add_argument(
            f'--{pos_name}',
            dest=name,
            **extra_args,
            action=BooleanOptionalAction,
        )
    else:
        if 'assumed' in field.meta:
            extra_args['dest'] = name
            name = f'--{pos_name}'

        parser.add_argument(
            name,
            **extra_args,
            type=ModelCliType(field),
        )


def hr():
    columns, _rows = os.get_terminal_size(0)
    return ('-' * columns)
