# @omniblack/localization

## 0.0.1
### Patch Changes

- 3d34507: Update lodash and internal deps
- Updated dependencies [3d34507]
  - @omniblack/error@0.0.1
  - @omniblack/utils@0.0.1
