export function configuredLuxon(cb, luxonObj, ...args) {
    if (typeof luxonObj.reconfigure !== 'function') {
        throw new TypeError(
            'The first argument of configuredLuxon'
            + ' must have a reconfigure method.',
            luxonObj,
        );
    }

    return {
        localize(lang) {
            const configured = luxonObj.reconfigure({ locale: lang });
            return cb(configured, ...args);
        },
    };
}
