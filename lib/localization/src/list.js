/* eslint-env shared-node-browser */
import { map } from 'itertools';
import { TXT } from '#src/text.js';

function createOr(listImpl) {
    return function or(iterable) {
        return listImpl(iterable, {
            type: 'disjunction',
            style: 'long',
        });
    };
}

export const or = createOr(list);
export const quotedOr = createOr(quotedList);


function createAnd(listImpl) {
    return function and(iterable) {
        return listImpl(iterable, {
            type: 'conjunction',
            style: 'long',
        });
    };
}

export const and = createAnd(list);
export const quotedAnd = createAnd(quotedList);

function createUnit(listImpl) {
    return function unitList(iterable) {
        return listImpl(iterable, {
            type: 'unit',
            style: 'long',
        });
    };
}

export const unitList = createUnit(list);
export const quotedUnitList = createUnit(quotedList);

export function list(iterable, opts) {
    return {
        localize(lang, locales, localize) {
            const formatter = new Intl.ListFormat(locales, opts);

            const localized = map(iterable, (item) => localize(item));

            return formatter.format(localized);
        },
    };
}

export function quotedList(iterable, opts) {
    return {
        localize(lang, locales, localize) {
            const formatter = new Intl.ListFormat(locales, opts);

            const quoted = map(iterable, (item) => TXT`'${item}'`);
            const localized = map(quoted, (item) => localize(item));


            return formatter.format(localized);
        },
    };
}
