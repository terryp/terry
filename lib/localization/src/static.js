import { getLang, createLocalize } from '#src/localization.js';

export const localize = createLocalize(getLang());

const sym = Symbol.for('omniblack.localization.localize');

globalThis[sym] = localize;
