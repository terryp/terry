import { NotLocalizable } from '@omniblack/error';

import { TXT } from '#src/text.js';

export { NotLocalizable };


export function useToLocaleString(obj, options) {
    return {
        localize(lang, locales) {
            return obj.toLocaleString(locales, options);
        },
    };
}

export function onLocalize(cb, ...args) {
    return {
        localize(lang) {
            return cb(lang, ...args);
        },
    };
}

export function isLocalizable(subject) {
    return typeof subject?.localize === 'function';
}


/*
 * Returns a translated string for a localizable (Text object or UiString
 * rec).
 * If the text translated string includes a template, it will be resolved
 * with args.
 *
 * If passed a string as localizable, the string is returned. Therefore,
 * localizing a variable twice [eg: localize(localize(x))] will not cause an
 * error.
 */
export function createLocalize(lang, locales) {
    return function localize(localizable, args) {
        args = localizeArgs(args);
        if (isLocalizable(localizable)) {
            return localizable.localize(lang, locales, localize);
        } else if (typeof localizable === 'object') {
            if (localizable[lang]) {
                return localizable[lang];
            } else if (localizable.en) {
                return localizable.en;
            }
        } else if (typeof localizable === 'string') {
            // localizable is already translated
            return localizable;
        } else {
            let betterValue = localizable;
            try {
                betterValue = JSON.stringify(localizable, null, 4);
            // eslint-disable-next-line no-empty
            } catch {}
            throw new NotLocalizable(
                TXT`Cannot translate type '${typeof localizable}'
             for localizable: ${betterValue}`,
            );
        }
    };
}

export function getLang() {
    const locale = new Intl.DateTimeFormat().resolvedOptions().locale;
    return locale.split('-')[0];
}

function localizeArgs(args, localize) {
    args = args ?? {};
    const argsCopy = { ...args };
    for (const [key, value] of Object.entries(args)) {
        if (isLocalizable(value)) {
            argsCopy[key] = localize(value);
        }
    }
    return argsCopy;
}
