/* eslint-env browser, node */
import { merge } from 'lodash-es';
import { imap as map, roundrobin } from 'itertools';
import { isLocalizable } from '#src/localization.js';
import { collapseWhitespace } from '@omniblack/utils';

export class Text {
    constructor(strings, args = {}) {
        if (strings instanceof Text && strings.args) {
            this.args = merge(args, strings.args);
            this.strings = strings.strings;
        } else if (typeof strings === 'string') {
            this.args = args;
            this.strings = [strings];
        } else {
            this.args = args;
            this.strings = strings;
        }
    }

    localize(_lang, _locales, localize) {
        // TODO lookup message based on text.
        return collapseWhitespace(''.concat(...roundrobin(
            this.strings,
            map(this.args, (arg) => {
                if (isLocalizable(arg)) {
                    return localize(arg);
                } else {
                    return arg;
                }
            }),
        )));
    }
}

const sym = Symbol.for('omniblack.localization.localize');

globalThis[sym] = TXT;

export function TXT(strings, ...args) {
    return new Text(strings, args);
}

let thrown = false;

/*
 * @deprecated
 */
export function T(...args) {
    if (!thrown) {
        console.error('The name T is deprecated use TXT instead.');
        thrown = true;
    }


    // eslint-disable-next-line new-cap
    return TXT(...args);
}
