/* globals window */
import { readable, derived } from 'svelte/store';
import { createLocalize } from '#src/localization.js';

function getLang() {
    const locale = navigator.language;
    const [lang] = locale.split('-');
    return lang;
}

export const lang = readable(getLang(), (set) => {
    window.addEventListener('languagechange', () => {
        set(getLang());
    });
});

function getLocales() {
    return navigator.languages;
}

export const locales = readable(getLocales(), (set) => {
    window.addEventListener('languagechange', () => {
        set(getLocales());
    });
});

export const localize = derived(
    [lang, locales],
    ([newLang, locales]) => createLocalize(newLang, locales),
);
