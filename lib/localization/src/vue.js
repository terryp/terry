/* globals window */
import { ref, readonly, computed } from 'vue';
import { createLocalize, getLang } from '#src/localization.js';

function readable(initialValue, setter) {
    const selfRef = ref(initialValue);

    function set(newValue) {
        selfRef.value = newValue;
    }

    setter(set);

    return readonly(selfRef);
}

export const lang = readable(getLang(), (set) => {
    window.addEventListener('languagechange', () => {
        set(getLang());
    });
});

function getLocales() {
    return navigator.languages;
}


export const locales = readable(getLocales(), (set) => {
    window.addEventListener('languagechange', () => {
        set(getLocales());
    });
});

export const localize = computed(
    () => createLocalize(lang.value, locales.value),
);

export default {
    install(app, options) {
        app.config.globalProperties.$localize = localize;
    },
};
