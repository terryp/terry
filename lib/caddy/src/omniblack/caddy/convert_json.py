from collections.abc import Mapping, Sequence
from functools import singledispatch, wraps
from json import dump as _dump
from typing import Any

from public import public


@public
@singledispatch
def convert(obj: Any):
    """
    Convert a caddy model object to a json compatible dict.
    """
    return obj


@convert.register
def convert_mapping(obj: Mapping):
    return {
        key: convert(value)
        for key, value in obj.items()
    }


@convert.register(str)
@convert.register(int)
@convert.register(float)
@convert.register(bool)
@convert.register(type(None))
def supported(obj):
    return obj


@convert.register
def convert_sequence(obj: Sequence):
    return [
        convert(value)
        for value in obj
    ]


@public
@wraps(_dump)
def dump(
    obj,
    *args,
    ensure_ascii=False,
    allow_nan=False,
    sort_keys=True,
    **kwargs,
):
    obj = convert(obj)

    return _dump(
        obj,
        *args,
        ensure_ascii=ensure_ascii,
        allow_nan=allow_nan,
        sort_keys=sort_keys,
        **kwargs,
    )


dump.__doc__ = """
A wrapper around :type:`json.dump` that converts caddy models to dicts first.
"""
