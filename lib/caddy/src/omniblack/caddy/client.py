from __future__ import annotations

from json import dump, load
from os import getcwd
from os.path import join
from urllib.parse import urljoin

from public import public
from requests import HTTPError

from .convert_json import convert
from .route import Site
from .unix import create_requests


def get_url(net_address, default_host):
    if net_address.startswith('unix/'):
        return net_address.replace('unix/', 'unix://')
    elif net_address.startswith('udp/'):
        msg = 'Upd sockets are not supported.'
        raise TypeError(msg)
    else:
        net_address = net_address.removeprefix('tcp/')
        if net_address.startswith(':'):
            net_address = default_host + net_address
        return f'http://{net_address}'


@public
class Caddy:
    """
    A client that can connect to the the caddy admin api.

    :ivar caddy_file: A Path to the caddyfile.
    :vartype caddyfile: :type:`typing.Optional[str]`

    :ivar socket_path: The path to the socket we are using
        to talk to caddy.
    :vartype socket_path: :type:`typing.Optional[str]`

    :ivar update_caddy_file: Should we write updates
        to the file at :python:`self.caddy_file`?
    :vartype update_caddy_file: :type:`bool`

    :ivar caddy_host: The hostname of the device caddy is running on.
    :vartype caddy_host: :type:`str`
    """

    def __init__(
        self,
        *,
        caddy_file: str = None,
        socket_path: str = None,
        update_caddy_file=False,
        caddy_host='localhost',
        cwd=None,
    ):
        if cwd is None:
            cwd = getcwd()

        if caddy_file is not None:
            caddy_file = join(cwd, caddy_file)

        if not socket_path:
            with open(caddy_file) as file_obj:
                config = load(file_obj)

            try:
                listen = config['admin']['listen']
            except KeyError:
                msg = 'Caddyfile must have an admin unix socket configured'
                raise TypeError(msg)

            if listen.startswith('unix/'):
                self.socket_path = join(cwd, listen.removeprefix('unix/'))
                self.base = 'unix://caddy'
            else:
                self.base = get_url(listen, caddy_host)
                self.socket_path = None

        else:
            self.socket_path = join(cwd, socket_path)

        self._requests = None
        self.update_caddy_file = update_caddy_file
        self.caddy_file = caddy_file

    def url(self, path: str) -> str:
        """
        Return a url that points to the caddy instance.

        :param path: The path to send a request to.
        :type path: :type:`str`

        :rtype: :type:`str`
        """
        return urljoin(self.base, path)

    def get(self, path, *args, **kwargs):
        """
        Send a :HTTP_METHOD:`GET` request to the caddy server.

        :param path: The path to send a request to.
            This will be turned into a full url using
            :type:`Caddy.url`.
        :type path: :type:`str`

        The remaining arguments will be passed to
        :type:`requests.get`.

        :rtype: :type:`requests.Response`
        """
        return self._requests.get(self.url(path), *args, **kwargs)

    def post(self, path, *args, **kwargs):
        """
        Send a :HTTP_METHOD:`POST` request to the caddy server.

        :param path: The path to send a request to.
            This will be turned into a full url using
            :type:`Caddy.url`.
        :type path: :type:`str`

        The remaining arguments will be passed to
        :type:`requests.post`.

        :rtype: :type:`requests.Response`
        """
        return self._requests.post(self.url(path), *args, **kwargs)

    def patch(self, path, *args, **kwargs):
        """
        Send a :HTTP_METHOD:`PATCH` request to the caddy server.

        :param path: The path to send a request to.
            This will be turned into a full url using
            :type:`Caddy.url`.
        :type path: :type:`str`

        The remaining arguments will be passed to
        :type:`requests.patch`.

        :rtype: :type:`requests.Response`
        """
        return self._requests.patch(self.url(path), *args, **kwargs)

    def add_site(self, site: Site):
        """
        Configure caddy to act as a reverse proxy for a site.
        """
        site_id = f'site_{site.name}'
        json = convert(site)
        json['@id'] = site_id

        try:
            resp = self._requests.patch(self.url(f'/id/{site_id}'), json=json)
            resp.raise_for_status()
        except HTTPError:
            url = self.url('/id/server/routes')
            resp = self._requests.post(url, json=json)
            resp.raise_for_status()

        if self.update_caddy_file:
            updated_resp = self.get('/config')
            updated_resp.raise_for_status()

            updated_config = updated_resp.json()

            with open(self.caddy_file, 'w') as file:
                dump(
                    updated_config,
                    file,
                    sort_keys=True,
                    indent=4,
                    ensure_ascii=False,
                )

    def __enter__(self):
        self._requests = create_requests(self.socket_path)

    def __exit__(self, exc_type=None, exc_value=None, traceback=None):
        if self._requests is not None:
            self._requests.close()
            self._requests = None

    __del__ = __exit__


def load_config(file_path):
    with open(file_path) as file_obj:
        return load(file_obj)


def dump_config(file_path, config):
    with open(file_path, 'w') as file_obj:
        return dump(
            config,
            file_obj,
            sort_keys=True,
            indent=4,
            ensure_ascii=False,
        )
