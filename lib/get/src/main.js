/* eslint-env browser */
import qs from 'qs';
import { sorted } from 'itertools';
import { HttpError, NetworkError } from '@omniblack/error';

export const phase = Symbol('The phase of request processing');
export const order = Symbol(
    'The position in the phase this interceptors should run.',
);
export const pre = Symbol('Pre-request');
export const post = Symbol('Post-request');


export async function extractData(responseProm) {
    const response = await responseProm;
    const headers = response.headers;

    const ctHeader = headers.get('content-type');
    if (ctHeader === 'application/json') {
        return await response.json();
    }
    return await response.text();
}

extractData[phase] = post;
extractData[order] = 1;

// convert error responses into ServiceErrors
export async function convertErrors(responseProm) {
    let response;
    try {
        response = await responseProm;
    } catch (err) {
        throw new NetworkError(err.message);
    }

    if (!response.ok) {
        throw await HttpError.from(response);
    } else {
        return response;
    }
}

convertErrors[phase] = post;
convertErrors[order] = 0;


export function addBaseURL(baseURL) {
    function baseURLAdder(requestConfig) {
        return {
            baseURL,
            ...requestConfig,
        };
    }

    baseURLAdder[phase] = pre;
    return baseURLAdder;
}


/*
 * request will always have a bound `this`
 * see `create` below
 */
async function request(requestConfig) {
    // eslint-disable-next-line no-invalid-this
    for (const pre of this.__pre) {
        requestConfig = await pre(requestConfig);
    }

    const { url, baseURL, params, body } = requestConfig;
    let finalURL = url;
    if (baseURL && baseURL !== '/') {
        finalURL = new URL(url, baseURL);
    }

    if (typeof body === 'object') {
        requestConfig.body = JSON.stringify(body);
        if (!requestConfig.headers) {
            requestConfig.headers = {};
        }

        requestConfig.headers['content-type'] = 'application/json';
    }

    if (params) {
        finalURL += `?${qs.stringify(params)}`;
    }

    requestConfig.cache = 'default';
    requestConfig.mode = 'cors';

    const resp = await fetch(finalURL, requestConfig);

    let retVal = resp;
    // eslint-disable-next-line no-invalid-this
    for (const post of this.__post) {
        retVal = await post(retVal, requestConfig);
    }

    return retVal;
}


const _methods = [
    'get',
    'delete',
    'head',
    'options',
    'post',
    'put',
    'patch',
];

const methods = new Map(
    _methods.map((method) => [method, method.toUpperCase()]),
);

export function create(...interceptors) {
    const preRequestFuncs = sorted(
        interceptors.filter((func) => func[phase] === pre),
        (interceptor) => interceptor[order] ?? Number.POSITIVE_INFINITY,
    );
    const postRequestFuncs = sorted(
        interceptors.filter((func) => func[phase] === post),
        (interceptor) => interceptor[order] ?? Number.POSITIVE_INFINITY,
    );
    const instance = { __pre: preRequestFuncs, __post: postRequestFuncs };
    for (const [funcName, httpMethod] of methods) {
        async function func(url, config) {
            return await request.call(instance, {
                method: httpMethod,
                url,
                ...config,
            });
        }
        instance[funcName] = func;
    }

    instance.request = request.bind(instance);

    return instance;
}

export const get = create(extractData, convertErrors);
