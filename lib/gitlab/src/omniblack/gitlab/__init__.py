from collections.abc import Mapping
from dataclasses import dataclass, fields
from datetime import datetime
from typing import Annotated, Any, Literal

from annotated_types import Len
from public import public

Script = tuple[str] | None


class Base:
    def _empty(self):
        next_val = next(self.items())

        return next_val is None

    def items(self):
        for field in fields(self):
            value = getattr(self, field.name, None)

            if value is not None and value != field.default:
                yield field.name, value

    def _is_base_seq(self, value):
        return (
            isinstance(value, tuple) and value and isinstance(value[0], Base)
        )

    def to_json(self):
        values = {}

        for key, value in self.items():
            if self._is_base_seq(value):
                value = tuple(
                    v.to_json()
                    for v in value
                )

            elif isinstance(value, Base):
                value = value.to_json()

            values[key] = value

        return values


@public
@dataclass
class Reports(Base):
    accessibility: str | None = None
    annotations: str | None = None
    api_fuzzing: str | None = None
    browser_performance: str | None = None
    coverage_report: str | None = None
    codequality: str | None = None
    container_scanning: str | None = None
    coverage_fuzzing: str | None = None
    cyclonedx: list[str] | str | None = None
    dast: str | None = None
    dependency_scanning: str | None = None
    dotenv: str | None = None
    junit: str | None = None
    load_performance: str | None = None
    metrics: str | None = None
    requirements: str | None = None
    repository_xray: str | None = None
    sast: str | None = None
    secret_detection: str | None = None
    terraform: str | None = None


@public
@dataclass
class Artifacts(Base):
    paths: tuple[str] | None = None
    exclude: tuple[str] | None = None
    expire_in: str | None = None
    expose_as: str | None = None
    name: str | None = None
    public: bool = True
    reports: Reports | None = None
    untracked: bool = False
    when: Literal['on_success', 'on_failure', 'always'] = 'on_success'


@public
@dataclass
class Cache(Base):
    prefix: str

    paths: list[str] | None = None
    key: str | None = None
    files: Annotated[tuple[str], Len(1, 2)] | None = None
    untracked: bool = False
    unprotect: bool = False
    when: Literal['on_success', 'on_failure', 'always'] = 'on_success'
    policy: Literal['pull', 'push', 'pull-push'] = 'pull-push'
    fallback_keys: tuple[str] | None = None


@public
@dataclass
class DastConfig(Base):
    site_profile: str | None = None
    scanner_profile: str | None = None


deployment_tiers = Literal[
    'production',
    'staging',
    'testing',
    'development',
    'other',
]


@public
@dataclass
class Environment(Base):
    name: str | None = None
    url: str | None = None
    on_stop: str | None = None
    action: Literal['start', 'prepare', 'stop', 'verify', 'access'] = 'start'
    auto_stop_in: str | None = None
    deployment_tier: deployment_tiers | None = None


@public
@dataclass
class Hooks(Base):
    pre_get_sources_script: Script = None


@public
@dataclass
class Token(Base):
    aud: str | tuple[str]


ImagePullPolicy = Literal['always', 'if-not-present', 'never']


@public
@dataclass
class Image(Base):
    name: str
    entrypoint: str | None = None

    # TODO: type the options
    docker: Mapping[str, Any] | None = None
    pull_policy: ImagePullPolicy | tuple[ImagePullPolicy] = None


@public
@dataclass
class Inherit(Base):
    default: bool | tuple[str] = True
    variables: bool | tuple[str] = True


@public
@dataclass
class Need(Base):
    job: str
    project: str = None
    ref: str = None
    pipeline: str = None
    artifacts: bool = True
    optional: bool = False


LinkType = Literal['runbook', 'package', 'image', 'other']


@public
@dataclass
class ReleaseLink(Base):
    name: str
    url: str
    filepath: str | None = None
    link_type: LinkType = 'other'


@public
@dataclass
class Assets(Base):
    links: tuple[ReleaseLink] | None = None


@public
@dataclass
class Release:
    tag_name: str
    tag_message: str | None = None
    name: str | None = None
    description: str | None = None
    ref: str | None = None
    milestones: tuple[str] | None = None
    released_at: datetime | None = None
    assets: Assets | None = None

    def to_json(self):
        values = super().to_json()
        if 'released_at' in values:
            values['released_at'] = values['released_at'].isoformat()

        return values


RetryValues = Literal[
    'always',
    'unknown_failure',
    'script_failure',
    'api_failure',
    'stuck_or_timeout_failure',
    'runner_system_failure',
    'runner_unsupported',
    'stale_schedule',
    'job_execution_timeout',
    'archived_failure',
    'unmet_prerequisites',
    'scheduler_failure',
    'data_integrity_failure',
]


@public
@dataclass
class Retry(Base):
    max: Annotated[int, range(3)]
    when: tuple[RetryValues] | RetryValues | None = None


WorkflowRuleWhen = Literal[
    'on_success',
    'delayed',
    'always',
]

RuleWhen = Literal[
    'on_success',
    'delayed',
    'always',
    'never',
]


@public
@dataclass
class Changes(Base):
    paths: tuple[str]
    compare_to: str | None = None


@public
@dataclass
class WorkflowRule(Base):
    _if: str | None = None
    changes: tuple[str] | Changes | None = None
    exists: tuple[str] | None = None
    variables: Mapping[str, str] | None = None
    when: WorkflowRuleWhen | None = None

    def to_json(self):
        values = super().to_json()

        if '_if' in values:
            values['if'] = values['_if']
            del values['_if']

        return values


@public
@dataclass
class Rule(WorkflowRule):
    when: RuleWhen
    allow_failure: bool = False
    needs: tuple[Need | str] | None = None


@public
@dataclass
class Variable(Base):
    value: str
    description: str | None = None
    options: tuple[str] | None = None
    expand: bool = True


When = Literal[
    'on_success',
    'on_failure',
    'never',
    'always',
    'manual',
    'delayed',
]


@public
@dataclass
class Default(Base):
    image: Image | str = None
    after_script: Script = None
    artifacts: Artifacts | None = None
    before_script: Script = None
    cache: Cache | None = None
    hooks: Hooks | None = None
    id_tokens: Mapping[str, Token] | None = None
    interruptible: bool = False
    retry: Retry | Annotated[int, range(3)] | None = None
    tags: tuple[str] | None = None
    timeout: str = None

    def to_json(self):
        values = super().to_json()

        if 'id_tokens' in values:
            values['id_tokens'] = {
                key: value.to_json()
                for key, value in values['id_tokens'].items()
            }

        return values


@public
@dataclass
class Job(Base):
    image: Image | str
    script: tuple[str]
    id_tokens: Mapping[str, Token] | None = None
    after_script: Script = None
    artifacts: Artifacts | None = None
    before_script: Script = None
    cache: Cache | None = None
    coverage: str | None = None
    dast_configuration: DastConfig | None = None
    dependencies: tuple[str] | None = None
    environment: Environment | str | None = None
    extends: str | list[str] | None = None
    hooks: Hooks | None = None
    inherit: Inherit | None = None
    interruptible: bool = False
    needs: tuple[Need | str] | None = None
    release: Release | None = None
    resource_group: str | None = None
    retry: Retry | Annotated[int, range(3)] | None = None
    rules: tuple[Rule] | None = None
    stage: str = 'test'
    tags: tuple[str] | None = None
    timeout: str = None
    when: When = 'on_success'


@public
@dataclass
class Workflow(Base):
    name: str | None = None
    rules: tuple[WorkflowRule] | None = None


@public
@dataclass
class PipelineConfig(Base):
    default: Default | None = None
    stages: tuple[str] | None = None
    workflow: Workflow | None = None
    jobs: Mapping[str, Job] | None = None

    def to_json(self):
        values = super().to_json()
        jobs = values.pop('jobs')
        values |= {
            key: value.to_json()
            for key, value in jobs.items()
        }

        return values
