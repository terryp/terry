```{include} ./README.md
```


## API Reference

```{eval-rst}
.. automodule:: omniblack.py_indexes
    :members:

    .. autodata:: pypi
    .. autodata:: test_pypi
```
