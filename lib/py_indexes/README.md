# Omniblack Python Indexes

Omniblack Python Indexes provides an
pythonic interface to access the
simple repository api.

## Example Usage

Python Code:

```python
from omniblack.py_indexes import Package

pkg = Package.get('omniblack-py-indexes')
```
