from asyncio import get_running_loop
from collections.abc import Sequence
from dataclasses import InitVar, dataclass, field
from enum import Enum
from functools import partial
from pathlib import Path
from shutil import copyfileobj
from tempfile import mkdtemp
from warnings import warn

from ruamel.yaml import YAML
from xdg.BaseDirectory import (
    get_runtime_dir,
    load_config_paths,
    load_data_paths,
    save_cache_path,
    save_config_path,
    save_data_path,
)

from .file_locking import lock_file

__all__ = (
    'File',
    'FileType',
    'ProgramFiles',
)


class FileType(Enum):
    config = 'config'
    program = 'program'
    data = 'data'
    runtime = 'runtime'
    cache = 'cache'


def set_attr(self, key, value):
    object.__setattr__(self, key, value)


@dataclass(frozen=True)
class File:
    name: str
    write_dir: InitVar[Path]
    search_dirs: InitVar[Sequence[Path]]
    type: FileType
    path: Path = field(init=False)
    search_files: tuple[Path] = field(init=False)

    yaml = YAML(pure=True)
    yaml.default_flow_style = False

    def __post_init__(self, write_dir, search_dirs):
        set_attr(self, 'path', write_dir / self.name)
        set_attr(self, 'search_files', tuple(
            path / self.name
            for path in search_dirs
        ))

    def merge(self, data_mappings):
        new = {}
        for mapping in data_mappings:
            new.update(mapping)

        return new

    def get_data_sync(self):
        data_mappings = (
            self.read_file(file)
            for file in reversed(self.search_files)
        )

        return self.merge(
            filter(
                lambda mapping: mapping is not None,
                data_mappings,
            ),
        )

    def read_file(self, path):
        try:
            with lock_file(path, 'r') as file_obj:
                return self.yaml.load(file_obj)
        except FileNotFoundError:
            return None

    async def get_data(self):
        return await get_running_loop().run_in_executor(
            None,
            self.get_data_sync,
        )

    def write_file_sync(self, new_data):
        self.path.parent.mkdir(parents=True, exist_ok=True)
        with lock_file(self.path, 'w', exclusive=True) as file_obj:
            self.yaml.dump(new_data, file_obj)

    async def write_file(self, new_data):
        return await get_running_loop().run_in_executor(
            None,
            self.write_file_sync,
            new_data,
        )

    def copy_file_sync(self, new_stream, *, mode='w'):
        self.path.parent.mkdir(parents=True, exist_ok=True)
        with lock_file(self.path, mode=mode, exclusive=True) as file_obj:
            copyfileobj(new_stream, file_obj)

    async def copy_file(self, new_stream, *, mode='w'):
        return await get_running_loop().run_in_executor(
            None,
            partial(self.copy_file_sync, new_stream, mode=mode)
        )


class DirList(tuple):
    __slots__ = ('write_path',)

    def __new__(cls, write_path, path_iter, /):
        instance = super().__new__(cls, (
            Path(path)
            for path in path_iter
        ))
        instance.write_path = Path(write_path)
        return instance


default_eager_files = object()


class ProgramFiles:
    def __init__(
            self,
            name,
            *,
            file_cls=File,
            eager_files=default_eager_files,
    ):
        self.name = name
        self.__file_cls = file_cls

        self.__data_dirs = DirList(
            save_data_path(name),
            load_data_paths(name),
        )

        self.__config_dirs = DirList(
            save_config_path(name),
            load_config_paths(name),
        )

        try:
            self.__runtime_dir = Path(get_runtime_dir())
        except KeyError:
            self.__runtime_dir = None

        self.__cache_dir = Path(save_cache_path(self.name))

        if eager_files:
            if eager_files is default_eager_files:
                eager_files = {
                    'config': (name, )
                }

            for file_type, files in eager_files.items():
                for file in files:
                    self.get(FileType(file_type), file)

    def get_config_file(self, name, *args, **kwargs):
        file = self.__file_cls(
            self.get_file_name(name, *args, **kwargs),
            self.__config_dirs.write_path,
            self.__config_dirs,
            FileType.config,
            *args,
            **kwargs,
        )

        return file

    def get_data_file(self, name, *args, **kwargs):
        file = self.__file_cls(
            self.get_file_name(name, *args, **kwargs),
            self.__data_dirs.write_path,
            self.__data_dirs,
            FileType.data,
            *args,
            **kwargs,
        )

        return file

    def get_runtime_file(self, name, *args, **kwargs):
        if self.__runtime_dir is None:
            warn(
                '$XDG_RUNTIME_DIR not set in env defaulting to temp dir',
                ResourceWarning,
            )
            self.__runtime_dir = Path(mkdtemp())

        file = self.__file_cls(
            self.get_file_name(name, *args, **kwargs),
            self.__runtime_dir,
            (self.__runtime_dir, ),
            FileType.runtime,
            *args,
            **kwargs,
        )

        return file

    def get_cache_file(self, name, *args, **kwargs):
        file = self.__file_cls(
            self.get_file_name(name, *args, **kwargs),
            self.__cache_dir,
            (self.__cache_dir, ),
            FileType.cache,
            *args,
            **kwargs,
        )

        return file

    def get(self, file_type, name, *args, **kwargs):
        if file_type is FileType.cache:
            return self.get_cache_file(name, *args, **kwargs)
        elif file_type is FileType.config:
            return self.get_config_file(name, *args, **kwargs)
        elif file_type is FileType.runtime:
            return self.get_runtime_file(name, *args, **kwargs)
        elif file_type is FileType.data:
            return self.get_data_file(name, *args, **kwargs)
        else:
            return None

    def get_file_name(self, name):
        return f'{name}.yaml'
