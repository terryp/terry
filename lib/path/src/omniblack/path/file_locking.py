from contextlib import contextmanager
from fcntl import LOCK_EX, LOCK_SH, LOCK_UN, lockf


@contextmanager
def lock_file(file_path, mode='r', *, exclusive=False):
    cmd = LOCK_EX if exclusive else LOCK_SH

    with open(file_path, mode=mode) as file_object:
        try:
            lockf(file_object, cmd)
            yield file_object
        finally:
            if file_object is not None:
                lockf(file_object, LOCK_UN)
