import { LitElement } from 'lit';

export class MithrilElement extends LitElement {
    static register() {
        const registered = customElements.get(this.element_name);

        if (registered) {
            if (registered !== this) {
                throw new Error(
                    `"${this}" has been used to `
                        + 'register a different custom element.',
                );
            } else {
                return;
            }
        }


        const existingName = customElements.getName(this);

        if (existingName) {
            if (existingName !== this.element_name) {
                throw new Error(
                    `"${this.element_name}" has been used `
                        + `registered as ${existingName} already.`,
                );
            } else {
                return;
            }
        }

        customElements.define(this.element_name, this);

        for (const child of this.needed_elements ?? []) {
            child.register();
        }
    }
}
