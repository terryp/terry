from contextlib import contextmanager
from io import TextIOBase
from textwrap import wrap


class Sheet:
    def __init__(self, stream: TextIOBase):
        self.stream = stream
        self.indent = ''
        self.prefix = None

    def __enter__(self):
        return self

    def __exit__(self, *args):
        return self.stream.__exit__(*args)

    def write(self, string=''):
        if self.prefix is not None:
            prefix = self.indent + self.prefix
        else:
            prefix = self.indent

        if string or self.prefix:
            final_string = f'{prefix}{string}'.rstrip()
            final_string += '\n'
            self.stream.write(final_string)
        else:
            self.stream.write('\n')

    def writelines(self, strings):
        for string in strings:
            self.write(string)

    @contextmanager
    def block_comment(self):

        self.write('/*')
        self.prefix = ' * '

        yield

        self.prefix = None
        self.write(' */')

    def comment_paragraph(self, paragraph):
        current_indent = len(self.indent)
        lines = wrap(paragraph, width=(79 - (4 + current_indent)))
        self.writelines(lines)

    @contextmanager
    def rule(self, selector):
        self.write(f'{selector} {{')
        old_indent = self.indent
        self.indent += (' ' * 4)

        yield

        self.indent = old_indent
        self.write('}')

    def property(self, name, value):
        self.write(f'{name}: {value};')

    def custom_property(self, name, value):
        self.write(f'--{name}: {value};')

    def theme_color(self, color):
        self.custom_property(color.name, color.value)
