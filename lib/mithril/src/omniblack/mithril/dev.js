import { Root } from './main.js';
import routes from './routes/index.js';
import config from '@omniblack/config/web';
import { Input } from './components/input.js';

globalThis.Input = Input;

config.setup();

// Get a ref to the app so it doesn't get GC-ed

const _app = new Root({
    target: document.getElementById('app'),
    props: {
        routes,
        baseURL: '/framework',
    },
});

