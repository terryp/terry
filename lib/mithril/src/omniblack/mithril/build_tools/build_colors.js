import { promisify } from 'node:util';
import { execFile } from 'node:child_process';
import { modDir } from '@omniblack/node-utils';
import { Path } from '@omniblack/pathlib';

const exec = promisify(execFile);

export function build_colors() {
    const virtualId = '@omniblack/mithril/colors.css';
    console.log('Build color');

    return {
        name: '@omniblack/mithril/colors',
        resolveId(id) {
            if (id === 'mithril.colors') {
                return { id: virtualId };
            }
            return null;
        },
        async load(id) {
            if (id !== virtualId) {
                return null;
            }

            const buildToolsDir = new Path(modDir(import.meta));
            const srcDir = buildToolsDir.parent;

            const colorsYamlPath = srcDir.join('colors.yaml');
            console.log(colorsYamlPath);
            this.addWatchFile(String(colorsYamlPath));
            this.addWatchFile(String(srcDir.join('colors.py')));
            this.addWatchFile(String(srcDir.join('css.py')));


            const pkgDir = srcDir.parent;

            const { stdout } = await exec(
                'python',
                [
                    '-m',
                    'omniblack.mithril.colors',
                    '--in',
                    String(colorsYamlPath),
                    '--out',
                    '-',
                ],
                { cwd: String(pkgDir) },
            );

            return stdout;
        },
    };
}
