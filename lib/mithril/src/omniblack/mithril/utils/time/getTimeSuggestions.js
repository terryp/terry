import { Temporal } from '@js-temporal/polyfill';
import { createStartwithRegex } from '#src/utils/text.js';


// If am/pm not specified, we will assume 7:00 means AM, and 6:59 means PM
const FIRST_HOUR_OF_WORKDAY = 7;
function isPmBiasedHour(hour) {
    return 1 <= hour && hour < FIRST_HOUR_OF_WORKDAY;
}

const r = String.raw;
const colonFormatRegexString = createStartwithRegex([
    r`(\d{1,2})`,
    ':',
    r`(\d{1,2})`,
    ':',
    r`(\d{1,2})`,
], false);

const colonFormatRegex = new RegExp(
    colonFormatRegexString + r`\s*([ap]m?)?\s*$`,
    'ui',
);

/*
 * A pattern's getTimePiecesFromMatch() should return the pieces as strings in
 * so that getTimeSuggestions() knows to suggest 3AM for '03:00'.
 */
const PATTERNS = [
    {

        /*
         * No colons just number and an optional am|pm
         * Examples:
         *
         * - 3
         * - 330
         * - 330pm
         * - 330 am
         * - 0330
         * - 33045
         */
        re: /^(\d{1,6})\s*([ap]m?)?$/ui,
        getTimePiecesFromMatch(match) {
            const str = match[1];
            const ampm = match[2];

            let hour,
                rest;
            if (str.length % 2 === 1) {
                // odd number of digits
                hour = str.slice(0, 1);
                rest = str.slice(1);
            } else {
                hour = str.slice(0, 2);
                rest = str.slice(2);
            }
            const minute = rest.slice(0, 2);
            const second = rest.slice(2, 4);

            // minute and second must be double digit, if present
            if (minute.length === 1 || second.length === 1) {
                return;
            }

            return { hour, minute, second, ampm };
        },
    },
    {

        /*
         * Colons hh:mm:ss and an optional am|pm
         * Examples:
         *
         * - 1:30pm
         * - 02:30:45
         * - 1:1:1
         */
        re: colonFormatRegex,
        getTimePiecesFromMatch(match) {
            const hour = match[1];
            let minute = match[2];
            let second = match[3];
            const ampm = match[4];

            // make it so 3:3:3 becomes 3:30:30, not 3:03:03
            [minute, second] = [minute, second].map((piece) => {
                const shouldBeConverted = piece
                    && piece.length === 1
                    && Number(piece) <= 5;

                if (shouldBeConverted) {
                    return `${piece}0`;
                } else {
                    return piece;
                }
            });

            return { hour, minute, second, ampm };
        },
    },
    {
        // noon
        re: createStartwithRegex('noon'),
        getTimePiecesFromMatch(match) {
            return { hour: '12', ampm: 'pm' };
        },
    },
    {
        // midnight
        re: createStartwithRegex('midnight'),
        getTimePiecesFromMatch(match) {
            return { hour: '12', ampm: 'am' };
        },
    },
    {
        re: /^$/,
        getTimePiecesFromMatch() {
            const now = Temporal.Now.plainTimeISO();

            let hour = now.hour;
            if (hour > 12) {
                hour -= 12;
            }
            return { hour };
        },
    },
];

/*
 * Return an array of suggested time strings based on userInput.
 *
 * There are three possible lengths for the returned array:
 *     0 - If the input was not time-like
 *     1 - If the input was time-like, and unambiguous with respect to AM/PM
 *     2 - If the input was time-like, and ambiguous with respect to AM/PM
 */
export default function getTimeSuggestions(userInput = '') {
    for (const { re, getTimePiecesFromMatch } of PATTERNS) {
        const match = userInput.match(re);
        if (!match) {
            continue;
        }

        const timePieces = getTimePiecesFromMatch(match);
        if (!timePieces) {
            continue;
        }

        if (timePieces.hour[0] === '0' && !timePieces.ampm) {
            // assume '0330' means AM unless pm is specified
            timePieces.ampm = 'am';
        }

        for (const field of ['hour', 'minute', 'second']) {
            timePieces[field] = Number(timePieces[field]) || 0;
        }

        const userInputDenotesAMPM = timePieces.ampm || timePieces.hour > 12;

        const time = getTimeFromPieces(timePieces);
        if (!time) {
            continue;
        }

        const suggestions = [makeSuggestion(time)];

        if (!userInputDenotesAMPM) {
            const alternativeHour = (time.hour + 12) % 24;
            const alternativeTime = time.with({ hour: alternativeHour });
            suggestions.push(makeSuggestion(alternativeTime));
        }

        return suggestions;
    }

    return [];
}


function getTimeFromPieces({ hour, minute = 0, second = 0, ampm = '' }) {
    ampm = ampm.toLowerCase().charAt(0);

    // adjust times to 0-23 format that Date prefers
    if (ampm) {
        if (hour === 12 && ampm === 'a') {
            hour = 0;
        } else if (hour !== 12 && ampm === 'p') {
            hour += 12;
        }
    } else if (isPmBiasedHour(hour)) {
        hour += 12;
    }

    if (hour === 24) {
        hour = 0;
    }

    try {
        return Temporal.PlainTime.from({ hour, minute, second });
    } catch (err) {
        if (err instanceof RangeError) {
            return;
        } else {
            throw err;
        }
    }
}

function makeSuggestion(time) {
    return {
        internal: time,
        display: time?.toLocaleString(),
    };
}
