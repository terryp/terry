import {
    format as dateformat,
    parseISO,
    isValid,
} from 'date-fns';

const TIME_FORMAT_WITH_SECONDS = "h:mm:ss' 'a";
const TIME_FORMAT_WITHOUT_SECONDS = "h:mm' 'a";

export const precisions = {
    SECONDS: Symbol('Time input precision seconds'),
};

const formats = {
    [precisions.SECONDS]: TIME_FORMAT_WITH_SECONDS,
    [false]: TIME_FORMAT_WITHOUT_SECONDS,
};
// Note: midnight = am, noon = pm

/*
 * Returns a formatted time string
 *
 * Args:
 *  date: a valid date
 *  precision: one of the above formats
 */
export function formatTime(date, precision = false) {
    const format = formats[precision];
    const timeString = dateformat(date, format);
    const result = { display: timeString };

    const hour = date.getHours();
    const minutesOrSeconds = date.getMinutes()
        || (precision === precisions.SECONDS && date.getSeconds());

    if (hour === 0 && !minutesOrSeconds) {
        result.clarification = 'midnight';
    } else if (hour === 12 && !minutesOrSeconds) {
        result.clarification = 'noon';
    }

    return result;
}

const timeStringRegex = /[0-2]\d:[0-6]\d:[0-6]{2}/;

function isTimeString(string) {
    return timeStringRegex.test(string);
}

/*
 * Returns a formatted time string. If string is invalid time
 * then returns null
 *
 * Args:
 *  timeString: string portion of a time
 *  precision: one of the above formats
 */
export function formatTimeString(timeString, precision = false) {
    /*
     * Any valid date portion will work here
     * We just want to get a date and validate the time string
     */
    const timeISOString = `2020-01-08T${timeString}Z`;
    const dateObj = parseISO(timeISOString);
    const isTime = isTimeString(timeString);
    if (isTime && dateObj instanceof Date && isValid(dateObj)) {
        return formatTime(dateObj, precision).display;
    }

    return null;
}
