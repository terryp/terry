import { Code } from '@omniblack/error';

/**
 * Yield the value indicated by cmpFunc from iterators until all iterators are
 * finalized.
 * Args:
 *      iterables: an array of iterables
 *      cmpFunc: a function following the signature of an
 *          array.sort compare function, used to select the winner from
 *          the iterators to yield
 */
export function *emitCompared(iterables, cmpFunc = defaultCompare) {
    /*
     * This function can take iterables (eg. Arrays) or
     * iterators (eg. generators).
     * this converts them all to iterators for use later in the function
     */
    const iterators = iterables.map(iter);
    const currentValues = iterators.map((iter, index) => {
        const { value } = iter.next();
        return { value, index };
    });
    while (true) {
        const sortedValues = currentValues.filter(({ value }) =>
            value !== undefined);
        // All iterators are finshed so we need to exit
        if (sortedValues.length === 0) {
            return;
        }
        sortedValues.sort((a, b) => cmpFunc(a.value, b.value));
        const { value, index } = sortedValues[0];
        yield value;
        // replace yielded value with new value from the same iterator
        replaceValue(iterators, currentValues, index);
    }
}

// Yield values from gen until a value exceeds one of the specified limits
export function *limit(
    {
        upper = Number.POSITIVE_INFINITY,
        lower = Number.NEGATIVE_INFINITY,
    },
    gen,
) {
    for (const value of gen) {
        if (value >= upper || value < lower) {
            return;
        }
        yield value;
    }
}

// protect gen from being closed by a for-of loop
export function *noClose(gen) {
    let { done, value } = gen.next();
    while (!done) {
        yield value;
        // the parens tell javascript that this is destructuring, not a block
        ({ done, value } = gen.next());
    }
}


/**
 * Replace the dirty/used value in valuesArray with a new value from
 *  the iterator it came from.
 *
 * Remove iterator if it is finished.
 *
 * sourceIterators - array of iterators available
 *
 * valuesArray - current list of the last value from each iterator
 *
 * index - index of the dirty value in valuesArray that needs to be
 *  replaced and the index of the iterator it came from
 */
function replaceValue(sourceIterators, valuesArray, index) {
    const { value, done } = sourceIterators[index].next();
    if (!done) {
        valuesArray[index] = { value, index };
    } else {
        delete valuesArray[index];
        delete sourceIterators[index];
    }
}

function defaultCompare(a, b) {
    if (a < b) {
        return -1;
    }
    if (a > b) {
        return 1;
    }
    return 0;
}


/*
 * convert an iterable to an iterator
 * iterators should be returned as is by Symbol.iterator
 */
function iter(iterable) {
    if (Symbol.iterator in iterable) {
        return iterable[Symbol.iterator]();
    }
    throw new Code('Non iterable passed to iter');
}
