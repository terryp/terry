export function register(cls, name, builtin) {
    const registered = customElements.get(name);

    if (registered && registered !== cls) {
        throw new Error(
            `"${name}" has been used to `
            + 'register a different custom element.',
        );
    }


    const existingName = customElements.getName(cls);

    if (existingName && existingName !== name) {
        throw new Error(
            `"${cls.name}" has been used `
            + `registered as ${existingName} already.`,
        );
    }

    if (!existingName && !registered) {
        customElements.define(name, cls, { extends: builtin });
    }
}
