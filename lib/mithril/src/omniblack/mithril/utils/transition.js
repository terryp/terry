import { derived } from 'svelte/store';
import * as transitions from 'svelte/transition';

import { animated } from './animate.js';

export function noOpTransition() {
    return {
        delay: 0,
        duration: 0,
        css: () => '',
    };
}

/**
 * Wraps a svelte transition in a derived store.
 *      If the user has indicated they don't want
 *      animation the transition will be replaced
 *      by a no-op transition.
 *
 * @param {Function} transition - The transition to use when animation
 *      is enabled.
 *
 *  @param {Function} [noAnimate] - The transition to use when animation
 *          is disabled. This should be some form of no-op.
 * @returns {derived}
 */
export function wrapTransition(transition, noAnimate = noOpTransition) {
    return derived(animated, ($animated) => {
        if ($animated) {
            return transition;
        } else {
            return noAnimate;
        }
    });
}

export const fade = wrapTransition(transitions.fade);

export const blur = wrapTransition(transitions.blur);

export const fly = wrapTransition(transitions.fly);

export const slide = wrapTransition(transitions.slide);

export const scale = wrapTransition(transitions.scale);

export const draw = wrapTransition(transitions.draw);

function crossFadeWrapper() {
    return [noOpTransition, noOpTransition];
}

export const crossfade = wrapTransition(
    transitions.crossfade,
    crossFadeWrapper,
);

export { crossfade as crossFade };

