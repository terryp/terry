/*
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */

import getAsyncDisposableStack
    from 'disposablestack/AsyncDisposableStack/polyfill';
import { notEqual } from '@lit/reactive-element';

const AsyncDisposableStack = getAsyncDisposableStack();

/**
 * States for task status
 */
export const TaskStatus = {
    INITIAL: Symbol('The task has not started.'),
    PENDING: Symbol('The task is running.'),
    COMPLETE: Symbol('The task completed successfully.'),
    ERROR: Symbol('The task failed.'),
};

/**
 * A special value that can be returned from task functions to reset the task
 * status to INITIAL.
 */
export const initialState = Symbol();

export class Task {
    #previousArgs = undefined;
    #task = undefined;
    #argsFn = undefined;
    #argsEqual = shallowArrayEquals;
    #callId = 0;
    #host = undefined;
    #value = undefined;
    #error = undefined;
    #abortController = undefined;
    #onComplete = undefined;
    #onError = undefined;
    #status = TaskStatus.INITIAL;
    #stack = new AsyncDisposableStack();
    #name = undefined;

    /**
     * Determines if the task is run automatically when arguments change after a
     * host update. Default to `true`.
     *
     * @see {@link TaskConfig.autoRun} for more information.
     */
    autoRun = true;

    /**
     * A Promise that resolve when the current task run is complete.
     *
     * If a new task run is started while a previous run is pending, the Promise
     * is kept and only resolved when the new run is completed.
     */
    get taskComplete() {
        /*
         * If a task run exists, return the cached promise. This is true in the case
         * where the user has called taskComplete in pending or completed state
         * before and has not started a new task run since.
         */
        if (this.#taskComplete) {
            return this.#taskComplete;
        }

        /*
         * Generate an in-progress promise if the status is pending and has been
         * cleared by .run().
         */
        if (this.#status === TaskStatus.PENDING) {
            const { promise, resolve, reject } = Promise.withResolvers();
            this.#taskComplete = promise;
            this.#resolveTaskComplete = resolve;
            this.#rejectTaskComplete = reject;
            // If the status is error, return a rejected promise.
        } else if (this.#status === TaskStatus.ERROR) {
            this.#taskComplete = Promise.reject(this.#error);

            /*
             * Otherwise we are at a task run's completion or this is the first
             * request and we are not in the middle of a task (i.e. INITIAL).
             */
        } else {
            this.#taskComplete = Promise.resolve(this.#value);
        }

        return this.#taskComplete;
    }

    #resolveTaskComplete = undefined;
    #rejectTaskComplete = undefined;
    #taskComplete = undefined;

    constructor(host, task, args) {
        this.#host = host;
        this.#host.addController(this);
        const taskConfig = typeof task === 'object'
            ? task
            : { task, args };
        this.#task = taskConfig.task;
        this.#argsFn = taskConfig.args;
        this.#onComplete = taskConfig.onComplete;
        this.#onError = taskConfig.onError;
        this.#name = taskConfig.name;
        if (Object.hasOwn(taskConfig, 'argsEqual')) {
            this.#argsEqual = taskConfig.argsEqual;
        }

        if (Object.hasOwn(taskConfig, 'autoRun')) {
            this.autoRun = taskConfig.autoRun;
        }

        /*
         * Providing initialValue puts the task in COMPLETE state and stores the
         * args immediately so it only runs when they change again.
         */
        if (Object.hasOwn(taskConfig, 'initialValue')) {
            this.#value = taskConfig.initialValue;
            this.#status = TaskStatus.COMPLETE;
            this.#previousArgs = this.#getArgs?.();
        }
    }

    hostUpdate() {
        if (this.autoRun === true) {
            this.#performTask();
        }
    }

    hostUpdated() {
        if (this.autoRun === 'afterUpdate') {
            this.#performTask();
        }
    }

    #getArgs() {
        if (this.#argsFn === undefined) {
            return undefined;
        }
        const args = this.#argsFn();
        if (!Array.isArray(args)) {
            throw new TypeError('The args function must return an array');
        }
        return args;
    }

    /**
     * Determines if the task should run when it's triggered because of a
     * host update, and runs the task if it should.
     *
     * A task should run when its arguments change from the previous run, based on
     * the args equality function.
     *
     * This method is side-effectful: it stores the new args as the previous args.
     */
    async #performTask() {
        const args = this.#getArgs();
        const prev = this.#previousArgs;
        this.#previousArgs = args;
        if (
            args !== prev
                && args !== undefined
                && (prev === undefined || !this.#argsEqual(prev, args))
        ) {
            await this.run(args);
        }
    }

    /**
     * Runs a task manually.
     *
     * This can be useful for running tasks in response to events as opposed to
     * automatically running when host element state changes.
     *
     * @param args an optional set of arguments to use for this task run. If args
     *     is not given, the args function is called to get the arguments for
     *     this run.
     */
    async run(args) {
        args ??= this.#getArgs();

        // Remember the args for potential future automatic runs.
        this.#previousArgs = args;

        if (this.#status === TaskStatus.PENDING) {
            this.#abortController?.abort();
        } else {
            /*
             * Clear the last complete task run in INITIAL because it may be a resolved
             * promise. Also clear if COMPLETE or ERROR because the value returned by
             * awaiting taskComplete may have changed since last run.
             */
            this.#taskComplete = undefined;
            this.#resolveTaskComplete = undefined;
            this.#rejectTaskComplete = undefined;
        }

        this.#status = TaskStatus.PENDING;
        let result;
        let error;

        // Request an update to report pending state.
        if (this.autoRun === 'afterUpdate') {
            // Avoids a change-in-update warning
            queueMicrotask(() => this.#host.requestUpdate(this.#name));
        } else {
            this.#host.requestUpdate(this.#name);
        }

        // eslint-disable-next-line no-plusplus
        const key = ++this.#callId;
        const stack = new AsyncDisposableStack();
        this.#abortController = new AbortController();
        let errored = false;
        try {
            result = await this.#task(
                args,
                {
                    signal: this.#abortController.signal,
                    stack,
                },
            );
        } catch (err) {
            errored = true;
            error = err;
            try {
                const stackValues = stack.move();
                await stackValues.disposeAsync();
            } catch {
                /* ignore cleanup errors */
            }
        }
        // If this is the most recent task call, process this value.
        if (this.#callId === key) {
            const oldStack = this.#stack;
            this.#stack = stack;
            if (result === initialState) {
                this.#status = TaskStatus.INITIAL;
            } else {
                if (errored === false) {
                    try {
                        this.#onComplete?.(result);
                    } catch {
                        // Ignore user errors from onComplete.
                    }
                    this.#status = TaskStatus.COMPLETE;
                    this.#resolveTaskComplete?.(result);
                } else {
                    try {
                        this.#onError?.(error);
                    } catch {
                        // Ignore user errors from onError.
                    }
                    this.#status = TaskStatus.ERROR;
                    this.#rejectTaskComplete?.(error);
                }
                this.#value = result;
                this.#error = error;
            }
            // Request an update with the final value.
            this.#host.requestUpdate(this.#name);

            try {
                await oldStack.disposeAsync();
            } catch {
                // ignore cleanup errors
            }
        } else {
            try {
                await stack.disposeAsync();
            } catch {
                // ignore cleanup errors
            }
        }
    }

    /**
     * Aborts the currently pending task run by aborting the AbortSignal
     * passed to the task function.
     *
     * Aborting a task does nothing if the task is not running: ie, in the
     * complete, error, or initial states.
     *
     * Aborting a task does not automatically cancel the task function. The task
     * function must be written to accept the AbortSignal and either forward it
     * to other APIs like `fetch()`, or handle cancellation manually by using
     * [`signal.throwIfAborted()`]{@link https://developer.mozilla.org/en-US/docs/Web/API/AbortSignal/throwIfAborted}
     * or the
     * [`abort`]{@link https://developer.mozilla.org/en-US/docs/Web/API/AbortSignal/abort_event}
     * event.
     *
     * @param reason The reason for aborting. Passed to
     *     `AbortController.abort()`.
     */
    abort(reason) {
        if (this.#status === TaskStatus.PENDING) {
            this.#abortController?.abort(reason);
        }
    }

    /**
     * The result of the previous task run, if it resolved.
     *
     * Is `undefined` if the task has not run yet, or if the previous run errored.
     */
    get value() {
        return this.#value;
    }

    /**
     * The error from the previous task run, if it rejected.
     *
     * Is `undefined` if the task has not run yet, or if the previous run
     * completed successfully.
     */
    get error() {
        return this.#error;
    }

    get status() {
        return this.#status;
    }

    render(renderer) {
        switch (this.#status) {
            case TaskStatus.INITIAL:
                return renderer.initial?.();
            case TaskStatus.PENDING:
                return renderer.pending?.();
            case TaskStatus.COMPLETE:
                return renderer.complete?.(this.value);
            case TaskStatus.ERROR:
                return renderer.error?.(this.error);
            default:
                throw new Error(`Unexpected status: ${this.#status}`);
        }
    }
}

export function shallowArrayEquals(oldArgs, newArgs) {
    if (oldArgs === newArgs) {
        return true;
    }

    if (oldArgs.length !== newArgs.length) {
        return false;
    }

    return oldArgs.every((v, i) => !notEqual(v, newArgs[i]));
}
