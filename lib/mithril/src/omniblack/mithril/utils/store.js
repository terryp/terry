import { AsyncDirective, directive } from 'lit/async-directive.js';

class Subscribe extends AsyncDirective {
    render(store) {
        if (this.store !== store) {
            this.unsub?.();
            this.store = store;

            queueMicrotask(() => {
                this.unsub = store.subscribe((newValue) => {
                    this.setValue(newValue);
                });
            });
        }

        return '';
    }

    disconnected() {
        this.unsub?.();
        this.unsub = null;
    }

    reconnected() {
        this.unsub = this.store.subscribe((newValue) => {
            this.setValue(newValue);
        });
    }
}

export const subscribe = directive(Subscribe);

export class StoreSub {
    constructor(host, store) {
        this.host = host;
        this.host.addController(host);
        this.store = store;
        this.unsub = null;
        this.storeCb = this.storeCb.bind(this);
    }

    hostConnected() {
        this.ensureUnsub();
        this.unsub = this.store.subscribe(this.storeCb);
    }

    storeCb(value) {
        this.value = value;
        this.host.requestUpdate();
    }

    ensureUnsub() {
        this.unsub?.();
        this.unsub = null;
    }
}
