export function to(url, method = 'get') {
    const form = document.createElement('form');
    form.method = method;
    form.action = url;
    document.body.appendChild(form);
    form.submit();
}
