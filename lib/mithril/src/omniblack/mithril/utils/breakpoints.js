import { readable } from 'svelte/store';

const isSmallQuery = window.matchMedia('(min-width: 576px)');

export const isSmall = readable(isSmallQuery.matches, (set) => {
    isSmallQuery.addEventListener('change', (event) => {
        set(event.matches);
    });
});

const isMediumQuery = window.matchMedia('(min-width: 768px)');

export const isMedium = readable(isMediumQuery.matches, (set) => {
    isMediumQuery.addEventListener('change', (event) => {
        set(event.matches);
    });
});

const isLargeQuery = window.matchMedia('(min-width: 992px)');

export const isLarge = readable(isLargeQuery.matches, (set) => {
    isLargeQuery.addEventListener('change', (event) => {
        set(event.matches);
    });
});

const isExtraLargeQuery = window.matchMedia('(min-width: 1200px)');

export const isExtraLarge = readable(isExtraLargeQuery.matches, (set) => {
    isExtraLargeQuery.addEventListener('change', (event) => {
        set(event.matches);
    });
});
