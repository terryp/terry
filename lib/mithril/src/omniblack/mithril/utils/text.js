// return an array of identifiers whose regular expressions match the provided
export function matchRegexList(regexList, value) {
    // eslint-disable-next-line unicorn/no-array-reduce
    const matches = regexList.reduce((acc, { re, identifier }) => {
        if (re.test(value)) {
            return [...acc, identifier];
        }
        return acc;
    }, []);
    return matches;
}


/*
 * Return regular expression that case insensitive matchs any string starting
 * with the input string, or token list
 * if compile is false a string that would compile to that regular experssion
 * returned
 */
export function createStartwithRegex(tokenList, compile = true) {
    let output = String.raw`^\s*${tokenList[0]}`;
    let outputEnd = '';
    for (const token of tokenList.slice(1)) {
        output += String.raw`(?:${token}`;
        outputEnd += ')?';
    }
    output += outputEnd;
    if (compile) {
        return new RegExp(output + String.raw`\s*$`, 'ui');
    } else {
        return output;
    }
}


/*
 * Return tagged template function enabling multiline Regex.
 *
 * See https://gist.github.com/shannonmoeller/b4f6fbab2ffec56213e7
 *
 * Args:
 *     flags: String of Regex flags, i.e. 'gm' or 'i'
 */
export function multilineRegex(flags) {
    const trailingComments = /\s+#.*$/gm;
    const surroundingWhitespace = /^\s+|\s+$/gm;
    const literalNewlines = /[\r\n]/g;

    return (strings, ...values) => {
        function toPattern(pattern, rawString, i) {
            let value = values[i];

            if (value === null || typeof value === 'undefined') {
                return pattern + rawString;
            }

            if (value instanceof RegExp) {
                value = value.source;
            }

            return pattern + rawString + value;
        }

        const compiledPattern = strings.raw
            // eslint-disable-next-line unicorn/no-array-reduce
            .reduce(toPattern, '')
            .replaceAll(trailingComments, '')
            .replaceAll(surroundingWhitespace, '')
            .replaceAll(literalNewlines, '');

        return new RegExp(compiledPattern, flags);
    };
}
