import { sorted, uniqueJustseen } from 'itertools';
import { AsyncDirective, directive, PartType } from 'lit/async-directive.js';

function getKey([key, _value]) {
    return key;
}


function applyProperties(styles, oldProps, add, remove) {
    if (!Array.isArray(styles)) {
        styles = [styles];
    }

    const allEntries = styles.flatMap((style = {}) => Object.entries(style));

    const properties = Array.from(
        uniqueJustseen(sorted(allEntries, getKey), getKey),
    );

    const toAdd = properties
        .filter(([_key, value]) =>
            value !== undefined && value !== null && value !== false);

    const addedNames = new Set(toAdd.map(([key]) => key));


    for (const [key, value] of toAdd) {
        add(key, value);
    }

    for (const prop of oldProps) {
        if (!addedNames.has(prop)) {
            remove(prop);
        }
    }

    return addedNames;
}


export function applyStyles(element, styles) {
    const declaration = element.style;
    function remove(key) {
        declaration.removeProperty(key);
    }

    function add(key, value) {
        declaration.setProperty(key, value);
    }

    let appliedProperties = applyProperties(element, styles, [], add, remove);

    return {
        update(styles) {
            appliedProperties = applyProperties(
                element,
                styles,
                appliedProperties,
                add,
                remove,
            );
        },
    };
}


class Classes extends AsyncDirective {
    constructor(partInfo) {
        super(partInfo);
        this.addClass = this.addClass.bind(this);
        this.removeClass = this.removeClass.bind(this);

        if (partInfo.type !== PartType.ELEMENT) {
            throw new Error(
                '`classes()` can only be used as an element directive.',
            );
        }
    }

    update(partInfo, classes) {
        this.el = partInfo.element;
        this.prevClasses = applyProperties(
            classes,
            this.prevClasses ?? [],
            this.addClass,
            this.removeClass,
        );
    }

    addClass(klass) {
        this.el.classList.add(klass);
    }

    removeClass(klass) {
        this.el.classList.remove(klass);
    }
}

export const classes = directive(Classes);

class Styles extends AsyncDirective {
    constructor(partInfo) {
        super(partInfo);
        this.setProperty = this.setProperty.bind(this);
        this.removeProperty = this.removeProperty.bind(this);

        if (partInfo.type !== PartType.ELEMENT) {
            throw new Error(
                '`styles()` can only be used as an element directive.',
            );
        }
    }

    update(partInfo, classes) {
        this.el = partInfo.element;
        this.prevStyles = applyProperties(
            classes,
            this.prevStyles ?? [],
            this.setProperty,
            this.removeProperty,
        );
    }

    setProperty(prop_name, value) {
        this.el.style.setProperty(prop_name, value);
    }

    removeProperty(prop_name) {
        this.el.style.removeProperty(prop_name);
    }
}

export const styles = directive(Styles);
