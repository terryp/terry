import * as df from 'date-fns';
import {
    BadDate,
    createDate,
    addMonth,
    addYear,
} from '#src/utils/date/dateUtils.js';
import { limit } from '#src/utils/iterators.js';

export const timeSteps = {
    day: df.addDays,
    weekday: df.addWeeks,
    monthDays: addDayInMonth,
    month: addMonth,
    year: addYear,
};

/**
 * Yield dates in direction by calling stepFunc with an offset,
 * and checking validity with all check functions.
 */
export function *stepper(
    {
        stepFunc,
        startDate,
        today,
        direction = 'future',
    },
) {
    let offset = 1;
    let sign = 1;
    const sameDay = df.isSameDay(today, startDate);
    if (direction === 'past') {
        sign = -1;
        if (df.isBefore(startDate, today) && !sameDay) {
            yield startDate;
        }
    } else if (df.isAfter(startDate, today) && !sameDay) {
        yield startDate;
    }
    while (true) {
        try {
            const newDate = stepFunc(startDate, offset * sign, direction);
            yield newDate;
        } catch (err) {
            if (!(err instanceof BadDate)) {
                throw err;
            }
        } finally {
            offset += 1;
        }
    }
}


/**
 * Yield dates in the next or past week based on direction. Starts at
 *  Monday, or Sunday, depending on the direction, and will go to Monday, or
 *  Sunday
 */
export function interweekStepper({ today, direction }) {
    let past = [];
    let future = [];
    if (direction === 'past') {
        const upper = df.setDay(today, 0);
        let startDate = df.setDay(upper, 6);
        startDate = df.subWeeks(startDate, 1);
        const lower = df.subWeeks(upper, 1);
        const limits = { lower, upper };
        past = limit(limits, stepper({
            stepFunc: timeSteps.day,
            today,
            direction: 'past',
            startDate,
        }));
    } else {
        let lower = df.addWeeks(today, 1);
        lower = df.setDay(lower, 0);
        const upper = df.addWeeks(lower, 1);
        const limits = { lower, upper };
        future = limit(limits, stepper({
            stepFunc: timeSteps.day,
            today,
            direction: 'future',
            startDate: lower,
        }));
    }

    return { past, future };
}


/**
 * Step by year for all years that match yearPrefix, centering on today.
 * Args:
 *      yearPrefix: the prefix all years should match
 *      monthIndex: the month of the year to yield
 *      day: the day of the month to yield
 */
export function yearStepper(
    {
        year,
        monthIndex,
        today,
        day,
    },
) {
    let past = [];
    let future = [];
    if (year.length === 4) {
        return { past, future };
    }
    const step = 'year';

    /*
     * we do not want to create dates no matching the prefix provided
     * by the user so we turn their prefix into an upper and lower limit set
     */
    const lowerBound = Number(year);
    const upperBound = lowerBound + 1;
    const lowerYear = lowerBound.toString().padEnd(4, '0');
    const upperYear = upperBound.toString().padEnd(4, '0');
    const upper = new Date(upperYear);
    const lower = new Date(lowerYear);
    const limits = { upper, lower };
    const todayYear = today.getFullYear();
    if (String(todayYear).startsWith(year)) {
        const startDate = createDate(todayYear, monthIndex, day);
        past = limit(limits, stepper({
            stepFunc: timeSteps[step],
            today,
            startDate,
            direction: 'past',
        }));
        future = limit(limits, stepper({
            stepFunc: timeSteps[step],
            today,
            startDate,
            direction: 'future',
        }));
    } else if (df.isAfter(lower, today)) {
        const startDate = createDate(lowerYear, monthIndex, day);
        future = limit(limits, stepper({
            stepFunc: timeSteps[step],
            today,
            startDate,
            direction: 'future',
        }));
    } else {
        // limit ends when a value equalling the upper limit is yielded
        const startDate = df.subYears(
            createDate(upperYear, monthIndex, day),
            1,
        );
        past = limit(limits, stepper({
            stepFunc: timeSteps[step],
            today,
            startDate,
            direction: 'past',
        }));
    }
    return { future, past };
}


/**
 * Return the next day of the month offset from date, if the month overflows
 *      go to the same month of the next year
 */
export function addDayInMonth(date, offset, direction = 'future') {
    let newDate = df.addDays(date, offset);
    const oldMonth = date.getMonth();
    const newMonth = newDate.getMonth();
    if (oldMonth !== newMonth) {
        const daysInMonth = df.getDaysInMonth(oldMonth);
        if (direction === 'future') {
            const remainingOffset = offset
                - df.differenceInDays(date, df.endOfMonth(date));
            const years = Math.floor(remainingOffset / daysInMonth);
            const remainingDays = remainingOffset % daysInMonth;
            newDate = df.addYears(date, years);
            newDate = df.startOfMonth(newDate);
            newDate = df.addDays(newDate, remainingDays);
        } else {
            const remainingOffset = Math.abs(offset)
                - df.differenceInDays(df.startOfMonth(date), date);
            const years = Math.floor(remainingOffset / daysInMonth);
            const remainingDays = remainingOffset % daysInMonth;
            newDate = df.subYears(date, years);
            newDate = df.endOfMonth(newDate);
            newDate.setHours(0, 0, 0, 0);
            newDate = df.subDays(newDate, remainingDays);
        }
    }
    return newDate;
}

