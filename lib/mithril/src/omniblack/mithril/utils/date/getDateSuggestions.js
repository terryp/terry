import * as df from 'date-fns';
import { uniqueEverseen, itake } from 'itertools';

import { noClose, emitCompared } from '#src/utils/iterators.js';
import {
    BadDate,
    createToday,
    createDate,
    setDay,
} from '#src/utils/date/dateUtils.js';
import {
    timeSteps,
    stepper,
    yearStepper,
    interweekStepper,
} from '#src/utils/date/dateSteppers.js';

import {
    createStartwithRegex,
    matchRegexList,
} from '#src/utils/text.js';

import { Code } from '@omniblack/error';
import { daysOfTheWeekNames, monthNames } from '@omniblack/localization/dates';

let $daysOfTheWeek;
daysOfTheWeekNames.subscribe((newNames) => {
    $daysOfTheWeek = newNames;
});

let $months;
monthNames.subscribe((newMonths) => {
    $months = newMonths;
});


// TODO: accept valid clarification text

const validBiases = ['future', 'past', 'nearest'];

export const RE = {
    '1or2Digits': /^ *\d{1,2} *$/,
};

const regexFormats = [
    // 09 Jan 2017
    {
        re: /^(\d{1,2})(?:\s+(\w{1,9})(?:\s+(\d{1,4}))?)?$/ui,
        groups: {
            day: 1,
            month: 2,
            year: 3,
        },
    },
    // 09-1-2017
    {
        re: /^(\d{1,2})-(?:(\d{1,2})(?:-(\d{1,4}))?)?$/ui,
        groups: {
            day: 1,
            month: 2,
            year: 3,
        },
    },
    // 1/09/2017
    {
        re: /^(\d{1,2})\/(?:(\d{1,2})(?:\/(\d{1,4}))?)?$/ui,
        groups: {
            day: 2,
            month: 1,
            year: 3,
        },
    },

];

const wordMatches = {
    today: today,
    yesterday: yesterday,
    tomorrow: tomorrow,
    next: nextWeek,
    last: lastWeek,
};

/*
 * the order of this array determines display order of the results
 * of the matched formats
 */
const formats = [
    {
        test: (value) => !value,
        exec: emptySuggestions,
    },
    ...createWordFormats(wordMatches),
    ...createRegexFormats(regexFormats),
    {
        test: isDayOfTheWeek,
        exec: getDaysOfTheWeekSuggestions,
    },
];


const dayRegexes = $daysOfTheWeek.map((day) => {
    return { identifier: day, re: createStartwithRegex(day) };
});

const monthRegexes = $months.map((month) => {
    return { identifier: month, re: createStartwithRegex(month) };
});


// Find all days of the week that starts with the user input
function matchDaysOfTheWeek(userInput) {
    return matchRegexList(dayRegexes, userInput);
}

// Find all months that match user input
function matchMonths(userInput) {
    return matchRegexList(monthRegexes, userInput);
}

function isDayOfTheWeek(value) {
    const matches = matchDaysOfTheWeek(value);
    return matches.length > 0;
}

export const MAX_SUGGESTIONS = 6;


/*
 * Returns an array of suggestions of dates based on userinput and the bias.
 * Arg:
 *      userInput: the string to suggest against
 * Kwargs:
 *      bias: The bias to use for ordering suggestions
 *      today: the date to use for the center of suggestions
 *      maxSuggestions: the maximum number of suggestions to provide.
 */
export function getDateSuggestions(
    userInput,
    {
        bias = 'future',
        today = createToday(),
        maxSuggestions = MAX_SUGGESTIONS,
    } = {},
) {
    checkEnumArg({ bias }, validBiases);
    const suggestions = [];

    // zero the time out so comparisons are unaffected by it
    today.setHours(0, 0, 0, 0);

    // if maxSuggestions is zero we are done
    if (maxSuggestions === 0) {
        return [];
    }
    let includeToday = false;
    const futureGens = [];
    const pastGens = [];
    for (const { test, exec } of formats) {
        try {
            if (test(userInput)) {
                const currentMax = maxSuggestions - suggestions.length;
                const {
                    past = [],
                    future = [],
                    matchedToday = false,
                } = exec({
                    userInput,
                    today,
                    maxSuggestions: currentMax,
                });
                if (matchedToday) {
                    includeToday = true;
                }
                futureGens.push(future);
                pastGens.push(past);
            }
        } catch (err) {
            if (err instanceof BadDate) {
                return [];
            } else {
                throw err;
            }
        }
    }

    if (includeToday) {
        suggestions.push(today);
        maxSuggestions -= 1;
    }
    return suggestions.concat(createSuggestions(
        emitCompared(futureGens, sortNearest(today)),
        emitCompared(pastGens, sortNearest(today)),
        bias,
        maxSuggestions,
        today,
    ));
}

/*
 * return suggestions that match the specified days in the order of the bias
 * Kwargs:
 *      matchedDays: the days of the week that suggestions should be created for
 *      maxSuggestions: the maximum number of suggestions to make
 *      bias: the bias to observe when suggesting dates
 */
function getDaysOfTheWeekSuggestions(
    {
        userInput,
        today,
    },
) {
    const matchedDays = matchDaysOfTheWeek(userInput);
    const futureGens = [];
    const pastGens = [];
    const todayIndex = df.getDay(today);
    let matchedToday = false;
    for (const day of matchedDays) {
        const dayIndex = $daysOfTheWeek.indexOf(day);
        let pastDate = today;
        let futureDate = today;
        if (todayIndex !== dayIndex) {
            pastDate = setDay(today, dayIndex, 'past');
            futureDate = setDay(today, dayIndex, 'future');
        } else {
            matchedToday = true;
        }
        const futureGen = stepper({
            stepFunc: timeSteps.weekday,
            today,
            startDate: futureDate,
            direction: 'future',
        });
        const pastGen = stepper({
            stepFunc: timeSteps.weekday,
            today,
            startDate: pastDate,
            direction: 'past',
        });
        futureGens.push(futureGen);
        pastGens.push(pastGen);
    }

    const future = emitCompared(futureGens, sortNearest(today));
    const past = emitCompared(pastGens, sortNearest(today));
    return { future, past, matchedToday };
}

/*
 * Return an Array for suggestions from the future, and past iterators
 * will not give more that maxSuggestions
 */
function createSuggestions(future, past, bias, maxSuggestions, today) {
    future = uniqueEverseen(future, (date) => date.valueOf());
    past = uniqueEverseen(past, (date) => date.valueOf());
    if (bias === 'future') {
        const numberOfFuture = Math.ceil(maxSuggestions / 2);
        return futureOrPast(future, numberOfFuture, past, maxSuggestions);
    } else if (bias === 'past') {
        const numberOfPast = Math.ceil(maxSuggestions / 2);
        return futureOrPast(past, numberOfPast, future, maxSuggestions);
    } else if (bias === 'nearest') {
        return nearest(future, past, today, maxSuggestions);
    }
}

/*
 * create suggestions from bias, and antibiasGen attempting to take biasNum from
 * biasGen, but allowing for either biasGen or antibiasGen to runout early
 * filling in the rest from the other generator if possible
 */
function futureOrPast(biasGen, biasNum, antibiasGen, max) {
    const biasResults = [];
    const antiBiasResults = [];
    // We may need the bias gen later
    for (const date of noClose(biasGen)) {
        biasResults.push(date);
        if (biasResults.length === biasNum) {
            break;
        }
    }
    for (const date of antibiasGen) {
        antiBiasResults.push(date);
        if (antiBiasResults.length + biasResults.length === max) {
            break;
        }
    }
    let total = biasResults.length + antiBiasResults.length;

    /*
     * the antiBias ran out before filling the expected max so start taking from
     * the bias to fill up to the expected max
     */
    if (total < max) {
        /*
         * if a generator is already done when a for loop is used on
         * it the for loop exits with no iterations
         */
        for (const date of biasGen) {
            biasResults.push(date);
            total += 1;
            if (total >= max) {
                break;
            }
        }
    }
    return biasResults.concat(antiBiasResults);
}

function nearest(past, future, today, max) {
    const compFnc = sortNearest(today);
    return Array.from(itake(max, emitCompared([past, future], compFnc)));
}

function getSuggestionsFromComplete(
    day,
    month,
    year,
    today = createToday(),
) {
    let date;
    try {
        date = createDate(year, month, day);
    } catch (err) {
        if (err instanceof BadDate) {
            date = createDate(year, day, month);
        }
    }

    const past = [];
    const future = [];
    let matchedToday = false;
    if (df.isEqual(date, today)) {
        matchedToday = true;
    } else if (df.isAfter(date, today)) {
        future.push(date);
    } else {
        past.push(date);
    }
    return { past, future, matchedToday };
}


function getSuggestionsFromPartial(
    day = false,
    matchedMonths = false,
    year = false,
    today = createToday(),
) {
    let matchedToday = false;

    const startYear = today.getFullYear();

    if (!day && matchedMonths) {
        /*
         * all formats that might have month, but not day current give
         * month in a numerical format so we will always have only 1
         */
        const [month] = matchedMonths;
        return nonAmbiguousMonth(month, today, year);
    }

    if (matchedMonths) {
        const futureMonths = [];
        const pastMonths = [];
        let badDates = 0;
        let matchedToday = false;
        const step = 'year';
        for (const monthIndex of matchedMonths) {
            try {
                const startDate = createDate(startYear, monthIndex, day);
                if (df.isSameDay(today, startDate)) {
                    matchedToday = true;
                }
                let past;
                let future;
                if (year) {
                    ({ past, future } = yearStepper({
                        year,
                        monthIndex,
                        today,
                        day,
                    }));
                } else {
                    past = stepper({
                        stepFunc: timeSteps[step],
                        direction: 'past',
                        startDate,
                        today,
                    });
                    future = stepper({
                        stepFunc: timeSteps[step],
                        startDate,
                        today,
                    });
                }
                pastMonths.push(past);
                futureMonths.push(future);
            } catch (err) {
                // If one date is invalid the user must not mean that month
                if (!(err instanceof BadDate)) {
                    throw err;
                } else {
                    badDates += 1;

                    /*
                     * If all dates are bad then this is an
                     * invalid date specification
                     */
                    if (badDates === matchedMonths.length) {
                        throw err;
                    }
                }
            }
        }
        const past = emitCompared(pastMonths, sortNearest(today));
        const future = emitCompared(futureMonths, sortNearest(today));
        return { past, future, matchedToday };
    } else {
        const monthIndex = today.getMonth();
        const startDate = createDate(startYear, monthIndex, day);
        if (df.isSameDay(today, startDate)) {
            matchedToday = true;
        }

        const past = stepper({
            stepFunc: timeSteps.month,
            direction: 'past',
            startDate,
            today,
        });
        const future = stepper({ stepFunc: timeSteps.month, startDate, today });
        return { past, future, matchedToday };
    }
}

function nonAmbiguousMonth(month, today) {
    const todayMonthIndex = today.getMonth();
    let matchedToday = false;
    let futureDate;
    let pastDate;
    if (todayMonthIndex === month) {
        matchedToday = true;
        futureDate = today;
        pastDate = today;
    } else {
        futureDate = setMonth(today, month, 'future');
        futureDate = df.startOfMonth(futureDate);

        pastDate = setMonth(today, month, 'past');
        pastDate = df.endOfMonth(pastDate);
        pastDate.setHours(0, 0, 0, 0);
    }
    const future = stepper({
        stepFunc: timeSteps.monthDays,
        direction: 'future',
        startDate: futureDate,
        today,
    });
    const past = stepper({
        stepFunc: timeSteps.monthDays,
        direction: 'past',
        startDate: pastDate,
        today,
    });

    return { future, past, matchedToday };
}

/*
 * Return a date Object from a string that fuzzily matches DD MMMM YYYY
 * If month or year are missing use today's month, or year
 * if month matches multiple months use the one occuring first in the year
 * returns undefined for ambiguous or invalid dates
 */
export function coerceDate(userInput, today = createToday()) {
    for (const { re, groups } of regexFormats) {
        const match = re.exec(userInput);
        if (match !== null) {
            const capturedGroups = getCapturedValues(groups, match);
            const {
                year = false,
                month = false,
                day = false,
            } = capturedGroups;
            if (year && month && day) {
                /*
                 * 202 becomes the year 202 without this check which is
                 * probably a valid date for our purposes
                 */
                if (year.length !== 4) {
                    return undefined;
                }
                let monthIndex;
                if (Number(month)) {
                    monthIndex = Number(month) - 1;
                } else {
                    const matchedMonths = matchMonths(month);
                    if (matchedMonths.length !== 1) {
                        return undefined;
                    }
                    monthIndex = $months.indexOf(matchedMonths[0]);
                }
                try {
                    return createDate(year, monthIndex, day);
                } catch (err) {
                    if (err instanceof BadDate) {
                        return undefined;
                    } else {
                        throw err;
                    }
                }
            }
        }
    }
}

function tomorrow({ today }) {
    return { future: [df.addDays(today, 1)] };
}

function yesterday({ today }) {
    return { past: [df.subDays(today, 1)] };
}

function today() {
    return { matchedToday: true };
}

function nextWeek({ today }) {
    return interweekStepper({ today, direction: 'future' });
}

function lastWeek({ today }) {
    return interweekStepper({ today, direction: 'past' });
}

/*
 * create tests, and exec objects out of an object of prefix, and executor
 * functions
 */
function createWordFormats(wordMatches) {
    const wordRegexps = Object.entries(wordMatches).map(([prefix, exec]) => {
        return { re: createStartwithRegex(prefix), exec };
    });
    const formats = wordRegexps.map(({ re, exec }) => {
        function test(userInput) {
            return re.test(userInput);
        }
        return {
            test,
            exec,
        };
    });
    return formats;
}

function checkEnumArg(argObj, validOptions) {
    const [name, value] = Object.entries(argObj)[0];
    if (!validOptions.includes(value)) {
        throw new Code(`If present, "${name}" must be one of `
            + `${validOptions.join(', ')}`);
    }
}

function sortNearest(today) {
    function cmpFunc(a, b) {
        const distanceA = Math.abs(df.differenceInCalendarDays(today, a));
        const distanceB = Math.abs(df.differenceInCalendarDays(today, b));
        if (distanceA < distanceB) {
            return -1;
        }
        if (distanceA > distanceB) {
            return 1;
        }
        return 0;
    }
    return cmpFunc;
}

function getCapturedValues(groups, result) {
    const returnVal = {};
    for (const [key, value] of Object.entries(groups)) {
        returnVal[key] = result[value];
    }
    return returnVal;
}

function createRegexFormats(formats) {
    return Object.values(formats).map(({ re, groups }) => {
        function exec({ userInput, today }) {
            const result = re.exec(userInput);
            const capturedGroups = getCapturedValues(groups, result);
            const {
                day = false,
                month = false,
                year = false,
            } = capturedGroups;
            let matchedMonths = false;
            if (month) {
                if (Number(month)) {
                    matchedMonths = [Number(month) - 1];
                } else {
                    matchedMonths = matchMonths(month);
                    matchedMonths = matchedMonths.map((month) =>
                        $months.indexOf(month));
                }

                if (year.length === 4 && matchedMonths.length === 1) {
                    return getSuggestionsFromComplete(
                        day,
                        matchedMonths[0],
                        year,
                        today,
                    );
                }
            }
            return getSuggestionsFromPartial(day, matchedMonths, year, today);
        }
        return {
            test: re.test.bind(re),
            exec,
        };
    });
}


function setMonth(date, month, direction) {
    const directions = {
        future: {
            test: df.isAfter,
            fix: df.addYears,
        },
        past: {
            test: df.isBefore,
            fix: df.subYears,
        },
    };
    let newDate = df.setMonth(date, month);
    const { test, fix } = directions[direction];
    if (!test(newDate, date) && !df.isSameDay(newDate, date)) {
        newDate = fix(newDate, 1);
    }
    return newDate;
}

function emptySuggestions({ today }) {
    const future = stepper({
        today,
        stepFunc: timeSteps.day,
        startDate: today,
        direction: 'future',
    });
    const past = stepper({
        today,
        stepFunc: timeSteps.day,
        startDate: today,
        direction: 'past',
    });
    return {
        matchedToday: true,
        future,
        past,
    };
}

