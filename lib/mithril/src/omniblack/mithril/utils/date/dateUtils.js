import * as df from 'date-fns';

import { multilineRegex } from '#src/utils/text.js';
import { Base } from '@omniblack/error';
import { TXT } from '@omniblack/localization';

const re = multilineRegex('u');

const strictISODate = re`^
    (?:[0-9]{4}) # year
    -
    (?:1[0-2]|0[1-9]) # month
    -
    (?:3[01]|0[1-9]|[12][0-9]) # day of the month
    T
    (?:2[0-3]|[01][0-9]) # Hour
    :
    (?:[0-5][0-9]) # Minute
    :
    (?:[0-5][0-9]) # Seconds
    (?:\.\d{1,3})? #  Fractional Seconds
    Z
$`;
export class BadDate extends Base {
    /*
     * Args:
     *     msg: String description of the problem
     *     retry: Bool if the operation can be retried without changes.
     *     notifyDev: Bool if we should notify development of the problem.
     */
    constructor(
        {
            year,
            month,
            day,
            date,
            msg = 'Bad date',
        } = {},
    ) {
        const details = {
            year,
            month,
            day,
            date,
        };
        super(msg, { details, retry: false, notifyDev: true });
    }
}


export function createToday() {
    const today = new Date();
    today.setHours(0, 0, 0, 0);
    return today;
}


/*
 * Return a new Date with the specified year, month, and day
 * Throw if the specified year, month, and day combination are not valid
 */
export function createDate(year, month, day) {
    const newDate = new Date(year, month, day);
    const valid = df.isValid(newDate);
    const sameYear = Number(year) === newDate.getFullYear();
    const sameMonth = Number(month) === newDate.getMonth();
    const sameDate = Number(day) === newDate.getDate();
    if (!(sameYear && sameMonth && sameDate) || !valid) {
        throw new BadDate({ year, month, day });
    }
    return newDate;
}


/*
 * add number of months to date
 * making sure the day of the month does not change
 */
export function addMonth(date, number) {
    const newDate = df.addMonths(date, number);
    if (df.getDate(date) === df.getDate(newDate)) {
        return newDate;
    } else {
        throw new BadDate({ date: newDate });
    }
}

/*
 * add number of years
 * making sure the day of the month, and month does not change
 */
export function addYear(date, number) {
    const newDate = df.addYears(date, number);
    const sameDayOfTheMonth = df.getDate(date) === df.getDate(newDate);
    const sameMonth = df.getMonth(date) === df.getMonth(newDate);
    if (sameDayOfTheMonth && sameMonth) {
        return newDate;
    } else {
        throw new BadDate({ date: newDate });
    }
}


export function setDay(date, newIndex, bias) {
    const dayIndex = df.getDay(date);
    const before = dayIndex > newIndex;
    const difference = dayIndex - newIndex;
    let newDate = date;

    if (bias === 'future') {
        if (before) {
            newDate = df.addWeeks(newDate, 1);
            newDate = df.setDay(newDate, newIndex);
        } else {
            newDate = df.setDay(newDate, newIndex);
        }
    } else if (bias === 'past') {
        if (before) {
            newDate = df.setDay(newDate, newIndex);
        } else {
            newDate = df.addWeeks(newDate, -1);
            newDate = df.setDay(newDate, newIndex);
        }
    } else if (bias === 'nearest') {
        /*
         * if the difference is greater that 4 reaching to a diffrent week will
         * be closer
         */
        if (difference > 4) {
            if (before) {
                newDate = df.addWeeks(newDate, -1);
                newDate = df.setDay(newDate, newIndex);
            } else {
                newDate = df.addWeeks(newDate, 1);
                newDate = df.setDay(newDate, newIndex);
            }
        } else {
            newDate = df.setDay(newDate, newIndex);
        }
    }
    return newDate;
}

/*
 * Format date object. Should be used for all places Beacon displays a date.
 * Args:
 *  date: Javascript Date object.
 *
 * Example return: 23 September 2019
 */
export function formatDate(date, format = "dd' 'MMMM' 'yyyy") {
    return df.format(date, format);
}

const timeFormat = new Intl.DateTimeFormat('default', {
    hour12: false,
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
});

/*
 * Returns a time string from a Date object
 *
 * Args:
 *  date: Javascript Date object
 *
 *  Example return: '15:27:12'
 */
export function getTimeFromDate(date) {
    return timeFormat.format(date);
}


export function isDate(string) {
    return strictISODate.test(string);
}

// TODO set up this to use date-fns locales to localize output
/*
 * If value is a date object or ISO string, return string formatted
 * appropriately else return empty string
 *
 * Should be used for all places Beacon displays a datetime.
 *
 * Params:
 *      value - string input to be converted into a date or time
 */
export function formatDateTime(value, { withTime = true, dateFormat } = {}) {
    let display = '';
    let dateObj = value;
    if (!(dateObj instanceof Date)) {
        if (isDate(value)) {
            dateObj = df.parseISO(value);
        }
    }

    if (dateObj instanceof Date && df.isValid(dateObj)) {
        const date = formatDate(dateObj, dateFormat);
        if (withTime) {
            const time = formatTime(dateObj).display;
            display = TXT`${date} at ${time}`;
        } else {
            return date;
        }
    }

    return display;
}


const TIME_FORMAT_WITH_SECONDS = "h:mm:ss' 'a";
const TIME_FORMAT_WITHOUT_SECONDS = "h:mm' 'a";

export const precisions = {
    SECONDS: Symbol('Time input precision seconds'),
};

const formats = {
    [precisions.SECONDS]: TIME_FORMAT_WITH_SECONDS,
    [false]: TIME_FORMAT_WITHOUT_SECONDS,
};
// Note: midnight = am, noon = pm

/*
 * Returns a formatted time string
 *
 * Args:
 *  date: a valid date
 *  precision: one of the above formats
 */
export function formatTime(date, precision = false) {
    const format = formats[precision];
    const timeString = df.format(date, format);
    const result = { display: timeString };

    const hour = date.getHours();
    const minutesOrSeconds = date.getMinutes()
        || (precision === precisions.SECONDS && date.getSeconds());

    if (hour === 0 && !minutesOrSeconds) {
        result.clarification = 'midnight';
    } else if (hour === 12 && !minutesOrSeconds) {
        result.clarification = 'noon';
    }

    return result;
}
