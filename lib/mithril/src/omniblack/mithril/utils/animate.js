import { readable } from 'svelte/store';
import { noChange } from 'lit';
import { AsyncDirective, directive, PartType } from 'lit/async-directive.js';

const preferReducedMotionQuery = window.matchMedia(
    '(prefers-reduced-motion: reduce)',
);

let preferReducedMotion = preferReducedMotionQuery.matches;
let currentAnimated = !preferReducedMotion;

let animatedSet;
const animatedStore = readable(currentAnimated, (set) => {
    animatedSet = set;
});

preferReducedMotionQuery.addEventListener('change', (event) => {
    preferReducedMotion = event.matches;

    currentAnimated = !preferReducedMotion;
    animatedSet(currentAnimated);
});

export class AnimatedController {
    constructor(host, attr = 'animated') {
        this.host = host;
        this.attr = attr;
        host.addController(this);
        this.unsub = null;
    }

    hostConnected() {
        this.unsub = animatedStore.subscribe((animated) => {
            const prevValue = this.host[this.attr];
            this.host[this.attr] = animated;
            this.host.requestUpdate(this.attr, prevValue);
        });
    }

    hostDisconnected() {
        this.unsub?.();
    }
}

const hasAnimated = Symbol('Does this class use the animated mixin?');

export function withAnimated(cls) {
    if (cls[hasAnimated]) {
        return cls;
    }

    class NewClass extends cls {
        #unsub = null;

        static properties = {
            animated: { type: Boolean, state: true },
        };

        connectedCallback() {
            super.connectedCallback();
            this.#unsub = animatedStore.subscribe((animated) => {
                this.animated = animated;
            });
        }

        disconnectedCallback() {
            super.disconnectedCallback();
            this.#unsub?.();
            this.#unsub = null;
        }
    }

    Object.defineProperty(NewClass, hasAnimated, {
        value: true,
        configurable: false,
        writable: false,
        enumerable: false,
    });

    return NewClass;
}

class AnimatedDirective extends AsyncDirective {
    constructor(partInfo) {
        super(partInfo);
        this.setMode(partInfo);
    }

    setMode(partInfo) {
        switch (partInfo.type) {
            case PartType.ELEMENT:
                this.mode = 'class';
                break;
            case PartType.BOOLEAN_ATTRIBUTE:
                this.mode = 'attribute';
                break;
            case PartType.CHILD:
                this.mode = 'conditional';
                break;
            default:
                throw new Error(
                    `'animated()' only support element, boolean attribute`
                    + 'and child positions.',
                );
        }
    }

    setupSub(props) {
        if (this.mode === 'class') {
            this.unsub = animatedStore.subscribe((value) => {
                this.el?.classList
                    .toggle('animated', value);
            });
        } else if (this.mode === 'attribute') {
            this.unsub = animatedStore.subscribe((value) => {
                this.setValue(value);
            });
        } else if (this.mode === 'conditional') {
            this.unsub = animatedStore.subscribe((value) => {
                const method = value
                    ? 'on'
                    : 'off';

                const methods = props?.[0];
                const newValue = methods?.[method]?.call(this.host);
                this.setValue(newValue);
            });
        }
    }

    disconnected() {
        this.unsub?.();
        this.unsub = null;
        this.el?.classList.remove('animated');
        this.el = null;
    }

    update(partInfo, props) {
        this.host = partInfo.options?.host;
        this.setMode(partInfo);
        this.el = partInfo.element;
        this.unsub?.();
        this.setupSub(props);
        return noChange;
    }
}

export const animated = directive(AnimatedDirective);

animated.subscribe = animatedStore.subscribe;

const watchedEls = new Set();
let elUnsub;
const elRegistry = new FinalizationRegistry((ref) => {
    watchedEls.delete(ref);
    if (watchedEls.size === 0) {
        console.log('Cleaned all animated els');
        elUnsub();
        elUnsub = undefined;
    }
});

function elSubscribe() {
    elUnsub = animatedStore.subscribe((newValue) => {
        for (const elRef of watchedEls) {
            elRef.deref()?.classList.toggle('animated', newValue);
        }
    });
}

export function animatedEl(el) {
    const elRef = new WeakRef(el);
    elRegistry.register(el, elRef);
    watchedEls.add(elRef);
    el.classList.toggle('animated', currentAnimated);
    if (!elUnsub) {
        elSubscribe();
    }
}
