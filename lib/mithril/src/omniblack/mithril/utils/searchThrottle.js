import { ConnectableStore } from '@omniblack/stores';

/*
 * Constructor for an unresolved promise holder
 */
function Deferred() {
    this.resolve = null;
    this.reject = null;

    /* eslint-disable-next-line promise/avoid-new */
    this.promise = new Promise((resolve, reject) => {
        this.resolve = resolve;
        this.reject = reject;
    });

    Object.freeze(this);
}


/*
 * Return a function that throttles (via promises) calls to a function.
 *
 * The number of outstanding calls is controllable with the `maxWaiting`
 * option, but keep in mind that the browser usually limit the outstanding
 * requests to a given domain (6 as of Jun 2019), so you likely don't want
 * to chew up all the connections for this one task...
 *
 * If additional calls are made before the previous call finishes, those later
 * calls are delayed and consolidated until the previous call finishes.
 *
 * In addition, the returned function has a `isWaiting` method that returns
 * true when there are still outstanding calls pending.
 */
export function searchThrottle(fn, { maxWaiting = 2 } = {}) {
    // count of active calls
    let active = 0;

    // the waiting call (deferred since one was already active)
    let deferred = null;
    let deferredArgs = null;

    // call can finish out of order, so always return the best known result
    let numCalls = 0;
    let greatestDoneCallNum = 0;
    let greatestDoneResult = null;

    // a svelte store for reactive loading props in svelte
    const {
        subscribe: waitingSubscribe,
        set: waitingSet,
    } = new ConnectableStore(false, 'searchThrottleWaiting');

    async function makeCall(...args) {
        // track the calls so we know what order they happen in
        numCalls += 1;
        const currCallNum = numCalls;

        try {
            const rslt = await fn.call(...args);

            // only keep the result from a more recent call
            if (currCallNum > greatestDoneCallNum) {
                greatestDoneResult = rslt;
                greatestDoneCallNum = currCallNum;
            }

            return greatestDoneResult;
        } finally {
            checkDeferred();
        }
    }

    function checkDeferred() {
        if (deferred) {
            // waiting request, launch it
            deferred.resolve(makeCall(...deferredArgs));

            // clear the old one so that future searches use their own
            deferred = null;
            deferredArgs = null;
        } else {
            active -= 1;
            waitingSet(throttle.isWaiting());
        }
    }

    function throttle(...args) {
        // If there are enough active calls, we need to wait for one to finish
        if (active >= maxWaiting) {
            if (!deferred) {
                // need a new placeholder
                deferred = new Deferred();
            }

            // just save the last set of args and context

            // eslint-disable-next-line no-invalid-this
            deferredArgs = [this, ...args];

            return deferred.promise;
        }

        // room left to make calls, so start one
        active += 1;
        waitingSet(throttle.isWaiting());

        // eslint-disable-next-line no-invalid-this
        return makeCall(this, ...args);
    }

    throttle.isWaiting = () => active > 0;
    throttle.waiting = { subscribe: waitingSubscribe };

    return throttle;
}
