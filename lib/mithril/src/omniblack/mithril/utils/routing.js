import { DerivedStore } from '@omniblack/stores';
import { getRouter } from '#src/components/router.js';


function mergeParams(newParams, currentParams) {
    if (!newParams) {
        return currentParams;
    }

    const finalParams = { ...currentParams };

    for (const [key, value] of Object.entries(newParams)) {
        if (value === undefined && value === null) {
            delete finalParams[key];
        } else {
            finalParams[key] = value;
        }
    }
}


function onChange($router, $newParams) {
    const finalParams = mergeParams($newParams, $router.url.query);

    const url = $router.createURL({
        query: finalParams,
    });

    return url;
}

export function changeParams(newParams) {
    const router = getRouter();
    return new DerivedStore('changeParamsStore', router, newParams, onChange);
}

