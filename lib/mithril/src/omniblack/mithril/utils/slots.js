import { AsyncDirective, PartType, directive } from 'lit/async-directive.js';
import { noChange } from 'lit';

export class MonitorSlot extends AsyncDirective {
    constructor(partInfo) {
        super(partInfo);
        this.listener = this.listener.bind(this);

        if (partInfo.type !== PartType.ELEMENT) {
            throw new Error(
                '`animated()` can only be used as an element directive.',
            );
        }
        this.el = partInfo.element;
        setHasContent(this.el);
        this.el.addEventListener('slotchange', this.listener);
    }

    update(partInfo) {
        this.el.removeEventListener('slotchange', this.listener);

        this.el = partInfo.element;

        setHasContent(this.el);
        this.el.addEventListener('slotchange', this.listener);

        return noChange;
    }

    disconnected() {
        this.el.removeEventListener('slotchange', this.listener);
    }

    reconnected() {
        this.el.addEventListener('slotchange', this.listener);
        setHasContent(this.el);
    }

    listener() {
        setHasContent(this.el);
    }
}

export const monitorSlot = directive(MonitorSlot);

function setHasContent(slotEl) {
    const allNodes = slotEl.assignedNodes();
    const hasNodes = allNodes.length > 0;

    if (hasNodes) {
        slotEl.setAttribute('has-content', true);
    } else {
        slotEl.removeAttribute('has-content');
    }
}
