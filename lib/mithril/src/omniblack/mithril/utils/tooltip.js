import { pick, omit } from 'lodash-es';
import {
    computePosition,
    autoUpdate as _autoUpdate,
    offset,
    shift,
    flip,
} from '@floating-ui/dom';
import { derived, writable } from 'svelte/store';
import { createHoverManger, createFocusManger } from '#src/utils/events.js';
import {
    watchStores,
    DerivedStore,
    AsyncDerivedStore,
} from '@omniblack/stores';


function roundByDPR(value) {
    const dpr = window.devicePixelRatio || 1;
    return Math.round(value * dpr) / dpr;
}


function autoUpdate(node, tip, updateTip) {
    if (!node || !tip) {
        return () => {};
    }

    return _autoUpdate(node, tip, updateTip);
}

function addMiddleware(opts) {
    return Object.assign({}, opts, {
        middleware: [
            offset(10),
            flip(),
            shift(),
        ],
    });
}

function createOpenStore(
    focused,
    hovered,
    args,
) {
    const store = new DerivedStore(
        'tooltip open',
        hovered,
        focused,
        args,
        ($hovered, $focused, $args) => {
            if ($args?.open !== undefined) {
                return $args.open;
            }

            const openOnHover
                = $args?.openOnHover
                || $args?.openOnHover === undefined;
            if (openOnHover && $hovered) {
                return true;
            }

            const openOnFocus
                = $args?.openOnFocus
                || $args?.openOnFocus === undefined;
            if (openOnFocus && $focused) {
                return true;
            }

            return false;
        },
    );

    return store;
}


const ourArgsNames = ['tip', 'open', 'openOnHover', 'openOnFocus'];

export function tooltip(node, args) {
    const ourArgs = pick(args, ourArgsNames);
    const initFloatingArgs = omit(args, ourArgsNames);

    const hovered = createHoverManger();
    const focused = createFocusManger();
    const nodeStore = writable(node);
    const ourArgsStore = writable(ourArgs);
    const rawFloatingArgsStore = writable(initFloatingArgs);
    const floatingArgs = derived(rawFloatingArgsStore, ($args) => {
        return addMiddleware($args);
    });

    const posStore = new AsyncDerivedStore(
        'tooltip position',
        ourArgsStore,
        floatingArgs,
        async ($ourArgs, $args) => {
            const $tip = $ourArgs.tip;

            if (!$tip) {
                return {};
            }

            return await computePosition(node, $tip, $args);
        },
    );

    const openStore = createOpenStore(focused, hovered, ourArgsStore);

    const unsubTip = watchStores(nodeStore, ourArgsStore, ($node, $args) => {
        hovered.setElements($node, $args.tip);
        focused.setElements($node, $args.tip);
    });

    const unsub = watchStores(
        posStore,
        ourArgsStore,
        openStore,
        ($pos, $args, $open) => {
            if ($args?.tip && $pos) {
                const { x, y } = $pos;

                if (x === undefined || y === undefined) {
                    return;
                }

                $args.tip.style.display = $open
                    ? 'block'
                    : 'none';

                $args.tip.style.position = 'absolute';
                $args.tip.style.left = `${roundByDPR(x)}px`;
                $args.tip.style.top = `${roundByDPR(y)}px`;
            }
        },
    );


    const tip = ourArgs.tip;
    let cleanup = autoUpdate(node, tip, posStore.recalculate);

    return {
        update(args) {
            const ourArgs = pick(args, ourArgsNames);
            ourArgsStore.set(ourArgs);

            const tip = ourArgs.tip;

            const floatingArgs = omit(args, ourArgsNames);
            rawFloatingArgsStore.set(floatingArgs);


            cleanup();
            cleanup = autoUpdate(node, tip, posStore.recalculate);
        },
        destroy() {
            unsub();
            unsubTip();
            cleanup();
        },
    };
}
