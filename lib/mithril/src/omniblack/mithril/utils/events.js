import { onMount, onDestroy, createEventDispatcher } from 'svelte';
import autoBind from 'auto-bind';
import { isNil } from 'lodash-es';

import { SyncableStore } from '@omniblack/stores';

export function createOutsideListener(cb) {
    let elements;

    function setElements(...newElements) {
        elements = newElements;
    }

    function listener(event) {
        const inside = elements.some((element) => {
            if (!isNil(element)) {
                return element.contains(event.target);
            }

            return false;
        });

        if (!inside) {
            cb(event);
        }
    }

    onMount(() => {
        document.addEventListener('click', listener);
    });

    onDestroy(() => {
        document.removeEventListener('click', listener);
    });

    return { setElements };
}

export function emitFocusEvents(manager) {
    let currentlyFocused = false;
    const dispatch = createEventDispatcher();

    const unsub = manager.subscribe((newValue) => {
        if (newValue !== currentlyFocused) {
            currentlyFocused = newValue;

            if (newValue) {
                dispatch('focus');
            } else {
                dispatch('blur');
            }
        }
    });

    onDestroy(unsub);
}

export function createFocusManger(...elements) {
    const store = new SyncableStore(false, 'focus_manager');
    store.debugEnabled = false;
    const clickOutside = createOutsideListener(() => {
        store.set(false);
    });

    store.setElements = setElements;
    setElements(...elements);

    onMount(() => {
        document.addEventListener('keyup', keyUp);
    });

    onDestroy(() => {
        document.removeEventListener('keyup', keyUp);
        removeListeners();
    });

    return store;

    function setElements(...newElements) {
        clickOutside.setElements(...newElements);
        removeListeners();

        elements = newElements;

        for (const element of elements) {
            if (!isNil(element)) {
                element.addEventListener('focusin', focusIn);
                element.addEventListener('focusout', focusOut);
            }
        }
    }


    function focusIn() {
        store.set(true);
    }

    function focusOut() {
        store.set(
            elements.some(
                (element) => element?.matches(':focus-within, :focus'),
            ),
        );
    }

    function keyUp(event) {
        if (event.key === 'Escape') {
            store.set(false);
        }
    }

    function removeListeners() {
        for (const element of elements) {
            if (!isNil(element)) {
                element.removeEventListener('focusin', focusIn);
                element.removeEventListener('focusout', focusOut);
            }
        }
    }
}

class HoverWatcher {
    constructor(element, change) {
        autoBind(this);
        this.element = element;
        this.change = change;
        this.hovered = element.matches(':hover');

        this.element.addEventListener('mouseenter', this.enter);
        this.element.addEventListener('mouseleave', this.leave);
    }

    enter() {
        this.hovered = true;
        this.change();
    }

    leave() {
        this.hovered = false;
        this.change();
    }

    destroy() {
        this.element.removeEventListener('mouseenter', this.enter);
        this.element.removeEventListener('mouseleave', this.leave);
    }
}


const QUARTER_SECOND = 250;
export function createHoverManger() {
    let watchers = [];
    let value = false;

    const store = new SyncableStore(false, 'hover_manager');
    store.debugEnabled = false;
    let leaveTimeout;

    function change() {
        clearTimer();
        const newValue = watchers.some(({ hovered }) => hovered);

        if (!newValue && value) {
            value = false;
            leaveTimeout = setTimeout(() => store.set(false), QUARTER_SECOND);
        } else if (newValue !== value) {
            value = newValue;
            store.set(newValue);
        }
    }


    function clearTimer() {
        clearTimeout(leaveTimeout);
        leaveTimeout = undefined;
    }

    function setElements(...newElements) {
        for (const watcher of watchers) {
            watcher.destroy();
        }

        watchers = newElements
            .filter((element) => !isNil(element))
            .map((element) => new HoverWatcher(element, change));
    }

    store.setElements = setElements;

    function suppress() {
        clearTimeout();
        value = false;
        store.set(false);
        leaveTimeout = setTimeout(() => change(), QUARTER_SECOND * 2);
    }

    store.suppress = suppress;


    return store;
}


export function keyDispatch(userKeys) {
    const keys = new Map(userKeys.flatMap((keyDef) => {
        const triggerKeys = keyDef.keys ?? [keyDef.key];
        return triggerKeys.map((key) => [key, keyDef]);
    }));

    return function eventHandler(event) {
        if (keys.has(event.key)) {
            const {
                preventDefault,
                stopPropagation,
                stopImmediatePropagation,
                ctrl,
                alt,
                shift,
                meta,
                func,
            } = keys.get(event.key);

            const ctrlNotMatched = ctrl !== undefined && event.ctrlKey !== ctrl;
            const altNotMatched = alt !== undefined && event.altKey !== alt;
            const shiftNotMatched = shift !== undefined
                && event.shiftKey !== shift;
            const metaNotMatched = meta !== undefined && event.metaKey !== meta;

            const anyNotMatched = ctrlNotMatched
                || altNotMatched
                || shiftNotMatched
                || metaNotMatched;

            if (anyNotMatched) {
                return;
            }

            if (preventDefault) {
                event.preventDefault();
            }

            if (stopPropagation) {
                event.stopPropagation();
            }


            if (stopImmediatePropagation) {
                event.stopImmediatePropagation();
            }

            return func(event);
        }
    };
}

const SPACE = '\u0020';
const triggerKeys = new Set([SPACE, 'Enter']);

export function activate(element, cb) {
    function keydown(event) {
        if (!element.disabled) {
            const hasKey = triggerKeys.has(event.key);
            const openInNewContext = event.ctrlKey;
            if (hasKey && !openInNewContext) {
                event.preventDefault();
                cb(event);
            }
        }
    }

    function click(event) {
        if (!element.disabled) {
            const openInNewContext
                = event.type === 'auxClick'
                || event.ctrlKey
                || event.shiftKey;
            if (!openInNewContext) {
                event.preventDefault();
                cb(event);
            }
        }
    }

    element.addEventListener('click', click);
    element.addEventListener('keydown', keydown);
}
