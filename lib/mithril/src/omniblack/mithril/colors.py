from argparse import ArgumentParser, FileType
from dataclasses import dataclass
from enum import Flag, auto
from typing import NamedTuple

from ruamel.yaml import YAML

from .css import Sheet


@dataclass
class ColorComponent:
    value: int | float | str
    percent: bool = False

    def __str__(self):
        str_value = str(self.value)
        if not isinstance(self.value, str) and self.percent:
            str_value += '%'

        return str_value


comp_names = {
    'h': 'hue',
    's': 'saturation',
    'l': 'lightness',
}


class ColorVariant(Flag):
    normal = 0
    emphasized = auto()
    de_emphasized = auto()
    inverted = auto()
    inverted_emphasized = emphasized | inverted
    inverted_de_emphasized = de_emphasized | inverted


class Color(NamedTuple):
    name: str
    hue: ColorComponent
    saturation: ColorComponent
    lightness: ColorComponent
    emphasize_name: str | None = None
    de_emphasize_name: str | None = None
    variant: ColorVariant = ColorVariant.normal

    @property
    def inverted(self):
        cls = self.__class__
        inverted_hue = (self.hue.value + 180) % 360

        inverted_saturation = min((self.saturation.value + 25), 100)
        saturation_component = ColorComponent(
            inverted_saturation,
            percent=True,
        )

        hue_component = ColorComponent(inverted_hue)

        return cls(
            name=f'{self.name}-inverted',
            hue=hue_component,
            saturation=saturation_component,
            lightness=self.lightness,
            variant=self.variant | ColorVariant.inverted,
        )

    @property
    def emphasized(self):
        cls = self.__class__
        emphasized_lightness = ColorComponent(
            value=min(self.lightness.value + 15, 100),
            percent=True,
        )

        return cls(
            name=self.emphasize_var(),
            hue=self.hue,
            saturation=self.saturation,
            lightness=emphasized_lightness,
            variant=self.variant | ColorVariant.emphasized,
        )

    @property
    def de_emphasized(self):
        cls = self.__class__
        de_emphasized_lightness = ColorComponent(
            value=max(self.lightness.value - 15, 0),
            percent=True,
        )

        return cls(
            name=self.de_emphasize_var(),
            hue=self.hue,
            saturation=self.saturation,
            lightness=de_emphasized_lightness,
            variant=self.variant | ColorVariant.de_emphasized,
        )

    def de_emphasize_var(self, component=None):
        var_str = f'{self.name}-de-emphasize'
        if component is not None:
            var_str += f'-{component}'

        return var_str

    def emphasize_var(self, component=None):
        var_str = f'{self.name}-emphasize'
        if component is not None:
            var_str += f'-{component}'

        return var_str

    @property
    def value(self):
        return f'hsl({self.hue} {self.saturation} {self.lightness})'


def variable_ref(component_str):
    return f'var(--{component_str})'


def convert_float(component_value: float | int):
    if isinstance(component_value, str):
        msg = 'Value is not a number'
        raise ValueError(msg)

    return component_value


converter_funcs = (convert_float, variable_ref)


def convert_color_component(value):
    for converter in converter_funcs:
        try:
            return converter(value)
        except ValueError:
            pass

    return None


def parse_color_value(color_values):
    hue = convert_color_component(color_values['hue'])
    saturation = convert_color_component(color_values['saturation'])
    lightness = convert_color_component(color_values['lightness'])

    return (
        ColorComponent(hue),
        ColorComponent(saturation, percent=True),
        ColorComponent(lightness, percent=True),
        color_values.get('emphasize_name', None),
        color_values.get('de_emphasize_name', None),
    )


def create_parser():
    parser = ArgumentParser()
    parser.add_argument('--in', type=FileType('r'), dest='in_file')
    parser.add_argument('--out', type=FileType('w'), dest='out_file')
    return parser


def main():
    yaml = YAML()

    parser = create_parser()

    args = parser.parse_args()

    in_file = args.in_file
    out_file = args.out_file

    with in_file as stream:
        colors = yaml.load(stream)

    with Sheet(out_file) as sheet, sheet.rule('html'):
        for color_name, color_value in colors.items():
            sheet.write()

            color_value = parse_color_value(color_value)
            color = Color(color_name, *color_value)

            sheet.theme_color(color)

            inverted_color = color.inverted
            if color_name in {'text', 'background'}:
                invert_name = 'background' if color_name == 'text' else 'text'

                sheet.custom_property(
                    inverted_color.name,
                    variable_ref(invert_name),
                )
            else:
                sheet.theme_color(inverted_color)

            sheet.theme_color(color.de_emphasized)

            if color.de_emphasize_name:
                sheet.custom_property(
                    f'{color.name}-{color.de_emphasize_name}',
                    variable_ref(color.de_emphasize_var()),
                )

            sheet.theme_color(color.emphasized)


if __name__ == '__main__':
    main()
