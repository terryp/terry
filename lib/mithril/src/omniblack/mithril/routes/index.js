import home from './home.svelte';
import test from './test.svelte';
import test2 from './test2.svelte';

export default [
    {
        component: home,
        path: '/',
        display: {
            en: 'Home Page',
            fr: 'French Home Page',
        },
    },
    {
        component: test,
        path: '/test',
        display: {
            en: 'Test',
        },
    },
    {
        component: test2,
        path: '/test2',
        display: {
            en: 'Test 2',
        },
    },
];

