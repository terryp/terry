import { Input } from './Input.js';

export class Password extends Input {
    static element_name = 'mithril-password';
    static formAssociated = true;
    static properties = {
        hide_password: { type: Boolean, reflect: true },
    };

    static validationProps = [
        'value',
        'required',
    ];


    constructor() {
        super();
        this.type = 'password';
        this.hide_password = true;
    }
}
