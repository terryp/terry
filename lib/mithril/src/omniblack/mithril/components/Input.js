import { monitorSlot } from '#src/utils/slots.js';
import { LitElement, html, css, nothing } from 'lit';
import { createRef, ref } from 'lit/directives/ref.js';
import { animated } from '#src/utils/animate.js';
import { classes as applyClasses } from '#src/utils/style.js';

import { MithrilElement } from '#src/base.js';


export class Input extends MithrilElement {
    static element_name = 'mithril-input';

    static formAssociated = true;
    static shadowRootOptions = {
        ...LitElement.shadowRootOptions,
        delegatesFocus: true,
        mode: 'open',
    };

    static properties = {
        color: { type: String, reflect: true },
        disabled: { type: Boolean, reflect: true },
        name: { type: String, reflect: true },
        required: { type: Boolean, reflect: true },
        type: { type: String, reflect: true },
        value: {},
    };

    static validationProps = [
        'value',
        'required',
    ];

    static styles = css`
        * {
            box-sizing: border-box;
        }


        .input, .input-wrapper {
            color: var(--omniblack-input-fg);
            background-color: var(--omniblack-input-bg);
        }

        .input {
            border: none;
            anchor-name: --anchor-input;
        }

        .input-wrapper.enabled:has( :focus-visible) {
            outline: var(--omniblack-focus-ring);
            outline-offset: var(--omniblack-focus-ring-offset);
        }

        .input:focus {
            outline: none;
        }

        .input-wrapper {
            border: var(--omniblack-border);
            border-color: var(--input-feedback);
            border-radius: 0.5em;
            margin: var(--omniblack-border-margin);

            align-items: center;
            display: flex;
            flex-direction: row;
            gap: 0.5em;
            grid-area: input;
            justify-content: space-between;

            width: max-content;
            padding: 0 0.2em;

            font: inherit;
            letter-spacing: inherit;
            word-spacing: inherit;
        }

        .input-wrapper.animated {
            transition-property:
                border-style,
                border-width,
                margin,
                border-color;
            transition-duration: 0.3s;
            transition-timing-function: ease-in-out;
        }

        .input-wrapper.invalid {
            border: var(--omniblack-attention-border);
            border-color: var(--input-feedback);
            margin: var(--omniblack-attention-border-margin);
        }

        ::slotted(*) {
            color: var(--omniblack-input-fg);
        }

        .suffix ::slotted(*) {
            color: var(--input-feedback);
        }

        .label {
            grid-area: label;
        }

        .label:has( :not(.label-slot[has-content])) {
            display: none;
        }

        .slot:not([has-content]) {
            display: none;
        }

        :host {
            display: inline-block;
            width: max-content;
            max-width: max-content;
        }

        @supports (position-anchor: --anchor-name) {
            .input-wrapper.invalid:focus-within .validationMessages {
                /* show when invalid */
                display: block;
            }

            .validationMessages {
                /* Hidden by default */
                display: none;
                border: var(--omniblack-border);
                border-color: var(--input-feedback);
                margin: 0.5em;
                box-shadow: var(--shadow-1);

                text-align: center;
                color: var(--omniblack-text-fg);
                background-color: var(--omniblack-text-bg);

                height: max-content;
                justify-self: anchor-center;
                position-anchor: --anchor-input;
                position: absolute;
                top: anchor(bottom);
                right: anchor(right);
                width: anchor-size(width);
            }

            .validationMessages.animated {
                transition-property: box-shadow;
                transition-duration: 0.3s;
                transition-timing-function: ease-in-out;
            }

            .validationMessages:hover {
                box-shadow: var(--shadow-5);
            }
        }

        :host {
            display: grid;
            gap: 0.5em;
            grid-template-areas:
                "label ."
                "input input";
        }
    `;

    validationMessagesEl = createRef();
    input = createRef();
    wrapper = createRef();

    get prefixContent() {
        return nothing;
    }

    get suffixContent() {
        return nothing;
    }

    constructor() {
        super();
        this.internals = this.attachInternals();
        this.disabled = false;
        this.type = 'text';
    }

    _validate() {
        const flags = {};
        const messages = [];

        if (this.required && !this.value) {
            flags.valueMissing = true;
            messages.push('Please fill out this field.');
        }

        const msg = messages.join('\n');
        this.setValidity(flags, msg);
    }

    onKeyUp(e) {
        if (!this.internals.validity.valid) {
            return;
        }

        const form = this.internals.form;
        if (e.key === 'Enter' && form.reportValidity()) {
            form.requestSubmit();
        }
    }

    render() {
        const classes = {
            disabled: this.disabled,
            enabled: !this.disabled,
            invalid: !this.validity.valid,
        };

        return html`
            <label for='input' class='label'>
                <slot
                    name='label'
                    id='label-slot'
                    class='label-slot'
                    ${monitorSlot()}
                >
                </slot>
            </label>

            <div
                class="input-wrapper"
                id="input-wrapper"
                type=${this.type}
                ${applyClasses(classes)}
                ${animated()}
                ${ref(this.wrapper)}
            >
                <div class='prefix'>
                    <slot
                        name="prefix"
                        id="prefix"
                        class="slot"
                        ${monitorSlot()}
                    >
                    </slot>
                </div>
                <input
                    class="input"
                    id='input'
                    type=${this.type}
                    @input=${this.onInput}
                    @keyup=${this.onKeyUp}
                    ${ref(this.input)}
                >
                <div class='suffix'>
                    ${this.suffixContent}
                    <slot
                        name="suffix"
                        class="slot"
                        id="suffix"
                        ${monitorSlot()}
                    >
                    </slot>
                </div>
                <div
                    ${animated()}
                    ${ref(this.validationMessagesEl)}
                    class="validationMessages"
                    popover="manual"
                >
                    ${this.internals.validationMessage}
                </div>
            </div>
        `;
    }

    firstUpdated() {
        this._validate();
    }

    willUpdate(changed) {
        if (changed.has('color')) {
            this.changeColor();
        }

        const revalidate = this.constructor.validationProps
            .some((prop) => changed.has(prop));

        if (revalidate) {
            this._validate();
        }
    }

    get _messages() {
        return this.validity?.validationMessage?.split('\n') ?? [];
    }

    setValidity(flags, message, anchor = undefined) {
        this.internals.setValidity(
            flags,
            message,
            anchor ?? this.input?.value,
        );

        this.changeColor();

        if (!CSS.supports('position-anchor', '--anchor-input')) {
            this.reportValidity();
        }
    }

    changeColor() {
        if (this.color) {
            this.wrapper?.style?.setProperty('--input-feedback', this.color);
        }

        const validityConcept = this.validity.valid
            ? 'success'
            : 'danger';

        this.wrapper?.value?.style?.setProperty(
            '--input-feedback',
            `var(--omniblack-input-feedback-${validityConcept})`,
        );
    }

    onInput(e) {
        this.setValue(e.target.value, true);
    }

    get form() {
        return this.internals.form;
    }

    get validity() {
        return this.internals.validity;
    }

    get validationMessage() {
        return this.internals.validationMessage;
    }

    get willValidate() {
        return this.internals.willValidate;
    }

    checkValidity() {
        return this.internals.checkValidity();
    }

    reportValidity() {
        return this.internals.reportValidity();
    }

    setValue(newValue, byEvent = false) {
        const oldValue = this.__value;

        if (typeof newValue !== 'string') {
            newValue = String(newValue);
        }

        if (newValue === this.__value) {
            return;
        }

        this.__value = newValue;

        if (this.name) {
            this.internals.setFormValue(newValue);
        }

        if (!byEvent) {
            this.input.value = newValue;
        }

        this.requestUpdate('value', oldValue);
    }

    get value() {
        return this.__value;
    }

    set value(newValue) {
        this.setValue(newValue, false);
    }
}
