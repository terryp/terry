import { LitElement, html, css, nothing } from 'lit';
import { classMap } from 'lit/directives/class-map.js';

import { MithrilElement } from '#src/base.js';
import { animated } from '#src/utils/animate.js';


export class Link extends MithrilElement {
    static element_name = 'mithril-link';

    static shadowRootOptions = {
        ...LitElement.shadowRootOptions,
        delegatesFocus: true,
    };

    static properties = {
        to: { reflect: true },
        block: { reflect: true, type: Boolean },
        disabled: { reflect: true, type: Boolean },
    };

    willUpdate() {
        this.enabled = !this.disabled && Boolean(this.to);
    }

    static styles = css`
        .link {
            display: inline-block;
            height: 100%;
            width: 100%;
            font-size: 1em;
        }

        .link.button {
            appearance: none;
            background: none;
            border: none;
            text-decoration: underline;
            cursor: pointer;
        }

        .link.enabled {
            color: var(--omniblack-link-fg);
            transition: transform ease 0.2s;
        }

        .link:hover.enabled.animated {
            transform: translateY(-0.1em);
        }

        .link:visited {
            color: var(--omniblack-link-visited-fg);
        }

        .link.disabled {
            color: inherit;
            text-decoration: inherit;
            cursor: inherit;
        }

        .link.block {
            display: block;
        }

        .link:focus-visible {
            outline: var(--omniblack-focus-ring);
            outline-offset: var(--omniblack-focus-ring-padding);
        }
    `;

    render() {
        const classes = {
            enabled: this.enabled,
            disabled: !this.enabled,
        };

        return html`
            <a
                class="link ${classMap(classes)}"
                href="${this.to ?? nothing}"
                ${animated()}
            >
                <slot class="text">
                </slot>
            </a>
        `;
    }
}
