import { html, css } from 'lit';
import { animated, withAnimated } from '#src/utils/animate.js';

import { Icon } from '#src/components/Icon.js';
import { MithrilElement } from '#src/base.js';

const circumference = 100;
const radius = circumference / (Math.PI * 2);
const diameter = radius * 2;
const size = diameter + 10;
const coord = size / 2;

export class Spinner extends withAnimated(MithrilElement) {
    static element_name = 'mithril-spinner';
    static needed_elements = [Icon];

    static properties = {
        max: { type: Number, reflect: true },
        value: { type: Number, reflect: true },
        color: { type: String, reflect: true },

        track_width: { type: Number, reflect: true },
        bar_width: { type: Number, reflect: true },

        percentage: { type: Number, state: true },
    };

    static styles = css`
        .bar {
            transform: rotate(-90deg) scaleY(-1);
            transform-origin: center;
            transition: 0.5s stroke-dasharray ease-in-out;
        }

        :host(:state(intermediate)) .bar {
            animation-name: intermediate-progress;
            animation-duration: 5s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes intermediate-progress {
            from {
                transform: rotate(-90deg);
            }

            to {
                transform: rotate(-450deg);
            }
        }
    `;

    constructor() {
        super();
        this.internals = this.attachInternals();
        this.internals.states.add('intermediate');

        this.max = 100;
        this.internals.ariaValueMin = 0;
        this.internals.role = 'progressbar';
        this.track_width = 2;
        this.bar_width = 1;
        this.color = 'success';
    }

    render() {
        return html`
            ${animated({
                on: this.renderAnimated,
                off: this.renderStatic,
            })}
        `;
    }

    renderAnimated() {
        return html`
            <svg
                viewBox="0 0 ${size} ${size}"
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
            >
                <!--
                    TODO: This is wrong track should be using a bg color I think.
                    I think we need a new usage to represent the fg and bg
                    of non text elements.
                -->
                <circle
                    r="${radius}"
                    pathLength="${circumference}"
                    cx="${coord}"
                    cy="${coord}"
                    fill="transparent"
                    stroke="var(--omniblack-${this.color}-fg)"
                    stroke-width="${this.track_width}"
                    class='track'
                >
                </circle>
                <circle
                    r="${radius}"
                    pathLength="${circumference}"
                    cx="${coord}"
                    cy="${coord}"
                    stroke="var(--omniblack-${this.color}-bg"
                    stroke-width="${this.bar_width}"
                    stroke-dasharray="${this.percentage},100"
                    fill="transparent"
                    class='bar'
                >
                </circle>
            </svg>
        `;
    }

    renderStatic() {
        return html`
            <mithril-icon
                src='/mithril/phosphor/regular/hourglass.svg'
            >
            </mithril-icon>
        `;
    }

    willUpdate(changed) {
        if (changed.has('value') || changed.has('max')) {
            if (this.value === undefined) {
                this.percentage = 50;
            } else {
                this.percentage = (this.value / this.max) * 100;
            }
        }

        if (changed.has('max')) {
            this.internals.ariaValueMax = this.max;
        }

        if (changed.has('value')) {
            this.internals.ariaValueNow = this.value;

            if (this.value === undefined) {
                this.internals.states.add('intermediate');
            } else {
                this.internals.states.delete('intermediate');
            }
        }
    }
}
