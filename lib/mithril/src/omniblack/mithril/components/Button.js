import { LitElement, html, css } from 'lit';
import { styles as applyStyles } from '#src/utils/style.js';

import { animated } from '#src/utils/animate.js';
import { MithrilElement } from '#src/base.js';

export class Button extends MithrilElement {
    static element_name = 'mithril-button';

    static formAssociated = true;
    static shadowRootOptions = {
        ...LitElement.shadowRootOptions,
        delegatesFocus: true,
        mode: 'open',
    };

    static properties = {
        color: { type: String, reflect: true },
        disabled: { type: Boolean, reflect: true },
        type: { type: String, reflect: true },
    };

    static styles = css`
        .button {
            color: var(--button-text-color);
            background-color: var(--button-color);
            border: var(--omniblack-border);
            border-radius: var(--omniblack-border-radius);
            box-shadow: var(--shadow-4);
        }

        .button.animated {
            transition-property: box-shadow;
            transition-duration: 0.3s;
            transition-timing-function: ease-in-out;
        }

        .button.animated:hover {
            box-shadow: var(--shadow-2);
        }

        .button:focus-visible {
            outline: var(--omniblack-focus-ring);
            outline-offset: var(--omniblack-focus-ring-offset);
        }
    `;

    constructor() {
        super();
        // defaults
        this.color = 'text';
        this.disabled = false;
        this.type = 'button';

        this.internals = this.attachInternals();
    }

    onClick() {
        const form = this.internals.form;
        if (this.type === 'submit' && form) {
            form.requestSubmit();
        }
    }

    willUpdate(changed) {
        if (changed.has('disabled')) {
            if (this.disabled) {
                this.internals.states.add('disabled');
                this.internals.states.delete('enabled');
            } else {
                this.internals.states.delete('disabled');
                this.internals.states.add('enabled');
            }
        }
    }

    firstUpdated() {
        if (this.disabled) {
            this.internals.states.add('disabled');
            this.internals.states.delete('enabled');
        } else {
            this.internals.states.delete('disabled');
            this.internals.states.add('enabled');
        }
    }

    render() {
        const styles = {
            '--button-color': `var(--omniblack-${this.color}-bg)`,
            '--button-text-color': `var(--omniblack-${this.color}-fg)`,
        };

        return html`
            <button
                ?disabled=${this.disabled}
                class='button'
                @click=${this.onClick}
                type=${this.type}
                ${applyStyles(styles)}
                ${animated()}
            >
                <slot></slot>
            </button>
        `;
    }
}

