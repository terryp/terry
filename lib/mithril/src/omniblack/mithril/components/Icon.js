import { LitElement, css, nothing } from 'lit';
import { Task, TaskStatus } from '@lit/task';

import { MithrilElement } from '#src/base.js';

function parseSvg(svgSourceText) {
    const parser = new DOMParser();
    const doc = parser.parseFromString(svgSourceText, 'image/svg+xml');
    const el = doc.documentElement;
    return el;
}


const placeholderSrc = `
    <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
        <path
            fill="var(--omniblack-loader-fg)"
            d="M10.14,1.16a11,11,0,0,0-9,8.92A1.59,1.59,0,0,0,2.46,12,1.52,1.52,0,0,0,4.11,10.7a8,8,0,0,1,6.66-6.61A1.42,1.42,0,0,0,12,2.69h0A1.57,1.57,0,0,0,10.14,1.16Z"
        >
            <animateTransform
                attributeName="transform"
                type="rotate"
                dur="1.25s"
                values="0 12 12;360 12 12"
                repeatCount="indefinite"
            />
        </path>
    </svg>
`;


const iconCache = new Map();

let placeholderImg;
function getPlaceholder() {
    if (!placeholderImg) {
        placeholderImg = parseSvg(placeholderSrc);
        placeholderImg.classList.add('mithril-image-el');
    }

    return placeholderImg.cloneNode(true);
}


export class Icon extends MithrilElement {
    static element_name = 'mithril-icon';

    static shadowRootOptions = {
        ...LitElement.shadowRootOptions,
        delegatesFocus: true,
    };

    static properties = {
        src: { reflect: true },
        height: { reflect: true },
        width: { reflect: true },
        color: { reflect: true },

        icon_el: { state: true },
        in_viewport: {
            state: true,
            type: Boolean,
        },
        page_visible: {
            state: true,
            type: Boolean,
        },
    };

    _loadImage = new Task(this, {
        task: async ([src], { signal }) => {
            if (iconCache.has(src)) {
                const el = iconCache.get(src);
                return el.cloneNode(true);
            }

            const resp = await fetch(src, {
                credentials: 'omit',
                priority: 'low',
                signal,
            });

            if (!resp.ok) {
                throw new Error(`HTTP error! Status: ${resp.status}`);
            }

            const text = await resp.text();

            const el = parseSvg(text);
            el.classList.add('mithril-image-el');
            iconCache.set(src, el);
            return el;
        },
    });

    constructor() {
        super();
        this.visibilityChange = this.visibilityChange.bind(this);
        this.intersectionCb = this.intersectionCb.bind(this);
        this.observer = new IntersectionObserver(this.intersectionCb);
        this.page_visible = !document.hidden;
        this.placeholder_el = getPlaceholder();
        this.color = 'currentcolor';
    }

    connectedCallback() {
        super.connectedCallback();

        document.addEventListener('visibilitychange', this.visibilityChange);
        this.observer.observe(this);
        this.runIfPossible();
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        document.removeEventListener('visibilitychange', this.visibilityChange);
        this.observer.unobserve(this);
    }

    get loading() {
        return (
            this._loadImage.status === TaskStatus.INITIAL
            || this._loadImage.status === TaskStatus.PENDING
        );
    }

    get loaded() {
        return this._loadImage.status === TaskStatus.COMPLETE;
    }

    get visible() {
        return this.page_visible && this.in_viewport;
    }

    static styles = css`
        * {
            box-sizing: border-box;
        }


        :host {
            display: block;
            width: max-content;
            height: auto;
        }

        .mithril-image-el {
            object-fit: contain;
            stroke-color: var(--omniblack-text-fg);

            height: 100%;
            width: 100%;
        }
    `;


    render() {
        if (!this.visible && !this.loaded) {
            return nothing;
        } else if (this.loading) {
            return this.placeholder_el;
        } else {
            return this._loadImage.value;
        }
    }

    visibilityChange() {
        this.page_visible = !document.hidden;
    }

    intersectionCb(entries) {
        this.in_viewport = entries.some((entry) => entry.isIntersecting);
    }

    runIfPossible() {
        if (this.visible && this.src) {
            this._loadImage.run([this.src]);
        }
    }

    willUpdate(changed) {
        const visibleChanged
            = changed.has('page_visible') || changed.has('in_viewport');
        if (changed.has('src') || visibleChanged) {
            this.runIfPossible();
        }
    }

    updated() {
        if (!this.loading && this._loadImage.value) {
            this._loadImage.value.style.width = this.width;
        }
    }
}
