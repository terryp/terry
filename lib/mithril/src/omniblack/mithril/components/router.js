import { setContext, getContext } from 'svelte';
import autoBind from 'auto-bind';
import {
    URL,
    ConnectableStore,
    NestingStore,
    URLPath,
    ReadonlyStore,
} from '@omniblack/stores';
import { localize } from '@omniblack/localization/svelte';

export function getRouter() {
    return getContext('omniblack.router');
}

export class Router extends ReadonlyStore {
    constructor(routes, basePath = '') {
        super();

        this.basePath = URLPath.fromString(basePath, 'router_path');

        this.__routes = new ConnectableStore(routes, 'router_routes');
        this.__state = new NestingStore(
            window.history.state ?? {},
            'router_state',
        );
        this.__url = URL.fromLocation(
            window.location,
            this.basePath,
            'router_url',
        );

        autoBind(this);
        setContext('omniblack.router', this);
        window.addEventListener('popstate', this.popState);
        this.state = window.history.state ?? {};
        this.matchRoute();

        this.routes.subscribe(this.queueTick);
        this.url.subscribe(this.queueTick);
        this.state.subscribe((newValue) => {
            this.currentState = newValue;
            this.queueTick();
        });
        this.__unsubLocalize = localize.subscribe(($localize) => {
            this.$localize = $localize;
            this.updateLocalize();
        });

        this.initFinished = true;
    }

    get routes() {
        return this.__routes;
    }

    set routes(newValue) {
        this.__routes.set(newValue);
    }

    get state() {
        return this.__state;
    }

    set state(newValue) {
        this.__state.set(newValue);
    }

    get url() {
        return this.__url;
    }

    set url(newURL) {
        this.__url.set(newURL);
    }

    get value() {
        return this;
    }

    createURL({
        query = this.url.query,
        hash = this.url.hash,
        path = this.url.path,
        name = 'temp_url',
    } = {}) {
        const newURL = new URL(path, query, hash, name);

        newURL.path.prepend(...this.basePath);
        return newURL;
    }

    fromLocation(location) {
        const path = URLPath.fromString(location.pathname);
        path.removePrefix(this.basePath);
        this.url.path = path;
        this.url.query = location.query;
        this.url.hash = location.hash;
    }

    popState(event) {
        this.fromLocation(window.location);
        this.state = event.state;
    }

    matchRoute() {
        const currentRoute = this.routes.value
            .find((route) => this.url.path.eq(route.path));
        this.route = currentRoute;
    }

    addHistory() {
        const currentURL = this.createURL();
        const title = this.$localize(this.route.display);

        document.title = title;
        window.history.pushState(this.currentState, title, String(currentURL));
    }

    runTick() {
        if (this.tickPending) {
            this.tickPending = false;
            this.matchRoute();
            this.addHistory();
            this.__changed();
        }
    }

    queueTick() {
        if (!this.tickPending && this.initFinished) {
            this.tickPending = true;
            queueMicrotask(this.runTick);
        }
    }

    updateLocalize() {
        const currentURL = URL.fromLocation(
            window.location,
            undefined,
            'updateLocalizeURL',
        );
        const title = this.$localize(this.route.display);

        document.title = title;
        window.history.replaceState(
            window.history.state,
            title,
            String(currentURL),
        );
    }
}
