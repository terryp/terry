import { css, html, nothing } from 'lit';
import { reportStreamProgress, streamToBlob } from '@omniblack/utils';
import { Task, TaskStatus, initialState } from '#src/utils/tasks.js';

import { MithrilElement } from '#src/base.js';
import { Icon } from '#src/components/Icon.js';

export class Image extends MithrilElement {
    static element_name = 'mithril-image';
    static needed_elements = [Icon];

    static styles = css`
        * {
            box-sizing: border-box;
        }


        :host {
            display: block;
            width: var(--omniblack-image-width, max-content);
            height: var(--omniblack-image-height, auto);
        }

        :host(:focus-visible) {
            outline: var(--omniblack-focus-ring);
            outline-offset: var(--omniblack-focus-ring-offset);
        }


        :host(:state(error)) {
            font-style: italic;
            font-weight: lighter;
            display: flex;
            flex-direction: column;
            align-items: center;
            border: var(--omniblack-border);
            border-radius: var(--omniblack-border-radius);
        }

        :host(:state(presentational):state(error)) {
            display: none;
        }

        .mithril-image-el {
            object-fit: contain;
            stroke-color: var(--omniblack-text-fg);

            height: 100%;
            width: 100%;
        }
    `;

    imgEl = new Task(this, {
        name: 'imgEl',
        async task([src, visible, progress], { stack }) {
            if (src === undefined || !visible) {
                return initialState;
            }

            progress.value = 0;

            const resp = await fetch(
                src,
                {
                    method: 'get',
                    mode: 'cors',
                    priority: 'low',
                    referrer: '',
                    referrerPolicy: 'no-referrer',
                }
            );

            const bodyStream = reportStreamProgress(resp.body, progress);
            const imageBlob = await streamToBlob(bodyStream);
            const imageUrl = URL.createObjectURL(imageBlob);
            stack.defer(() => URL.revokeObjectURL(imageUrl));

            const el = document.createElement('img');
            el.src = imageUrl;
            el.inert = true;
            el.classList.add('mithril-image-el');

            await el.decode();
            return el;
        },
        args: () => [this.src, this.everVisible, this.placeholder_el],
        argsEqual(oldArgs, newArgs) {
            const [oldSrc, oldEverVisible] = oldArgs;
            const [newSrc, newEverVisible] = newArgs;

            return newSrc === oldSrc && newEverVisible === oldEverVisible;
        }
    });

    static properties = {
        alt: { reflect: true },
        title: { reflect: true },
        src: { reflect: true },
        state: { reflect: true },
        height: { reflect: true },
        width: { reflect: true },
        fileSize: {
            attribute: 'file-size',
            type: Number,
            reflect: true,
        },
        _loading: {
            state: true,
            type: Boolean,
        },

        /*
         * We track our visibility
         * to delay the first render
         * until we can actually be seen.
         */
        everVisible: {
            state: true,
            type: Boolean,
        },
        inViewport: {
            state: true,
            type: Boolean,
        },
        pageVisible: {
            state: true,
            type: Boolean,
        },
    };

    get state() {
        return this._state;
    }

    set state(newValue) {
        if (newValue === this._state) {
            return;
        }
        const oldValue = this._state;
        this._state = newValue;
        this.requestUpdate('state', oldValue);
        this.internals.states.delete(oldValue);
        this.internals.states.add(newValue);
    }

    constructor() {
        super();
        this.internals = this.attachInternals();
        this.visibilityChange = this.visibilityChange.bind(this);
        this.intersectionCb = this.intersectionCb.bind(this);
        this.observer = new IntersectionObserver(this.intersectionCb);
        this.pageVisible = !document.hidden;
        this.state = 'loading';
        this.alt = '';
        this.internals.states.add('presentational');
        this.internals.role = 'presentation';
    }

    get loaded() {
        return !this._loading && this.media_el.src === this.src;
    }

    connectedCallback() {
        super.connectedCallback();
        this.placeholder_el = document.createElement('mithril-spinner');
        this.placeholder_el.max = this.fileSize;
        document.addEventListener('visibilitychange', this.visibilityChange);
        this.observer.observe(this);
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        document.removeEventListener('visibilitychange', this.visibilityChange);
        this.observer.unobserve(this);
    }

    get loading() {
        return this._loading;
    }

    get visible() {
        return this.pageVisible && this.inViewport;
    }


    render() {
        return this.imgEl.render({
            initial: () => nothing,
            pending: () => this.placeholder_el,
            complete: (v) => v,
            error: () => this.broken(),
        });
    }

    broken() {
        return html`
            <mithril-icon
                src='/mithril/phosphor/light/image-broken-light.svg'
                width='3em'
                aria-hidden='true'
            >
            </mithril-icon>
            <span>
                ${this.alt}
            </span>
        `;
    }

    visibilityChange() {
        this.pageVisible = !document.hidden;
    }

    intersectionCb(entries) {
        this.inViewport = entries.some((entry) => entry.isIntersecting);
    }

    willUpdate(changed) {
        if (this.pageVisible && this.inViewport) {
            this.everVisible = true;
        }

        if (changed.has('alt')) {
            if (this.alt) {
                this.internals.states.delete('presentational');
                this.internals.role = 'img';
            } else {
                this.internals.states.add('presentational');
                this.internals.role = 'presentation';
            }
        }

        this.state = this.stateString();

        if (changed.has('fileSize')) {
            this.placeholder_el.max = this.fileSize;
        }
    }

    stateString() {
        switch (this.imgEl.status) {
            case TaskStatus.INITIAL:
            case TaskStatus.PENDING:
                return 'loading';
            case TaskStatus.ERROR:
                return 'error';
            case TaskStatus.COMPLETE:
                return 'loaded';
            default:
                console.error(`Invalid task status ${this.imgEl.state}`);
                return 'error';
        }
    }

    updated(changed) {
        if (changed.has('width')) {
            if (this.width !== undefined && this.width !== null) {
                this.style.setProperty('--omniblack-image-width', this.width);
            } else {
                this.style.removeProperty('--omniblack-image-width');
            }
        }

        if (changed.has('height')) {
            if (this.height !== undefined && this.height !== null) {
                this.style.setProperty('--omniblack-image-height', this.height);
            } else {
                this.style.removeProperty('--omniblack-image-height');
            }
        }

        if (changed.has('alt')) {
            this.internals.ariaLabel = this.alt;
        }
    }
}
