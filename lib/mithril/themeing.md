# Theme in omniblack.omniblack

We use CSS variables to theme our components.
We define concepts in the UI and use variables
to hold the colors for those concepts. Each concept has a foreground shade
and background shade for every color.
Foreground color are more vibrant or bright, and should be used when
coloring text, buttons etc.
Background colors should be less vibrant and less bright, so that they
be used while coloring larger areas of the UI like the header.

The concepts are mapped to css variables using this format:
    `--omniblack-<concept>-fg`
    `--omniblack-<concept>-bg`

## Concepts and variables

| Concept    | Description                                                  |
|------------|--------------------------------------------------------------|
| `branding` | Should be used in large structural pieces of the UI .i.e the site header. This should *not* be used for functional elements like buttons as branding color have overlap or similarities with other colors and could confuse the user. |
| `text`     | The color used for normal text.                              |
| `link`     | An operation that will cause navigation without state change on the backend. |
| `link-visited` | A link that the user has already visited                 |

```{note}
`link` and `link-visited` should only have a foreground color.
As they are embedded in large bodies of text they should share text-bg.
```


### Action Severity

| Concept   | Action Description | State Description |
|-----------|--------------------|-------------------|
| `normal`  | Does not fit in to other categories | .... |
| `warning` | May discard client-side state, but does not affect the server. | The control may need attention from the user, but will not block the user from progressing the current task. |
| `danger`  | Will discard or delete server side state. | The control is in an invalid state and must be corrected before the user can proceed. |
| `success` | Will complete or progress the current user task .i.e form submit | The control is completely valid. |
| `info`    | Provides information or assistance to the user. | ... |

### Interactivity Pieces

#### Simple Variables

| Concept   | Description                                      |
|-----------|--------------------------------------------------|
| `hovered` | The control is being hovered over by the mouse.  |

#### Focus Ring

`--omniblack-focus-ring` & `--omniblack-focus-ring-padding`

The control has keyboard focus.


##### Example CSS

```css
:focus-visible {
    outline: var(--omniblack-focus-ring);
    outline-offset: var(--omniblack-focus-ring-padding);
}
```

```{note}
Where possible `:focus-visible` should be used instead of `:focus` as
the browser will turn `:focus-visible` off when the user is not likely to
want a focus ring i.e. a mouse user.
```

##### Example

![Example Focus Ring](https://res.cloudinary.com/nicchan/image/upload/f_auto,q_auto,c_limit,w_556/focus-outline4.PNG)

##### Component  Sub Variables

| CSS Variable                     | Description                                      |
|----------------------------------|--------------------------------------------------|
| `--omniblack-focus-ring-color`   | The color of the ring                            |
| `--omniblack-focus-ring-width`   | The width of the ring                            |
| `--omniblack-focus-ring-style`   | The style of the outline.                        |
| `--omniblack-focus-ring-padding` | The space between the focus ring and the control |

