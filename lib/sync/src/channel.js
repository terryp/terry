import { EventEmitter } from './events.js';
import { Fifo } from '@omniblack/collections';
import { deferred } from '@omniblack/promise';

export class SendChannel {
    constructor(sender) {
        this.sender = sender;
        this.closed = false;
        this.sender.register(this);
    }

    async send(obj) {
        if (this.closed) {
            throw new Error('Channel is closed');
        }

        return await this.sender.send(obj);
    }

    async clone() {
        if (this.closed) {
            throw new Error('Channel is closed');
        }

        return new SendChannel(this.sender);
    }

    async close() {
        if (this.closed) {
            return;
        }

        this.closed = true;
        await this.sender.deregister(this);
    }
}

class Sender extends EventEmitter {
    constructor() {
        super();
        this.closed = false;
        this.endpoints = new Set();
        this.receiver = null;
    }

    async close() {
        this.closed = true;
        for (const endpoint of this.endpoints) {
            await endpoint.close();
        }

        if (!this.receiver.closed) {
            await this.receiver.close();
        }
    }

    async deregister(endpoint) {
        this.endpoints.delete(endpoint);

        if (this.endpoints.size === 0) {
            await this.close();
        }
    }

    async register(endpoint) {
        if (this.closed) {
            throw new Error('Sender is closed');
        }

        this.endpoints.add(endpoint);
    }

    async send(item) {
        return await this.receiver.enqueue(item);
    }
}

export class ReceiverChannel {
    constructor(receiver) {
        this.receiver = receiver;
        this.closed = false;
        this.receiver.register(this);
        this.queue = new Fifo();
        this.waiters = new Fifo();
    }

    async clone() {
        if (this.closed) {
            throw new Error('Channel is closed');
        }

        return new ReceiverChannel(this.receiver);
    }

    async close() {
        if (this.closed) {
            return;
        }

        this.closed = true;
        await this.receiver.deregister(this);

        for (const waiter of this.waiters) {
            waiter.resolve();
        }
    }

    async enqueue(item) {
        if (this.closed) {
            throw new Error('Channel is closed');
        }

        this.queue.push(item);
        return await this._run();
    }

    async _run() {
        for (const [waiter, item] of this.waiters.zip(this.queue)) {
            waiter.resolve(item);
        }
    }

    [Symbol.asyncIterator]() {
        return this;
    }

    async receive() {
        if (this.closed) {
            throw new Error('Channel is closed');
        }

        const prom = deferred();
        this.waiters.push(prom);
        this._run();
        const value = await prom;

        if (this.closed) {
            throw new Error('Channel is closed');
        }

        return value;
    }

    async next() {
        if (this.closed) {
            return { done: true };
        }

        const prom = deferred();
        this.waiters.push(prom);
        this._run();
        const value = await prom;

        if (this.closed) {
            return { done: true };
        }

        return { value, done: false };
    }
}

class Receiver extends EventEmitter {
    constructor() {
        super();
        this.closed = false;
        this.receivers = [];
        this.sender = null;
    }

    async close() {
        this.closed = true;
        for (const receiver of this.receivers) {
            await receiver.close();
        }

        if (!this.sender.closed) {
            await this.sender.close();
        }
    }

    async register(receiver) {
        this.receivers.push(receiver);
    }

    async enqueue(item) {
        return Promise.allSettled(this.receivers.map((receiver) =>
            receiver.enqueue(item)));
    }

    async deregister(receiver) {
        this.receivers = this.receivers.filter((r) =>
            r !== receiver);

        if (this.receivers.length === 0) {
            await this.close();
        }
    }
}


export function createChannel() {
    const receiverControl = new Receiver();
    const senderControl = new Sender();
    const receiveChannel = new ReceiverChannel(receiverControl);
    const sendChannel = new SendChannel(senderControl);

    receiverControl.sender = senderControl;
    senderControl.receiver = receiverControl;


    return { receiveChannel, sendChannel };
}
