import { Semaphore } from '#src/semaphore.js';

export class Lock extends Semaphore {
    constructor() {
        super(1);
    }

    get locked() {
        return this.blocked;
    }
}
