/* eslint-env node */
import { deferred } from '@omniblack/promise';

export const sharedLocked = 'readLocked';
export const exclusiveLocked = 'writeLocked';
export const unlocked = 'unlocked';

/** A shared/exclusive lock. */
export class SharedExclusiveLock {
    /** Create a lock. */
    constructor() {
        this.pendingShareds = [];
        this.pendingExclusives = [];
        this.activeShareds = new Set();

        this.state = unlocked;
    }

    /**
     * Grant access to a waiter who has requested shared access.
     * @private
     * @async
     * @param {LockAccess} cb - The waiter to grant access to.
     * @param {...*} args - The args to pass to the waiter.
     * @returns {Promise<Any>} The return value of cb.
     */
    grantShared(cb, ...args) {
        const promise = Promise.resolve(cb(...args));
        this.activeShareds.add(promise);

        return promise.finally(() => {
            this.activeShareds.delete(promise);
            if (this.activeShareds.size === 0) {
                this.release();
            }
        });
    }

    /**
     * Grant access to a waiter who has requested exclusive access.
     * @async
     * @private
     * @param {LockAccess} cb - the waiter to grant access to.
     * @param {...*} args - the args to pass to the waiter.
     * @returns {Promise<Any>} The return value from cb.
     */
    grantExclusive(cb, ...args) {
        const promise = Promise.resolve(cb(...args));

        return promise.finally(() => {
            if (this.pendingExclusives.length > 0) {
                const { args, cb } = this.pendingExclusives.shift();
                this.grantExclusive(cb, ...args);
            } else {
                this.release();
            }
        });
    }

    /**
     * The callback to be called when the lock is granted.
     *      Resource accesses should only occur inside this function.
     *  @async
     *  @callback LockAccess
     *  @param {...*} args - The args originally passed to
     *      sharedLock/exclusiveLock will be passed to the callback.
     *  @returns {Promise<Any>}
     */

    /**
     * Request a shared lock.
     * @param {LockAccess} cb - a function to be called when
     *       accessing the resource.
     * @param {...*} args - The arguments to be passed in to the callback.
     * @returns {Promise<Any>} The return value of cb.
     */
    async sharedLock(cb, ...args) {
        switch (this.state) {
            case sharedLocked: {
                return await this.grantShared(cb, ...args);
            }

            case exclusiveLocked: {
                const retPromise = deferred();
                this.pendingShareds.push({ args, cb, retPromise });
                return await retPromise;
            }

            case unlocked: {
                this.state = sharedLocked;
                return await this.grantShared(cb, ...args);
            }

            default: {
                break;
            }
        }
    }

    /**
     * Request an exclusive lock.
     * @param {LockAccess} cb - a function to be called when
     *       accessing the resource.
     * @param {...*} args - The arguments to be passed to the callback.
     * @returns {Promise<Any>} The return value of cb.
     */
    async exclusiveLock(cb, ...args) {
        switch (this.state) {
            case unlocked: {
                this.state = exclusiveLocked;
                return await this.grantExclusive(cb, ...args);
            }

            default: {
                const retPromise = deferred();
                this.pendingExclusives.push({ cb, args, retPromise });
                return await retPromise;
            }
        }
    }

    /**
     * Release the current lock and grant a new lock if there is a waiter.
     * @private
     * @returns {undefined}
     */
    release() {
        switch (this.state) {
            case sharedLocked: {
                if (this.pendingExclusives.length > 0) {
                    this.state = exclusiveLocked;
                    const { args, cb, retPromise }
                        = this.pendingExclusives.shift();
                    retPromise.resolve(this.grantExclusive(cb, args));
                } else {
                    this.state = unlocked;
                }
                break;
            }

            case exclusiveLocked: {
                if (this.pendingShareds.length > 0) {
                    this.state = sharedLocked;
                    for (const { cb, args, retPromise }
                        of this.pendingShareds) {
                        retPromise.resolve(this.grantShared(cb, ...args));
                    }
                    this.pendingShareds = [];
                } else {
                    this.state = unlocked;
                }
                break;
            }

            default: {
                break;
            }
        }
    }
}
