import { map, enumerate } from 'itertools';
import { Fifo, Set } from '@omniblack/collections';
import { deferred } from '@omniblack/promise';
import { getArgsAndCb } from '@omniblack/utils';

const State = Object.freeze({
    unStarted: Symbol('Continuation has no started yet'),
    running: Symbol('Continuation running'),
    waiting: Symbol('Continuation waiting on children'),
    resumable: Symbol('Continuation maybe resumed'),
    done: Symbol('Continuation finished running'),
    error: Symbol('Continuation has encountered an error'),
});

// TODO add locks and channels
class Continuation {
    constructor(ctx, parent, cb, args = [], finalizers = []) {
        this.cb = cb;
        this.args = args;
        this.ctx = ctx;
        this.state = State.unStarted;
        this.parent = parent;

        // notify parent when done
        this.retPromise = deferred();
        this.suspendedProm = null;
        this.childValues = [];
        this.childIndexes = new Map();
        this.finalizers = [];
    }

    async _run() {
        this.state = State.running;

        try {
            const value = await this.cb(this, ...this.args);

            const finalizers = [...this.finalizers];
            finalizers.reverse();
            for (const { cb, args } of finalizers) {
                await cb(...args);
            }

            return await this.retPromise.resolve(value);
        } catch (err) {
            this.state = State.error;
            this.retPromise.reject(err);
        } finally {
            if (this.state === State.running) {
                this.state = State.done;
            }

            // notify the context we are done
            this.ctx._contFinished(this);
        }
    }

    defer(...args) {
        this.finalizers.push(getArgsAndCb('defer', args));
    }

    async _suspend(...conts) {
        this.childIndexes = new Map(
            conts.map((value, index) => [value, index]),
        );

        for (const cont of conts) {
            cont.retPromise
                .then((v) => this._childDone(cont, v))
                .catch((err) => this.suspendedProm.reject(err));
        }

        this.suspendedProm = deferred();
        this.state = State.waiting;
        this.ctx._suspend(this, conts);
        return await this.suspendedProm;
    }

    _childDone(cont, value) {
        const index = this.childIndexes.get(cont);
        this.childIndexes.delete(cont);
        this.childValues[index] = value;
        if (this.childIndexes.size === 0) {
            this.state = State.resumable;
        }
    }

    _resume() {
        this.state = State.running;
        this.suspendedProm.resolve(this.childValues);
        this.childValues = [];
    }


    async map(iter, cb) {
        const conts = map(enumerate(iter), ([index, value]) =>
            new Continuation(this.ctx, this, cb, [value, index]));

        return await this._suspend(...conts);
    }

    async filter(iter, cb) {
        const results = await this.map(iter, createWrapper(cb));
        return results.filter(({ keep }) => keep).map(({ value }) => value);
    }

    async flatMap(iter, cb) {
        const results = await this.map(iter, cb);
        return results.flat();
    }
}

function createWrapper(fn) {
    return async function wrapper(value, ...args) {
        const keep = await fn(value, ...args);
        return { keep, value };
    };
}


export class Context {
    constructor(limit = Number.POSITIVE_INFINITY) {
        this.limit = limit;
        this.queue = new Fifo();
        this.active = new Set();
    }

    get blocked() {
        return this.active.size >= this.limit;
    }

    async run(...userArgs) {
        const name = this.constructor.name;

        const { cb, args } = getArgsAndCb(`${name}.run`, userArgs);

        const cont = new Continuation(this, null, cb, args);
        this._schedule(cont);
        return await cont.retPromise;
    }

    async map(iter, cb) {
        const cont = new Continuation(
            this,
            null,
            async (ctx) => await ctx.map(iter, cb),
        );

        this._schedule(cont);
        return await cont.retPromise;
    }

    async flatMap(iter, cb) {
        const cont = new Continuation(
            this,
            null,
            async (ctx) => await ctx.flatMap(iter, cb),
        );

        this._schedule(cont);
        return await cont.retPromise;
    }

    async filter(iter, cb) {
        const cont = new Continuation(
            this,
            null,
            async (ctx) => await ctx.flatMap(iter, cb),
        );

        this._schedule(cont);
        return await cont.retPromise;
    }

    _contFinished(cont) {
        this.active.delete(cont);
        const resumable = cont.parent?.state === State.resumable;
        if (cont.parent !== null && resumable) {
            this._schedule(cont.parent);
        } else if (this.queue.length > 0 && !this.blocked) {
            const cont = this.queue.pop();
            this._schedule(cont);
        }
    }

    _suspend(parent, conts) {
        this.active.delete(parent);

        for (const cont of conts) {
            this._schedule(cont);
        }
    }

    _schedule(cont) {
        if (this.blocked) {
            this.queue.push(cont);
        // eslint-disable-next-line curly
        } else switch (cont.state) {
            case State.unStarted: {
                this.active.add(cont);
                cont._run();
                break;
            }
            case State.resumable: {
                this.active.add(cont);
                cont._resume();
                break;
            }
            case State.error:
            case State.running:
            case State.done:
            case State.waiting: {
                throw new Error('Internal error: Invalid state', {
                    details: {
                        state: cont.state,
                    },
                });
            }
            default: {
                throw new Error('Internal error: unhandled state', {
                    details: {
                        state: cont.state,
                    },
                });
            }
        }
    }
}
