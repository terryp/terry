import { map } from 'itertools';

export const NOT_PRESENT = Symbol('The listener is not registered');
export const ALREADY_REGISTERED = Symbol('The listener is already registered');

export class Event {
    constructor(name, { abortable = false }) {
        this.name = name;
        this.abortable = abortable;
        this.aborted = false;
        this.abortMessages = [];
        this.errors = [];
    }

    abort(msg) {
        if (!this.abortable) {
            throw new Error(`"${this.name}" events are not abortable.`);
        }

        this.abortMessages.push(msg);

        this.aborted = true;
    }
}

const eventConfig = {
    abortable: true,
};

export class EventEmitter {
    constructor(events = []) {
        events = Array.isArray(events)
            ? new Map(events.map((eventName) => [eventName, eventConfig]))
            : new Map(Object.entries(events));

        this.events = events;
        this.events.set('error', { abortable: false });

        this.listeners = new Map(
            map(this.events.keys(), (name) => [name, new Set()]),
        );
    }

    on(eventName, listener) {
        if (!this.events.has(eventName)) {
            throw new Error(`"${eventName}" is not an event for this emitter.`);
        }

        const listeners = this.listeners.get(eventName);

        if (listeners.has(listener)) {
            return ALREADY_REGISTERED;
        }

        listeners.add(listener);

        return false;
    }

    off(eventName, listener) {
        if (!this.events.has(eventName)) {
            throw new Error(`"${eventName}" is not an event for this emitter.`);
        }

        const listeners = this.listeners.get(eventName);

        if (!listeners.has(listener)) {
            return NOT_PRESENT;
        }

        listeners.remove(listener);

        return false;
    }

    _prepareEvent(eventName) {
        if (!this.events.has(eventName)) {
            throw new Error(`"${eventName}" is not an event for this emitter.`);
        }

        const eventConfig = this.events.get(eventName);
        return new Event(eventName, eventConfig);
    }

    async emit(event, ...args) {
        if (typeof event === 'string') {
            event = this._prepareEvent(event);
        }

        const listeners = this.listeners.get(event.name);
        const proms = map(listeners, async (listener) => {
            try {
                return await listener(event, ...args);
            } catch (err) {
                event.events.push(err);
            }
        });

        return await Promise.all(proms);
    }
}
