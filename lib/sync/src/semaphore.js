import { Fifo } from '@omniblack/collections';
import { deferred } from '@omniblack/promise';
import { getArgsAndCb } from '@omniblack/utils';

export class Semaphore {
    constructor(limit = Number.POSITIVE_INFINITY) {
        this.limit = limit;
        this.queue = new Fifo();
        this.active = new Set();
    }

    get blocked() {
        return this.active.size >= this.limit;
    }

    async run(...userArgs) {
        const name = this.constructor.name;

        const { cb, args } = getArgsAndCb(`${name}.run`, userArgs);

        if (this.blocked) {
            const retPromise = deferred();
            this.queue.push({
                cb,
                args,
                retPromise,
            });

            return await retPromise;
        } else {
            return await this._run(cb, args);
        }
    }


    async _run(cb, args) {
        const prom = Promise.resolve(cb(...args));

        this.active.add(prom);

        return await prom.finally(() => {
            this.active.delete(prom);
            if (this.queue.length > 0 && !this.blocked) {
                const { cb, args } = this.queue.pop();
                this.prom.retPromise.resolve(this._run(cb, args));
            }
        });
    }
}
