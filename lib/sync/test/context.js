import test from 'ava';
import { Context } from '#src/context.js';
import { deferred } from '@omniblack/promise';

test('context run', async (t) => {
    let cbsCalled = 0;

    const ctx = new Context(1);


    // eslint-disable-next-line no-return-assign
    t.is(await ctx.run(() => cbsCalled += 1), 1);
    t.is(cbsCalled, 1);


    const flag = deferred();
    const prom = ctx.run(async () => {
        const value = cbsCalled += 1;
        await flag;
        return value;
    });

    t.is(cbsCalled, 2);

    const prom2 = ctx.run(async () => {
        const value = cbsCalled += 1;
        return value;
    });

    t.is(cbsCalled, 2);

    flag.resolve();
    t.deepEqual(await Promise.all([prom, prom2]), [2, 3]);

    t.is(cbsCalled, 3);
});

test('context map', async (t) => {
    let cbsCalled = 0;
    const ctx = new Context(3);
    const orig = ctx._schedule;
    ctx._schedule = (...args) => {
        orig.apply(ctx, args);
        const concurrentCalls = ctx.active.size;
        t.assert(
            concurrentCalls <= 3,
            `Too many calls: ${concurrentCalls}`,
        );
    };

    await ctx.run(async (cont) => {
        cbsCalled += 1;
        await cont.map([1, 2, 3, 4, 5], async (cont, v) => {
            t.is(v, cbsCalled);
            cbsCalled += 1;
            // just yield to the event loop so others can run
            await Promise.resolve();
        });
    });
});
