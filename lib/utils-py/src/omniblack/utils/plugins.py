from public import public


@public
def load_plugins(plugins, init_plugin):
    initialized = [
        init_plugin(plugin)
        for plugin in plugins
    ]

    index = 0
    while index < len(initialized):
        plugin = initialized[index]

        if plugin.plugins:
            initialized.extend(
                init_plugin(new_plugin)
                for new_plugin in plugin.plugins
            )

        index += 1

    return tuple(initialized)
