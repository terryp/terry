#! /usr/bin/python3
from enum import auto
from os import environ
from typing import NamedTuple

from public import public

from omniblack.utils import Enum


@public
class EnvTypes(Enum):
    home = auto()
    work = auto()


@public
class Env(NamedTuple):
    type: EnvTypes
    headless: bool = False


current_env = environ.get('ENV', 'home')
headless = False
env = Env(type=getattr(EnvTypes, current_env), headless=headless)
