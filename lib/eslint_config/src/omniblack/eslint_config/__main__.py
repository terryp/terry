from sys import stdout

from msgspec.json import Encoder

from omniblack.cli import CLI
from omniblack.eslint_config.model import model
from omniblack.repo import build_tree, find_packages, find_root

app = CLI('omniblack.eslint_config', model=model)

js_exts = (
    '.js',
    # '.svelte',
)


def js_files(pkg, tree):
    for ext in js_exts:
        yield from (
            entry.path
            for entry in tree.glob(f'**/*{ext}', pkg.rel_path)
            if not entry.info.is_dir
        )


@app.command
def main():
    """
    Lint all files in the repository.
    """

    root = find_root()
    tree = build_tree(root=root)

    pkgs = find_packages(root=root, fs=tree)

    js_pkgs = (pkg for pkg in pkgs if pkg.js)

    encoder = Encoder()
    encoding_buffer = bytearray(1024)

    for pkg in js_pkgs:
        envs = pkg.config.get('environments', ())
        envs = tuple(env.name for env in envs)
        all_files = tuple(js_files(pkg, tree))

        if all_files:
            msg = (envs, all_files)
            encoder.encode_into(msg, encoding_buffer)
            encoding_buffer.append(ord('\0'))
            stdout.buffer.write(encoding_buffer)


if __name__ == '__main__':
    main()
