import stylistic from '@stylistic/eslint-plugin-js';

const severity = 'warn';

export default [
    {
        plugins: {
            stylistic,
        },
        rules: {
            'stylistic/array-bracket-newline': [
                severity,
                { multiline: true },
            ],
            'stylistic/array-bracket-spacing': [severity],
            'stylistic/array-element-newline': [
                severity,
                'consistent',
            ],
            'stylistic/arrow-parens': [severity],
            'stylistic/arrow-spacing': [severity],
            'stylistic/block-spacing': [severity, 'never'],
            'stylistic/brace-style': [severity],
            'stylistic/comma-dangle': [severity, 'only-multiline'],
            'stylistic/comma-spacing': [severity],
            'stylistic/comma-style': [severity],
            'stylistic/computed-property-spacing': [severity],
            'stylistic/dot-location': [severity, 'property'],
            'stylistic/eol-last': ['error'],
            'stylistic/function-call-argument-newline': [
                severity,
                'consistent',
            ],
            'stylistic/function-call-spacing': [severity],
            'stylistic/function-paren-newline': [
                severity,
                'multiline-arguments',
            ],
            'stylistic/generator-star-spacing': [severity],
            'stylistic/indent': [
                severity,
                4,
                {
                    SwitchCase: 1,
                    ignoredNodes: ['TemplateLiteral *'],
                },

            ],
            'stylistic/jsx-quotes': [severity, 'prefer-single'],
            'stylistic/key-spacing': [severity],
            'stylistic/keyword-spacing': [severity],
            'stylistic/line-comment-position': [severity],
            'stylistic/linebreak-style': ['error'],
            'stylistic/lines-around-comment': [
                severity,
                {
                    allowBlockStart: true,
                    allowClassStart: true,
                },
            ],
            'stylistic/lines-between-class-members': [
                severity,
                'always',
                { exceptAfterSingleLine: true },
            ],
            'stylistic/max-len': [
                severity,
                {
                    ignoreComments: true,
                    ignoreTrailingComments: true,
                    ignoreUrls: true,
                    ignoreStrings: true,
                    ignoreTemplateLiterals: true,
                    ignoreRegExpLiterals: true,
                },
            ],
            'stylistic/max-statements-per-line': [severity],
            'stylistic/multiline-comment-style': [severity],
            'stylistic/multiline-ternary': [severity],
            'stylistic/new-parens': [severity],
            'stylistic/newline-per-chained-call': [severity],
            'stylistic/no-confusing-arrow': ['error'],
            'stylistic/no-extra-parens': [
                severity,
                'all',
                {
                    nestedBinaryExpressions: false,
                    ternaryOperandBinaryExpressions: false,
                },
            ],
            'stylistic/no-extra-semi': [severity],
            'stylistic/no-floating-decimal': [severity],
            'stylistic/no-mixed-operators': [severity],
            'stylistic/no-multi-spaces': ['error'],
            'stylistic/no-multiple-empty-lines': [severity],
            'stylistic/no-tabs': ['error'],
            'stylistic/no-trailing-spaces': ['error'],
            'stylistic/no-whitespace-before-property': [severity],
            'stylistic/object-curly-newline': [severity],
            'stylistic/object-curly-spacing': [severity, 'always'],
            'stylistic/object-property-newline': [
                severity,
                { allowAllPropertiesOnSameLine: true },
            ],
            'stylistic/one-var-declaration-per-line': [severity, 'always'],
            'stylistic/operator-linebreak': [severity, 'before'],
            'stylistic/padded-blocks': [severity, 'never'],
            'stylistic/quote-props': [severity, 'consistent-as-needed'],
            'stylistic/quotes': [severity, 'single', { avoidEscape: true }],
            'stylistic/rest-spread-spacing': [severity],
            'stylistic/semi': [severity],
            'stylistic/semi-spacing': [
                severity,
                {
                    after: false,
                    before: false,
                },
            ],
            'stylistic/semi-style': [severity],
            'stylistic/space-before-blocks': [severity],
            'stylistic/space-before-function-paren': [
                severity,
                {
                    anonymous: 'never',
                    named: 'never',
                    asyncArrow: 'always',
                },
            ],
            'stylistic/space-in-parens': [severity, 'never'],
            'stylistic/space-infix-ops': [severity],
            'stylistic/space-unary-ops': [
                severity,
                {
                    words: true,
                    nonwords: false,
                },
            ],
            'stylistic/spaced-comment': [severity, 'always'],
            'stylistic/switch-colon-spacing': [severity],
            'stylistic/template-curly-spacing': [severity],
            'stylistic/template-tag-spacing': [severity],
            'stylistic/wrap-iife': [severity, 'inside'],
            'stylistic/wrap-regex': [severity],
            'stylistic/yield-star-spacing': [severity],
        },
    },
];
