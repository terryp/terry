import compat from '#src/compat.js';

export default [
    ...compat.plugins('eslint-comments'),
    ...compat.extends('plugin:eslint-comments/recommended'),
];
