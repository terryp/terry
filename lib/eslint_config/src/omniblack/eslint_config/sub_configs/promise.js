import compat from '#src/compat.js';

export default [
    ...compat.plugins('promise'),
    ...compat.extends('plugin:promise/recommended'),
    {
        rules: {
            'promise/no-nesting': ['off'],
            'promise/no-promise-in-callback': ['error'],
            'promise/no-callback-in-promise': ['error'],
            'promise/avoid-new': ['error'],
        },
    },
];
