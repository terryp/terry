import node from 'eslint-plugin-n';
import globals from 'globals';

export default function(files, sourceType = 'module') {
    return {
        files,
        plugins: {
            n: node,
        },
        rules: {
            'n/no-missing-import': ['off'],
            'n/no-sync': ['error'],
            'n/prefer-promises/dns': ['error'],
            'n/prefer-promises/fs': ['error'],

            /*
             * It might be nice to turn theses on later, but at the moment
             * I am closely following the node releases
             */
            'n/no-unsupported-features/es-syntax': ['off'],
            'n/no-unsupported-features/es-builtins': ['off'],
            'n/no-unsupported-features/node-builtins': ['off'],
            'n/prefer-global/buffer': ['error', 'never'],

            // importing `Console` is okay
            'n/prefer-global/console': ['warn', 'always'],
            'n/prefer-global/process': ['error', 'always'],
            'n/prefer-global/text-decoder': ['error', 'never'],
            'n/prefer-global/text-encoder': ['error', 'never'],
            'n/prefer-global/url-search-params': ['error', 'always'],
            'n/prefer-global/url': ['error', 'always'],
            'n/no-deprecated-api': ['error'],

            'n/no-path-concat': ['error'],

            'n/no-process-env': ['warn'],

            'n/no-process-exit': ['warn'],

            'n/no-unpublished-bin': ['error'],

            'n/no-unpublished-import': ['error'],

            'n/no-unpublished-require': ['error'],

            /*
             * this rules doesn't actually ever report anything
             * It changes eslint static analyze to treat a process.exit similar
             * to a throw
             */
            'n/process-exit-as-throw': ['error'],

            'n/hashbang': ['error'],
        },
        languageOptions: {
            sourceType,
            globals: {
                ...globals.es2024,
                ...globals['nodeBuiltin'],
            },
        },
    };
}
