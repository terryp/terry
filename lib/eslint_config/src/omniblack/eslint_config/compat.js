import { FlatCompat } from '@eslint/eslintrc';
import { fileURLToPath } from 'node:url';
import { dirname, join } from 'node:path';

const filePath = fileURLToPath(import.meta.url);
const dirName = join(
    dirname(dirname(filePath)),
    'sub_configs',
);

export default new FlatCompat({
    baseDirectory: dirName,
    resolvePluginsRelativeTo: dirName,
});

