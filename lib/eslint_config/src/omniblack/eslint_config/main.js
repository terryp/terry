import { join } from 'node:path';
import { Buffer } from 'node:buffer';
import { execFile } from 'node:child_process';

import globals from 'globals';
import js from '@eslint/js';
import restrictedGlobals from 'confusing-browser-globals';
import { includeIgnoreFile } from '@eslint/compat';
import which from 'which';

import mainRules from '#src/rules.js';
import promise from '#src/sub_configs/promise.js';
import json from '#src/sub_configs/json.js';
import nodeRules from '#src/sub_configs/node.js';
import unicorn from '#src/sub_configs/unicorn.js';
import stylistic from '#src/sub_configs/stylistic.js';

export const base = [
    // eslint-disable-next-line n/no-process-env
    includeIgnoreFile(join(process.env.SRC, '.gitignore')),
    js.configs.recommended,
    ...stylistic,
    ...promise,
    ...json,
    ...unicorn,
    mainRules,
    {
        languageOptions: {
            globals: {
                config: true,
                ...globals.es2024,
                ...globals['shared-node-browser'],
            },
            ecmaVersion: 2024,
            sourceType: 'module',
            parserOptions: {
                requireConfigFile: false,
            },
        },
        rules: {
            'no-restricted-globals': ['error', ...restrictedGlobals],
        },
    },
    {
        files: ['**/node/**'],
        languageOptions: {
            sourceType: 'module',
            globals: {
                ...globals.es2024,
                ...globals.nodeBuiltin,
            },
        },
    },
];


export const node = nodeRules;

export function browser(files) {
    return {
        files,
        languageOptions: {
            sourceType: 'module',
            globals: {
                ...globals.es2024,
                ...globals.browser,
            },
        },
    };
}

const envToConfigs = {
    browser,
    node,
};

export async function getPkgConfigs() {
    const py = await which('python3.12');
    const subProc = execFile(
        py,
        ['-m', 'omniblack.eslint_config'],
        {
            stdio: 'pipe',
            encoding: undefined,
        },
    );
    const configs = [];

    subProc.ref();

    let currentConfig = Buffer.alloc(1024, 0);
    let index = 0;

    for await (const buf of subProc.stdout) {
        for (const byte of buf) {
            if (byte !== 0) {
                if (currentConfig.length <= index) {
                    const newBuffer = Buffer.alloc(currentConfig.length * 2, 0);
                    currentConfig.copy(newBuffer, 0, 0, index);
                    currentConfig = newBuffer;
                }

                currentConfig[index] = byte;
                index += 1;
            } else {
                const configString = currentConfig
                    .toString('utf8', 0, index);
                const [envs, files] = JSON.parse(configString);
                for (const env of envs) {
                    configs.push(envToConfigs[env](files));
                }
                index = 0;
            }
        }
    }

    return configs;
}
