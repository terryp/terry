# Omniblack Entry

[fields]: #omniblack.model.Field

`omniblack.entry` turns functions into {type}`omniblack.entry.entry`


```{eval-rst}
.. autoclass:: omniblack.entry.entry
    :members:
    :special-members: __call__
```

## How parameters are converted to [fields][fields]

The field name and display name will be the parameter's name.
The descrption of the fields will be pulled from the parsed
docstring.


### Getting the field's type

The field's type will by pulled from the parameter's type hint.

#### Possible type hint types

##### `Field`

If the type hint is a {type}`omniblack.model.Field`
it will be used as the parameters field.

##### `ModelType`

If the type hint is a {type}`omniblack.model.ModelType`
is will be used as the field's type.


##### `Annotated`
If the type hint is a {type}`typing.Annotated` the metadata
will be searched for a
{type}`omniblack.model.Field` or {type}`omniblack.model.ModelType`.
A {type}`omniblack.model.Field` will take
priority over a {type}`omniblack.model.ModelType`.
If neither is found the type that was annotated will be used
[as if it was the primary type hint.](#type-hint-is-type)

{#type-hint-is-type}
##### `type`

If the type hint is a {type}`type` the model will be checked for an associated
type in `model.types.types_by_impl`.
