from collections.abc import Sequence
from functools import update_wrapper
from inspect import Parameter, getdoc, signature
from types import FunctionType, GenericAlias
from typing import Annotated, Protocol, get_args, get_origin, get_type_hints

from docstring_parser import parse
from public import public

from omniblack.model import Field, Model


@public
def model_type_from_type(provided_type, model):
    try:
        return model.types.types_by_impl[provided_type]
    except KeyError:
        msg = f'Model {model.name} does not have a type for {provided_type}.'
        raise ValueError(msg) from None


def validate_sequence(field_name, seq_type):
    origin = get_origin(seq_type)
    args = get_args(seq_type)

    if issubclass(origin, tuple):
        if len(args) != 2 or args[1] is not Ellipsis:
            raise ValueError(
                f"{field_name}'s annotation is not valid."
                + ' omniblack.entry only supports variable length tuples.'
                + ' variable length tuples are indicated by two type args;'
                + ' the item type and ellipsis.',
            )

        return args[0]
    else:
        return args[0]


# Sadly the value of `Annotated[str, 'hi'] is not actually
# an instance of `Annotated` but instead a private class
AnnotatedActual = type(Annotated[str, 'hi'])


class _FieldAsAnnotation(Exception):  # noqa N818
    pass


@public
def is_a_from_type(
    param_name: str,
    model: Model,
    target_type: type,
    required,
) -> dict:
    model_type = None
    MetaField = model.meta_structs.Field

    if isinstance(target_type, AnnotatedActual):
        for data in target_type.__metadata__:
            if isinstance(data, MetaField):
                raise _FieldAsAnnotation(data)

        if model_type is None:
            primary_type, *_ = get_origin(target_type)
            model_type = model_type_from_type(primary_type, model)
            return model_type
        return None
    elif isinstance(target_type, MetaField | Field):
        raise _FieldAsAnnotation(target_type)
    elif isinstance(target_type, GenericAlias):
        origin = get_origin(target_type)
        if issubclass(origin, Sequence):
            sub_type = validate_sequence(param_name, target_type)
            sub_is_a = is_a_from_type(
                param_name,
                model,
                sub_type,
                Parameter.empty,
            )

            return {
                'type': 'list',
                'required': required,
                'list_attrs': {
                    'item': sub_is_a,
                },
            }
        else:
            msg = f'"{origin}" is not a recognized generic'
            raise ValueError(msg)

    elif isinstance(target_type, type):
        (model_type, attrs) = model_type_from_type(target_type, model)
        type_name = model_type.name

        extra_data = {}

        if attrs is not None:
            extra_data[f'{type_name}_attrs'] = attrs

        return {'type': type_name, 'required': required} | extra_data
    else:
        msg = f'{param_name} has an unrecognized type annotation.'
        raise ValueError(msg)


def field_from_parameter(model: Model, param: Parameter, desc: str) -> Field:
    param_type = param.annotation
    UIString = model.meta_structs.UIString
    MetaField = model.meta_structs.Field

    field_def = MetaField(
        name=param.name,
        display=UIString(en=param.name),
        desc=UIString(en=desc),
        model=model,
    )
    required = True

    if param.default is not Parameter.empty:
        field_def.assumed = param.default
        required = False

    try:
        field_def.is_a = is_a_from_type(
            param.name,
            model,
            param_type,
            required,
        )
    except _FieldAsAnnotation as err:
        match err.args[0]:
            case MetaField() as field:
                field_def = field
            case Field() as field:
                return field

    try:
        return Field(meta=field_def, model=model)
    except _FieldAsAnnotation as container:
        return container.args[0]


class EntryBase(Protocol):
    name: str
    short_desc: str
    long_desc: str
    fields: Sequence[Field]

    def __call__(self, *args, **kwargs):
        pass


class entry:  # noqa: N801
    """
    Analyze a function's signature to generate a entry definition.

    :ivar name: The name of the function.
    :vartype name: :type:`str`

    :ivar func: The wrapped function.
    :vartype func: :type:`callable`

    :ivar short_desc: A short description of the function.
        Derived from the function's docstring.
    :vartype short_desc: :type:`str`

    :ivar long_desc: A longer description of the function.
        Derived from the function's docstring.
    :vartype long_desc: :type:`str`

    :ivar fields: Fields derived from the function's parameters.
    :vartype fields: :type:`list[omniblack.model.Field]`

    :param model: The model that should be used to look up types.
    :type model: :type:`omniblack.model.Model`

    :param func: The function to analyze.
    :type func: :type:`callable`
    """

    def __init__(self, model: Model, func: FunctionType):
        sig = signature(func)

        self.name = func.__name__
        self.func = func
        type_hints = get_type_hints(func, include_extras=True)
        docstring_parsed = parse(getdoc(func))
        param_descs = {
            param.arg_name: param.description
            for param in docstring_parsed.params
        }

        self.short_desc = docstring_parsed.short_description
        self.long_desc = docstring_parsed.long_description

        parameters = [
            param.replace(annotation=type_hints.get(param.name, str))
            for param in sig.parameters.values()

        ]

        self.fields = [
            field_from_parameter(model, param, param_descs.get(param.name, ''))
            for param in parameters
        ]

        update_wrapper(self, self.func)

    def __call__(self, *args, **kwargs):
        """Call the wrapped function."""
        return self.func(*args, **kwargs)

    def __repr__(self):
        cls = self.__class__

        return f'{cls.__name__}(name={self.name}, func={self.func})'

    def __rich_repr__(self):
        yield self.name
        yield self.func
        yield 'short_desc', self.short_desc
        yield 'fields', self.fields
