import sys

from os.path import dirname

from rich import pretty
from werkzeug.test import Client, create_environ
from werkzeug.wrappers import Request

sys.path.append(dirname(__file__))

from omniblack.app import config  # noqa: E402
from omniblack.server_test.model import model  # noqa: E402

pretty.install()


config.load_app('omniblack.vulcan')
config.finish_setup()
config.app.model = model

from omniblack.server_test import app, routes  # noqa: E402

client = Client(app)

r = routes.test


def create_req(path):
    env = create_environ(path)
    return Request(env)
