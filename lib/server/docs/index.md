
# Omniblack Server

```{eval-rst}
.. automodule:: omniblack.server
   :members:
   :undoc-members:
   :exclude-members: render, route


.. autofunction:: omniblack.server.render
```
::::{class} omniblack.server.route

The route class offers class and instance methods for all supported
http methods.

:::{decorator} class_http_method(handler)  -> Self
   Create a new route, registering `handler` for `http_method` requests.
:::

:::{decorator} instance_http_method(handler) -> self
   Register `handler` for `http_method` requests on this route.
:::

```python
from omniblack.server import route

name = 'world'

@route.get
def hello():
    return f'Hello {name}!'

@hello.post
def hello(new_name: str):
    name = new_name
    return f'The name has been changed to {name}'
```

:::{seealso}
[Routing](#routing)
: Routing explains how `omniblack.server` matches urls to routes.
:::
::::
