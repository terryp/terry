from omniblack.server import create_app
from omniblack.server_test.model import model
from omniblack.utils import improve_module

app = create_app(
    'omniblack.server_test',
    model=model,
    option_structs=[model.structs.thing],
)

improve_module(__name__, ('routes',))
