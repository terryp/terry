from omniblack.model import Model
from omniblack.mongo import MongoPlugin
from omniblack.server import ServerModelPlugin

struct_def = {
    'name': 'thing',
    'top_level_struct': True,
    'display': {'en': 'Thing'},
    'desc': {'en': 'A Thing'},
    'fields': [
        {
            'name': 'int',
            'display': {'en': 'One'},
            'desc': {'en': 'One'},
            'is_a': {'type': 'integer'},
        },
        {
            'name': 'str',
            'display': {'en': 'String'},
            'desc': {'en': 'String'},
            'is_a': {'type': 'string'},
        },
    ],
}

model = Model(
    'omniblack.server.test',
    plugins=[ServerModelPlugin, MongoPlugin],
    struct_defs=[struct_def],
)
