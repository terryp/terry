from pathlib import PurePosixPath as Path

from omniblack.server import route


@route.get(path='<*path>')
def test(names: list[str], path: Path = None):
    """
    Say hello

    Args:
        names: The names to say hello to.
        path: The path to echo.
    """
    return {
        'path': str(path),
        'names': [
            f'Hello {name}!'
            for name in names
        ],
    }


@route.get
def root():
    return {}
