import pytest

from ruamel.yaml import YAML
from werkzeug.test import Client

from omniblack.server_test import app


@pytest.fixture
def client():
    return Client(app)


@pytest.fixture
def yaml():
    return YAML()


@pytest.fixture(scope='module', autouse=True)
def _setup_config():
    app.setup()


def test_get(client):
    resp = client.get('/')
    assert resp.status_code == 200
    assert resp.content_type == 'application/json'
    assert resp.json == {}


def test_accept_header(client, yaml):
    resp = client.get('/', headers={'accept': 'application/yaml'})
    assert resp.status_code == 200
    assert resp.content_type == 'application/yaml'
    assert yaml.load(resp.text) == {}


def test_path_parameter(client):
    resp = client.get('/test/first/second?names=terry')
    assert resp.status_code == 200
    assert resp.content_type == 'application/json'
    assert resp.json == {
        'names': ['Hello terry!'],
        'path': 'first/second',
    }
