import contextlib
import mimetypes

from collections.abc import Mapping, Sequence
from dataclasses import asdict, is_dataclass
from http import HTTPStatus
from io import BytesIO
from logging import getLogger
from pathlib import PurePosixPath as Path
from re import M as MULTILINE, U as UNICODE, compile
from typing import Any, NamedTuple

from bs4 import BeautifulSoup
from jinja2 import Environment
from more_itertools import unique_everseen
from PIL import Image
from public import public
from pydantic import BaseModel
from werkzeug.datastructures import Headers
from werkzeug.wrappers import Response

from omniblack.app import config
from omniblack.string_case import Cases

from .theme import get_theme
from .url import url_for

try:
    import attrs
except ImportError:
    attrs = None

try:
    from box import Box, BoxList
except ImportError:
    Box = None
    BoxList = None

log = getLogger(__name__)


@public
class ResponsePieces(NamedTuple):
    """Represents a response to an HTTP request.

    Routes can return an instance of this type instead of a :type:`Response`
        when they want to customize the response, but still want the body
        to be processed by the default logic.
        Especially useful for template routes where returning a
        :type:`Response` skips the template entirely.
    """

    body: Any = None
    """Supports NamedTuples, attrs, Box, BoxList, and dataclasses"""

    status: int | str | HTTPStatus = None

    headers: Headers | dict[str, str] | Sequence[tuple[str, str]] = None

    mimetype: str = None

    content_type: str = None

    def merge_resp(self, resp):
        if self.headers is not None:
            resp.headers.extend(self.headers)

        if self.status is not None:
            resp.status = self.status

        if self.mimetype is not None:
            resp.mimetype = self.mimetype

        if self.content_type is not None:
            resp.content_type = self.content_type


mimetypes.init()


def prepare_all_files(file):
    all_files = []

    all_files.append(file)
    file_path = Path(file)
    is_html = '.html' in file_path.suffixes
    is_jinja = '.jinja' in file_path.suffixes

    if not is_html:
        all_files.append(f'{file}.html')

    if not is_jinja:
        all_files.append(f'{file}.jinja')
        if not is_html:
            all_files.append(f'{file}.html.jinja')

    return tuple(all_files)


custom_el_pattern = compile(r'\w*-\w*')


def mithril_component(component_name):
    component_name = Cases.Pascal.to(component_name)

    url = url_for(
        f'/components/{component_name}.js',
        package='mithril',
        external=True,
        hostname_elide=True,
    )
    return component_name, url


component_namespaces = {
    'mithril': mithril_component,
}


def create_import(name, url):
    return 'import {' + name + '} from "' + url + '";'


def enhance_image(tag):
    if 'src' not in tag:
        return

    resp = config.http.get(tag['src'])
    if not resp.ok:
        return

    file_size = len(resp.content)
    image_io = BytesIO(resp.content)
    image = Image.open(image_io)
    width, height = image.size
    tag.attrs.setdefault('width', width)
    tag.attrs.setdefault('height', height)
    tag['file-size'] = file_size


def enhance_custom_tag(tag):
    match tag.name:
        case 'mithril-image':
            enhance_image(tag)

    return None


def get_tag_names(tag):
    return tuple(tag.name.split('-', 1)) + (tag.name,)


def process_custom_elements(doc):
    cust_el_tags = [
        *doc.find_all(custom_el_pattern),
    ]

    for tag in cust_el_tags:
        enhance_custom_tag(tag)

    custom_el_names = [
        get_tag_names(tag)
        for tag in cust_el_tags
    ]

    custom_el_defs = [
        component_namespaces[namespace](name)
        for namespace, name, tag_name in unique_everseen(
            custom_el_names,
        )
        if namespace in component_namespaces
    ]

    if not custom_el_names:
        return

    imports = [
        create_import(name, url)
        for name, url in custom_el_defs
    ]

    names = [name for name, url in custom_el_defs]

    constructor_array = f'[{", ".join(names)}]'

    loop = (
        f'for (const el of {constructor_array}) {"{"}'
        + 'el.register();'
        + '}'
    )

    script = doc.new_tag('script', type='module')

    script.extend(imports)
    script.append(loop)
    doc.head.append(script)


whitespace = compile(r'\s', UNICODE | MULTILINE)


def process_styles(doc):
    final_style = doc.new_tag('style')
    included_text = set()

    for style in doc.body.find_all('style'):
        if not style.find_parent('template'):
            style_text = whitespace.sub('', style.get_text())
            if style_text not in included_text:
                included_text.add(style_text)
                final_style.extend(style_text)
                style.decompose()

    if included_text:
        doc.head.append(final_style)


def process_template(
    file,
    jinja: Environment,
    data: Mapping,
    response_params: Mapping,
):
    files = prepare_all_files(file)
    template = jinja.get_or_select_template(files)
    template_path = Path(template.filename)

    response_params = {} if response_params is None else response_params.copy()

    headers = None
    if isinstance(data, tuple):
        data, headers = data

    mime_type = response_params.get('mimetype')

    with contextlib.suppress(KeyError):
        del response_params['mimetype']

    if template_path.suffixes and not mime_type:
        suffixes = template_path.suffixes
        if suffixes[-1] == '.jinja':
            suffixes = suffixes[:-1]

        if suffixes:
            suffix = ''.join(suffixes)
            suffix = mimetypes.suffix_map.get(suffix, suffix)
            mime_type = mimetypes.types_map[suffix]

    if mime_type is None:
        mime_type = 'text/html'

    data = data if data is not None else {}

    if isinstance(data, Mapping):
        data['theme'] = get_theme()

    out = template.render(data)
    if mime_type == 'text/html':
        doc = BeautifulSoup(out, 'lxml')

        process_custom_elements(doc)
        process_styles(doc)
        out = doc.encode(formatter='html5')

    return Response(
        out,
        mimetype=mime_type,
        headers=headers,
        **response_params,
    )


def is_namedtuple(obj) -> bool:
    return (
        isinstance(obj, tuple) and
        hasattr(obj, '_asdict') and
        hasattr(obj, '_fields')
    )


def prepare_return(ret):
    if isinstance(ret, BaseModel):
        return ret.dict()
    elif is_dataclass(ret):
        return asdict(ret)
    elif is_namedtuple(ret):
        return ret._asdict()
    elif attrs and attrs.has(type(ret)):
        return attrs.asdict(ret, recurse=True)
    elif Box and isinstance(ret, Box | BoxList):
        return ret.to_dict()
    else:
        return ret
