import logging

from importlib.resources import files

from msgspec import Struct
from tinycss2 import (
    parse_declaration_list,
    parse_one_component_value,
    parse_stylesheet,
    serialize,
)
from tinycss2.ast import AtRule

log = logging.getLogger(__name__)


class Variable(Struct):
    name: str
    value: str


class ThemeStorage:
    def _fix_key(self, key):
        return key.removeprefix('--omniblack-').replace('-', '_')

    def __getitem__(self, key):
        return getattr(self, self._fix_key(key))

    def __rich_repr__(self):
        yield from self.__dict__.items()


class Theme(ThemeStorage):
    def __init__(self):
        # TODO re-implement Variable resolution
        self.resolved = ThemeStorage()

    def add_color(self, prop: Variable):
        new_value = prop.value
        key = self._fix_key(prop.name)
        setattr(self.resolved, key, new_value)
        setattr(self, key, prop.value)


def parse_property_rule(rule: AtRule):
    decs = parse_declaration_list(
        rule.content,
        skip_whitespace=True,
        skip_comments=True,
    )

    for dec in decs:
        if dec.type == 'declaration' and dec.lower_name == 'initial-value':
            value = parse_one_component_value(dec.value)
            break

    name = parse_one_component_value(rule.prelude)

    return Variable(
        name=serialize([name]),
        value=serialize([value]),
    )


def get_theme():
    theme = Theme()
    theme_text = files('omniblack.mithril').joinpath('theme.css').read_text()
    css_sheet = parse_stylesheet(
        theme_text,
        skip_comments=True,
        skip_whitespace=True,
    )

    properties = [
        rule
        for rule in css_sheet
        if rule.type == 'at-rule'
        if rule.lower_at_keyword == 'property'
    ]

    props = [
        parse_property_rule(rule)
        for rule in properties
    ]

    for prop in props:
        theme.add_color(prop)

    return theme
