from itertools import chain
from re import compile
from urllib.parse import parse_qs, urlencode

from packaging.utils import canonicalize_name
from public import public
from urllib3.util import Url, parse_url

from omniblack.app import config

mutli_slash = compile('/{2,}')


def clean_url(url):
    url = mutli_slash.sub('/', url)
    url = url.removesuffix('/').removeprefix('/')
    return url


def join_url_paths(*paths):
    paths = (
        path.split('/')
        for path in paths
    )

    segs = (seg for seg in chain.from_iterable(paths) if seg)

    return '/' + '/'.join(segs)


def merge_urls(base, url):
    if not isinstance(base, Url):
        base = parse_url(base)

    if not isinstance(url, Url):
        url = parse_url(url)

    pieces = {
        'scheme': base.scheme or url.scheme,
        'auth': base.auth or url.auth,
        'host': base.host or url.host,
        'port': base.port or url.port,
        'fragment': url.fragment or base.fragment,
    }

    query_dict = parse_qs(base.query) | parse_qs(url.query)

    pieces['query'] = (
        urlencode(query_dict, doseq=True)
        if query_dict
        else None
    )

    if url.path is not None:
        path = (
            url.path
            if base.path is None
            else join_url_paths(base.path, url.path)
        )

        pieces['path'] = path
    else:
        pieces['path'] = base.path

    url = Url(**pieces)

    return url


@public
def url_for(path, *, package=None, external=False, hostname_elide=False):
    if package is None:
        package = config.app.package_config.name

    package = canonicalize_name(package)

    deployment_url = (
        config.deployment.public_url
        if external
        else config.deployment.cluster_url
    )

    for deployed in config.deployment.packages:
        if canonicalize_name(deployed.package) == package:
            deployed_package = deployed
            break
    else:
        msg = f'Could not find deployed package {package}'
        raise ValueError(msg)

    package_url = merge_urls(deployment_url, deployed_package.url)
    full_url = merge_urls(package_url, path)

    if hostname_elide and full_url.netloc == deployment_url.netloc:
        return full_url.request_uri
    else:
        return str(full_url)


@public
def url_for_template(
    path,
    *,
    package=None,
    external=True,
    hostname_elide=True,
):
    return url_for(
        path,
        package=package,
        external=external,
        hostname_elide=hostname_elide,
    )
