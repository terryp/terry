from logging import getLogger
from multiprocessing import Process, current_process

from gunicorn.glogging import Logger

from omniblack.app import config as _app_config

log = getLogger(__name__)

accesslog = '-'
access_log_format = (
        '%(h)s %(l)s %(u)s "%(m)s %({http_x_forwarded_prefix}e)s'
        + '%(U)s?%(q)s %(H)s" %(s)s "%(f)s" "%(a)s"'
)


class LoggerClass(Logger):
    def __init__(self, cfg):
        super().__init__(cfg)
        self.error_log.propagate = True
        self.access_log.propagate = True

    def setup(self, _cfg):
        return None


logger_class = LoggerClass


def get_name():
    display_name = _app_config.app.display_name
    return getattr(display_name, 'en', display_name)


def run_startup(server):
    # load our app
    server.app.wsgi()
    if hasattr(server.app.callable, 'startup'):
        server.app.callable.setup()
        app_name = get_name()
        current_process().name = f'{app_name} Background Startup'
        server.app.callable.startup()


def post_worker_init(worker):
    current_process().name = f'Starting Worker {worker.age}'
    worker.app.callable.setup()
    app_name = get_name()
    current_process().name = f'{app_name} Worker {worker.age}'


def on_starting(server):
    global p

    # Start the startup background process
    p = Process(
        target=run_startup,
        args=(server,),
        name='Background-Startup'
    )
    p.start()
    server.app.wsgi()
    server.app.callable.setup()
    app_name = get_name()
    current_process().name = f'{app_name} Arbiter'


def when_ready(_server):
    p.join()
