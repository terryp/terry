import sys

from contextlib import ExitStack
from functools import wraps
from importlib import import_module
from importlib.resources import files
from logging import getLogger
from operator import methodcaller

from jinja2 import ChoiceLoader, Environment, select_autoescape
from public import public
from ruamel.yaml import YAML
from werkzeug.datastructures import MultiDict
from werkzeug.exceptions import HTTPException
from werkzeug.wrappers import Request, Response

from omniblack.app import config
from omniblack.mongo import MongoOptions, MongoStorage

from .context import _request
from .jinja import ResourceLoader
from .url import url_for_template

try:
    import attrs
except ImportError:
    attrs = None

try:
    from box import Box, BoxList
except ImportError:
    Box = None
    BoxList = None


try:
    from .yaml import yaml
except ImportError:
    yaml = None

from .model import BodyUsage, no_body
from .response import ResponsePieces, prepare_return, process_template
from .routing import get_handler

self_mod = import_module(__name__)

log = getLogger(__name__)


def create_templates(package, pages):
    env = Environment(
        loader=ChoiceLoader(
            [
                ResourceLoader(package, 'templates'),
                ResourceLoader('omniblack.mithril', 'templates'),
            ]
        ),
        autoescape=select_autoescape(),
        auto_reload=True,
    )

    env.globals['pages'] = pages
    env.globals['url_for'] = url_for_template
    env.globals['config'] = config

    return env


def handle_files(req: Request, field):
    return req.files[field.meta.name]


def parse_body(model, req: Request):
    if req.mimetype not in model.formats.by_mime_type:
        return None, {}

    req_format = model.formats.by_mime_type[req.mimetype]
    return req_format.name, req_format.load(req.stream)


def serialize(model, mime_type, data):
    selected_format = model.formats.by_mime_type[mime_type]
    return selected_format.dumps(data)


def parse_data(req, field, body_data, body_format, args):
    is_seq = field.is_a.type == 'list'
    getitem = (
        methodcaller('getlist', field.meta.name)
        if is_seq
        else methodcaller('get', field.meta.name)
    )

    if field.meta.name in req.form:
        format_name = 'http_form'
        value = getitem(req.form)
    elif field.meta.name in req.args:
        format_name = 'query_string'
        value = getitem(req.args)
    elif field.meta.name in body_data:
        format_name = body_format
        value = body_data[field.meta.name]
    else:
        return

    if (is_seq and not value) or value is None:
        return

    parsed_value = field.model.types.from_format(format_name, value, field)
    args[field.meta.name] = parsed_value


def prepare_request(environ):
    """
    Turn the WSGI environ into a werkzeug request.
    """
    return OmniblackRequest(environ)


__all__ = ['Server']


class OmniblackRequest(Request):
    dict_storage_class = MultiDict


@public
class Server:
    def __init__(
        self,
        dist_name,
        model,
        middleware=(),
        plugins=(),
        module=None,
        option_structs=None,
    ):
        if module is None:
            try:
                frame = sys._getframe(1)  # noqa: SLF001
                module = frame.f_globals['__name__']
            finally:
                del frame

        self.dist_name = dist_name
        self.host_module = module
        self.model = model
        self.option_structs = option_structs

        self.unloaded_plugins = plugins
        self._server_middleware = middleware

    def setup(self):
        option_structs = [
            MongoOptions,
            self.model.structs.server,
            *(self.option_structs or ()),
        ]

        config.load_app(
            self.dist_name,
            model=self.model,
            module=self.host_module,
            plugins=self.unloaded_plugins,
            option_structs=option_structs,
        )

        storage = self.prepare_storage()
        config.finish_setup(storage=storage)

        self._create_middleware()

        deployment = self.model.structs.deployment.load_file(
            config.server.deployment_path,
        )
        config.deployment = deployment

        pages_file = files(self.host_module).joinpath('pages.yaml')

        self.pages = {}
        if pages_file.is_file():
            yaml = YAML()
            with pages_file.open('r') as pages_stream:
                self.pages = yaml.load(pages_stream)

        self.jinja = create_templates(self.dist_name, self.pages)

    def _create_middleware(self):
        middleware = list(self._server_middleware)

        for plugin in config.plugins:
            middleware.extend(plugin.middlewares)

        self.middleware = tuple(middleware)

    def __call__(self, environ, start_response):
        with config.task():
            middleware_stack = ExitStack()
            config.enter_context(middleware_stack)

            try:
                req = prepare_request(environ)
                req.base_path = req.headers['X-Forwarded-Prefix'] or ''
                _request.set(req)

                handler, template, route_data = get_handler(
                    req,
                    self.host_module,
                )

                self._enter_middleware(middleware_stack, handler)

                args = self.parse_args(req, handler, route_data)

                raw_ret = handler(**args)
            except HTTPException as err:
                resp = err.get_response(environ)

                return resp(environ, start_response)

            response_params = {}

            if isinstance(raw_ret, Response):
                return raw_ret(environ, start_response)

            selected_mime = None
            if isinstance(raw_ret, ResponsePieces):
                response_params = raw_ret._asdict()
                raw_ret = raw_ret.body
                del response_params['body']

            prepared = prepare_return(raw_ret)

            if template is not None:
                response = process_template(
                    template,
                    self.jinja,
                    prepared,
                    response_params,
                )
            else:
                selected_mime = response_params.get('mimetype')
                return_mime = selected_mime or req.accept_mimetypes.best_match(
                    tuple(config.model.formats.by_mime_type),
                    'application/json',
                )

                serialized = serialize(config.model, return_mime, prepared)
                response = Response(
                    serialized,
                    mimetype=return_mime,
                    **response_params,
                )

            middleware_stack.close()
            return response(environ, start_response)

    def _enter_middleware(self, middleware_stack, handler):
        for middleware in self.middleware:
            result = middleware(server=self, handler=handler)
            if result is not None:
                middleware_stack.enter_context(result)

    def parse_args(self, req: Request, handler, route_data):
        """
        Return request arguments from the request.
        They will have been parsed into their respective python types.
        """
        args = {} | route_data

        body_usage: BodyUsage = handler.body_usage

        if body_usage == BodyUsage.unused and req.method not in no_body:
            body_format, body_data = parse_body(self.model, req)
        else:
            body_data = {}
            body_format = None

        for field in handler.fields:
            if field.meta.name in args:
                # parameter was parsed from the path.
                continue

            match field.is_a.type:
                case 'File':
                    args[field.meta.name] = req.files[field.meta.name]
                case 'Body':
                    args[field.meta.name] = req.stream
                case _:
                    parse_data(req, field, body_data, body_format, args)

        return args

    def prepare_storage(self):
        return MongoStorage(
            config.mongo.mongo_url,
            model=config.model,
            uuidRepresentation='standard',
            username=config.mongo.mongo_username,
            password=config.mongo.mongo_password.reveal(),
        )

    def helper(self, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            self.setup()
            return func(*args, **kwargs)

        return wrapper
