from collections.abc import Sequence
from importlib import import_module
from itertools import zip_longest
from logging import getLogger

from werkzeug.exceptions import MethodNotAllowed, NotFound
from werkzeug.wrappers import Request

from .model import RouteBase
from .url import clean_url

log = getLogger(__name__)


def is_parent(err, target):
    err_path = [
        seg
        for seg in err.split('.')
        if seg
    ]

    target_path = [
        seg
        for seg in target.split('.')
        if seg
    ]

    for (err_seg, target_seg) in zip_longest(err_path, target_path):
        if target_seg is None:
            return False
        elif err_seg is None:
            return True
        elif err_seg != target_seg:
            return False
    else:
        return True


def py_path(segs, package):
    package = (
        pkg
        for pkg in package.split('.')
        if pkg
    )

    all_segments = (*package, 'routes', *segs)
    return '.'.join(all_segments)


def route_paths(route_path: Sequence[str], package: str):
    yield py_path((*route_path, 'root'), package), 'root'
    yield py_path(route_path, package), 'root'

    if route_path:
        *route_path, name = route_path
        yield py_path((*route_path, 'root'), package), name
        yield py_path(route_path, package), name


def import_route(route_path: Sequence[str], package: str):
    for path, name in route_paths(route_path, package):
        try:
            mod = import_module(path)
        except ModuleNotFoundError as err:
            if not is_parent(err.name, path):
                raise err
            else:
                continue
        except ImportError:
            continue

        try:
            route = getattr(mod, name)
        except AttributeError:
            continue

        return (True, route)
    else:
        return (False, None)


def iter_req_path(req_path):
    for num in range(len(req_path), -1, -1):
        yield req_path[:num], req_path[num:]


def get_handler(req: Request, package: str):
    """
    Return the handler that matches `req`

    Args:
        req: The request to match.
        package: The package to search for routes in.
    """
    req_path = [seg for seg in clean_url(req.path).split('/') if seg]

    possible_method_mismatch = False

    for req_path, rest_of_path in iter_req_path(req_path):
        (found, route) = import_route(req_path, package)

        if found and isinstance(route, RouteBase):
            try:
                matched, data = route.match(rest_of_path, req)
            except MethodNotAllowed:
                possible_method_mismatch = True
            else:
                if matched:
                    handler = route.handlers[req.method]
                    return (
                        handler,
                        route.return_templates.get(req.method, None),
                        data,
                    )

    if possible_method_mismatch:
        raise MethodNotAllowed(tuple(route.methods))
    else:
        return NotFound()
