import mimetypes
import sys

from abc import ABC
from collections.abc import Mapping, Sequence
from enum import Enum
from functools import partial
from http import HTTPMethod
from inspect import Parameter, getmodule, signature
from logging import getLogger
from pathlib import PurePosixPath as Path
from string import Template
from types import MethodType
from typing import (
    GenericAlias,
    get_args,
    get_origin,
)
from warnings import warn

from jinja2 import Environment
from public import public
from werkzeug import Request, Response
from werkzeug.exceptions import MethodNotAllowed

from omniblack.app import config
from omniblack.entry import EntryBase, entry as create_entry

from .parse_url_template import RoutePart, parse_template

log = getLogger(__name__)


@public
class BodyReused(UserWarning):
    """
        The route handler requests the body be parsed twice.
        This is not possible.
    """


no_body = {
    'GET',
    'HEAD',
    'CONNECT',
    'TRACE',
}


@public
class BodyNotAllowed(UserWarning):
    """Bodies are not allowed for these http methods:
    """

    for _method in sorted(no_body):
        __doc__ += f'\n* :HTTP_METHOD:`{_method}`'


@public
class Seq(ABC):  # noqa: B024
    """
    An abstract base class, similar to
    :py:class:`Sequence<collections.abc.Sequence>`,
    but doesn't treat :py:class:`str` or
    :py:class:`bytes` as subclasses.
    """
    @classmethod
    def __subclasshook__(cls, other_cls):
        if cls is not Seq:
            return NotImplemented
        elif issubclass(other_cls, str | bytes):
            return False
        else:
            return NotImplemented


Seq.register(Sequence)


class method:
    __slots__ = (
        '__name__',
        '__doc__',
        '__qualname__',
        '__instance_method',
        '__cls_method',
    )

    def __init__(self, instance_method):
        self.__instance_method = instance_method
        self.__cls_method = None

    def __get__(self, instance, owner=None):
        if instance is not None:
            return MethodType(self.__instance_method, instance)
        else:
            return MethodType(self.__cls_method, owner)

    def __repr__(self):
        return repr(self.__instance_method)

    def cls_method(self, cls_method):
        self.__cls_method = cls_method
        return self


def create_register_methods(body: dict):
    for http_method in HTTPMethod:
        http_method = http_method.value.lower()
        method = create_register_method(http_method)
        body[http_method] = method


register_method_doc = Template("""
Register `handler` to handler ${method} requests.
""")


def create_register_method(http_method: str):
    method_name = http_method
    http_method = http_method.upper()

    def register_inst(self, method=None, /, *, template=None, _entry=None):
        if method is None:
            if template is None:
                raise TypeError(
                    f'route.{http_method} must be called '
                    + 'with a handler, template.',
                )
            else:
                return partial(register_inst, self, template=template)

        if _entry is None:
            entry = create_entry(config.model, method)
        else:
            entry = _entry

        entry.public = getattr(method, 'public', False)
        self._last_handler = entry

        add_route_annotation(entry)

        if entry.body_usage and http_method.upper() in no_body:
            warn(
                f'Bodies are not allowed for {http_method} requests.',
                BodyNotAllowed,
                stacklevel=2,
            )

        self.methods.add(http_method)
        self.handlers[http_method] = entry
        self.return_templates[http_method] = template
        return self

    register_inst.__name__ = http_method
    register_inst.__qualname__ = f'route.{method_name}'
    register_inst.__doc__ = register_method_doc.substitute(
        {'method': http_method},
    )

    def register_cls(cls, method=None, /, *, template=None, path=None):
        if method is None:
            if template is None and path is None:
                raise TypeError(
                    f'route.{http_method} must be called '
                    + 'with a handler, path or template.',
                )
            else:
                return partial(
                    register_cls,
                    cls,
                    template=template,
                    path=path,
                )

        entry = create_entry(config.model, method)

        self = cls(entry, route_name=method.__name__, path=path)

        return register_inst(self, entry, template=template)

    register_cls.__name__ = http_method
    register_cls.__qualname__ = f'route.{http_method}'
    register_cls.__doc__ = register_method_doc.substitute(
        {'method': http_method},
    )

    descriptor = method(register_inst)
    descriptor.cls_method(register_cls)

    descriptor.__name__ = http_method
    descriptor.__qualname__ = f'route.{http_method}'
    descriptor.__doc__ = register_method_doc.substitute(
        {'method': http_method},
    )

    return descriptor


def strip_routes(module_path):
    segs = iter(module_path.split('.'))

    for seg in segs:
        if seg == 'routes':
            break

    yield from segs


@public
class RouteBase:
    name: str
    path: tuple[RoutePart]
    template: str
    methods: set[HTTPMethod]
    handlers: dict[HTTPMethod, EntryBase]

    def __init__(
        self,
        name,
        path=None,
        methods=None,
        handlers=None,
    ):
        self.name = name
        self.methods = methods or set()
        self.handlers = handlers or {}
        self.path = path or ()
        self.return_templates = {}

    def match(self, remaining_path, req: Request):
        part_num = 0
        data = {}

        if req.method not in self.methods:
            raise MethodNotAllowed(self.methods)

        while len(self.path) > part_num < len(remaining_path):
            part = self.path[part_num]
            if part.rest:
                matched, part_data = part.match(remaining_path[part_num:])
                part_num = len(remaining_path)
            else:
                matched, part_data = part.match(remaining_path[part_num])

            if not matched:
                return False, data

            if part_data:
                data |= part_data

            part_num += 1

        for part in self.path[part_num:]:
            if part.required:
                return False, data

        if len(remaining_path) > part_num:
            return False, data

        return True, data


@public
class route(RouteBase):
    """Mark a handler function."""

    def __init__(
        self,
        entry,
        host_module=None,
        route_name=None,
        path=None,
    ):
        self._last_handler = None

        if host_module is None:
            if host_module := getmodule(entry):
                self.host_module = host_module.__name__
            else:
                try:
                    frame = sys._getframe(1)  # noqa SLF001
                    self.host_module = frame.f_globals['__name__']
                finally:
                    del frame
        else:
            self.host_module = host_module

        if path is not None:
            path = parse_template(path, entry.fields)

        super().__init__(
            name=route_name,
            path=path,
            methods=set(),
            handlers={},
        )

    create_register_methods(vars())

    @property
    def public(self):
        return False

    @public.setter
    def public(self, new_value):
        if self._last_handler is not None:
            self._last_handler.public = new_value


@public
class Body[T]:
    """
    Marker type that instructs omniblack.server
    to parse the body stream as :type:`T` type.

    Attributes
    ----------
    T:
        The type the body should be parsed into.
        Must be parsable by
        :type:`pydantic.type_adapter.TypeAdapter.validate_python`.
    """


@public
class File[F]:
    """
    Marker type that instructs ``omniblack.server`` to access multipart files.

    Attributes
    ----------
    F:
        If provided the files will be passed as an instance of :type:`F`.
        :type:`F` can be a subclass of

        * :type:`collections.abc.Mapping`
        * :type:`omniblack.server.Seq`

        :type:`F` must support construction via
        :type:`F._from_iterable(collections.abc.Iterable)`
        or :type:`F(collections.abc.Iterable)`.

        When :type:`F` is a :type:`collections.abc.Mapping` the iterable
        passed to the constructor will be
        :type:`collections.abc.Iterable[
            str,
            werkzeug.datastructures.FileStorage,
        ]`.
        When :type:`F` is a :type:`Seq` the iterable
        pass to the constructor will be
        :type:`collections.abc.Iterable[werkzeug.datastructures.FileStorage]`.

    Warning
    -------
    Generic args to :type:`F` are not considered.
    """


def is_file(to_check: type) -> bool:
    if isinstance(to_check, GenericAlias):
        origin = get_origin(to_check)
        if issubclass(origin, Mapping):
            _, value_type = get_args(to_check)
            return issubclass(value_type, File)
        elif issubclass(origin, Sequence):
            arg, *_ = get_args(to_check)
            return issubclass(arg, File)
        else:
            return False
    else:
        return issubclass(to_check, File)


def get_annotation(param: Parameter):
    if param.annotation is Parameter.empty:
        return str
    else:
        return param.annotation


class BodyUsage(Enum):
    stream = 'stream'
    parse = 'parse'
    files = 'files'
    unused = 'unused'

    def __bool__(self):
        return self != type(self).unused

    @classmethod
    def find_usage(cls, sig):
        params = sig.parameters.values()
        usage = cls.unused

        for param in params:
            param_type = get_annotation(param)

            if isinstance(param_type, GenericAlias):
                param_type = get_origin(param_type)

            if param_type in use_body_types:
                if usage is cls.unused:
                    if issubclass(param_type, Body):
                        if get_args(param_type):
                            usage = cls.parse
                        else:
                            usage = cls.stream
                    elif is_file(param_type):
                        usage = cls.files
                else:
                    warn(
                        f'{param.name} would be a reuse of the body.',
                        BodyReused,
                        stacklevel=3,
                    )

        return usage


def has_rest_path(sig):
    try:
        param = next(reversed(sig.parameters.values()))

        param_type = get_annotation(param)

        if issubclass(param_type, Path):
            return (param.name, param_type)

    except StopIteration:
        pass

    return (None, None)


def add_route_annotation(func):
    sig = signature(func)
    func.body_usage = BodyUsage.find_usage(sig)
    rest_path, path_cls = has_rest_path(sig)
    func.rest_path = rest_path
    func.path_cls = path_cls


use_body_types = {File, Body}


mimetypes.init()


def prepare_all_files(files):
    all_files = []

    for file in files:
        all_files.append(file)
        file_path = Path(file)
        is_html = '.html' in file_path.suffixes
        is_jinja = '.jinja' in file_path.suffixes

        if not is_html:
            all_files.append(f'{file}.html')

        if not is_jinja:
            all_files.append(f'{file}.jinja')
            if not is_html:
                all_files.append(f'{file}.html.jinja')

    return tuple(all_files)


@public
class render:
    """Prepare a template to be rendered.

    The return from this function, should be return from the route handler.
    """

    def __init__(
        self,
        file: str | Sequence[str],
        context: Mapping,
        *,
        mime_type: str = None,
    ):
        self.ctx = context

        files = file if isinstance(file, Seq) else (file,)

        self.files = prepare_all_files(files)

        self.mime_type = mime_type

    def __call__(self, jinja: Environment):
        template = jinja.get_or_select_template(self.files)
        template_path = Path(template.filename)

        mime_type = None
        if self.mime_type is not None:
            mime_type = self.mime_type
        elif template_path.suffixes:
            suffixes = template_path.suffixes
            if suffixes[-1] == '.jinja':
                suffixes = suffixes[:-1]

            if suffixes:
                suffix = ''.join(suffixes)
                suffix = mimetypes.suffix_map.get(suffix, suffix)
                mime_type = mimetypes.types_map[suffix]

        if not mime_type:
            mime_type = 'text/html'

        gen = template.render(self.ctx)
        return Response(gen, content_type=mime_type)
