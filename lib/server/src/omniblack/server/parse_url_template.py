from public import public

from .url import clean_url


@public
class StaticPart(str):
    __slots__ = ('required', 'rest')

    def __new__(cls, *, required=True, rest=False):
        self = super().__new__(cls)
        self.required = required
        self.rest = rest
        return self

    def match(self, value: str):
        return value == self, {}


@public
class DynamicPart:
    def __init__(self, name, field, *, rest=False):
        self.name = name
        self.field = field
        self.rest = rest

    def __rich_repr__(self):
        yield self.name
        yield 'type', self.field.is_a.type

    def __repr__(self):
        cls = self.__class__
        type_name = self.field.is_a.type
        return f'{cls.__name__}({self.name}, type={repr(type_name)})'

    def match(self, value: str | tuple[str, ...]):
        data_format = 'path_segment' if not self.rest else 'path_parts'

        try:
            converted = self.field.model.types.from_format(
                data_format,
                value,
                self.field,
            )
        except ValueError:
            return False, {}

        return True, {self.name: converted}

    @property
    def required(self):
        return self.field.is_a.required


type RoutePart = DynamicPart | StaticPart

public(RoutePart=RoutePart)


def convert_part(part, fields):
    not_escaped_end = part.endswith('>') and not part.endswith(r'\>')
    if part.startswith('<') and not_escaped_end:
        name = part.removeprefix('<').removesuffix('>')
        rest = name.startswith('*')
        name = name.removeprefix('*')
        if not name.isidentifier():
            msg = 'url parameters must be valid identifiers'
            raise ValueError(msg)
        else:
            return DynamicPart(name, fields[name], rest)
    else:
        return StaticPart(part)


def parse_template(url_template, fields):
    if url_template is None:
        return None

    if url_template.startswith('/'):
        msg = 'Url Templates must not be an absolute path'
        raise ValueError(msg)

    fields = {
        field.meta.name: field
        for field in fields
    }

    template_parts = tuple(
        convert_part(part, fields)
        for part in clean_url(url_template).split('/')
    )

    iter_template = iter(template_parts)

    rest_seen = False
    for part in iter_template:
        if rest_seen:
            msg = 'Path parameter follows rest path parameter'
            raise ValueError(msg)

        if not part.required:
            break

        if part.rest:
            rest_seen = True

    for part in iter_template:
        if rest_seen:
            msg = 'Path parameter follows rest path parameter'
            raise ValueError(msg)

        if part.required:
            msg = (
                'Required path parameters cannot follow an optional parameter.'
            )
            raise ValueError(msg)

        if part.rest:
            rest_seen = True

    return template_parts
