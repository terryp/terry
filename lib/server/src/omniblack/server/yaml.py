from ruamel.yaml import YAML

from omniblack.context_proxy import ContextProxy

yaml = ContextProxy(init=lambda: YAML(typ='safe'), name='YAML')
