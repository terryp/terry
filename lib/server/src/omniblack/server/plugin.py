from __future__ import annotations

from collections.abc import Callable
from contextlib import AbstractContextManager
from typing import TYPE_CHECKING

from public import public

from omniblack.model import StructBase

if TYPE_CHECKING:
    from .main import Server


type Middleware = Callable[[Server], AbstractContextManager]


@public
class ServerPlugin:
    plugins: tuple[ServerPlugin] = ()
    option_structs: tuple[StructBase] = ()
    middlewares: tuple[Middleware] = ()

    def __init__(self, server):
        self.server = server
