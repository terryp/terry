from os import fspath
from pathlib import PurePosixPath

from public import public

from omniblack.model import (
    AttributeFields,
    ModelType,
    ValidationResult,
    native_adapter,
    walk,
)


class UrlPath(ModelType):
    implementation = PurePosixPath

    def from_string(self, string: str):
        try:
            return self.implementation(string)
        except TypeError as err:
            msg = 'Invalid path'
            raise ValueError(msg, string) from err

    def to_string(self, value: PurePosixPath):
        return fspath(value)

    def from_path_parts(self, value: tuple[str, ...]):
        return self.implementation(*value)

    def validator(self, _path):
        messages = []
        return ValidationResult(not messages, messages)


class File(ModelType):
    def validator(self, _value):
        return ValidationResult([], valid=True)

    attributes = AttributeFields(
        {
            'name': 'allowed_content_types',
            'display': {
                'en': 'Allowed Content Types',
            },
            'desc': {
                'en': 'The Content types that should be accepted.',
            },
            'is_a': {
                'type': 'list',
                'list_attrs': {
                    'item': {
                        'type': 'string',
                    },
                },
            },
        },
    )


class IdBase(str):
    __slots__ = ()


class Id(ModelType):
    implementation = IdBase

    to_string = native_adapter
    from_string = native_adapter

    def validator(self, _value, _path):
        return ValidationResult([], valid=True)


class ReferBase(str):
    __slots__ = ()


@public
class sort_by_deps:
    """A key function that can be used in `sorted` or `list.sort`.

    The resulting order should allow the records to be saved without
    a missing reference error.
    NOTE: this will not correctly handle circular deps, or colliding ids.
    """

    def __init__(self, indiv):
        deps_gen = walk(indiv, should_visit=self.should_visit)
        self.id = indiv._id
        self.deps = {
            value
            for path, value, field, is_a in deps_gen
        }

    def should_visit(self, _path, _value, _field, is_a):
        return is_a.type == 'reference'

    def check_circle(self, other):
        if self.id in other.deps and other.id in self.deps:
            msg = 'Circular dep'
            raise RuntimeError(msg, self.id, other.id)

    def __lt__(self, other):
        if not isinstance(other, sort_by_deps):
            return NotImplemented

        self.check_circle(other)
        # `sorted` returns smallest to largest
        # so when we return `False` it means
        # we come after `other` so if we
        # depend on `other` we return False
        if other.id in self.deps:
            return False
        elif self.id in other.deps:
            return True

        # fallback to sorting by id
        # Doesn't really matter just gives us an order
        # if two records don't depend on each other.
        return self.id <= other.id


class Reference(ModelType):
    implementation = ReferBase

    def from_string(self, value):
        return ReferBase(value)

    def to_string(self, value):
        return str(value)

    def validator(self, _value):
        return ValidationResult([], valid=True)

    attributes = AttributeFields(
        {
            'name': 'struct',
            'display': {'en': 'Referenced Struct'},
            'desc': {'en': 'The struct this field refers to.'},
            'is_a': {'type': 'string'},
        }
    )
