from public import public

from omniblack.model import Plugin, TypeExt
from omniblack.mongo import MongoPlugin

from ..model import Seq
from .types import File, Id, Reference, UrlPath


class List(TypeExt):
    name = 'list'

    def from_query_string(self, value):
        if isinstance(value, Seq):
            return self.from_generic(value, 'query_string')
        else:
            coerced_value = self.model.types.from_format(
                format_name='query_string',
                value=value,
                field=self.field,
                is_a=self.attrs.item,
            )

            return [coerced_value]


@public
class ServerModelPlugin(Plugin):
    types = (File, UrlPath, Id, Reference)
    struct_packages = ('omniblack.server.structs',)
    plugins = (MongoPlugin,)

    def on_create_class(self, _name, _bases, _attrs, struct_def):
        UiString = self.model.meta_structs.UIString
        IsA = self.model.meta_structs.IsA
        Field = self.model.meta_structs.Field

        if struct_def.get('top_level_struct'):
            en_display = struct_def['display']['en']

            id_field = Field(
                name='_id',
                display=UiString(en='Id'),
                desc=UiString(
                    en=f"A unique identifier for this '{en_display}'."
                ),
                required=True,
                is_a=IsA(type='id'),
                added_by='omniblack.server',
            )
            struct_def['fields'].insert(0, id_field)

            version_field = Field(
                name='version',
                display=UiString(en='Version'),
                desc=UiString(
                    en=f"The current version of this '{en_display}'.",
                ),
                required=False,
                is_a=IsA(type='integer'),
                added_by='omniblack.server',
            )
            struct_def['fields'].append(version_field)

            audit_field = Field(
                name='audit',
                display=UiString(en='Audit Log'),
                desc=UiString(
                    en=(
                        'A log of changes that have'
                        + f" occurred to this '{en_display}'.",
                    ),
                ),
                required=False,
                is_a=IsA(
                    type='list',
                    list_attrs={
                        'item': {
                            'type': 'child',
                            'child_attrs': {'struct': 'diff'},
                        },
                    },
                ),
                added_by='omniblack.server',
            )
            struct_def['fields'].append(audit_field)
