from jinja2.ext import Environment, Extension


class CustomElementExtension(Extension):
    """Support custom element use."""

    tag = {'custom'}

    def __init__(self, env: Environment) -> None:
        self.env = env
