from importlib import import_module
from importlib.resources import files
from importlib.resources.abc import Traversable

from .model import route


def load_routes(package: str) -> dict[str, route]:
    all_files = files(package).joinpath('routes')
    return tuple(_load_routes(all_files, package))


def _load_routes(directory: Traversable, prefix: str):
    for entry in directory.iterdir():
        name = entry.name.removesuffix('.py')
        full_py_path = prefix + '.' + name
        if entry.is_file() and entry.name.endswith('.py'):
            full_mod_path = prefix + '.' + name
            yield import_module(full_mod_path)
        elif entry.is_dir():
            yield from _load_routes(entry, full_py_path)
