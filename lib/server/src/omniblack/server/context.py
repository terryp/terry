from contextvars import ContextVar

from public import public
from werkzeug.local import LocalProxy

_request = ContextVar('request')
current_request = LocalProxy(_request)
public(current_request=current_request)
