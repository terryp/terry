from hashlib import md5
from importlib.resources import files
from os import fspath
from pathlib import Path

from jinja2 import BaseLoader, TemplateNotFound


class ResourceLoader(BaseLoader):
    def __init__(self, package, template_directory='templates'):
        self.files = files(package).joinpath(template_directory)

    def get_source(self, _environment, template):
        template_file = self.files.joinpath(template)

        if not template_file.is_file():
            raise TemplateNotFound(template)

        with template_file.open() as file:
            source = file.read()

        source_bytes = source.encode('utf-8')
        checksum = md5(source_bytes, usedforsecurity=False).hexdigest()

        file_path = None
        if isinstance(template_file, Path):
            file_path = fspath(template_file)
        else:
            file_path = template_file

        return source, file_path, self.create_update(checksum, template_file)

    def create_update(self, checksum, template_file):
        def is_up_to_date():
            with template_file.open() as file:
                source = file.read()

            source_bytes = source.encode('utf-8')
            new_checksum = md5(source_bytes, usedforsecurity=False).hexdigest()

            return new_checksum == checksum

        return is_up_to_date
