export class Node {
    constructor({ loc = null, type = new.target.name }) {
        this.type = type;
        this.loc = loc;
    }
}

export class Program extends Node {
    constructor({ body, loc }) {
        super({ loc });
        this.body = body;
    }
}

export class Expression extends Node {}

export class LogicalExpression extends Expression {
    constructor({ loc, operator, left, right }) {
        super({ loc });
        this.operator = operator;
        this.left = left;
        this.right = right;
    }
}

export class Literal extends Expression {
    constructor({ value, loc }) {
        super({ loc });
        this.value = value;
    }
}

export class Identifier extends Expression {
    constructor({ name, loc }) {
        super({ loc });
        this.name = name;
    }
}

export class ModuleSpecifier extends Node {
    constructor({ local, loc }) {
        super({ loc });
        this.local = local;
    }
}

export class ImportSpecifier extends ModuleSpecifier {
    constructor({ local, imported = local, loc }) {
        super({ local, loc });
        this.imported = imported;
    }
}

export class ExportSpecifier extends ModuleSpecifier {
    constructor({ local, exported = local, loc }) {
        super({ local, loc });
        this.exported = exported;
    }
}

export class ModuleDeclaration extends Node {}

export class ImportDeclaration extends ModuleDeclaration {
    constructor({ specifiers, source, loc }) {
        super({ loc });
        this.specifiers = specifiers;
        this.source = source;
    }
}

export class ImportDefaultSpecifier extends ModuleSpecifier {}

export class ExportDefaultDeclaration extends ModuleDeclaration {
    constructor({ declaration, loc }) {
        super({ loc });
        this.declaration = declaration;
    }
}

export class ExportNamedDeclaration extends ModuleDeclaration {
    constructor({ declaration = null, specifiers = [], source = null, loc }) {
        super({ loc });
        this.declaration = declaration;
        this.specifiers = specifiers;
        this.source = source;
    }
}

export class ObjectExpression extends Expression {
    constructor({ properties, loc }) {
        super({ loc });
        this.properties = properties;
    }
}

export class Property extends Node {
    constructor({ key, value, kind = 'init', loc }) {
        super({ loc });
        this.key = key;
        this.value = value;
        this.kind = kind;
    }
}

export class ChainElement extends Expression {
    constructor({ optional = false, loc }) {
        super({ loc });
        this.optional = optional;
    }
}

export class MemberExpression extends ChainElement {
    constructor({ object, property, computed, loc, optional }) {
        super({ loc, optional });
        this.object = object;
        this.property = property;
        this.computed = computed;
    }
}

export class ChainExpression extends Expression {
    constructor({ expression, loc }) {
        super({ loc });
        this.expression = expression;
    }
}

export class ArrayExpression extends Expression {
    constructor({ elements, loc }) {
        super({ loc });
        this.elements = elements;
    }
}
