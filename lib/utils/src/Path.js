import { izip as zip } from 'itertools';

export class Path extends Array {
    static sep = '/';

    __init__(...segments) {
        super.push(...segments);
    }

    constructor(...args) {
        super();
        this.__init__(...args);

        Object.freeze(this);
    }

    toString() {
        return this.constructor.sep + this.join(this.constructor.sep);
    }

    append(newSegment) {
        return new this.constructor(...this, newSegment);
    }

    static fromString(sourceString) {
        const split = sourceString.split(this.sep);
        return new this(...split);
    }

    eq(other) {
        if (!(other instanceof this[Symbol.species])) {
            other = this.constructor.fromString(other);
        }

        if (this.length !== other.length) {
            return false;
        }
        for (const [segment, otherSegment] of zip(this, other)) {
            if (segment !== otherSegment) {
                return false;
            }
        }

        return true;
    }

    startsWith(otherPath) {
        if (!(otherPath instanceof this.constructor)) {
            otherPath = this.constructor.fromString(otherPath);
        }

        if (otherPath.length > this.length) {
            return false;
        }
        for (const [seg, otherSeg] of zip(this, otherPath)) {
            if (seg !== otherSeg) {
                return false;
            }
        }

        return true;
    }

    concat(...newValues) {
        newValues.flat();
        return new this.constructor(...this, ...newValues);
    }
}
