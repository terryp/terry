
export function reportStreamProgress(readable, progress) {
    let loaded = 0;

    const stream = new TransformStream({
        transform(chunk, controller) {
            loaded += chunk.byteLength;
            progress.value = loaded;
            controller.enqueue(chunk);
        },
    });

    return readable.pipeThrough(stream);
}


export async function streamToBlob(stream) {
    const chunks = await Array.fromAsync(stream);
    return new Blob(chunks);
}
