/**
 * Replace all whitespace with one space character.
 *      Similar to html's whitespace handling
 *
 *  @param {string} string - The string to collapse whitespace in.
 *
 *  @return {string}
 */
export function collapseWhitespace(string) {
    return string.replaceAll(/\s+/g, ' ');
}

export function deepFreeze(object, _cached = new Set()) {
    if (_cached.has(object)) {
        return object;
    } else {
        _cached.add(object);
    }

    // Retrieve the property names defined on object
    const propNames = Reflect.ownKeys(object);

    // Freeze properties before freezing self

    for (const name of propNames) {
        const value = object[name];

        if (value && typeof value === 'object') {
            deepFreeze(value, _cached);
        }
    }

    return Object.freeze(object);
}

export function bindAll(object) {
    for (const key of Reflect.ownKeys(object)) {
        if (key !== 'constructor' && typeof object[key] === 'function') {
            object[key] = object[key].bind(object);
        }
    }
    return object;
}

export function getArgsAndCb(name, args, num = 'one') {
    if (args.length === 0) {
        throw new TypeError(
            `${name} must be passed at least ${num} arguments`,
        );
    }
    const cb = args.at(-1);

    if (typeof cb !== 'function') {
        throw new TypeError(
            `The final arg to ${name} must be a callback to set`
                + ' the new value.',
        );
    }

    return {
        cb,
        args: args.slice(0, -1),
    };
}

export const isNode = globalThis?.process?.versions?.node !== undefined;


export { DotPath } from '#src/DotPath.js';
export { default as dedent } from 'strip-indent';
export { Path } from '#src/Path.js';
export { re, reFlags } from '#src/regex.js';
export { Safe, safe } from '#src/prototype.js';
export { parseAcceptValues } from '#src/http.js';
export { reportStreamProgress, streamToBlob } from '#src/streams.js';
