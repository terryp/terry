if (!Object.hasOwn) {
    Object.defineProperty(Object, 'hasOwn', {
        value: function(object, property) {
            if (object === null || object === undefined) {
                throw new TypeError(
                    'Cannot convert undefined or null to object',
                );
            }
            return Object.prototype
                // eslint-disable-next-line unicorn/new-for-builtins
                .hasOwnProperty.call(Object(object), property);
        },
        configurable: true,
        enumerable: false,
        writable: true,
    });
}
