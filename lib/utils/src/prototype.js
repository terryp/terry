const props = Object.fromEntries([
    copyProtected('hasOwnProperty', 'has'),
    copyProtected('isPrototypeOf'),
    copyProtected('propertyIsEnumerable'),
    copyProtected('toString'),
    createProtected(Symbol.iterator, iterator),
    [
        Symbol.toStringTag,
        {
            get() {
                return this.constructor.name;
            },
            enumerable: false,
            configurable: false,
        },
    ],
    [
        'constructor',
        {
            value: Safe,
            writable: true,
            enumerable: false,
            configurable: true,
        },
    ],
]);

export const safe = Object.create(null, props);

export function Safe(initial = {}) {
    Object.assign(this, initial);
}

Safe.prototype = Object.create(safe);


function copyProtected(key, newKey = key) {
    const propDesc = createDesc(newKey, Object.prototype[key]);

    return [newKey, propDesc];
}

function createDesc(key, value) {
    return {
        get() {
            return value;
        },
        set() {
            throw new TypeError(`'${key}' may not be set.`);
        },
        enumerable: false,
        configurable: false,
    };
}

function createProtected(key, value) {
    const propDesc = createDesc(key, value);

    return [key, propDesc];
}

function *iterator() {
    // eslint-disable-next-line no-invalid-this
    yield* Object.keys(this);
}
