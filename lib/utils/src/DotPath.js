import { set, get } from 'lodash-es';
import { Path } from '#src/Path.js';

export class DotPath extends Path {
    static sep = '.';

    static fromString(sourceString) {
        const split = sourceString.replace(/[.[\]]+/g)
            .split('.')
            .map((name) => name.replaceAll('\'"`', ''));
        return new this(...split);
    }

    set(obj, value) {
        return set(obj, this, value);
    }

    get(obj, defaultValue) {
        return get(obj, this, defaultValue);
    }
}
