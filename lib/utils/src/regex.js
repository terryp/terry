import { roundrobin, imap, reduce } from 'itertools';

function getSource(strings, regexes) {
    const regexStrs = imap(regexes, (item) => {
        if (item instanceof RegExp) {
            return item.source;
        }
        return item;
    });

    const combined = roundrobin(strings.raw, regexStrs);
    return reduce(combined, (curr, next) => curr + next, '');
}

export function reFlags(flags) {
    return function(strings, ...regexes) {
        const source = getSource(strings, regexes);
        return new RegExp(source, flags);
    };
}

export function re(strings, ...regexes) {
    const source = getSource(strings, regexes);
    return new RegExp(source);
}
