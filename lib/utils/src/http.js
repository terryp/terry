import { sorted } from 'itertools';

export function parseAcceptValues(valuesStr = '') {
    const valueDefs = valuesStr.split(',')
        .map((valueDef) => valueDef.split(';'))
        .map(([value, weight]) => [value, weight?.replace(/^q=/)])
        .map(([value, weight]) => [value, weight ?? 1])
        .map(([value, weight]) => [value, Number.parseInt(weight)]);

    return sorted(valueDefs, ([value, weight]) => weight)
        .map(([value]) => value);
}
