import local from 'localforage';
import { auditTime, map, tap, throttleTime } from 'rxjs/operators';
import { Duration } from 'luxon';
import { Subject } from 'rxjs';

import { NestingStore } from '@omniblack/stores';
import { fromStore } from '@omniblack/stores/rxjs';
import { Code } from '@omniblack/error';

export default {
    asyncSetup,
    setup,
    name: 'storage',
};

function setup() {
    const name = `@${config.component_name}/storage`;
    const state = {
        __stores: new Map(),
        __bus: new Subject(),
        __prefix: name,
        __writes_pending: 0,
        __audits_pending: 0,
        get,
    };

    const proms = state.__bus.pipe(
        // eslint-disable-next-line no-return-assign
        tap(() => config.storage.__writes_pending += 1),
        map(async ([storeName, currentValue]) => {
            await save(storeName, currentValue);
        }),
    );

    window.addEventListener('beforeunload', (event) => {
        const pending = config.storage.__writes_pending
            + config.storage.__audits_pending;
        if (pending > 0) {
            event.preventDefault();
        }
    });

    state.__proms = proms;

    proms.subscribe(async (prom) => {
        try {
            await prom;
        } catch (err) {
            console.error(err);
        } finally {
            config.storage.__writes_pending -= 1;
        }
    });


    Object.assign(config.storage, state);
}

function key(storeName) {
    return `${config.storage.__prefix}/${storeName}`;
}

const saveDuration = Duration.fromObject({ seconds: 1 });

function newStore(storeName, initialValue) {
    if (config.storage.__stores.has(storeName)) {
        throw new Code('Trying to create a store that already exists', {
            details: {
                storeName,
                initialValue,
            },
        });
    }

    const store = new NestingStore(
        initialValue,
        `@omniblack/storage/${storeName}`,
    );

    const obs = fromStore(store);
    const throttled = obs.pipe(
        throttleTime(saveDuration.toMillis()),
    );
    const audited = obs.pipe(auditTime(saveDuration.toMillis()));

    // eslint-disable-next-line no-return-assign
    throttled.subscribe((_value) => config.storage.__audits_pending += 1);
    audited.subscribe((value) => {
        config.storage.__bus.next([storeName, value]);
        config.storage.__audits_pending -= 1;
    });

    config.storage.__stores.set(storeName, store);

    (async function() {
        const names = Array.from(config.storage.__stores.keys());

        try {
            await local.setItem(key('__stores'), names);
        } catch (err) {
            console.error(err);
        }
    })();

    return store;
}

async function save(storeName, currentValue) {
    try {
        await local.setItem(key(storeName), currentValue);
    } catch (err) {
        console.log(err);
    }
}

function get(storeName, defaultValue) {
    let store = config.storage.__stores.get(storeName);

    if (store) {
        store.update((currentValue) => {
            if (currentValue === undefined || currentValue === null) {
                return defaultValue;
            }
            return currentValue;
        });
    } else {
        store = newStore(storeName, defaultValue);
    }

    return store;
}

async function asyncSetup() {
    const stores = await local.getItem(key('__stores')) ?? [];

    for (const storeName of stores) {
        const value = await local.getItem(key(storeName));
        newStore(storeName, value);
    }
}


