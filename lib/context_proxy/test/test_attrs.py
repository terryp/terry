from contextvars import ContextVar
from types import SimpleNamespace

from omniblack.context_proxy import ContextProxy


class TestSetattr:
    def test_with_init(self):
        proxy = ContextProxy(init=SimpleNamespace)
        proxy.test = 1

    def test_with_var(self):
        var = ContextVar('setattr var test')
        var.set(SimpleNamespace())
        proxy = ContextProxy(var=var)
        proxy.test = 1


class TestGetattr:
    def test_with_init(self):
        proxy = ContextProxy(init=SimpleNamespace)
        proxy.test = 1
        assert proxy.test == 1

    def test_with_var(self):
        var = ContextVar('getattr var test')
        var.set(SimpleNamespace())
        proxy = ContextProxy(var=var)
        proxy.test = 1
        assert proxy.test == 1
        raise KeyError
