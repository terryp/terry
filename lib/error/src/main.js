const sym = Symbol.for('omniblack.localization.localize');

/**
 * Localize a localizable.
 * @param {Localizable} localizable - The thing to localize, might be a string,
 *      Text, or UIString.
 *
 * @param {Object<Localizable>} args - Args to be used to do replacements in
 *      the localizable.
 *
 * @returns {String}
 *
 * @private
 */

let localizeMsg = function localize(localizable, args) {
    if (globalThis[sym] !== undefined) {
        localizeMsg = globalThis[sym];
        return globalThis[sym](localizable, args);
    }

    if (typeof localizable === 'object') {
        if (localizable.en !== undefined) {
            return localizable.en;
        } else if (typeof localizable.localize === 'function') {
            return localizable.localize('en');
        }
    }

    return localizable;
};

/**
 * The Base error class that all omniblack error's should derive from.
 *      The message will automatically localized.
 */
export class Base extends Error {
    /**
     * @param {String} msg - Description of the problem.
     * @param {Object} [options] - Other options
     * @param {Boolean} [options.retry=false] - If the operation can be retried
     *      without changes.
     * @param {Boolean} [options.notifyDev=false] - if we should notify
     *      development of the problem.
     * @param {Object<Any>} [options.details={}] - object other details to be
     *      included about the error, or the context the error occurred in.
     */
    constructor(msg, { retry = false, notifyDev = true, details = {} } = {}) {
        try {
            msg = localizeMsg(msg);
        } catch (err) {
            // eslint-disable-next-line no-use-before-define
            if (!(err instanceof NotLocalizable)) {
                throw err;
            }
        }

        if (typeof msg === 'string') {
            // Replace all whitespace groups with one space
            msg = collapseWhitespace(msg);
        }

        super(msg);
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, this.constructor);
        }
        this.message = msg;
        this.name = this.constructor.name;
        this.retry = retry;
        this.notifyDev = notifyDev;
        this.details = details;
    }
}


function collapseWhitespace(string) {
    return string.replaceAll(/\s+/g, ' ');
}

/**
 * An error that must have resulted from an error in program logic.
 * @extends Base
 */
export class Code extends Base {
    /**
     * @param {String} msg - Description of the problem.
     * @param {Object} [options] - Other options
     * @param {Boolean} [options.retry=false] - If the operation can be retried
     *      without changes.
     * @param {Boolean} [options.notifyDev=true] - if we should notify
     *      development of the problem.
     * @param {Object<Any>} [options.details={}] - object other details to be
     *      included about the error, or the context the error occurred in.
     */
    constructor(msg, { retry = false, notifyDev = true, details = {} } = {}) {
        super(msg, { retry, notifyDev, details });
    }
}

/**
 * Indicates a function that has not been implemented yet, but
 *      is in the works.
 *  @extends {Code}
 *  @class
 */
export class NotImplemented extends Code {}

/**
 * Something that is not cannot be localized was passed to localize.
 *
 * @extends {Base}
 * @class
 */
export class NotLocalizable extends Base {}
