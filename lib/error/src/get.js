import { Base } from '#src/main.js';


export class HttpError extends Base {
    constructor(status, body, notifyDev, retry, details) {
        body = body ?? 'The response body could not be decoded';
        const ourDetails = {
            ...details,
            body,
            status,
        };

        super(
            'The server responded with an error code',
            { retry, notifyDev, details: ourDetails },
        );

        this.body = body;
        this.status = status;
    }

    async from(resp, {
        notifyDev = false,
        retry = false,
        details = {},
    } = {}) {
        let contentType;
        let body;

        if (resp.headers.has('content-type')) {
            contentType = resp.headers.get('content-type');
        }

        if (contentType === 'application/json') {
            body = await resp.json();
        } else if (contentType.startsWith('text')) {
            body = await resp.text();
        }

        return this(body, resp.status, notifyDev, retry, details);
    }
}


export class NetworkError extends Base {
    constructor(msg, { notifyDev = config.dev, retry = false, details } = {}) {
        super(msg, { retry, notifyDev, details });
    }
}
