import { TXT } from '#src/text.js';
import { Code } from '#src/main.js';

export class KeyError extends Code {
    constructor(key, extraDetails = {}) {
        super(TXT`KeyError: '${key}'`, { details: { key, ...extraDetails } });
    }
}
