import { TXT } from '#src/text.js';
import { Base } from '#src/main.js';

/**
 * An error that relates to the Model.
 */
export class ModelError extends Base {}


/**
 * An UnknownType was referenced.
 */
export class UnknownType extends ModelError {
    /**
     * @param {String} typeName - The name of the unknown type.
     */
    constructor(typeName) {
        // TODO Add doc links to this message
        super(TXT`Unknown type '${typeName}'. Please register it
             with the model.`);
    }
}

export class CannotConvert extends ModelError {
    constructor(typeName, direction, adapterName) {
        super(TXT`Cannot convert ${typeName} ${direction} ${adapterName}.`);
    }
}

export class AttributeError extends ModelError {
    constructor(msg, attribute, extraDetails = {}) {
        const details = {
            ...extraDetails,
            attribute,
        };

        super(msg, { retry: false, notifyDev: true, details });
    }
}

export class CoercionError extends ModelError {}
