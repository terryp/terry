import { Base } from '#src/main.js';


export class ParserError extends Base {}

export class UnexpectedCharacter extends ParserError {
    constructor(str, index, context, details) {
        const char = str.at(index);

        super(`'${char}' is not allowed in '${context}'.`, {
            details: {
                ...details,
                str,
                index,
                char,
                context,
            },
        });
    }
}
