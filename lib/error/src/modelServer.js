import { Base } from '#src/main.js';

export class BadRequest extends Base {
    static statusCode = 400;
    static statusMessage = 'Bad Request';
}

export class UnsupportedMediaType extends Base {
    static statusCode = 415;
    static statusMessage = 'Unsupported Media Type';
}

export class UnsupportedContentEncoding extends UnsupportedMediaType {
    static statusMessage = 'Unsupported Content Encoding';
}

export class NotAcceptable extends Base {
    static statusCode = 406;
    static statusMessage = 'Not Acceptable';
}
