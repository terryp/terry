import { roundrobin } from 'itertools';

const sym = Symbol.for('omniblack.localization.TXT');

/**
 * Return a Text object, or a string with all the args included.
 *      A text object will be returned when @omniblack/localization
 *      is loaded.
 *
 * @package
 */

// eslint-disable-next-line prefer-const
export let TXT = function TXT(strings, ...args) {
    if (globalThis[sym] !== undefined) {
        // eslint-disable-next-line no-func-assign
        TXT = globalThis[sym];

        // eslint-disable-next-line new-cap
        return TXT(strings, ...args);
    }

    const segs = Array.from(roundrobin(strings, args));
    return segs.join('');
};
