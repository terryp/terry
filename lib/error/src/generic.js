import { Base } from '#src/main.js';

export class ParseError extends Base {
    constructor(format, string, details) {
        const msg = `${string} can not be parsed as ${format}.`;

        const fullDetails = {
            format,
            string,
            ...details,
        };
        super(msg, { retry: false, details: fullDetails });
    }
}
