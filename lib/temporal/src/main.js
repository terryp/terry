import { toNanoseconds } from '#src/consts.js';

const units = [
    'hour',
    'minute',
    'second',
    'millisecond',
    'microsecond',
    'nanosecond',
];


/*
 * Wrap `plainTime` in a new class that inherits from `plainTime`'s class
 * but has a useful `valueOf`.
 *
 * The original value is available as the `wrapped` attribute.
 *
 * This is useful if you want to have a comparable object but want to return
 * the original object un-modified.
 */
export function plainTimeComp(plainTime) {
    const cls = plainTime.constructor;

    class PlainTime extends cls {
        constructor(wrapped) {
            super(
                wrapped?.hour,
                wrapped?.minute,
                wrapped?.second,
                wrapped?.millisecond,
                wrapped?.microsecond,
                wrapped?.nanosecond,
            );
            this.wrapped = wrapped;
        }

        valueOf() {
            return units.reduce(
                (sum, unit) => toNanoseconds(unit, this[unit]) + sum,
                0n,
            );
        }
    }

    return new PlainTime(plainTime);
}
