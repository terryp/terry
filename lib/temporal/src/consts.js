export const nanoPerDay = 864n * (10n ** 11n);
export const nanoPerHour = 36n * (10n ** 11n);
export const nanoPerMinute = 6n * (10n ** 10n);
export const nanoPerSecond = 10n ** 9n;
export const nanoPerMillisecond = 10n ** 6n;
export const nanoPerMicrosecond = 1000n;

// TODO: This is a crap name
const convertFactors = {
    day: nanoPerDay,
    hour: nanoPerHour,
    minute: nanoPerMinute,
    second: nanoPerSecond,
    millisecond: nanoPerMillisecond,
    microsecond: nanoPerMicrosecond,
    nanosecond: 1n,
};


export function toNanoseconds(unit, number) {
    return convertFactors[unit] * BigInt(number);
}
