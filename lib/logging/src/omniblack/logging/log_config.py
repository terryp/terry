import logging

from enum import IntEnum
from multiprocessing import current_process
from os import environ
from sys import stderr

from public import public


@public
class Level(IntEnum):
    CRITICAL = logging.CRITICAL
    FATAL = logging.FATAL
    ERROR = logging.ERROR
    WARNING = logging.WARNING
    INFO = logging.INFO
    DEBUG = logging.DEBUG
    NOTSET = logging.NOTSET


@public
class Verbosity(IntEnum):
    NOTSET = 0
    CRITICAL = 1
    ERROR = 2
    WARNING = 3
    WARN = 3
    INFO = 4
    DEBUG = 5

    def to_level(self):
        cls = self.__class__

        match self:
            case cls.NOTSET:
                return Level.NOTSET
            case cls.CRITICAL:
                return Level.CRITICAL
            case cls.ERROR:
                return Level.ERROR
            case cls.WARNING:
                return Level.WARNING
            case cls.INFO:
                return Level.INFO
            case cls.DEBUG:
                return Level.DEBUG


date_format = '%m/%d/%Y %I:%M:%S %p'
public(date_format=date_format)


def is_set(env_name):
    value = environ.get(env_name, '')
    return bool(value.strip())


def delegate_to_root(logger=None):
    if logger is None:
        logger = logging.getLogger()

    for child in logger.getChildren():
        child.setLevel(logging.NOTSET)
        child.propagate = True
        delegate_to_root(child)


def use_rich(level):
    from rich.logging import RichHandler

    formatter = logging.Formatter(
        datefmt=date_format,
        fmt='%(message)s',
    )

    handler = RichHandler(
        level=level,
        rich_tracebacks=True,
        log_time_format=date_format,
        omit_repeated_times=False,
    )

    handler.setFormatter(formatter)

    omniblack_handler = RichHandler(
        level=level,
        rich_tracebacks=True,
        log_time_format=date_format,
        omit_repeated_times=False,
        markup=True,
    )

    omniblack_handler.setFormatter(formatter)

    omniblack_logger = logging.getLogger('omniblack')
    omniblack_logger.setLevel(level)
    omniblack_logger.addHandler(omniblack_handler)
    omniblack_logger.propagate = False

    return handler


def use_systemd(level):
    from systemd.journal import JournalHandler

    formatter = logging.Formatter(
        datefmt=date_format,
        fmt='%(levelname)s %(name)s %(asctime)s: %(message)s',
    )

    handler = JournalHandler(level)
    handler.setFormatter(formatter)

    return handler


@public
class Formatter(logging.Formatter):
    def formatMessage(self, record):  # noqa N802
        record.processName = current_process().name
        return super().formatMessage(record)


def use_standard(level):
    formatter = Formatter(
        datefmt=date_format,
        fmt=(
            '%(levelname)s %(name)s <%(processName)s> '
            + '[%(asctime)s]: %(message)s'
        ),
    )

    handler = logging.StreamHandler(stream=stderr)
    handler.setLevel(level)
    handler.setFormatter(formatter)

    return handler


def configure_sh(_level):
    try:
        import sh  # noqa: F401
    except ImportError:
        return

    sh_log = logging.getLogger('sh')
    sh_log.setLevel(Level.WARNING)


@public
def get_level():
    from omniblack.app import config

    if isinstance(config.verbosity, int):
        return config.verbosity
    return config.verbosity.to_level()


@public
def get_handler(level=None):
    from omniblack.app import Modes, config

    if level is None:
        level = get_level()

    if is_set('OMNIBLACK_USE_RICH'):
        try:
            return use_rich(level)
        except ImportError:
            pass

    match config.mode:
        case Modes.interactive if config.color:
            try:
                return use_rich(level)
            except ImportError:
                pass

        case Modes.systemd:
            try:
                return use_systemd(level)
            except ImportError:
                pass

    return use_standard(level)


prev_handler = None


@public
def configure_config() -> None:
    """Configure logging to use a nice format."""
    root_logger = logging.getLogger()
    level = get_level()

    handler = get_handler()

    global prev_handler
    if prev_handler is not None:
        root_logger.removeHandler(prev_handler)

    prev_handler = handler
    delegate_to_root()

    root_logger.addHandler(handler)
    root_logger.setLevel(level)
    logging.captureWarnings(capture=True)
    configure_sh(level)
