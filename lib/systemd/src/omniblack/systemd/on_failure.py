from datetime import datetime
from enum import auto
from os import environ
from pathlib import Path
from pickle import dump, load
from signal import Signals
from string import Template

from arrow import get as to_arrow
from pydantic import BaseModel
from sh import systemctl

from omniblack.notifier import Message, Severity, discord
from omniblack.utils import Enum
from systemd.journal import Reader


class ServiceResult(Enum):
    success = auto()
    protocol = auto()
    timeout = auto()
    exit_code = auto()
    signal = auto()
    core_dump = auto()
    watchdog = auto()
    start_limit_hit = auto()
    oom_kill = auto()
    unknown = auto()

    @classmethod
    def _missing_(cls, value):
        if value == 'resources':
            return cls.unknown

        value = value.replace('-', '_')

        return cls[value]

    def __str__(self):
        return self.name


class ExitCode(Enum):
    killed = auto()
    exited = auto()
    dumped = auto()
    not_set = auto()

    @classmethod
    def _missing_(cls, value):
        try:
            return cls[value]
        except KeyError:
            return cls.not_set

    def __str__(self):
        return self.name


class LogMessage(BaseModel):
    message: str
    severity: Severity
    date: datetime

    @classmethod
    def from_entry(cls, entry: dict):
        if 'PRIORITY' in entry:
            severity = Severity.from_systemd_priority(entry['PRIORITY'])
        else:
            severity = Severity.notice

        return cls(
            message=entry['MESSAGE'],
            severity=severity,
            date=entry.get(
                '_SOURCE_REALTIME_TIMESTAMP',
                entry['__REALTIME_TIMESTAMP'],
            ),
        )

    def format(self) -> str:
        arrow = to_arrow(self.date)
        date_str = arrow.format('YYYY-MM-DDTHH:MM:SSZ')
        return f'[{date_str}] <{self.severity}> {self.message}'

    def len(self) -> int:
        return self.format().len()


def create_summary(service_result, status, verb, limit_hit):
    match service_result:
        case ServiceResult.protocol:
            if verb == ExitCode.exited:
                summary = '$unit exited due to a protocol violation.'
            else:
                summary = '$unit failed due to a protocol violation.'
        case ServiceResult.timeout:
            summary = '$unit timed out.'

        case ServiceResult.signal:
            summary = f'$unit was killed by {status.name}.'

        case ServiceResult.core_dump:
            summary = f'$unit was dumped by {status.name}.'

        case ServiceResult.watchdog:
            summary = (
                f'$unit was {verb} due to a '
                + 'watchdog deadline being missed.'
            )

        case ServiceResult.start_limit_hit:
            summary = (
                "$unit has hit it's start limit, and will not be restarted."
            )

        case _:
            summary = '$unit has failed.'

    if limit_hit:
        summary += '\n'
        summary += (
            "$unit has hit it's start limit, and will not be restarted."
        )

    return summary


systemd_docs = (
        'https://www.freedesktop.org/software/'
        + 'systemd/man/systemd.exec.html#%24SERVICE_RESULT'
)


def main():
    """
        Designed to by run by systemd upon the failure of a service.
    """
    start = datetime.now()

    storage_dir = Path(environ['RUNTIME_DIRECTORY'])

    try:
        with open(storage_dir / 'restarts.pickle', 'rb') as file:
            restarts = load(file)
    except FileNotFoundError:
        restarts = 1

    limit_hit = restarts >= 3

    service_result = ServiceResult(environ['MONITOR_SERVICE_RESULT'])

    exit_code = ExitCode(environ['MONITOR_EXIT_CODE'])

    exit_status_str = environ['MONITOR_EXIT_STATUS']

    try:
        exit_status = int(exit_status_str, base=10)
    except ValueError:
        exit_status = Signals[f'SIG{exit_status_str}']

    invocation_id = environ['MONITOR_INVOCATION_ID']
    unit = environ['MONITOR_UNIT']

    log_reader = Reader()
    log_reader.add_match(_SYSTEMD_INVOCATION_ID=invocation_id)

    summary = create_summary(
        service_result,
        exit_status,
        exit_code,
        limit_hit,
    )

    summary = Template(summary)

    summary = summary.substitute(unit=unit)

    messages = (
        LogMessage.from_entry(entry)
        for entry in log_reader
    )

    message_text = '\n'.join(
        msg.format()
        for msg in messages
    )

    msg = Message(
        title=f'{unit} failed',
        summary=summary,
        message=message_text,
        source='Systemd',
        severity=Severity.error,
        timestamp=start,
        data={
            'Service Result': f'[{service_result}]({systemd_docs})',
            'Exit Verb': str(exit_code),
            'Exit Code': str(exit_status),
        },
    )

    discord(msg)

    if not limit_hit:
        with open(storage_dir / 'restarts.pickle', 'wb') as file:
            dump(restarts + 1, file)

        systemctl.restart(unit, user=True)
