import { zip, filter } from 'itertools';
import { ReadonlyStore } from './base.js';

const SEP = '/';

function toSegments(string) {
    const segments = string.split(SEP);

    /*
     * filter to remove empty segments
     * this will allow some weird paths to pass like
     * /test//test
     * but that follows generate tight accept loose
     */
    return filter(segments, (seg) => seg && seg !== '.');
}

export class URLPath extends ReadonlyStore {
    constructor(...segments) {
        super();
        if (segments.length === 1 && typeof segments[0] !== 'string') {
            const [initValue] = segments;
            this.set(initValue);
        } else {
            this.array = segments;
        }
    }

    get value() {
        return this;
    }

    toString() {
        return SEP + this.array.join(SEP);
    }

    append(...newSegments) {
        this.array.push(...newSegments);
        this.__changed();
    }

    prepend(...newSegments) {
        this.array.unshift(...newSegments);
        this.__changed();
    }

    pop(index) {
        index = this.___convertIndex(index);
        const [value] = this.array.splice(index, 1);
        return value;
    }

    at(index) {
        index = this.___convertIndex(index);
        return this.array[index];
    }

    ___convertIndex(index) {
        index = Number.parseInt(index, 10);
        if (index < 0) {
            index += this.array.length;
        }

        if (index > this.array.length || index < 0) {
            throw new RangeError('Path index out of range');
        }
        return index;
    }


    static fromString(sourceString, name) {
        const newPath = new this(...toSegments(sourceString));
        newPath.name = name;
        return newPath;
    }

    eq(other) {
        other = this.__convertPath(other);

        if (this.array.length !== other.array.length) {
            return false;
        }
        for (const [segment, otherSegment] of zip(this, other)) {
            if (segment !== otherSegment) {
                return false;
            }
        }

        return true;
    }

    __convertPath(pathLike) {
        if (pathLike instanceof this.constructor) {
            return pathLike;
        }

        pathLike = String(pathLike);

        return this.constructor.fromString(pathLike);
    }

    startsWith(otherPath) {
        otherPath = this.__convertPath(otherPath);

        if (otherPath.length > this.length) {
            return false;
        }
        for (const [seg, otherSeg] of zip(this, otherPath)) {
            if (seg !== otherSeg) {
                return false;
            }
        }

        return true;
    }

    removePrefix(prefix) {
        prefix = this.__convertPath(prefix);

        if (!this.startsWith(prefix)) {
            return;
        }

        this.array.splice(0, prefix.array.length);

        this.__changed();
    }


    set(newValue) {
        if (newValue === undefined) {
            this.array = [];
        } else if (typeof newValue === 'string') {
            this.array = toSegments(newValue);
        } else {
            this.array = newValue.array;
        }

        this.__changed();
    }


    [Symbol.iterator]() {
        return this.array[Symbol.iterator]();
    }


    [Symbol.for('nodejs.util.inspect.custom')]() {
        const clsName = this.constructor.name;
        return `${clsName} <${this}>`;
    }
}

