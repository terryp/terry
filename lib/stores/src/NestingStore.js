import { uniqueEverseen, chain, all } from 'itertools';
import { isContainerObj } from '#src/container_object.js';
import { SyncableStore, ConnectableStore } from '#src/base.js';
import { KeyError, Code } from '@omniblack/error';
import { TXT } from '@omniblack/localization';


const notPassed = Symbol('Parameter not passed in.');

export class NestingStore extends SyncableStore {
    constructor(
        initialValue,
        name,
        parent = undefined,
        key = undefined,
    ) {
        super(initialValue, name);
        this.__children = new Map();
        this.__derivedStores = new Map();
        this.children = new ConnectableStore(
            this.__children,
            `${name}.children`,
        );
        this.__processValues(initialValue);
        this.parent = parent;
        // Cancel the pending tick as we don't want a dead check yet
        this.__tickQueued = false;
        if (parent) {
            this.root = parent.root;
            this.key = key;
        } else {
            this.root = this;
        }
    }

    __display(display, memo) {
        const children = Array.from(this.__children)
            .map(([key, store]) => [key, store.__get_display(memo)]);

        display.children = Object.fromEntries(children);
        return super.__display(display, memo);
    }

    __dead() {
        const childrenDead = all(
            this.__children.values(),
            (child) => child.__dead(),
        );

        return childrenDead && super.__dead();
    }

    getChild(key, defaultValue = notPassed) {
        key = String(key);
        if (this.__children.has(key)) {
            return this.__children.get(key);
        } else if (defaultValue === notPassed) {
            throw new KeyError(key);
        } else {
            const newChild = this.__createChild(key, defaultValue);
            this.__changed();
            return newChild;
        }
    }

    setChild(key, value) {
        key = String(key);
        if (!this.__children.has(key)) {
            const newChild = this.__createChild(key, value);
            this.__changed();
            return newChild;
        } else if (this.__derivedStores.has(key)) {
            throw new Code(`The derived store ${key} may not be set.`);
        } else {
            const child = this.__children.get(key);
            child.set(value);
            return child;
        }
    }

    deleteChild(key) {
        key = String(key);
        this.children.update((children) => {
            delete children[key];
            return children;
        });

        if (this.__derivedStores.has(key)) {
            const unsub = this.__derivedStores.get(key);
            unsub();
            this.__derivedStores.delete(key);
        }

        delete this.value[key];

        this.__changed();
    }

    __createChild(key, initialValue) {
        const self = this;
        const newStore = new this.constructor(
            initialValue,
            `${this.name}[${key}]`,
            self,
            key,
        );

        this.children.update((currentChildren) => {
            currentChildren.set(key, newStore);
            return currentChildren;
        });

        return newStore;
    }

    __processValues(values, { parent = false } = {}) {
        const oldValues = this.value;
        this.value = values;
        if (isContainerObj(values)) {
            const newKeys = Object.keys(values ?? {});
            const oldKeys = Object.keys(oldValues ?? {});
            const allKeys = chain(newKeys, oldKeys);

            for (const key of uniqueEverseen(allKeys)) {
                if (this.__derivedStores.has(String(key))) {
                    continue;
                }

                const value = values[key];
                if (this.__children.has(key)) {
                    const childStore = this.__children.get(key);
                    childStore.__setFromParent(value);
                } else {
                    this.__createChild(key, value);
                }
            }
        }

        this.__changed({ parent });
    }

    __setFromChild(key, newValue, childStore) {
        try {
            const currentStore = this.getChild(key);
            if (currentStore === childStore) {
                this.value[key] = newValue;
            } else {
                throw new Code(TXT`Deleted store was reused.`);
            }
        } catch (err) {
            const error = err instanceof KeyError
                ? new Code(TXT`Deleted store was reused.`)
                : err;
            throw error;
        }

        this.debugArgs.source = 'set_from_child';
        this.debugArgs.childStore = childStore;

        this.__changed();
    }

    __setFromParent(newValue) {
        this.debugArgs.source = 'set_from_parent';
        this.__processValues(newValue, { parent: true });
    }

    set(newValue) {
        if (typeof newValue === 'object') {
            this.__processValues(newValue);
        } else {
            super.set(newValue);
        }
    }

    update(updater) {
        const newValue = updater(this.value);
        this.set(newValue);
    }

    async updateAsync(updater) {
        const newValue = await updater(this.value);
        this.set(newValue);
    }

    __changed({ parent = false, ...rest } = {}) {
        this.scheduleCheckDead();
        if (!parent) {
            this.parent?.__setFromChild(this.key, this.value, this);
        }

        super.__changed(rest);
    }

    subscribe(subscription) {
        this.subscribers.add(subscription);

        subscription(this.value);

        return () => this.unsubscribe(subscription);
    }


    scheduleCheckDead() {
        if (!this.__tickQueued) {
            this.__tickQueued = true;
            globalThis.queueMicrotask(this.checkDead.bind(this));
        }
    }

    checkDead() {
        this.__tickQueued = false;
        if (this.__dead()) {
            this.parent?.deleteChild(this.key);
        }
    }

    unsubscribe(subscription) {
        this.subscribers.delete(subscription);
        this.scheduleCheckDead();
    }


    attachDerivedStore(key, store) {
        key = String(key);
        this.deleteChild(key);
        this.children.update((children) => {
            children.set(key, store);
            return children;
        });

        const sub = store.subscribe((value) => {
            this.__setFromChild(key, value, store);
        });

        this.__derivedStores.set(key, sub);
    }
}
