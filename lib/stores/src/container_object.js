import { isPlainObject } from 'lodash-es';

/**
 * Return true is `obj` is a container obj.
 *      Container objs are intended to store multiple child values.
 *      As compared to `big` or `Date` which instances of represent
 *      a single logical value.
 */
export function isContainerObj(obj) {
    if (obj === null || obj === undefined) {
        return false;
    }
    return isPlainObject(obj)
        || Array.isArray(obj);
}

