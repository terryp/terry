import { parse, stringify } from 'qs';
import { isEmpty } from 'lodash-es';

import { ReadonlyStore } from './base.js';
import { NestingStore } from './NestingStore.js';
import { URLPath } from './Path.js';

import { T } from '@omniblack/localization';
import { Code } from '@omniblack/error';


if (!Object.hasOwn) {
    Object.defineProperty(Object, 'hasOwn', {
        value: function(object, property) {
            if (object === null || object === undefined) {
                throw new TypeError(
                    'Cannot convert undefined or null to object',
                );
            }
            return Object.prototype
                // eslint-disable-next-line unicorn/new-for-builtins
                .hasOwnProperty.call(Object(object), property);
        },
        configurable: true,
        enumerable: false,
        writable: true,
    });
}

function removePrefix(string = '', prefix) {
    if (!string.startsWith(prefix)) {
        return string;
    }

    return string.slice(prefix.length);
}

function *noComplete(iter) {
    iter = iter[Symbol.iterator]();

    let { value, done } = iter.next();

    while (!done) {
        yield value;
        ({ value, done } = iter.next());
    }
}

/*
 * Parse a string to a path, query and hash parts.
 * The query and hashs will not have the prefixes ? and #.
 * This doesn't not validate allow characters just looks for ?
 * and # to separate sections.
 */
export function parseURL(url) {
    let path = '';
    let query = '';
    let hash = '';
    let queryFound = false;
    let hashFound = false;

    const urlIter = url[Symbol.iterator]();

    for (const char of noComplete(urlIter)) {
        if (char === '?') {
            queryFound = true;
            break;
        } else if (char === '#') {
            hashFound = true;
            break;
        } else {
            path += char;
        }
    }

    if (queryFound) {
        for (const char of noComplete(urlIter)) {
            if (char === '#') {
                hashFound = true;
                break;
            } else {
                query += char;
            }
        }
    }

    if (hashFound) {
        hash = Array.from(urlIter).join('');
    }

    return {
        path,
        query,
        hash,
    };
}


export class URL extends ReadonlyStore {
    constructor(path, query = '', hash = '', name = '') {
        super();

        this.subsToZero = this.__unsub_children;
        this.subsToNotZero = this.__sub_children;
        this.value = this;
        this.__path = new URLPath();
        this.__query = new NestingStore({}, `${name}.query`);

        this.path = path;
        this.query = query;
        this.hash = hash;
        this.name = name;
    }

    static fromString(newValue) {
        const { path, hash, query } = parseURL(newValue);
        return new this(path, query, hash);
    }

    static fromLocation(location, base, name) {
        let path;
        if (base === undefined) {
            path = location.pathname;
        } else {
            const urlPath = URLPath.fromString(location.pathname);
            urlPath.removePrefix(base);
            path = String(urlPath);
        }
        return new this(path, location.query, location.hash, name);
    }


    get path() {
        return this.__path;
    }

    set path(newValue) {
        this.__path.set(newValue);
    }

    get hash() {
        return this.__hash;
    }

    set hash(newValue) {
        this.__hash = removePrefix(newValue, '#');
        this.__changed();
    }

    get hashElem() {
        if (globalThis.document === undefined) {
            throw new Code(
                T`hashElem only works when the global document is provided.`,
            );
        } else {
            // eslint-disable-next-line no-undef
            return document.getElementById(this.hash);
        }
    }

    set hashElem(newElem) {
        this.hash = newElem.id;
    }

    get query() {
        return this.__query;
    }

    set query(newValue) {
        if (newValue instanceof NestingStore) {
            newValue = newValue.value;
        }

        if (typeof newValue === 'string') {
            newValue = removePrefix(newValue, '?');
            newValue = parse(newValue);
        }

        this.__query.set(newValue);
    }

    set(newValue) {
        if (typeof newValue === 'string') {
            const { path, hash, query } = parseURL(newValue);
            this.path = path;
            this.hash = hash;
            this.query = query;
        } else {
            this.path = newValue.path;
            this.hash = newValue.hash;
            this.query = newValue.query;
        }
    }

    toString() {
        let string = String(this.path);

        if (!isEmpty(this.query.value)) {
            string += `?${stringify(this.query.value)}`;
        }

        if (this.hash) {
            string += `#${this.hash}`;
        }

        return string;
    }

    [Symbol.for('nodejs.util.inspect.custom')]() {
        const clsName = this.constructor.name;
        return `${clsName} < ${this} >`;
    }

    copy() {
        return new this.constructor(this.path, this.query, this.hash);
    }


    __unsub_children() {
        this.__unsubPath();
        this.__unsubQuery();
    }

    __sub_children() {
        this.__unsubPath = this.__path.subscribe((newValue) => {
            this.__changed();
        });

        this.__unsubQuery = this.__query.subscribe((newValue) => {
            this.__changed();
        });
    }
}
