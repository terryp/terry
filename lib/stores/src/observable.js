import { BehaviorSubject } from 'rxjs';

export function fromStore(svelteStore) {
    const subject = new BehaviorSubject();
    const unsub = svelteStore.subscribe((value) => {
        subject.next(value);
    });
    subject.subscribe({
        complete: () => unsub(),
    });
    return subject;
}
