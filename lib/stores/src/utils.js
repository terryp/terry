import { ConnectableStore } from '#src/base.js';
import { getArgsAndCb } from '@omniblack/utils';

export function maybeStore(value) {
    return typeof value?.subscribe === 'function'
        ? value
        : new ConnectableStore(value);
}


export function writableToReadable(store) {
    return {
        subscribe: store.subscribe,
    };
}


export function watchStores(...args) {
    const {
        cb,
        args: watchedStores,
    } = getArgsAndCb('watchStores', args, 'two');

    let taskQueued = false;
    const values = [];

    const unsubscribes = watchedStores.map((store, index) => {
        return store.subscribe((value) => {
            values[index] = value;

            if (!taskQueued) {
                taskQueued = true;
                queueMicrotask(flushChanges);
            }
        });
    });


    function flushChanges() {
        taskQueued = false;
        cb(...values);
    }

    return function() {
        for (const unsub of unsubscribes) {
            unsub();
        }
    };
}
