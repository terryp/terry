import { writable } from 'svelte/store';

export function syncTo(fromSelf, other, fromOther) {
    let settingOther = false;
    let value;
    const innerSelf = writable();

    function set(newValue) {
        value = newValue;
        innerSelf.set(value);
        settingOther = true;
        other.set(fromSelf(value));
        settingOther = false;
    }

    function update(updater) {
        const newValue = updater(value);
        set(newValue);
    }

    other.subscribe((newOtherValue) => {
        if (settingOther) {
            return;
        }

        value = fromOther(newOtherValue);

        innerSelf.set(value);
    });

    return {
        set,
        update,
        subscribe: innerSelf.subscribe,
    };
}
