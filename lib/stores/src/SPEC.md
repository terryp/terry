# Store Interfaces

## Keywords

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL
NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and
"OPTIONAL" in this document are to be interpreted as described in
[RFC 2119].

## Svelte Store Contract

A svelte store [MUST] implement a [subscribe] method to be considered a valid
svelte store. A svelte store [MAY] implement a [set] method to become a
writable store. Writable stores [MAY] also implement an [update] method.

### Subscribe

The subscribe method takes one argument, the subscription function.
The subscription function [MUST] be immediately and synchronously called with
the store's current value. When the store's value is changed, all active
subscription functions [MUST] be synchronously called. The return of subscribe
method [MUST] be either an object with an unsubscribe method, or an unsubscribe
function. Once this  unsubscribe function is called the store [MUST] not call
the subscription function again.

### Set

The set method takes one argument, the new value of the store. When the set
method is called the store [MUST] synchronously call all subscription functions
with the new store value.

### Update

The update method takes one argument, an updater function. The updater
function [MUST] be called with the store's current value. The return value
of the updater function [MUST] be used as the store's new value.

### Connectable Stores

Connectable stores are an extension of the [svelte store contract]


[MUST]: https://www.ietf.org/rfc/rfc2119.txt
[MAY]: https://www.ietf.org/rfc/rfc2119.txt
[RFC 2119]: https://www.ietf.org/rfc/rfc2119.txt
[set]: #markdown-header-set
[update]: #markdown-header-update
[subscribe]: #markdown-header-subscribe
