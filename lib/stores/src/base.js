/* eslint-env shared-node-browser */
import autoBind from 'auto-bind';
import { enumerate } from 'itertools';
import { getArgsAndCb } from '@omniblack/utils';
import { searchThrottle } from '#src/searchThrottle.js';

const noOp = () => {};

export class ReadonlyStore {
    constructor(subsToZero = noOp, subsToNotZero = noOp) {
        this.subsToNotZero = subsToNotZero;
        this.subsToZero = subsToZero;
        this.subscribers = new Set();
        autoBind(this);
    }

    __changed() {
        for (const sub of this.subscribers) {
            sub(this.value);
        }
    }

    subscribe(subscription) {
        const prevCount = this.subscribers.size;
        this.subscribers.add(subscription);
        subscription(this.value);
        const newCount = this.subscribers.size;

        if (prevCount === 0 && newCount > 0) {
            this.subsToNotZero(this);
        }

        return () => this.__unsubscribe(subscription);
    }

    __unsubscribe(subscription) {
        const prevCount = this.subscribers.size;
        this.subscribers.delete(subscription);
        const newCount = this.subscribers.size;

        if (prevCount > 0 && newCount === 0) {
            this.subsToZero(this);
        }
    }
}

export class BaseStore {
    constructor(initialValue, name) {
        this.subscribers = new Set();
        this.value = initialValue;
        this.name = name;
        this.debugArgs = {};
        this.debugEnabled = true;
        autoBind(this);
    }

    __dead() {
        return this.subscribers.size === 0 && this.value === undefined;
    }

    __changed({ connected = false } = {}) {
        for (const sub of this.subscribers) {
            sub(this.value);
        }

        this.debug();
    }

    extraArgs(...otherArgs) {
        return Object.assign({}, ...otherArgs);
    }

    subscribe(subscription) {
        this.subscribers.add(subscription);

        subscription(this.value);

        return () => this.subscribers.delete(subscription);
    }

    debug(args) {
        const config = globalThis.config;
        if (!this.debugEnabled || !config?.store_debug || !config.dev) {
            return;
        }

        const finalArgs = {
            ...this.__get_display(),
            source: 'external',
            ...this.debugArgs,
            ...args,
        };

        this.debugArgs = {};

        console.log(finalArgs);
    }

    __display(display, memo) {
        display.name = this.name;
        display.value = this.value;
        display.cls = this.constructor.name;

        return display;
    }

    __get_display(memo = new Map()) {
        if (memo.has(this)) {
            return memo.get(this);
        }

        const display = {};
        memo.set(this, display);

        this.__display(display, memo);
        return display;
    }
}

export class AsyncDerivedStore extends BaseStore {
    constructor(name, ...args) {
        const { cb: deriveValue, args: watchedStores } = getArgsAndCb(
            new.target.name,
            args,
            'two',
        );

        super(undefined, name);

        this.watchedStores = watchedStores;
        this.deriveValue = searchThrottle(deriveValue);
        this.values = [];

        for (const [index, store] of enumerate(watchedStores)) {
            store.subscribe(
                (newValue) => this.__storeChanged(index, newValue),
            );
        }
    }

    async recalculate() {
        this.value = await this.deriveValue(...this.values);
        this.__changed();
    }

    async __storeChanged(index, newValue) {
        this.values[index] = newValue;
        this.value = await this.deriveValue(...this.values);
        this.__changed();
    }

    __dead() {
        const watchedDead = this.watchedStores
            .every((store) => store.__dead());

        return watchedDead && super.__dead();
    }
}

export class DerivedStore extends BaseStore {
    constructor(name, ...args) {
        const { cb: deriveValue, args: watchedStores } = getArgsAndCb(
            new.target.name,
            args,
            'two',
        );

        super(undefined, name);

        this.watchedStores = watchedStores;
        this.deriveValue = deriveValue;
        this.values = [];

        for (const [index, store] of enumerate(watchedStores)) {
            store.subscribe(
                (newValue) => this.__storeChanged(index, newValue),
            );
        }
    }

    __storeChanged(index, newValue) {
        this.values[index] = newValue;
        this.value = this.deriveValue(...this.values);
        this.__changed();
    }

    __dead() {
        const watchedDead = this.watchedStores
            .every((store) => store.__dead());

        return watchedDead && super.__dead();
    }
}


export class WritableStore extends BaseStore {
    set(newValue) {
        this.debugArgs.oldValue = this.value;
        this.value = newValue;
        this.__changed();
    }

    update(updater) {
        const newValue = updater(this.value);
        this.set(newValue);
    }

    async updateAsync(updater) {
        const newValue = await updater(this.value);
        this.set(newValue);
    }
}

export class ConnectableStore extends connectable(WritableStore) {}


/*
 * A mixin to add the connectable store interface.
 * This mixin assumes:
 *      1. The `__changed` method is used to notify subscribers
 *      1. `this.value` will be used to store the current value of the store.
 */
export function connectable(klass) {
    return class ConnectableStore extends klass {
        constructor(...args) {
            super(...args);
            this.__connected = [];
        }

        __changed({ connected = false, ...rest } = {}) {
            for (const otherStore of this.__connected) {
                if (otherStore !== connected) {
                    this.__connected?.__setFromConnected(this.value, this);
                }
            }

            return super.__changed(rest);
        }

        __dead() {
            return this.__connected.length === 0 && super.__dead();
        }

        __display(display, memo) {
            const connectedStores = this.__connected
                .map((store) => store.__get_display(memo));
            display.connectedStores = connectedStores;
            return super.__display(display);
        }

        connect(otherStore) {
            this.__connected.push(otherStore);
            otherStore.__connect(this, this.value);
            this.debug({
                source: 'connecting',
            });

            return () => this.disconnect(otherStore);
        }

        __connect(callerStore, newValue) {
            this.debugArgs.source = 'connecting';
            this.debugArgs.oldValue = this.value;
            this.value = newValue;
            this.__connected.push(callerStore);
            this.__changed({ connected: callerStore });
        }

        __setFromConnected(newValue, otherStore) {
            this.debugArgs.source = 'connected set';
            this.debugArgs.oldValue = this.value;
            this.value = newValue;
            this.__changed({ connected: otherStore });
        }

        disconnect(otherStore) {
            const index = this.__connected.indexOf(otherStore);

            if (index !== -1) {
                this.__connected.splice(index, 1);
            }

            otherStore.__disconnect(this);
        }

        __disconnect(otherStore) {
            const index = this.__connected.indexOf(otherStore);

            if (index !== -1) {
                this.__connected.splice(index, 1);
            }
        }
    };
}

export const SyncableStore = syncable(ConnectableStore);

export function syncable(klass) {
    return class SyncableStore extends klass {
        constructor(...args) {
            super(...args);
            this.__sync_stores = new Map();
        }


        __dead() {
            return this.__sync_stores.size === 0 && super.__dead();
        }

        __changed({ sync = false, ...rest } = {}) {
            for (const store of this.__sync_stores.keys()) {
                if (store !== sync) {
                    store.__sync_set(this.value, this);
                }
            }

            super.__changed(rest);
        }

        __display(display, memo) {
            const syncedStores = Array.from(this.__sync_stores.keys())
                .map((store) => store.__get_display(memo));

            display.syncedStores = syncedStores;

            return super.__display(display, memo);
        }

        __sync_set(otherValue, otherStore) {
            this.debugArgs.source = 'sync_set';
            const fromSync = this.__sync_stores.get(otherStore);
            const newValue = fromSync(otherValue);
            this.value = newValue;
            this.__changed({ sync: otherStore });
        }

        sync(fromSelf, otherStore, fromOther) {
            this.__sync_stores.set(otherStore, fromOther);
            this.debugArgs.source = 'syncing';
            otherStore.__sync(this, fromSelf);
            this.debug();

            return () => this.unsync(otherStore);
        }

        unsync(otherStore) {
            otherStore.__unsync(this);
            this.__sync_stores.delete(otherStore);
        }

        __unsync(otherStore) {
            this.__sync_stores.delete(otherStore);
        }

        __sync(otherStore, fromOther) {
            this.debugArgs.source = 'syncing';
            this.__sync_stores.set(otherStore, fromOther);

            this.value = fromOther(otherStore.value);
            this.__changed({ sync: otherStore });
        }
    };
}
