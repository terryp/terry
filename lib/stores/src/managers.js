import { onDestroy } from 'svelte';

export function connectionManager() {
    let aStore;
    let bStore;
    let disconnect;

    function setStores(newAStore, newBStore) {
        if (newAStore !== aStore && newBStore !== bStore) {
            if (disconnect) {
                disconnect();
            }

            disconnect = newAStore.connect(newBStore);
            aStore = newAStore;
            bStore = newBStore;
        } else if (newAStore !== aStore) {
            if (disconnect) {
                disconnect();
            }

            disconnect = newAStore.connect(bStore);
            aStore = newAStore;
        } else if (newBStore !== bStore) {
            if (disconnect) {
                disconnect();
            }

            disconnect = newBStore.connect(aStore);
            bStore = newBStore;
        }
    }

    onDestroy(() => {
        aStore?.disconnect();
    });

    return { setStores };
}


export function syncManager(fromA, fromB) {
    let aStore;
    let bStore;
    let unsync;

    function setStores(newAStore, newBStore) {
        if (newAStore !== aStore && newBStore !== bStore) {
            if (unsync) {
                unsync();
            }
            unsync = newAStore.sync(fromA, newBStore, fromB);
            aStore = newAStore;
            bStore = newBStore;
        } else if (newAStore !== aStore) {
            if (unsync) {
                unsync();
            }

            unsync = newAStore.sync(fromA, bStore, fromB);
            aStore = newAStore;
        } else if (newBStore !== bStore) {
            if (unsync) {
                unsync();
            }
            unsync = newBStore.sync(fromB, aStore, fromA);
            bStore = newBStore;
        }
    }


    onDestroy(() => {
        if (unsync) {
            unsync();
            unsync = undefined;
        }
    });

    return { setStores };
}
