'use modules';
export const plugins = [
    { src: '#src/plugins/persist.js', ssr: false },
    { src: '#src/plugins/now.js' },
];
