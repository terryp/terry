import { Root } from '@omniblack/mithril';
import appConfig from '@omniblack/vulcan/current_config';
import config_setup from '@omniblack/config/web';
import routes from '#src/pages/index.js';
import storage from '@omniblack/storage';
import rpg from '#src/utils/rpg.js';

config_setup.setup(storage, rpg);


// Keep a ref to the app to prevent GC

const _app = new Root({
    target: document.getElementById('app'),
    props: {
        routes,
        baseURL: appConfig.public_path,
        asyncSetup: config_setup.asyncSetup,
    },
});
