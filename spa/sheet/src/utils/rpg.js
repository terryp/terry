import { create, convertErrors, addBaseURL, extractData } from '@omniblack/get';
import { EventEmitter } from '@omniblack/sync';
export default {
    name: 'rpg',
    setup,
    asyncSetup,
};

const base = 'https://terryp.gitlab.io';

function setup() {
    config.rpg.events = new EventEmitter([
        'short_rest',
        'long_rest',
    ]);
}

const req = create(convertErrors, addBaseURL(base), extractData);
async function asyncSetup() {
    const allNames = await req.get('fifth_edition_data/index.json');
    for (const name of allNames) {
        config.rpg[name] = await req.get(`/fifth_edition_data/${name}.json`);
        // we will handle the LICENSE in a different way
        delete config.rpg[name].LICENSE;
    }
}
