import { range, sum } from 'itertools';
import { ParseError, Code } from '@omniblack/error';

const diceRe = /^(?<number>\d+)d(?<size>\d+)$/u;

export class Dice {
    static from(str) {
        const match = diceRe.exec(str);
        if (!match) {
            throw new ParseError('dice', str);
        }

        const number = Number.parseInt(match.groups.number);
        const size = Number.parseInt(match.groups.size);
        return new this({ number, size });
    }

    constructor({ number = 1, size }) {
        this.number = number;
        this.size = size;
    }

    roll(number) {
        const min = 1;
        const max = this.size;
        const values = [];

        const num = number ?? this.number;

        if (num === undefined || num === null) {
            throw new Code(`
                Dice must either have number set or a number must be passed
                to roll.
            `);
        }


        for (const _ of range(this.number)) {
            const rand = Math.random();
            values.push(Math.floor((rand * (max - min + 1)) + min));
        }

        const total = sum(values);

        return { values, total };
    }
}
