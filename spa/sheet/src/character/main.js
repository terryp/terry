import functions from '#src/character/index.js';
import defaultChar from '#src/data/default_character.yaml';
import { DerivedStore } from '@omniblack/stores';

export function getChar() {
    const characters = config.storage.get('characters', []);

    const character = characters.getChild('0', defaultChar);

    const level = character.getChild('level');

    const profBonus = new DerivedStore(
        'proficiencyBonus',
        level,
        ($level) => Math.ceil(1 + ($level / 4)),
    );

    character.attachDerivedStore('proficiencyBonus', profBonus);

    const klass = character.getChild('class');

    const classDef = new DerivedStore('classDef', klass, ($class) => {
        return config.rpg.classes[$class];
    });

    character.attachDerivedStore('classDef', classDef);

    for (const func of functions) {
        func(character);
    }

    console.log(character);

    return character;
}

