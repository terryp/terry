import { DerivedStore } from '@omniblack/stores';

export function addAttributes(character) {
    const profBonus = character.getChild('proficiencyBonus');
    const attributes = character.getChild('attributes');

    for (const attrKey of Object.keys(config.rpg.attributes)) {
        const attrStore = attributes.getChild(attrKey);
        const valueStore = attrStore.getChild('value');
        const proficient = attrStore.getChild('proficient');
        const modStore = new DerivedStore(
            `${attrKey}.modifier`,
            valueStore,
            ($attrValue) => Math.floor(($attrValue - 10) / 2),
        );

        attrStore.attachDerivedStore('modifier', modStore);

        const savingThrowMod = new DerivedStore(
            `${attrKey}.savingThrowMod`,
            modStore,
            profBonus,
            proficient,
            ($modifier, $proficiencyBonus, $proficient) => {
                if ($proficient) {
                    return $modifier + $proficiencyBonus;
                }
                return $modifier;
            },
        );

        attrStore.attachDerivedStore('saving_throw_modifier', savingThrowMod);
    }
}
