import { addAttributes } from './attributes.js';
import { add_hit_dice } from './hit_dice.js';

const functions = [
    addAttributes,
    add_hit_dice,
];

export default functions;
