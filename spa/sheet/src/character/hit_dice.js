import { DerivedStore } from '@omniblack/stores';
import { Dice } from '#src/utils/dice.js';

export function add_hit_dice(character) {
    console.log('adding hit dice');
    const level = character.getChild('level');

    const klassDef = character.getChild('classDef');

    const rollableDice = new DerivedStore('hitDice', klassDef, ($klassDef) => {
        return new Dice({
            size: $klassDef?.hit_dice,
        });
    });

    const hitDiceStore = character.getChild('hit_dice', { current: null });

    const maxHitDice = new DerivedStore(
        'max_hit_dice',
        level,
        ($level) => $level,
    );

    hitDiceStore.attachDerivedStore('max', maxHitDice);

    hitDiceStore.attachDerivedStore('dice', rollableDice);

    hitDiceStore.update(({ max, current }) => {
        if (current === null) {
            return { max, current: max };
        }

        return { max, current };
    });

    config.rpg.events.on('long_rest', () => {
        hitDiceStore.update(({ max, current }) => {
            const toAdd = Math.floor(max / 2);
            const newCurrentUncapped = current + toAdd;
            const newCurrent = Math.min(newCurrentUncapped, max);
            return {
                max,
                current: newCurrent,
            };
        });
    });
}
