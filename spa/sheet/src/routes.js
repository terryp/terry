export default {
    '/': {
        display: 'Home',
    },
    'edit': {
        display: 'Edit',
    },
    'schedule': {
        display: 'Schedule',
    },
    'settings': {
        display: 'Settings',
    },
};
