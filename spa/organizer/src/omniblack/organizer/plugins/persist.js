import VuexPersistence from 'vuex-persist';
import { get, cloneDeep } from 'lodash-es';
import { DateTime } from 'luxon';
import localforage from 'localforage';
import pkg from '@#src/package.json';

localforage.config({
    name: pkg.productName,
    storeName: 'terrys_organzer',
});

function persist({ store }) {
    window.onNuxtReady(() => {
        new VuexPersistence({
            strictMode: false,
            key: 'organizer',
            storage: localforage,
            asyncStorage: true,
            async saveState(key, state, storage) {
                const savableState = cloneDeep(state);
                delete savableState.now;
                const { events, tasks } = savableState;
                for (const event of Object.values(events)) {
                    event.date = event.date.toISO();
                }

                for (const task of Object.values(tasks)) {
                    task.due = task.due.toISO();
                }

                return await storage.setItem(key, savableState);
            },
            async restoreState(key, storage) {
                const state = await storage.getItem(key);

                const events = get(state, 'events', {});
                const tasks = get(state, 'tasks', {});

                for (const task of Object.values(tasks)) {
                    task.due = DateTime.fromISO(task.due);
                }

                for (const event of Object.values(events)) {
                    event.date = DateTime.fromISO(event.date);
                }

                return state;
            },

            reducer({ user, events, tasks, schedule }) {
                return { user, events, tasks, schedule };
            },

            filter({ type }) {
                return type !== 'setDate';
            },
        }).plugin(store);
    });
}
export default persist;
