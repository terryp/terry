import { Root } from '@omniblack/mithril';
import appConfig from '@omniblack/vulcan/current_config';
import config from '@omniblack/config/web';
import routes from '#src/routes/index.js';
import { loadPyodide } from 'pyodide';

console.log(loadPyodide);

config.setup();
// Keep a ref to the app to prevent GC
const _app = new Root({
    target: document.getElementById('app'),
    props: {
        routes,
        baseURL: appConfig.public_path,
    },
});
