import index from './index.svelte';
import appConfig from '@omniblack/vulcan/current_config';

export default [
    {
        component: index,
        path: '/',
        display: appConfig.product_name,
    },
];
