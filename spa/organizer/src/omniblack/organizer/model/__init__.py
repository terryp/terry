from omniblack.model import Model
from omniblack.mongo import mongo

model = Model(
    'Organizer',
    plugins=[mongo('mongo', 3270)],
)
