import { DateTime, Duration } from 'luxon';
import { readable, derived } from 'svelte/store';

const minute = Duration.fromObject({ minute: 1 });

const time = DateTime.local();

export const now = readable(time, (set) => {
    const interval = setInterval(
        () => {
            set(DateTime.local());
        },
        minute.as('millisecond'),
    );

    return () => clearInterval(interval);
});


export const currentWeekDay = derived(now, ($now) => $now.weekdayLong);

export const currentHour = derived(now, ($now) => $now.hour);

export const timeOfDay = derived(currentHour, ($currentHour) => {
    const hour = $currentHour;
    if (hour < 12) {
        return 'morning';
    } else if (hour >= 12 && hour < 17) {
        return 'afternoon';
    } else {
        return 'evening';
    }
});


export function isPast(now, date) {
    const minDate = DateTime.min(now, date);
    const isPast = minDate === date;
    return isPast;
}

export function isToday(now, date) {
    return now.hasSame(date, 'day');
}

export function isUpcoming(now, date) {
    const diff = date.diff(now, ['days']);
    const sameDay = isToday(now, date);
    const upcoming = diff.days < 7;
    return upcoming && !sameDay;
}

export function isFuture(now, date) {
    return !isUpcoming(now, date) && !isToday(now, date);
}

