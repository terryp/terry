import { sorted } from 'itertools';
import { v4 as uuid } from 'uuid';
import { DateTime } from 'luxon';
import {
    NestingStore,
    DerivedStore,
} from '@omniblack/stores';

import { isPast, isToday, isUpcoming, isFuture, now } from '#src/store/now.js';

export function getID(eventOrId) {
    return typeof eventOrId === 'string'
        ? eventOrId
        : eventOrId.id;
}

function createEntryState(name) {
    const entries = new NestingStore({}, name.toLowerCase());

    const sortedEntries = new DerivedStore(
        `sorted${name}`,
        entries,
        ($entries) =>
            sorted(Object.values($entries), ({ date }) => date.valueOf()),
    );

    const overdueEntries = new DerivedStore(
        `overdue${name}`,
        sortedEntries,
        now,
        ($sortedEntries, $now) =>
            $sortedEntries.filter(({ date }) => isPast($now, date)),
    );


    const currentEntries = new DerivedStore(
        `current${name}`,
        sortedEntries,
        now,
        ($sortedEntries, $now) =>
            $sortedEntries.filter(({ date }) => !isPast($now, date)),
    );

    const todayEntries = new DerivedStore(
        `today${name}`,
        currentEntries,
        now,
        ($currentEntries, $now) =>
            $currentEntries.filter(({ date }) => isToday($now, date)),
    );

    overdueEntries.subscribe((values) => console.log(values));

    const upcomingEntries = new DerivedStore(
        `upcoming${name}`,
        currentEntries,
        now,
        ($currentEntries, $now) =>
            $currentEntries.filter(({ date }) => isUpcoming($now, date)),
    );

    const futureEntries = new DerivedStore(
        `future${name}`,
        currentEntries,
        now,
        ($currentEntries, $now) =>
            $currentEntries.filter(({ date }) => isFuture($now, date)),
    );

    function edit(entry) {
        if (!entry.id) {
            entry.id = uuid();
        }

        entries.update((currentEntries) => {
            currentEntries[entry.id] = entry;
            return currentEntries;
        });
    }

    function del(entry) {
        const id = getID(entry);

        entries.update((currentEntries) => {
            delete currentEntries[id];
            return currentEntries;
        });
    }

    const readonlyStore = {
        overdue: overdueEntries,
        current: currentEntries,
        today: todayEntries,
        upcoming: upcomingEntries,
        future: futureEntries,
        sorted: sortedEntries,
        edit: edit,
        delete: del,
    };

    return readonlyStore;
}

export const events = createEntryState('Events');
export const tasks = createEntryState('Tasks');

tasks.edit({
    name: 'Overdue Task',
    date: DateTime.now().minus({ day: 2 }),
});

events.edit({
    name: 'Today Event',
    date: DateTime.now().plus({ hour: 1 }),
});
