import { NestingStore, DerivedStore } from '@omniblack/stores';
import { currentWeekDay, currentHour } from '#src/store/now.js';

export const schedule = new NestingStore({
    Monday: {},
    Tuesday: {},
    Wednesday: {
        secondary: 'Omniblack',
    },
    Thursday: {},
    Friday: {},
    Saturday: {},
    Sunday: {},
}, 'schedule');

export const categories = ['primary', 'secondary'];

export const todaySchedule = new DerivedStore(
    'todaySchedule',
    schedule,
    currentWeekDay,
    ($schedule, $currentWeekDay) => {
        const todayEntry = $schedule[$currentWeekDay];
        return todayEntry;
    },
);

export const currentFocus = new DerivedStore(
    'currentFocus',
    todaySchedule,
    currentHour,
    ($todaySchedule, $currentHour) => {
        return $currentHour < 16
            ? $todaySchedule?.primary
            : $todaySchedule?.secondary;
    },
);


