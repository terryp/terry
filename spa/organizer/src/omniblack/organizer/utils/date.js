import { DateTime } from 'luxon';

export function isPast(now, date) {
    const minDate = DateTime.min(now, date);
    const isPast = minDate === date;
    return isPast;
}

export function isToday(now, date) {
    return now.hasSame(date, 'day');
}

export function isUpcoming(now, date) {
    const diff = date.diff(now, ['days']);
    const sameDay = isToday(now, date);
    const upcoming = diff.days < 7;
    return upcoming && !sameDay;
}

export function isFuture(now, date) {
    return !isUpcoming(now, date) && !isToday(now, date);
}

