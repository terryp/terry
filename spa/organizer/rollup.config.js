import alias from '@rollup/plugin-alias';

export default {
    external: ['https://cdn.jsdelivr.net/pyodide/v0.24.1/full/pyodide.mjs'],
    plugins: [
        alias({
            entries: {
                pyodide: 'https://cdn.jsdelivr.net/pyodide/v0.24.1/full/pyodide.mjs',
            },
        }),
    ],
};
