export default {
    '/': {
        display: 'Home',
    },
    'settings': {
        display: 'Settings',
    },
};
