export const state = {
    firstName: '',
};

export const mutations = {
    setFirstName(state, name) {
        state.firstName = name;
    },
};

export const actions = {
    setFirstName({ commit }, name) {
        commit('setFirstName', name);
    },
};
