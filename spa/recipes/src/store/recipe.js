export const state = {
    id: {
        id: 'id',
        name: 'name',
        ingredients: [
            {
                ingredient: 'beansId',
                amount: '28',
                unit: 'ounce',
            },
        ],
        equipment: [
            {
                // id to an equipment type
                item: 'bowlId',
                // number of this equipment needed
                number: 1,
            },
        ],
        steps: [
            'Do stuff',
            'Put {beansId|28|ounce} in {bowlId|1}',
        ],
    },
};
