export const state = {
    id: {
        id: 'id',
        name: {
            en: 'Bowl',
        },
        desc: {
            en: 'A large glass mixing bowl',
        },
    },
};
