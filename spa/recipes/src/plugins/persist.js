import VuexPersistence from 'vuex-persist';
import localforage from 'localforage';
import app from '@#src/app.yaml';

localforage.config({
    name: app.title,
    storeName: app.name,
});

function persist({ store }) {
    window.onNuxtReady(() => {
        new VuexPersistence({
            strictMode: false,
            key: app.name,
            storage: localforage,
            asyncStorage: true,
        }).plugin(store);
    });
}

export default persist;
