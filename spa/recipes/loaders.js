'use module';

export const loaders = [
    {
        test: /\.yaml$/,
        use: 'js-yaml-loader',
    },
];
