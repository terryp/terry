import globals from 'globals';

import { base, node, getPkgConfigs } from '@omniblack/eslint_config';

const pkgConfigs = await getPkgConfigs();


export default [
    ...base,
    node(['dev-tools/bin/*']),
    {
        files: ['dev-tools/bin/*'],
        rules: {
            'n/hashbang': 'off',
        },
    },
    {
        files: ['dev-tools/mongo.init.d/*'],
        languageOptions: {
            globals: {
                ...globals.mongo,
            },
        },
    },
    node(['pnpmfile.cjs'], 'commonjs'),
    {
        files: ['pnpmfile.cjs'],
        languageOptions: {
            globals: {
                ...globals.es2024,
                ...globals.node,
            },
        },
    },
    ...pkgConfigs,
];
