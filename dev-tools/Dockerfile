ARG FED_VER

FROM registry.fedoraproject.org/fedora:${FED_VER:-40}

COPY --from=repos . /etc/yum.repos.d

ARG SRC
ARG USER
ARG UID
ARG GROUP
ARG GID

RUN --mount=type=cache,target=/var/cache/dnf \
    dnf install -y --nodocs --setopt=install_weak_deps=false \
        git \
        make \
        pnpm \
        nodejs \
        python \
        shadow-utils \
        python3-omniblack-secret \
        python3-ruamel-yaml \
        python3-dnf \
        python3-rpm \
        python3-pillow \
        && dnf clean all


RUN groupadd --gid ${GID}  ${USER} \
    && useradd --uid ${UID} \
        --gid ${USER} \
        --home-dir "${SRC}" \
        --no-create-home \
        --shell /bin/bash \
        ${USER} \
    && mkdir -p ${SRC} \
    && chown -R ${UID}:${GID} ${SRC}

ARG __FORCE_RESTART

ENV PATH="${SRC}/node_modules/.bin:${PATH}"
ENV PATH="${SRC}/dev-tools/bin:${PATH}"
ENV PATH="${SRC}/.node/bin:${PATH}"
ENV PATH="${SRC}/.venv/bin:${PATH}"
ENV SRC="${SRC}"
ENV VERSION="development"
ENV HOME="${SRC}"
ENV __FORCE_RESTART=${__FORCE_RESTART}
