from json import dump, load
from os import environ
from pathlib import Path

from package_json import find
from sh import sort_package_json

SRC = Path(environ['SRC'])
homepage_root = 'https://gitlab.com/terryp/terry/-/tree/next/'

root_pkg_path = SRC.joinpath('package.json')

with root_pkg_path.open('r') as root_pkg_file:
    root_pkg = load(root_pkg_file)

license = root_pkg['license']
author = root_pkg['author']
repository = root_pkg['repository']


for pkg, path in find(SRC):
    if (pkg['name'] == '@omniblack/rollup-plugin-html-template'
            or path == root_pkg_path):
        continue

    relative_to_root = path.relative_to(SRC)
    pkg_dir = relative_to_root.parent
    homepage = homepage_root + str(pkg_dir)
    pkg_repo = {'directory': str(pkg_dir)}
    pkg_repo.update(repository)

    if 'exports' in pkg and './package.json' not in pkg['exports']:
        pkg['exports']['./package.json'] = './package.json'

    pkg.setdefault('imports', {})
    pkg['imports']['#src/*'] = './src/*'
    pkg['imports']['#pkg/*'] = './*'

    pkg['repository'] = pkg_repo
    pkg['license'] = license
    pkg['author'] = author
    pkg['homepage'] = homepage

    with path.open('w') as pkg_file:
        dump(pkg, pkg_file, indent=4)

sort_package_json()
