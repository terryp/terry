from os import environ
from pathlib import Path

from .packages import find

SRC = Path(environ['SRC'])

dirs = tuple(
    file.parent
    for pkg, file in find(SRC)
    if pkg['name'] != '@omniblack/rollup-plugin-html-template'
    if pkg['name'] != 'terry'
)

for directory in dirs:
    print(directory)
