from json import load
from os import environ
from pathlib import Path

__all__ = (
    'find',
)


def find(root):
    for entry in root.iterdir():
        if entry.name == 'package.json' and entry.is_file():
            with entry.open('r') as file_obj:
                pkg = load(file_obj)
            yield pkg, entry
        elif entry.is_dir() and entry.name != 'node_modules':
            yield from find(entry)


if __name__ == '__main__':
    SRC = Path(environ['SRC'])
    for pkg, path in find(SRC):
        print(path)
