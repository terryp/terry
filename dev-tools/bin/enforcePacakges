#! /usr/bin/env python
from json import load
from pathlib import Path
from os import environ
from collections import namedtuple
from re import compile

semver_re = compile(r'''
    ^(?P<major>0|[1-9]\d*)
    \.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)
    (?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)
    (?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?
    (?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$
'''.replace(' ', '').replace('\n', ''))

SRC = environ['SRC']
root = Path(SRC)


def findPackages(root):
    for entry in root.iterdir():
        if entry.name == 'package.json' and entry.is_file():
            with entry.open('r') as file_obj:
                pkg = load(file_obj)
            yield pkg, entry
        elif entry.is_dir() and not entry.name == 'node_modules':
            yield from findPackages(entry)


Dep = namedtuple('Dep', ('version',  'name', 'package_path', 'from_src'))

allDeps = {}

allPkgs = {}

invalidDeps = []
multipleVersions = {}

for pkg, path in findPackages(root):
    allPkgs[path] = pkg
    for name, version in pkg.get('dependencies', {}).items():
        dep = Dep(
            version=version,
            name=name,
            package_path=path,
            from_src=path.relative_to(SRC)
        )
        if not semver_re.fullmatch(version):
            invalidDeps.append(dep)

        if name in allDeps and allDeps[name].version != version:
            if name not in multipleVersions:
                multipleVersions[name] = {dep, allDeps[name]}
            else:
                multipleVersions[name].add(dep)
        else:
            allDeps[name] = dep

print('There deps are referenced by multiple versions in the repo')

for name, deps in multipleVersions.items():
    print(f'{name}:')
    for dep in deps:
        print(f'    {dep.from_src}: {dep.version}')
