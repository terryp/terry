#! /usr/bin/env node
/* eslint-env node */

/*
 * read the package.json for a package and follow the dependency tree
 * to output a list of all of our packages that the specified package
 * depends on
 */

import fs from 'node:fs/promises';

// eslint-disable-next-line n/no-process-env
const rootDir = process.env['SRC'];

// track what we have seen already
const seen = new Set();

async function getOurPackages(dir) {
    // only need to check each package once...
    if (seen.has(dir)) {
        return;
    }
    seen.add(dir);

    // get the package details
    let raw;
    try {
        raw = await fs.readFile(`${rootDir}/${dir}/package.json`);
    } catch {
        // no package.json means it is not our package, or not a package
        return;
    }

    // now that we know it is a package and ours, log it
    console.log(dir);

    const details = JSON.parse(raw);

    // look in all the places that dependencies can come from
    const sources = [
        'dependencies',
        'devDependencies',
        'peerDependencies',
        'optionalDependencies',
    ];
    const deps = [];

    for (const src of sources) {
        const all = details[src] ?? {};

        // get the package names (but drop any leading @)
        const pkgs = Object.keys(all).map((key) => {
            if (key.startsWith('@')) {
                return key.slice(1);
            }
            return key;
        });

        // add all the packages from this section
        deps.push(...pkgs);
    }

    // Scan for our packages
    return Promise.all(
        deps.map(async (pkg) => {
            // output this package and go check its dependencies
            return await getOurPackages(pkg);
        }),
    );
}

const pkgPath = process.argv[2];

// start with the one passed in
getOurPackages(pkgPath);
