#! /usr/bin/env node
import { join } from 'node:path';
import { readFile } from 'node:fs/promises';
import { load } from 'js-yaml';
import { program } from 'commander';
import { get } from 'lodash-es';

program
    .description('Get a port for an omniblack package')
    .version('0.0.1')
    .argument('<package>', 'The package to get a port for')
    .argument('<port>', 'The port to get for `package`.')
    .parse();

// eslint-disable-next-line n/no-process-env
const { SRC } = process.env;
const fileName = join(SRC, 'dev-tools/ports.yaml');
const yamlText = await readFile(join(SRC, 'dev-tools/ports.yaml'));

const ports = load(yamlText, { fileName });

const port = get(ports, program.args);

if (port === undefined) {
    // eslint-disable-next-line n/no-process-exit
    process.exit(1);
}

process.stdout.write(String(port));


