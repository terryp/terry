#! /bin/bash

set -e

CMD="$SRC/dev-tools/bin/tilt"
SHORT="$(basename $CMD)"
VER_FILE="$SRC/.tilt-version"
DOWNLOAD=false

function parse_version () {
    echo "$1" | cut -d, -f1 | cut -dv -f2
}

function get_version () {
    parse_version $(test -x $CMD && $CMD version || true)
}


if [[ $1 == "--help" || $1 == "-?" ]]; then
    echo "usage: $0 [--force] [version]"
    exit 1
fi

if [[ $1 == "--force" ]]; then
    DOWNLOAD=true
    shift 1
fi

# manual version selection?
if [[ $# -ge 1 ]]; then
    VERSION="$1"
else
    VERSION="$(cat $VER_FILE)"
fi


# missing or wrong version?
INSTALLED=$(get_version)
if [[ $INSTALLED != $VERSION ]]; then
    echo "Installed $SHORT ($INSTALLED) does not match desired ($VERSION)"
    DOWNLOAD=true
fi

if $DOWNLOAD; then
    BASE=https://github.com/tilt-dev/tilt/releases/download
    DIR=v${VERSION}
    FILE=tilt.${VERSION}.linux.x86_64.tar.gz

    # get the correct version
    curl -fsSL "$BASE/$DIR/$FILE" | tar -xzO $SHORT > "$CMD"
    chmod a+x "$CMD"

    # update the version to match what we just downloaded
    get_version > "$VER_FILE"
    echo $SHORT updated to $(cat "$VER_FILE")
fi
