const db = globalThis.db.getSiblingDB('auth');

db.createUser({
    user: 'auth_user',
    pwd: 'auth',
    roles: [{ role: 'readWrite', db: 'auth' }],
});
