const db = globalThis.db.getSiblingDB('organizer');

db.createUser({
    user: 'organizer_user',
    pwd: 'organizer',
    roles: [{ role: 'readWrite', db: 'organizer' }],
});
