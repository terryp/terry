const db = globalThis.db.getSiblingDB('library');

db.createUser({
    user: 'library_user',
    pwd: 'library',
    roles: [{ role: 'readWrite', db: 'library' }],
});
