const db = globalThis.db.getSiblingDB('server_test');

db.createUser({
    user: 'server_test_user',
    pwd: 'server_test',
    roles: [{ role: 'readWrite', db: 'server_test' }],
});
