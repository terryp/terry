const db = globalThis.db.getSiblingDB('music');

db.createUser({
    user: 'music_user',
    pwd: 'music',
    roles: [{ role: 'readWrite', db: 'music' }],
});
