const db = globalThis.db.getSiblingDB('symptom');

db.createUser({
    user: 'symptom_user',
    pwd: 'symptom',
    roles: [{ role: 'readWrite', db: 'symptom' }],
});
