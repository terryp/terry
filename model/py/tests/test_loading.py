import pytest

from omniblack.model import Model


@pytest.fixture
def model():
    return Model(__name__)


meta_names = {
    'Struct',
    'Field',
    'UIString',
}


def test_load_from_package(model):
    assert not (set(model.structs) & meta_names)
    model.structs.load_model_package('omniblack.model.meta_model')
    assert len(set(model.structs) & meta_names) == len(meta_names)
