from string import capwords

import pytest

from omniblack.model import CoercionError, Model, coerce_from
from omniblack.model.types import TypeDef, native_adapter


def create_test_field(name, field_type):
    display = capwords(name.replace('_', ' '))
    return {
        'name': name,
        'display': {'en': display},
        'desc': {'en': display},
        'type': field_type,
    }


top_struct = {
    'name': 'top_level',
    'display': {'en': 'Top Level'},
    'fields': [
        create_test_field('field_1', 'integer'),
        create_test_field('test_type_field', 'test_type'),
    ],
}


def return_true(*_args, **_kwargs):
    return True


@pytest.fixture
def model():
    model = Model(__name__)
    model.structs(top_struct)
    test_type = TypeDef(
        name='test_type',
        implementation=int,
        adapters={'json': native_adapter},
        validator=return_true,
        attributes=(),
    )
    model.types(**test_type.dict)

    return model


pre_rec = {
    'field_1': 1,
    'test_type_field': 1,
}

expected_post_rec = {
    'field_1': '1',
    'test_type_field': 1,
}


def test_to(model):
    # simple conversion works
    indiv = model.structs.top_level(pre_rec)
    post = model.coerce_to(indiv, 'json')
    assert post == expected_post_rec

    with pytest.raises(CoercionError):
        model.coerce_to(indiv, 'string')


def test_from(model):
    expected_post_indiv = model.structs.top_level(pre_rec)
    post_indiv = coerce_from(model, expected_post_rec, 'json', 'top_level')
    assert post_indiv == expected_post_indiv
