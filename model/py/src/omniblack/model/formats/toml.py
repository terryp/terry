from io import TextIOBase
from tomllib import loads

from public import public

from ..format import Format

write_lib = None
dump = None
dumps = None

try:
    import tomlkit
except ImportError:
    pass
else:
    dump = staticmethod(tomlkit.dump)
    dumps = staticmethod(tomlkit.dumps)

if write_lib is None:
    try:
        import tomli_w
    except ImportError:
        pass
    else:
        dumps = staticmethod(tomli_w.dumps)

        @staticmethod
        def dump_wrapper(file, data):
            string = tomli_w.dumps(data)
            file.write(string)

        dump = dump_wrapper

if write_lib is None:
    try:
        import toml
    except ImportError:
        pass
    else:
        dump = staticmethod(toml.dump)
        dumps = staticmethod(toml.dumps)


@public
class TomlFormat(Format):
    loads = staticmethod(loads)

    @staticmethod
    def load(file: TextIOBase):
        text = file.read()
        return loads(text)

    dump = dump
    dumps = dumps
