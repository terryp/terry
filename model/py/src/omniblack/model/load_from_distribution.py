from importlib.metadata import distribution

from public import public


@public
def load_dist_model(distribution_name):
    dist = distribution(distribution_name)

    model_entries = (
        dist.entry_points.select(group='omniblack.model')
    )

    if len(model_entries) == 1:
        model_entry, = model_entries
        return model_entry.load()
    elif main_model_matches := model_entries.select(name='main'):
        model_entry, = main_model_matches
        return model_entry.load()
    elif model_entries:
        msg = 'Could not load model, too many model entry points.'
        raise ValueError(msg, distribution_name)
    return None
