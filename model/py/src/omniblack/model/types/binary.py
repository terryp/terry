from base64 import b64decode, b64encode
from binascii import Error as B64Error
from sys import maxsize

from humanize import naturalsize as natural_size

from ..field import AttributeFields
from ..localization import TXT
from ..types import native_adapter
from ..validation_result import ErrorTypes, ValidationMessage, ValidationResult
from . import ModelType


class Binary(ModelType):
    implementation = bytes

    to_yaml = native_adapter
    from_yaml = native_adapter
    to_mongo = native_adapter
    from_mongo = native_adapter

    def from_string(self, string: str):
        try:
            return b64decode(string, validate=True)
        except B64Error as err:
            msg = 'Invalid base64 data.'
            raise ValueError(msg) from err

    def to_string(self, value: bytes):
        return b64encode(value).decode('ascii')

    def json_schema(self):
        return {
            'type': 'string',
            'pattern': '^[a-zA-Z0-9+/]*=?$'
        }

    def validator(self, value, path):
        messages = []
        if len(value) < self.attrs.min:
            min_human = natural_size(self.attrs.min, binary=True)
            messages.append(ValidationMessage(
                ErrorTypes.constraint_error,
                TXT('${path} must not be less than ${min_human}.', locals()),
                path,
            ))
        elif len(value) > self.attrs.max:
            max_human = natural_size(self.attrs.max, binary=True)
            messages.append(ValidationMessage(
                ErrorTypes.constraint_error,
                TXT(
                    '${path} must not be greater than ${max_human}.',
                    locals(),
                ),
                path,
            ))

        return ValidationResult(messages, valid=not messages)

    attributes = AttributeFields(
        {
            'name': 'min',
            'display': {'en': 'Minimum'},
            'desc': {'en': "The minimum size of the field's data, in bytes."},
            'is_a': {
                'type': 'integer',
                'integer_attrs': {
                    'assumed': 0,
                    'min': 0,
                    'max': maxsize,
                },
            },
        },
        {
            'name': 'max',
            'display': {'en': 'Maximum'},
            'desc': {'en': "The maximum size of the field's data, in bytes."},
            'is_a': {
                'type': 'integer',
                'integer_attrs': {
                    'assumed': maxsize,
                    'min': 0,
                    'max': maxsize,
                },
            },
        },
    )
