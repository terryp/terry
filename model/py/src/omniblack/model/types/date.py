from datetime import date

from ..validation_result import ValidationResult
from . import ModelType


class Date(ModelType):
    implementation = date

    def from_string(self, string: str):
        try:
            return date.fromisoformat(string)
        except TypeError as err:
            msg = 'Invalid date time'
            raise ValueError(msg, string) from err

    def to_string(self, value: date):
        return value.isoformat()

    def validator(self, _value, _path):
        return ValidationResult((), valid=True)
