from sys import float_info

from ..field import AttributeFields
from ..localization import TXT
from ..types import native_adapter
from ..validation_result import ErrorTypes, ValidationMessage, ValidationResult
from . import ModelType


class Estimate(ModelType):
    implementation = float

    def from_string(self, string):
        return float(string)

    def to_string(self, num):
        return str(num)

    to_json = native_adapter
    from_json = native_adapter
    to_yaml = native_adapter
    from_yaml = native_adapter
    to_toml = native_adapter
    from_toml = native_adapter

    def validator(self, value, path):
        messages = []

        attrs = self.is_a.estimate_attrs
        field_max = attrs.max
        field_min = attrs.min

        if value > field_max:
            messages.append(ValidationMessage(
                ErrorTypes.constraint_error,
                TXT('${path} must not be greater than ${max}.', locals()),
                path,
            ))
        elif value < field_min:
            messages.append(ValidationMessage(
                ErrorTypes.constraint_error,
                TXT('${path} must not be less than ${min}.', locals()),
                path,
            ))

        return ValidationResult(messages, valid=not messages)

    attributes = AttributeFields(
        {
            'name': 'min',
            'display': {'en': 'Minimum'},
            'desc': {'en': 'The minimum value allowed for this field.'},
            'is_a': {
                'type': 'estimate',
                'estimate_attrs': {
                    'assumed': float_info.min,
                },
            },
        },
        {
            'name': 'max',
            'display': {'en': 'Maximum'},
            'desc': {'en': 'The maximum value allowed for this field.'},
            'is_a': {
                'type': 'estimate',
                'estimate_attrs': {
                    'assumed': float_info.max,
                },
            },
        },
    )
