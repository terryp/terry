from datetime import datetime

from ..validation_result import ValidationResult
from . import ModelType


class Datetime(ModelType):
    implementation = datetime

    def from_string(self, string: str):
        try:
            return datetime.fromisoformat(string)
        except TypeError as err:
            msg = 'Invalid date time'
            raise ValueError(msg, string) from err

    def to_string(self, value: datetime):
        return value.isoformat()

    def validator(self, _value, _path):
        return ValidationResult((), valid=True)
