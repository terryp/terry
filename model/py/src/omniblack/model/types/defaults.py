from . import ModelType


class Initial(ModelType):
    def from_string(self, value):
        return value

    def to_string(self, value):
        return value


class Assumed(ModelType):
    def from_string(self, value):
        return value

    def to_string(self, value):
        return value
