from importlib import import_module

from . import ModelType


class PyObject(ModelType):
    implementation = object

    def from_string(self, value):
        import_path, object_path = value.split(':', 1)
        try:
            mod = import_module(import_path)

            obj = mod
            for seg in object_path.split('.'):
                obj = getattr(obj, seg)

            return obj
        except ImportError:
            msg = f'"{import_path}" could not be imported.'
            raise ValueError(msg)
        except AttributeError:
            msg = f'"{object_path}" could not be found in "{import_path}"'
            raise ValueError(msg)

    def to_string(self, value):
        try:
            return f'{value.__module__}:{value.__qualname__}'
        except AttributeError:
            raise ValueError(
                'Python objects must have "__module__" and "__qualname__"'
                + ' attributes to be converted to a string repersentation.',
            )
