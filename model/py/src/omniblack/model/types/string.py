from functools import cache
from re import Pattern, compile
from sys import maxsize

from ..field import AttributeFields
from ..types import native_adapter
from ..validation_result import ErrorTypes, ValidationMessage, ValidationResult
from . import ModelType


@cache
def create_allowed_chars_re(allowed_chars: str) -> Pattern:
    return compile(rf'[^{allowed_chars}]')


class String(ModelType):
    implementation = str

    from_json = native_adapter
    to_json = native_adapter

    from_yaml = native_adapter
    to_yaml = native_adapter

    from_string = native_adapter
    to_string = native_adapter

    def json_schema(self):
        attrs = getattr(
            self.is_a,
            'string_attrs',
            self.model.meta_structs.StringAttrs(),
        )

        schema = {
            'type': 'string',
            'minLength': attrs.min_len,
            'maxLength': attrs.max_len,
        }

        if 'allowed_chars' in attrs:
            schema['pattern'] = f'^[{attrs.allowed_chars}]*$'

        return schema

    def validator(self, value, path):
        attrs = self.attrs
        if attrs is None:
            return ValidationResult(valid=True)

        max_len = attrs.max_len
        min_len = attrs.min_len
        allowed_chars = attrs.get('allowed_chars', None)

        messages = []
        if len(value) > max_len:
            messages.append(ValidationMessage(
                ErrorTypes.constraint_error,
                f'{path} may not be longer than {max_len}.',
                path,
            ))

        elif len(value) < min_len:
            messages.append(ValidationMessage(
                ErrorTypes.constraint_error,
                f'{path} may not be shorter than {min_len}.',
                path,
            ))

        if allowed_chars:
            sorted_chars = ''.join(sorted(allowed_chars))

            allowed_chars_re = create_allowed_chars_re(sorted_chars)

            if allowed_chars_re.search(value) is not None:
                msg = (
                    f'${path} may only contain'
                    + f' the characters ${allowed_chars}.'
                )
                messages.append(ValidationMessage(
                    ErrorTypes.constraint_error,
                    msg,
                    path,
                ))

        return ValidationResult(messages, valid=not messages)

    attributes = AttributeFields(
        {
            'name': 'allowed_chars',
            'display': {'en': 'Allowed Characters'},
            'desc': {
                'en': 'A string of characters that'
                      + "are allowed in the field's values.",
            },
            'is_a': {'type': 'string'},
        },
        {
            'name': 'min_len',
            'display': {'en': 'Minimum Length'},
            'desc': {'en': "The minimum length of the field's value."},
            'is_a': {
                'type': 'integer',
                'assumed': 0,
                'integer_attrs': {
                    'min': 0,
                    'max': maxsize,
                },
            },
        },
        {
            'name': 'max_len',
            'display': {'en': 'Maximum'},
            'desc': {'en': "The maximum length of the field's value."},
            'is_a': {
                'type': 'integer',
                'assumed': maxsize,
                'integer_attrs': {
                    'min': 0,
                    'max': maxsize,
                },
            },
        },
    )
