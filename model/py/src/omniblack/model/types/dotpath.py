from ..dot_path import DotPath as DotPathImpl
from ..validation_result import ValidationResult
from . import ModelType


class Dotpath(ModelType):
    implementation = DotPathImpl

    def from_string(self, string: str):
        try:
            return self.implementation.from_string(string)
        except TypeError as err:
            msg = 'Invalid path'
            raise ValueError(msg, string) from err

    def to_string(self, value: DotPathImpl):
        return str(value)

    def validator(self, _path):
        return ValidationResult((), valid=True)
