from collections.abc import Callable, Iterable
from typing import Any

from public import public

from omniblack.string_case import Cases

from ..dot_path import DotPath
from ..field import AttributeFields, Field
from ..metadata import MetaBase
from ..undefined import undefined
from ..validation_result import ErrorTypes, ValidationMessage, ValidationResult


@public
def native_adapter(_self, value):
    return value


type GetDiffs[T] = Callable[[T, T], tuple[object, ...]]
type Walk[T] = Callable[
    [T, DotPath],
    Iterable[tuple[DotPath, T, Field, MetaBase]],
]


@public
class TypeExt:
    name: str
    impl: type | None = None
    attributes: AttributeFields | None = None
    validator: Callable[[Any], ValidationResult] | None = None
    prepare_metadata: Callable[[], None] | None = None
    get_implementation: Callable[[], type] | None = None
    json_schema: Callable[[], dict] | None = None
    walk: Walk | None = None


@public
class ModelType:
    """
    A type for the model, and details of how to manipulate it.

    Attributes
    ----------

    attributes: :type:`typing.Optional[omniblack.model.AttributeFields]`
        attributes that describe a field of this type.
        Will be add to the field struct with the name
        :code:`f'{self.name}_attrs'`

    implementation: :type:`type`
        The python type that this class describes.

    name: :type:`str`
        The name of this type. Derived from the class name.

    sensitive: :type:`bool` = False
        Does the type represent sensitive information.
            Coercion interfaces will attempt to dispose of the original
            pre-coercion value after it has been coerced.
            The type should take measures to not leak secrets
            if introspected, by tools like `rich.inspect`, or in their repr.

            This ***will not*** delete the data from an indiv when converting
            an indiv to an output format.

    container: :type:`bool` = False
        Is the type a container holding sub values?
            `container` types must implement the `walk` method.
            Field `required` logic will treat false-y container
            fields as absent.

    Methods
    -------
    json_schema()
        Returns the json schema representing this field.

        Returns
        -------
        :type:`dict`

    prepare_metadata()
        Called when a field is created. Can be used to prepare
        metadata from the fields attributes.
    """

    sensitive = False
    container = False
    attributes = None
    prepare_metadata = None
    walk: Walk | None = None

    def __init_subclass__(cls):
        cls.name = Cases.Snake.to(cls.__name__)

    def __init__(
        self,
        *,
        field,
        is_a,
        model,
    ):
        self.field = field
        self.is_a = is_a
        self.model = model
        if self.prepare_metadata:
            self.prepare_metadata()
        self.is_a.metadata.type = self

    def __call__(self, *_args, **_kwargs):
        msg = 'Calling a model type is not supported'
        raise DeprecationWarning(msg)

    def __repr__(self):
        cls_name = self.__class__.__name__

        repr_str = f'<{cls_name} name={repr(self.name)}'

        if self.implementation is not None:
            repr_str += f' implementation={repr(self.implementation)}'

        return repr_str + '>'

    def __bool__(self):
        return True

    @property
    def implementation(self):
        return self.get_implementation()

    @property
    def attrs(self):
        return getattr(self.is_a, f'{self.name}_attrs', None)

    def get_implementation(self):
        """
        Return the implementation for the associated field.
        Is called by the default :code:`implementation` property.

        Returns
        -------
        :type:`typing.Optional[type]`
        """
        return None

    def validate(self, value: Any, path: DotPath):
        """
        The validator for this type.

        .. note::
            Does not need to handle required.
            The model function :code:`validate` handles required validation
            before calling the type specific validator.

        Parameters
        ----------

        value: :type:`typing.Any`
            The value of the field to validate.

        path: :type:`omniblack.model.DotPath`
            The path to the field in the overall struct.

        Returns
        -------
        :type:`omniblack.model.validation_Result.ValidationResult`
        """
        impl = self.implementation

        nullish_value = (
            value is undefined
            or value is None
            or (self.container and not value)  # an empty container
        )

        if nullish_value:
            if self.field.required:
                msg = ValidationMessage(
                    ErrorTypes.constraint_error,
                    f'"{path}" is required.',
                    path,
                )
                return ValidationResult(msg, valid=False)
            else:
                return ValidationResult(valid=True)
        elif not isinstance(value, impl):
            name = self.name
            txt = f'{value} is not valid for the type {name}.'
            msg = ValidationMessage(ErrorTypes.invalid_value, txt, path)
            return ValidationResult(msg, valid=False)

        elif validator := getattr(self, 'validator', None):
            return validator(value, path)
        else:
            return ValidationResult(valid=True)

    def __eq__(self, other):
        if isinstance(other, str):
            return self.name == other
        elif isinstance(other, ModelType):
            return self.name == other.name
        else:
            return NotImplemented
