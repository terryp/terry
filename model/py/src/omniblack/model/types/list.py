from ..field import AttributeFields
from ..validation_result import ValidationResult
from . import ModelType


class List(ModelType):
    implementation = list
    container = True

    def from_generic(self, lst: list, format_name: str):
        return [
            self.model.types.from_format(
                format_name=format_name,
                value=value,
                field=self.field,
                is_a=self.attrs.item,
            )
            for value in lst
        ]

    def from_env(self, env_str):
        items = env_str.split(':')
        return self.from_generic(items, 'env')

    def to_generic(self, lst: list, format_name: str):
        return [
            self.model.types.to_format(
                format_name=format_name,
                value=value,
                field=self.field,
                is_a=self.attrs.item,
            )
            for value in lst
        ]

    def prepare_metadata(self):
        item_type = self.is_a.list_attrs.item.type

        type_cls = self.model.types[item_type]
        type_cls(
            field=self.field,
            is_a=self.is_a.list_attrs.item,
            model=self.model,
        )

        self.item_is_a = self.is_a.list_attrs.item

    def walk(self, value, path):
        if value is None:
            return

        for index, value in enumerate(value):
            value_path = path | index
            yield value_path, value, self.field, self.item_is_a

    def validator(self, values, path):
        result = ValidationResult(valid=True)

        for index, value in enumerate(values):
            sub_path = path | index
            result = result.merge(
                self.item_is_a.metadata.type.validate(value, sub_path),
            )

        return result

    attributes = AttributeFields(
        {
            'name': 'item',
            'display': {'en': 'Items Type'},
            'desc': {'en': 'What type should items be?'},
            'is_a': {
                'type': 'child',
                'child_attrs': {
                    'struct': 'IsA',
                },
            },
        },
        {
            'name': 'singular_display',
            'display': {'en': 'Singular Display Name'},
            'desc': {
                'en': 'A display name used to refer to one item in the list',
            },
            'is_a': {
                'required': True,
                'type': 'string',
            },
        },
        {
            'name': 'minLen',
            'desc': {'en': 'The minimum length of the list.'},
            'display': {
                'en': 'Minimum Length',
            },
            'is_a': {
                'type': 'integer',
                'integer_attrs': {
                    'min': 2,
                },
            },
        },
        {
            'name': 'maxLen',
            'desc': {'en': 'The maximum length of the field.'},
            'display': {'en': 'Maximum Length'},
            'is_a': {
                'type': 'integer',
                'integer_attrs': {
                    'min': 1,
                },
            },
        },
    )
