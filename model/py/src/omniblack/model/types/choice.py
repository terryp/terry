from enum import Enum

from ..field import AttributeFields
from ..validation_result import ValidationResult
from . import ModelType


class EnumBase(Enum):
    @classmethod
    def _missing_(cls, value):
        return cls.name_to_member.get(value)

    @property
    def display(self):
        cls = self.__class__
        return cls.displays[self.value]

    def __repr__(self):
        cls = self.__class__
        display = cls.displays[self.value]

        return f'{cls.__name__}({display["en"]})'

    def __str__(self):
        return self.choice.name

    def __rich_repr__(self):
        yield from self.choice.items()


class Choice(ModelType):
    def from_string(self, string: str):
        enum_cls = self.get_implementation()

        try:
            return enum_cls[string]
        except KeyError as err:
            if member := enum_cls._missing_(string):
                return member

            msg = 'Value is not valid'
            raise ValueError(msg, string) from err

    def to_string(self, value):
        return str(value)

    def validator(self, _value, _path):
        return ValidationResult((), valid=True)

    def prepare_metadata(self):
        names = []
        displays = []
        descriptions = []

        for choice in self.is_a.choice_attrs.choices:
            descriptions.append(getattr(choice, 'desc', None))
            displays.append(choice.display)

            if choice.get('internal'):
                names.append(choice['internal'])
            else:
                names.append(choice['name'])

        enum_name = self.is_a.choice_attrs.enum_name

        enum_cls = EnumBase(
            enum_name,
            names,
            start=0,
            module=self.model.module_name,
        )

        enum_cls.displays = displays
        enum_cls.descriptions = descriptions
        enum_cls.__doc__ = self.field.desc.en

        enum_cls.name_to_member = {}

        for choice in self.is_a.choice_attrs.choices:
            if choice.get('internal'):
                member = enum_cls[choice.internal]
                enum_cls.name_to_member[choice.name] = member
            else:
                member = enum_cls[choice.name]

            member.choice = choice
            if 'desc' in choice:
                member.__doc__ = choice.desc.en

        self.is_a.metadata.enum_cls = enum_cls

        self.model.expose(enum_name, enum_cls)

    def get_implementation(self):
        return self.is_a.metadata.enum_cls

    def json_schema(self):
        all_choices = self.is_a.choice_attrs.choices

        valid_values = [
            choice.name
            for choice in all_choices
        ]

        return {
            'type': 'string',
            'enum': valid_values,
        }

    attributes = AttributeFields(
        {
            'name': 'enum_name',
            'display': {'en': 'Enum Name'},
            'desc': {'en': 'The name of the enum class in code.'},
            'is_a': {
                'type': 'string',
                'required': True,
            },
        },
        {
            'name': 'choices',
            'display': {'en': 'Choices'},
            'desc': {'en': 'The choices this field is limited to.'},
            'required': True,
            'is_a': {
                'type': 'list',
                'list_attrs': {
                    'item': {
                        'type': 'child',
                        'child_attrs': {'struct': 'ChoiceMember'},
                    },
                },
            },
        },
        required=True,
    )
