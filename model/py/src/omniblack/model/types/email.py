from email_validator import EmailNotValidError, validate_email

from ..validation_result import ErrorTypes, ValidationMessage, ValidationResult
from . import ModelType


class Email(str):
    __slots__ = ()


EmailImpl = Email


class Email(ModelType):
    implementation = EmailImpl

    def to_string(self, v):
        return str(v)

    def from_string(self, v):
        return EmailImpl(v)

    def validator(self, value, path):
        messages = []

        try:
            validate_email(value)
        except EmailNotValidError as err:
            messages.append(
                ValidationMessage(ErrorTypes.invalid_value, str(err), [path]),
            )

        return ValidationResult(messages, valid=not messages)

    def json_schema(self):
        return {
            'type': 'string',
            'format': 'idn-email',
        }
