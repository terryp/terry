from os import fspath
from pathlib import Path as PathImpl

from ..validation_result import ValidationResult
from . import ModelType


class Path(ModelType):
    implementation = PathImpl

    def from_string(self, string: str):
        try:
            return self.implementation(string)
        except TypeError as err:
            msg = 'Invalid path'
            raise ValueError(msg, string) from err

    def to_string(self, value: PathImpl):
        return fspath(value)

    def validator(self, _value, _path):
        return ValidationResult((), valid=True)
