from ..field import AttributeFields
from ..localization import TXT
from ..validation_result import ErrorTypes, ValidationMessage, ValidationResult
from . import ModelType

max_64bit = (2**63) - 1
min_64bit = -2**63

max_32bit = (2**31) - 1
min_32bit = (-2**31)


def native_64_bit_to(_self, value):
    if min_64bit <= value <= max_64bit:
        return value
    else:
        return str(value)


def native_32_bit_to(_self, value):
    if min_32bit <= value <= max_32bit:
        return value
    else:
        return str(value)


def native_or_str(_self, value):
    if isinstance(value, str):
        return int(value, 10)
    else:
        return value


class Integer(ModelType):
    implementation = int

    def to_string(self, integer):
        return str(integer)

    def from_string(self, string):
        if isinstance(string, int):
            return string

        return int(string, 10)

    to_mongo = native_64_bit_to
    from_mongo = native_or_str

    to_toml = native_64_bit_to
    from_toml = native_or_str

    to_json = native_32_bit_to
    from_json = native_or_str

    to_yaml = native_32_bit_to
    from_yaml = native_or_str

    def validator(self, value, path):
        attrs = self.is_a.integer_attrs
        field_max = attrs.max
        field_min = attrs.min

        messages = []
        if field_max is not None and value > field_max:
            messages.append(ValidationMessage(
                ErrorTypes.constraint_error,
                TXT('${value} must not be greater than ${max}.', locals()),
                path,
            ))

        elif field_min is not None and value < field_min:
            messages.append(ValidationMessage(
                ErrorTypes.constraint_error,
                TXT('${value} must not be less than ${min}.', locals()),
                path,
            ))

        return ValidationResult(messages, valid=not messages)

    def json_schema(self):
        attrs = self.is_a.integer_attrs

        schema = {
            'type': 'integer',
        }

        if attrs.get('min') is not None:
            schema['minimum'] = attrs.min

        if attrs.get('max') is not None:
            schema['maximum'] = attrs.max

        return schema

    attributes = AttributeFields(
        {
            'name': 'min',
            'display': {'en': 'Minimum'},
            'desc': {'en': 'The minimum value allowed for this field.'},
            'is_a': {'type': 'integer'},
        },
        {
            'name': 'max',
            'display': {'en': 'Maximum'},
            'desc': {'en': 'The maximum value allowed for this field.'},
            'is_a': {'type': 'integer'},
        },
    )
