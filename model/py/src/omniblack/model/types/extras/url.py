try:
    from urllib3.util import Url as _Url, parse_url
except ImportError:
    Url = None
else:
    from ...field import AttributeFields
    from ...validation_result import (
        ErrorTypes,
        ValidationMessage,
        ValidationResult,
    )
    from .. import ModelType

    class Url(ModelType):
        implementation = _Url

        def from_string(self, string: str):
            return parse_url(string)

        def to_string(self, value: _Url):
            return value.url

        def validator(self, value, path):
            msgs = []

            allowed_schemes = getattr(self.attrs, 'allowed_schemes', None)
            if allowed_schemes and value.scheme not in allowed_schemes:
                allowed = ', '.join(self.attrs.allowed_schemes)

                msgs.append(ValidationMessage(
                    ErrorTypes.constraint_error,
                    f'"{path}" may only have a scheme of {allowed}',
                    paths=(path,),
                ))

            return ValidationResult(msgs, valid=not msgs)

        def json_schema(self):
            return {
                'type': 'string',
                'format': 'iri',
            }

        attributes = AttributeFields(
            {
                'name': 'allowed_schemes',
                'display': {'en': 'Allowed Schemes'},
                'desc': {
                    'en': 'The schemes the url is allowed to have.',
                },
                'is_a': {
                    'type': 'list',
                    'list_attrs': {
                        'item': {
                            'type': 'string',
                        },
                    },
                },
            },
        )
