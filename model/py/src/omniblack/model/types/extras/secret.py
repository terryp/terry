from ...validation_result import (
    ErrorTypes,
    ValidationMessage,
    ValidationResult,
)
from .. import ModelType

try:
    from omniblack.secret import Password as PasswordImpl, Secret as SecretImpl
except ImportError:
    Secret = None
    Password = None
else:
    class Secret(ModelType):
        sensitive = True
        implementation = SecretImpl

        def from_string(self, value):
            return self.implementation(value)

        def to_string(self, value):
            return value.reveal()

        def json_schema(self):
            return {'type': 'string'}

    class Password(ModelType):
        sensitive = True
        implementation = PasswordImpl

        def from_string(self, value):
            return self.implementation(value)

        def to_string(self, value):
            return value.reveal()

        def json_schema(self):
            return {'type': 'string'}

        def validator(self, value, path):
            messages = []
            msg = value.check_quality()
            if msg:
                messages.append(ValidationMessage(
                    ErrorTypes.constraint_error,
                    f'"{path}" is not strong enough. Reason {msg}.',
                    path,
                ))

            return ValidationResult(messages, valid=not messages)
