from __future__ import annotations

from collections.abc import Iterable

from . import ModelType


class StructReference:
    meta: bool
    name: str

    __slots__ = ('meta', 'name')
    __match_args__ = ('name',)

    def __init__(self, value: str):
        self.meta = value.startswith('meta:')
        self.name = value.removeprefix('meta:')
        if not self.name:
            msg = 'A struct reference must not have an empty'
            raise ValueError(msg)

    def __str__(self) -> str:
        if self.meta:
            return 'meta:' + self.name
        else:
            return self.name

    def __repr__(self) -> str:
        cls_name = self.__class__.__name__

        return f'<{cls_name} {self.name} meta={self.meta}>'

    def __rich_repr__(self) -> Iterable[str | (str, bool)]:
        yield self.name
        yield 'meta', self.meta

    def _get_other_name(self, other: object) -> str | type(NotImplemented):
        match other:
            case str(name):
                return name
            case __class__(name):
                return name
            case _:
                return NotImplemented

    def __eq__(self, other: StructRef | str) -> bool:
        other_name = self._get_other_name(other)
        if other_name is NotImplemented:
            return NotImplemented

        return self.name == other_name

    def __ne__(self, other: StructRef | str) -> bool:
        other_name = self._get_other_name(other)
        if other_name is NotImplemented:
            return NotImplemented

        return self.name != other_name

    def __le__(self, other: StructRef | str) -> bool:
        other_name = self._get_other_name(other)
        if other_name is NotImplemented:
            return NotImplemented

        return self.name <= other_name

    def __lt__(self, other: StructRef | str) -> bool:
        other_name = self._get_other_name(other)
        if other_name is NotImplemented:
            return NotImplemented

        return self.name < other_name

    def __ge__(self, other: StructRef | str) -> bool:
        other_name = self._get_other_name(other)
        if other_name is NotImplemented:
            return NotImplemented

        return self.name >= other_name

    def __gt__(self, other: StructRef | str) -> bool:
        other_name = self._get_other_name(other)
        if other_name is NotImplemented:
            return NotImplemented

        return self.name > other_name

    def __hash__(self):
        return hash(self.name)

    def __len__(self):
        return len(self.name)

    def __bool__(self):
        return True


StructRef = StructReference


class StructReference(ModelType):
    implementation = StructRef

    def from_string(self, ref_str: str) -> StructRef:
        return self.implementation(ref_str)

    def to_string(self, ref: StructRef) -> str:
        return str(ref)

    def json_schema(self):
        return {
            'type': 'string',
        }
