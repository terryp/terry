from ..field import AttributeFields
from ..format import Format
from ..undefined import undefined
from ..validation_result import ValidationResult
from . import ModelType


class Child(ModelType):
    container = True

    def get_implementation(self):
        return self.model.structs[self.field.is_a.child_attrs.struct]

    def from_generic(self, rec: dict, format_name: str):
        """
        Coerce a record from a format.

        Parameters
        ----------

        model: :type:`omniblack.model.Model`
            The model :code:`struct_name` is defined in.

        rec: :type:`collections.abc.Mapping`
            The rec to convert to :code:`struct_name`.

        format: :type:`str | omniblack.model.Format`
            The format :code:`rec` should converted from.

        struct_name: :type:`str`
            The struct :code:`rec` should be converted to.
        """

        struct_name = self.is_a.child_attrs.struct
        Struct = self.model.structs[struct_name]

        if isinstance(format_name, Format):
            format_name = format.name

        new_rec = {}
        for field in Struct.meta.fields:
            value = rec.get(field.name, undefined)

            if value is undefined:
                continue

            new_value = self.model.types.from_format(
                format_name=format_name,
                value=value,
                field=field,
                is_a=field.type.is_a,
            )

            if new_value is not undefined:
                new_rec[field.name] = new_value

        if not new_rec:
            return undefined

        return Struct(new_rec)

    def to_generic(self, indiv, format_name: str):
        """
        Coerce an indiv to format.

        Parameters
        ----------

        indiv: :type:`omniblack.model.StructBase`
            The indiv to be converted to `format`

        format: :type:`omniblack.model.Format | str`
            The format the output should be compatible with.
        """

        if isinstance(format_name, Format):
            format_name = format_name.name

        new_rec = {}
        for field in indiv.meta.fields:
            value = getattr(indiv, field.name, undefined)

            if 'assumed' in field and value == field.assumed:
                continue

            if value is undefined:
                continue

            new_value = self.model.types.to_format(
                field=field,
                format_name=format_name,
                value=value,
                is_a=field.type.is_a,
            )

            if new_value is not undefined:
                new_rec[field.name] = new_value

        if not new_rec:
            return undefined

        return new_rec

    def json_schema(self):
        return {
            'type': 'object',
            '$ref': f'./{self.attrs.struct}.schema.json',
        }

    @property
    def child_struct(self):
        return self.model.structs[self.attrs.struct]

    def walk(self, value, path):
        for field_meta in self.child_struct.meta.fields:
            field = getattr(self.child_struct, field_meta.name)
            field_path = path | field_meta.name
            field_value = getattr(value, field_meta.name, None)
            yield field_path, field_value, field, field.is_a

    def validator(self, value, path):
        result = ValidationResult(valid=True)

        for field in self.child_struct.meta.fields:
            field_path = path | field.name
            field_value = getattr(value, field.name, None)
            result = result.merge(
                field.is_a.metadata.type.validate(field_value, field_path),
            )

        return result

    attributes = AttributeFields(
        {
            'name': 'struct',
            'display': {'en': 'Struct Name'},
            'desc': {
                'en': 'The name of the struct to embed in the field.',
            },
            'is_a': {
                'type': 'struct_reference',
                'required': True,
            },
        },
        required=True,
    )
