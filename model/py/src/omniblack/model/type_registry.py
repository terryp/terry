from typing import Literal

from .abc import Registry
from .errors import CoercionError
from .field import AttributeFields
from .types.any import Any
from .types.binary import Binary
from .types.boolean import Boolean
from .types.child import Child
from .types.choice import Choice
from .types.date import Date
from .types.datetime import Datetime
from .types.defaults import Assumed, Initial
from .types.dotpath import Dotpath
from .types.email import Email
from .types.estimate import Estimate
from .types.integer import Integer
from .types.list import List
from .types.path import Path
from .types.pyobject import PyObject
from .types.string import String
from .types.struct_reference import StructReference

# Types implemented by extras
# TODO: switch to relative imports after astral-sh/ruff/#11542 is fixed
from omniblack.model.types.extras.secret import Password, Secret
from omniblack.model.types.extras.url import Url

direction_displays = {
    'from_': 'from',
    'to': 'to',
}


def create_adapter(direction: Literal['from_', 'to']):
    def adapt(
        self,
        format_name,
        value,
        field,
        is_a=None,
        original_format=None,
    ):
        converter_name = f'{direction_displays[direction]}_{format_name}'
        generic_name = f'{direction_displays[direction]}_generic'

        if is_a is None:
            is_a = field.is_a

        if converter := getattr(is_a.metadata.type, converter_name, None):
            return converter(value)

        if converter := getattr(is_a.metadata.type, generic_name, None):
            return converter(value, format_name)

        if format_name != 'string':
            return adapt(
                self=self,
                format_name='string',
                value=value,
                original_format=format_name,
                field=field,
                is_a=is_a,
            )
        else:
            dir_display = direction_displays[direction]
            if original_format is None:
                original_format = format_name

            msg = (
                'Cannot convert '
                + f'{is_a.type} {dir_display} {original_format}.'
            )
            raise CoercionError(msg)

    return adapt


builtin_types = (
    Any,
    Assumed,
    Binary,
    Boolean,
    Child,
    Choice,
    Date,
    Datetime,
    Dotpath,
    Email,
    Estimate,
    Initial,
    Integer,
    List,
    Path,
    PyObject,
    String,
    StructReference,

    # These are the types that are optional and maybe `None`
    Password,
    Secret,
    Url,
)


builtin_types = {
    builtin_type.name: builtin_type
    for builtin_type in builtin_types
    if builtin_type is not None
}


def create_final_class(model, base_type, type_exts):
    final_attributes = AttributeFields()
    if base_type.attributes:
        final_attributes._merge(base_type.attributes, model)

    for ext in type_exts:
        if ext.attributes:
            final_attributes._merge(ext.attributes, model)

    body = {}
    if final_attributes:
        body['attributes'] = final_attributes

    return type(base_type.__name__, (*reversed(type_exts), base_type), body)


class TypeRegistry(Registry):
    to_format = create_adapter('to')
    from_format = create_adapter('from_')

    def new_type_impl(self, new_type, impl, attrs=None):
        self.types_by_impl[impl] = (new_type, attrs)

    def __init__(self, model, types):
        super().__init__(model)

        types = {
            model_type.name: model_type
            for model_type in types
        }

        plugin_types = {
            plugin_type.name: plugin_type
            for plugin in self.model.plugins
            for plugin_type in plugin.types or []
        }

        types = builtin_types | plugin_types | types

        type_exts = {}
        for plugin in self.model.plugins:
            for ext in plugin.type_exts or []:
                type_exts.setdefault(ext.name, [])
                type_exts[ext.name].append(ext)

        self.types_by_impl = {}

        for name, model_type in types.items():
            exts = type_exts.get(name, [])
            final_type = create_final_class(model, model_type, exts)

            if not isinstance(final_type.implementation, property):
                if final_type.implementation in self.types_by_impl:
                    msg = (
                        f'"{final_type.implementation!r}" is already'
                        ' associated with a model type.'
                    )
                    raise KeyError(msg)
                self.types_by_impl[final_type.implementation] = (
                    final_type,
                    None,
                )

            self.register_meta_info(final_type)
            self._add(name, final_type)

    def register_meta_info(self, new_type):
        if new_type.attributes:
            self.model.meta_structs.add_type_attrs(
                new_type.name,
                new_type.attributes,
            )


def validate(*_args, **_kwargs):
    return True
