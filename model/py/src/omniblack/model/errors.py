from public import public


@public
class CoercionError(TypeError):
    pass
