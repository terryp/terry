from __future__ import annotations

import contextlib

from collections.abc import Iterable, Mapping
from importlib import import_module
from importlib.resources import Resource, files
from itertools import groupby
from operator import attrgetter, getitem
from os import PathLike, path
from tomllib import TOMLDecodeError, load
from typing import TYPE_CHECKING

from public import public
from rich import print

from .abc import Registry
from .dot_path import DotPath
from .field import Field
from .format import Format, get_preferred_file
from .preprocess_struct import preprocess_struct_def
from .undefined import undefined

if TYPE_CHECKING:
    from .model import Model

bases_path = DotPath(('implementation', 'python', 'bases'))


meta_model = files('omniblack.model.meta_model')
struct_traversable = meta_model.joinpath('Struct.toml')

with struct_traversable.open('rb') as struct_def_file:
    # The struct def of struct
    struct_struct_def = load(struct_def_file)


def is_ui_string(field):
    return (
        field['is_a']['type'] == 'child'
        and field['is_a']['child_attrs']['struct'] == 'UIString'
    )


from .coercion import coerce_from  # noqa: E402


@public
def get_path(indiv):
    return indiv._Struct__path  # noqa: SLF001


@public
def get_struct_name(indiv):
    return type(indiv).name


root_ui_string = {
    field['name']
    for field in struct_struct_def['fields']
    if is_ui_string(field)
}

struct_fields = tuple(
    field['name']
    for field in struct_struct_def['fields']
)


def import_base(base_str):
    module_id, name = base_str.split(':')
    module = import_module(module_id)
    return getattr(module, name)


def referenced_structs(struct_def: dict) -> Iterable[str]:
    if inherits := struct_def.get('inherits'):
        yield inherits


class _sort_by_struct_refs:
    """A key function that can be used in `sorted` or `list.sort`.

    The resulting order should allow the records to be saved without
    a missing reference error.
    NOTE: this will not correctly handle circular deps, or colliding ids.
    """

    def __init__(self, struct_def):
        self.name = struct_def['name']
        self.deps = set(referenced_structs(struct_def))

    def should_visit(self, _path, _value, _field, is_a):
        return is_a.type == 'reference'

    def check_circle(self, other):
        if self.name in other.deps and other.name in self.deps:
            msg = 'Circular dep'
            raise RuntimeError(msg, self.name, other.name)

    def __lt__(self, other):
        if not isinstance(other, _sort_by_struct_refs):
            return NotImplemented

        self.check_circle(other)
        # `sorted` returns smallest to largest
        # so when we return `False` it means
        # we come after `other` so if we
        # depend on `other` we return False
        if other.name in self.deps:
            return False
        elif self.name in other.deps:
            return True

        # fallback to sorting by id
        # Doesn't really matter just gives us an order
        # if two records don't depend on each other.
        return self.name <= other.name


def _is_field(member):
    return isinstance(member, Field)


def get_final_fields(final_cls):
    seen = set()

    fields = set()

    for cls in final_cls.__mro__:
        for key, member in cls.__dict__.items():
            if isinstance(member, Field) and key not in seen:
                seen.add(key)
                fields.add(id(member.meta))

    return fields


class Struct(type):
    def __new__(
            cls,
            name,
            bases,
            attrs: dict,
            struct_def: dict,
            model: Model,
    ):
        assert not bases

        struct_def = model.meta_structs.Struct(struct_def)
        calculated_bases = [
            import_base(base)
            for base in bases_path.get(struct_def, ())
        ]

        if 'inherits' in struct_def:
            super_struct = model.structs[struct_def.inherits]
            struct_def.fields = [
                *super_struct.meta.fields,
                *struct_def.fields,
            ]
            calculated_bases.append(super_struct)

        pass_struct, post_event = model.call_handlers(
            'create_class',
            name,
            calculated_bases,
            attrs,
            struct_def,
        )

        calculated_bases.append(StructBase)

        final_bases = tuple(calculated_bases)

        fields = []
        attrs['model'] = model
        attrs['meta'] = struct_def
        attrs['__module__'] = model.module_name
        MetaField = model.meta_structs.Field

        root_meta_field = MetaField(
            model=model,
            name='indiv_root',
            display={'en': '<Indiv Root>'},
            desc={'en': f'The root of a standalone "{struct_def.display.en}"'},
            is_a={
                'type': 'child',
                'child_attrs': {
                    'struct': name,
                },
            },
        )

        attrs['root_field'] = Field(meta=root_meta_field, model=model)

        own_fields = [
            field
            for field in struct_def.fields
            if field.owner_cls is None
        ]

        for field in own_fields:
            field_name = field.name
            field_descriptor = Field(meta=field, model=model)
            fields.append(field_descriptor)
            attrs[field_name] = field_descriptor

        new_cls = super().__new__(
            cls,
            name,
            final_bases,
            attrs,
        )

        final_fields = get_final_fields(new_cls)
        new_cls.meta.fields = tuple(
            field
            for field in new_cls.meta.fields
            if id(field) in final_fields
        )

        if pass_struct:
            post_event(new_cls)
        else:
            post_event()

        return new_cls


@public
class StructBase:
    def __init__(self, values=None, *, is_base=False, **kwargs):
        if values is None or values is undefined:
            values = {}

        values |= kwargs

        if not is_base:
            cls = self.__class__
            for field in cls.meta.fields:
                value = values.get(field.name, undefined)
                setattr(self, field.name, value)

        super().__init__()

    def __repr__(self):
        cls = self.__class__
        name = cls.__name__

        field_names = (
            field.name
            for field in self.meta.fields
        )
        values = tuple(
            f'{field_name}={repr(getattr(self, field_name, undefined))}'
            for field_name in field_names
        )

        values_str = ', '.join(values)

        return f'{name}({values_str})'

    def __rich_repr__(self):
        for field in self.meta.fields:
            field_value = getattr(self, field.name, undefined)
            yield field.name, field_value

    def __contains__(self, key):
        try:
            return self[key] is not undefined
        except KeyError:
            return False
        else:
            return True

    def __bool__(self):
        for field in self.meta.fields:
            if field.type == 'child':
                with contextlib.suppress(KeyError):
                    if self[field.name]:
                        return True
            elif field.name in self:
                return True
        else:
            return False

    def __len__(self):
        count = 0
        for field in self.meta.fields:
            try:
                self[field.name]
            except KeyError:
                pass
            else:
                count += 1

        return count

    def __eq__(self, other):
        if not isinstance(other, Struct):
            return NotImplemented

        for field in self.meta.fields:
            try:
                self_value = self[field.name]
            except KeyError:
                self_value = undefined

            try:
                other_value = other[field.name]
            except KeyError:
                other_value = undefined

            if self_value != other_value:
                return False
        else:
            return True

    def __getitem__(self, key):
        try:
            return getattr(self, key)
        except AttributeError as err:
            raise KeyError(*err.args) from None

    def __setitem__(self, key, value):
        try:
            return setattr(self, key, value)
        except AttributeError as err:
            raise KeyError(*err.args) from None

    def __delitem__(self, key):
        try:
            return delattr(self, key)
        except AttributeError as err:
            raise KeyError(*err.args) from None

    def __iter__(self):
        for field in self.meta.fields:
            if field.name in self:
                yield field.name

    def __reversed__(self):
        for field in reversed(self.meta.fields):
            if field.name in self:
                yield field.name

    def __copy__(self):
        values = {}
        cls = self.__class__
        for field in self.meta.fields:
            values[field.meta.name] = self[field.meta.name]

        return cls(**values)

    def get(self, key, default=None):
        if key in self:
            return self[key]
        else:
            return default

    def __set_path(self, path: DotPath):
        self.__path = path

    def __deepcopy__(self, memo):
        from copy import deepcopy
        cls = self.__class__
        values = {}

        for field in self.meta.fields:
            values[field.name] = deepcopy(self[field.name], memo)

        return cls(**values)

    @classmethod
    def load_file(cls, file: PathLike):
        model = cls.model
        _, suffix = path.splitext(file)

        file_format = model.formats.by_suffix[suffix]

        with open(file) as file_obj:
            rec = file_format.load(file_obj)
            indiv = coerce_from(model, rec, file_format, cls.meta.name)
            return indiv

    @classmethod
    def hydrate(cls, data=None, /, **kw_data):
        data = data | kw_data

        self = object.__new__(cls)

        for key, value in data.items():
            setattr(self, key, value)

        if hasattr(self, 'post_hydrate'):
            self.post_hydrate()

        return self


def get_contents(package, model: Model):
    pkg_files = files(package)

    resources = (
        resource
        for resource in pkg_files.iterdir()
        if resource.is_file() and resource.suffix in model.formats.by_suffix
    )

    return tuple(
        get_preferred_file(files, model)
        for name, files in groupby(resources, attrgetter('stem'))
    )


def is_loadable(path, model):
    return path.suffix in model.formats.by_suffix


@public
class StructRegistry(Registry):
    def __init__(self, model, struct_packages, struct_defs):
        super().__init__(model)
        self._struct_defs = list(struct_defs)

        self._struct_pkgs = struct_packages

    def _load(self):
        struct_defs = self._struct_defs
        del self._struct_defs

        for pkg in self._struct_pkgs:
            struct_defs.extend(self.load_model_package(pkg))

        del self._struct_pkgs

        for plugin in self.model.plugins:
            for pkg in plugin.struct_packages or ():
                struct_defs.extend(self.load_model_package(pkg))

        sorted_defs = sorted(struct_defs, key=_sort_by_struct_refs)

        for struct_def in sorted_defs:
            self._add(struct_def)

    def _add(self, struct_def: dict, *bases):
        meta_structs, = preprocess_struct_def(struct_def)

        for struct_name in meta_structs:
            self.register_meta_struct(struct_name)

        name = struct_def['name']
        attrs = {'__doc__': struct_def['desc']['en']}

        new_struct = Struct(
            name,
            bases,
            attrs,
            struct_def=struct_def,
            model=self.model,
        )

        self.model.expose(name, new_struct)
        self.model.types.new_type_impl(
            self.model.types.child,
            new_struct,
            {'struct': name},
        )
        return super()._add(name, new_struct)

    __call__ = _add

    def register_meta_struct(self, struct_name):
        if struct_name in self:
            return self[struct_name]

        meta_model_files = files('omniblack.model.meta_model')
        struct_def_resource = meta_model_files.joinpath(f'{struct_name}.toml')

        struct_def = self.load_struct_def_resource(struct_def_resource)
        struct_cls = self._add(struct_def)

        child_fields = (
            field
            for field in struct_cls.meta.fields
            if field['is_a']['type'] == 'child'
        )

        for child_field in child_fields:
            self.register_meta_struct(child_field['child_attrs']['struct'])

        return self[struct_name]

    def load_model_package(self, package):
        for resource in get_contents(package, self.model):
            yield self.load_struct_def_resource(resource)

    def load_struct_def_resource(self, resource_path):
        resource_format = self.model.formats.by_suffix[resource_path.suffix]

        try:
            struct_def = self.load_resource(
                resource_path,
                resource_format,
            )

            return struct_def
        except AttributeError as err:
            msg = 'Missing field in'
            raise ValueError(msg, resource_path) from err
        except KeyError as err:
            msg = 'Missing needed value in'
            raise ValueError(msg, resource_path) from err
        except TOMLDecodeError:
            print(f'File {resource_path}')
            raise

    def load_resource(self, resource: Resource, resource_format: Format):
        with resource.open('r') as file_obj:
            return resource_format.load(file_obj)

    def __dir__(self):
        super_items = super().__dir__()
        super_items.extend(self.keys())
        return super_items


class ChildField(Field, type='child'):
    def __set__(self, parent_indiv, new_value):
        indiv = self.__get__(parent_indiv, type(parent_indiv))

        if isinstance(indiv, self.__objclass__):
            super().__set__(parent_indiv, indiv)
        elif new_value is not undefined:
            getter = (
                getitem
                if isinstance(new_value, Mapping)
                else getattr
            )

            for field in indiv.meta.fields:
                try:
                    indiv[field.name] = getter(new_value, field.name)
                except (KeyError, AttributeError):
                    del indiv[field.meta.name]
        else:
            for field in indiv.meta.fields:
                del indiv[field.name]

    def __get__(self, obj, obj_type):
        if obj is None:
            return self

        try:
            return super().__get__(obj, obj_type=obj_type)
        except AttributeError:
            child_cls = self.model.structs[self.meta.child_attrs.struct]
            new_child = child_cls()
            super().__set__(obj, new_child)
            return new_child
