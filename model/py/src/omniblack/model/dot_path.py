from __future__ import annotations

from collections.abc import Iterable, Mapping
from itertools import chain
from typing import Any

from public import public

NOT_FOUND = object()


@public
class DotPath(tuple):
    __slots__ = ()

    """
    A path into a structure.
    """

    def starts_with(self, other_path):
        """
        Does :code:`self` start with :code:`other_path`?
        Similar to :type:`str.starts_With`.
        """
        if len(self) < len(other_path):
            return False

        return all(
            seg == other_seg
            for seg, other_seg in zip(self, other_path)
        )

    def set(self, target, value):
        """
        Set the mapping at :code:`self` in :code:`target` to :code:`value`.

        .. note::
            This will create intermediate mappings under :code:`target`
            if they are missing.

        .. note::
            :code:`target` must be :type:`collections.abc.MutableMapping`
            that can be constructed by being called with no arguments.
        """
        return self.merge(target, lambda _: value)

    def merge(self, target, merge_func):
        """
        Merge a value into :code:`target`.

        :code:`merge_func` will be called with the current value of
        :code:`self` under :code:`target` and will be replaced
        with the return value of :code:`merge_func`.

        .. note::
            This will create intermediate mappings under :code:`target`
            if they are missing.

        .. note::
            :code:`target` must be :type:`collections.abc.MutableMapping`
            that can be constructed by being called with no arguments.
        """
        mapping_type = type(target)
        current = target
        for seg in self[:-1]:
            current.setdefault(seg, mapping_type())
            current = current[seg]

        last = self[-1]
        current_value = current.get(last, None)
        current[last] = merge_func(current_value)
        return target

    @classmethod
    def from_string(cls, path_str):
        """Create a Dotpath from :code:`path_str`."""
        return cls(path_str.split('.'))

    def __str__(self):
        return '.'.join(str(seg) for seg in self)

    def __repr__(self):
        repr_segments = (repr(seg) for seg in self)
        cls_name = self.__class__.__name__
        return f'{cls_name}({", ".join(repr_segments)})'

    def get(self, target: Mapping, default: Any = None):
        current = target
        for seg in self:
            if not isinstance(current, Mapping):
                return default
            else:
                current = current.get(seg, NOT_FOUND)
                if current is NOT_FOUND:
                    return default

        return current

    def __or__(self, other):
        """
        Merge `self` and `other` into a new :type:`DotPath`.
        """
        is_str = isinstance(other, str)
        if isinstance(other, Iterable) and not is_str:
            return self.__class__(chain(self, other))
        else:
            return self.__class__(chain(self, (other, )))

    def metadata_path(self):
        return self.__class__(
            seg
            for seg in self
            if isinstance(seg, str)
        )


if __name__ == '__main__':
    import code
    code.interact(local=globals())
