from public import public

from .dot_path import DotPath
from .field import Field


@public
def walk(
    indiv,
    should_visit=None,
    should_descend=None,
):
    def _should_visit(field_path, value, field, is_a):
        if should_visit is None:
            return True
        else:
            return should_visit(field_path, value, field, is_a)

    def _should_descend(field_path, value, field, is_a):
        if should_descend is None:
            return True
        else:
            return should_descend(field_path, value, field, is_a)

    model = indiv.meta.model
    MetaField = model.meta_structs.Field

    root_meta_field = MetaField(
        model=indiv.meta.model,
        name='walker_root',
        display={'en': 'Walker Root'},
        desc={'en': 'The root of a walk operation'},
        is_a={
            'type': 'child',
            'child_attrs': {
                'struct': indiv.meta.name
            },
        },
    )

    root_field = Field(meta=root_meta_field, model=model)

    yield from _walk(
        indiv,
        root_field.is_a,
        visit=_should_visit,
        descend=_should_descend,
    )


empty_dotpath = DotPath()


def _walk(value, is_a, path=empty_dotpath, visit=None, descend=None):
    sub_field_iter = is_a.metadata.type.walk(value, path)

    for sub_path, sub_value, sub_field, sub_is_a in sub_field_iter:
        if visit(sub_path, sub_value, sub_field, sub_is_a):
            yield (sub_path, sub_value, sub_field, sub_is_a)

        is_walkable = sub_is_a.metadata.type.walk is not None
        if is_walkable and descend(sub_path, sub_value, sub_field, sub_is_a):
            yield from _walk(
                value=sub_value,
                is_a=sub_is_a,
                path=sub_path,
                visit=visit,
                descend=descend,
            )
