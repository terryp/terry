import contextlib

from public import public

from .format import Format
from .undefined import undefined


@public
def coerce_to(indiv, target_format: str | Format):
    """
    Coerce an indiv to format.

    Parameters
    ----------

    indiv: :type:`omniblack.model.StructBase`
        The indiv to be converted to `format`

    format: :type:`omniblack.model.Format | str`
        The format the output should be comptabile with.
    """

    format_name = format.name if isinstance(target_format, Format) else format

    model = indiv.model

    new_rec = {}
    for field in indiv.meta.fields:
        value = getattr(indiv, field.name, undefined)

        if 'assumed' in field and value == field.assumed:
            continue

        if value is undefined:
            continue

        new_value = model.types.to_format(
            format_name=format_name,
            value=value,
            field=field,
            is_a=field.type.is_a,
        )

        if new_value is not undefined:
            new_rec[field.name] = new_value

    if not new_rec:
        return undefined

    return new_rec


@public
def coerce_from(model, rec, target_format: str | Format, struct_name: str):
    """
    Coerce from a format.

    Parameters
    ----------

    model: :type:`omniblack.model.Model`
        The model :code:`struct_name` is defined in.

    rec: :type:`collections.abc.Mapping`
        The rec to convert to :code:`struct_name`.

    format: :type:`str | omniblack.model.Format`
        The format :code:`rec` should converted from.

    struct_name: :type:`str`
        The struct :code:`rec` should be converted to.
    """

    Struct = model.structs[struct_name]

    if isinstance(target_format, Format):
        format_name = target_format.name
    else:
        format_name = target_format

    new_rec = {}
    for field in Struct.meta.fields:
        value = rec.get(field.name, undefined)

        if value is undefined:
            continue

        new_value = model.types.from_format(
            format_name=format_name,
            value=value,
            field=field,
            is_a=field.type.is_a,
        )

        if new_value is not undefined:
            new_rec[field.name] = new_value

        if field.type.sensitive:
            value = None
            with contextlib.suppress(KeyError):
                del rec[field.name]

    if not new_rec:
        return undefined

    return Struct(new_rec)
