from public import public

from .metadata import MetaBase
from .struct import Struct


@public
def to_json_schema(struct_cls: Struct) -> dict:
    struct_def = struct_cls.meta

    schema = {
        '$schema': 'https://json-schema.org/draft/2020-12/schema',
        'title': struct_def.name,
        'description': struct_def.desc.en,
        'type': 'object',
    }

    properties = {
        field.name: field_schema(field)
        for field in struct_cls.meta.fields
    }

    schema['properties'] = properties

    required_fields = [
        field.name
        for field in struct_def.fields
        if field.required
    ]

    schema['required'] = required_fields

    return schema


def field_schema(field: MetaBase):
    type_def = field.type

    if type_def.json_schema is None:
        msg = f'"{field.is_a.type}" does not support json schemas.'
        raise TypeError(msg)

    field_schema = type_def.json_schema()

    field_schema['title'] = field.display.en
    field_schema['description'] = field.desc.en

    return field_schema
