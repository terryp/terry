from collections.abc import ItemsView, KeysView, ValuesView
from copy import deepcopy
from importlib.resources import files
from tomllib import TOMLDecodeError, load

from omniblack.string_case import Cases
from omniblack.utils import public

from .abc import Registry
from .undefined import undefined

meta_json_cache = {}


def load_meta_json(struct_name):
    if struct_name not in meta_json_cache:
        meta_pkg = files('omniblack.model.meta_model')
        pkg_path = meta_pkg.joinpath(struct_name + '.toml')
        with pkg_path.open('rb') as file:
            meta_json_cache[struct_name] = load(file)

    return deepcopy(meta_json_cache[struct_name])


def items_to_merge(target, source):
    for key, value in target.items():
        if key in source:
            yield key, value, source[key]

    for key, value in source.items():
        if key not in target:
            yield key, target.get(key, undefined), value


class classproperty:  # noqa N801
    def __init__(self, f):
        self.f = f

    def __get__(self, obj, owner):
        return self.f(owner)


@public
class MetaBase:
    def __init_subclass__(
        cls,
        model=None,
        struct_def=None,
        struct_name=None,
        *,
        plain=False,
        **kwargs,
    ):
        if not plain:
            if model is None:
                msg = 'Model may not be none for smart MetaBase classes'
                raise TypeError(msg)
            cls.model = model

            if struct_name is None:
                if struct_def is not None:
                    struct_name = struct_def['name']
                else:
                    struct_name = Cases.Snake.to(cls.__name__)

            cls.struct_name = struct_name

            if struct_def is None:
                cls.meta = load_meta_json(struct_name)
            else:
                cls.meta = struct_def

        else:
            cls.struct_name = None
            cls.field_names = None
            cls.meta = None
            cls.model = None

        super().__init_subclass__(**kwargs)

    @classproperty
    def field_names(cls):  # noqa N805
        if cls.meta:
            for field in cls.meta['fields']:
                yield field['name']
        else:
            return None

    def __init__(self, values=None, model=None, *, added_by=None, **kwargs):
        cls = self.__class__
        self._model = model or cls.model
        self.added_by = added_by
        self.owner_cls = None

        if values is not None:
            self.update(values)

        self.update(kwargs)

    def __getattr__(self, name):
        field = self.get_field(name)

        if field is None:
            raise AttributeError(name)

        assumed = field.get('assumed', undefined)

        if assumed is undefined:
            raise AttributeError(name)

        return assumed

    def _to_child_type(self, value, child_type):
        if isinstance(value, child_type):
            return value
        else:
            return child_type(**value, model=self._model)

    def __setattr__(self, name, value):
        try:
            if value is undefined:
                if name in self.__dict__:
                    del self.__dict__[name]
            else:
                child_type, is_list = self._get_child_type(name)
                if child_type is not None:
                    struct = self.model.meta_structs[child_type]
                    if is_list:
                        value = [
                            self._to_child_type(item, struct)
                            for item in value
                        ]
                    else:
                        value = self._to_child_type(value, struct)

                self.__dict__[name] = value
        except KeyError as err:
            raise AttributeError(*err.args) from None

    def _get_child_type(self, name):
        field = self.get_field(name)

        if not field:
            return None, False

        is_a = field['is_a']

        child_list = (
            is_a['type'] == 'list'
            and is_a['list_attrs']['item']['type'] == 'child'
        )

        if child_list:
            return is_a['list_attrs']['item']['child_attrs']['struct'], True

        if is_a['type'] == 'child':
            return is_a['child_attrs']['struct'], False

        return None, is_a['type'] == 'list'

    def __delattr__(self, name):
        try:
            del self.__dict__[name]
        except KeyError as err:
            raise AttributeError(*err.args) from None

    def __getitem__(self, key):
        try:
            return self.__dict__[key]
        except KeyError:
            field = self.get_field(key)
            if field is None:
                raise

            assumed = field.get('assumed', undefined)

            if assumed is undefined:
                raise

            return assumed

    def __setitem__(self, key, value):
        if value is undefined:
            if key in self.__dict__:
                del self.__dict__[key]
        else:
            self.__dict__[key] = value

    def __delitem__(self, key):
        del self.__dict__[key]

    def __iter__(self):
        if self.meta:
            for field_name in self.field_names:
                if field_name in self:
                    yield field_name
        else:
            for key, value in self.__dict__.items():
                if value is not undefined:
                    yield key

    def __len__(self):
        return len(self.__dict__)

    def __reversed__(self):
        return reversed(self.__dict__)

    def __contains__(self, key):
        return hasattr(self, key)

    def __rich_repr__(self):
        for key, value in self.items():
            if not key.startswith('_'):
                yield key, value

        yield '@added_by', self.added_by, None
        yield '@owner_cls', self.owner_cls, None

    def __repr__(self):
        item_strs = (
            f'{key}={repr(value)}'
            for key, value in self.items()
            if not key.startswith('_')
        )

        item_str = ', '.join(item_strs)

        cls = self.__class__
        cls_name = cls.__name__

        return f'{cls_name}({item_str})'

    def keys(self):
        return KeysView(self)

    def values(self):
        return ValuesView(self)

    def items(self):
        return ItemsView(self)

    def get(self, key, default=None):
        return getattr(self, key, default)

    def __copy__(self):
        cls = self.__class__
        return cls(self.__dict__)

    def __deepcopy__(self, memo):
        cls = self.__class__

        values = {
            key: deepcopy(value, memo)
            for key, value in self.items()
        }

        return cls(values)

    def update(self, other, **kwargs):
        for key, value in other.items():
            setattr(self, key, value)

        for key, value in kwargs.items():
            setattr(self, key, value)

    def __or__(self, other):
        return self.__dict__ | other

    def __ior__(self, other):
        self.__dict__ |= other
        return self.__dict__

    def copy(self):
        return self.__copy__()

    @classmethod
    def get_field(cls, field_name):
        if cls.meta is None:
            return

        for field in cls.meta.get('fields', ()):
            if field['name'] == field_name:
                return field

        return None

    def merge(self, other):
        for attr, self_value, other_value in items_to_merge(self, other):
            if self_value is undefined:
                self[attr] = other_value
            elif isinstance(self_value, MetaBase):
                self_value.merge(other_value)
            else:
                self[attr] = other_value


@public
class Metadata(MetaBase, plain=True):
    pass


class Lazy:
    def __init__(self, func):
        self.__func = func

    def __set_name__(self, owner, name):
        self.__objclass__ = owner
        self.__name = name

    def __get__(self, inst, cls=None):
        if inst is None:
            return self

        inst.__dict__[self.__name] = self.__func()
        return getattr(inst, self.__name)


class _AttachMetadata:
    metadata = Lazy(Metadata)


@public
class MetaStructRegistry(Registry):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for struct_path in files('omniblack.model.meta_model').iterdir():
            if struct_path.name.endswith('.toml'):
                with struct_path.open('rb') as struct_file:
                    try:
                        struct_def = load(struct_file)
                    except TOMLDecodeError:
                        print(f'Processing {struct_path}')
                        raise

                self._add(struct_def)

    def _add(self, struct_def: dict):
        struct_name = struct_def['name']

        meta_cls = type(
            Cases.Pascal.to(struct_name),
            (_AttachMetadata, MetaBase),
            {},
            struct_def=struct_def,
            struct_name=struct_name,
            model=self.model,
        )
        return super()._add(meta_cls.struct_name, meta_cls)

    def add_type_attrs(self, type_name, attributes):
        field_name = Cases.Snake.to(f'{type_name}_attrs')
        display = {'en': Cases.Title.to(f'{type_name} Attributes')}
        desc = {
            'en': f'Attributes that configure a {type_name} field.',
        }

        attr_struct = {
            'name': field_name,
            'desc': desc,
            'display': display,
            'fields': list(attributes.fields.values()),
        }

        self._add(attr_struct)

        field_def = {
            'name': field_name,
            'desc': desc,
            'display': display,
            'is_a': {
                'required': attributes.get('required', False),
                'type': 'child',
                'child_attrs': {'struct': field_name},
            },
        }

        self.IsA.meta['fields'].append(field_def)
