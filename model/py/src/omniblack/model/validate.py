from public import public

from .dot_path import DotPath


def should_descend(_field_path, value, _field, is_a):
    if is_a.metadata.type.container:
        return bool(value)
    else:
        return True


@public
def validate(indiv):
    struct_cls = indiv.__class__

    return struct_cls.root_field.type.validate(indiv, DotPath())
