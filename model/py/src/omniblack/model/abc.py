from abc import ABC, abstractmethod
from collections.abc import ItemsView, Iterable, KeysView, Mapping, ValuesView
from sys import getsizeof
from typing import Self

import rich.repr

from public import public


@public
class Localizable(ABC):
    @abstractmethod
    def localize(self, lang: str) -> str:
        pass


# Strings are considered localizeable so that double localization does not
# cause errors
Localizable.register(str)


@public
class Data[K, V](Mapping[K, V]):
    _data: Mapping[K, V]

    def __init__(self, data: Mapping[K, V] = None, **more_data):
        if data is not None:
            object.__setattr__(self, '_data', data | more_data)
        else:
            object.__setattr__(self, '_data', more_data)

    def __bool__(self) -> bool:
        return bool(self._data)

    def __contains__(self, key: K) -> bool:
        return key in self._data

    def __eq__(self, other: Self) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented

        return self._data.__eq__(other._data)

    def __getattr__(self, key: str) -> V:
        try:
            return self._data[key]
        except KeyError as err:
            raise AttributeError(*err.args) from None

    def __ge__(self, other: Self) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented

        return self._data.__ge__(other._data)

    def __getitem__(self, key: K) -> V:
        return self._data[key]

    def __gt__(self, other: Self) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented

        return self._data.__gt__(other._data)

    def __iter__(self) -> Iterable[K]:
        return iter(self._data)

    def __le__(self, other: Self) -> bool:
        return self._data.__le__(other._data)

    def __len__(self) -> int:
        return len(self._data)

    def __lt__(self, other: Self) -> bool:
        return self._data.__lt__(other._data)

    def __ne__(self, other: Self) -> bool:
        return self._data.__ne__(other._data)

    def __or__(self, other: Mapping[K, V]) -> Self:
        return self.__class__(self._data | other)

    def __repr__(self) -> str:
        cls_name = self.__class__.__name__
        data_repr = ', '.join(self.keys())
        return f'{cls_name}({data_repr})'

    def __ror__(self, other: Mapping[K, V]) -> Self:
        return self.__class__(other | self._data)

    def __reversed__(self) -> Iterable[K]:
        return reversed(self._data)

    def __rich_repr__(self) -> rich.repr.Result:
        yield from self._data.items()

    def __sizeof__(self):
        return getsizeof(self._data)

    def copy(self) -> Self:
        return self.__class__(self._data.copy())

    @classmethod
    def fromkeys(cls, key_iter, value=None) -> Self:
        return cls({
            key: value
            for key in key_iter
        })

    def get(self, key: K, default: V = None) -> V:
        return self._data.get(key, default)

    def items(self) -> ItemsView[K, V]:
        return self._data.items()

    def keys(self) -> KeysView[K]:
        return self._data.keys()

    def values(self) -> ValuesView[V]:
        return self._data.values()


UNSET = object()


@public
class MutableData[K, V](Data[K, V]):
    def __delattr__(self, key: K):
        if not hasattr(self.__class__, key):
            del self._data[key]
        else:
            cls_name = self.__class__.__name__
            msg = (
                f'"{key}" is a method of "{cls_name}" and may not be mutated.'
            )
            raise AttributeError(msg)

    def __delitem__(self, key: K):
        del self._data[key]

    def __ior__(self, other: Mapping[K, V]) -> Self:
        self._data |= other
        return self

    def __setattr__(self, key: K, value: V):
        if not hasattr(self.__class__, key):
            self._data[key] = value
        else:
            cls_name = self.__class__.__name__
            msg = (
                f'"{key}" is a method of "{cls_name}" and may not be mutated.'
            )
            raise AttributeError(msg)

    def __setitem__(self, key: K, value: V):
        self._data[key] = value

    def clear(self):
        self._data.clear()

    def pop(self, key: K, default=UNSET) -> V:
        try:
            return self._data[key]
        except KeyError:
            if default is not UNSET:
                return default
            else:
                raise

    def popitem(self) -> tuple[K, V]:
        return self._data.popitem()

    def setdefault(self, key: K, default: V = None, /) -> V:
        return self._data.setdefault(key, default)

    def update(self, mapping: None | Mapping[K, V] = None, **kwargs: V):
        self._data.update(mapping, **kwargs)


@public
class Registry(Data):
    def __init__(self, model):
        super().__init__()

        self.model = model
        cls_name = self.__class__.__name__
        setattr(self, f'_{cls_name}__model', model)

    def _add(self, name, item):
        self._data[name] = item
        return item

    def __rich_repr__(self):
        yield from self._data.keys()
