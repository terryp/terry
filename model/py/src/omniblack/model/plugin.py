from public import public

from .format import Format
from .types import ModelType, TypeExt

"""
    Event listeners are run before the event occurs.
    If the listeners is a generator that yields
    the generator will be resumed after the event completes.
"""


@public
class Plugin:
    # Expected attrs
    # new types
    types: list[ModelType] = None

    # Extenstions to existing types
    type_exts: list[TypeExt] = None

    # New formats
    formats: list[Format] = None

    # Struct packages to load into the model
    struct_packages: list[str] = None

    # Plugins to load into the model
    # NOTE: There is no plugin de-duplication
    plugins: list[type] = None

    def __init__(self, model):
        self.model = model

    def on_class_create(self, name, bases, attrs, struct_def):
        """
            Modify the struct def before the class is generated.
        """

    def on_save(self, indiv, storage_engine):
        """
            Runs before an indiv is saved.
        """

    def on_delete(self, indiv, storage_engine):
        """
            Runs before an indiv is deleted.
        """

    def on_read(self, indiv, storage_engine):
        """
            Runs before an indiv is read.
        """

    def on_search(self, indiv, storage_engine, params: dict):
        """
            Runs before a search is performed.
        """

    def on_validate(self, indiv):
        """
            Runs before an indiv is validated.
        """

    def on_create_meta_struct(self, struct_def):
        """
            Modify a meta struct def before it is added to the meta registry.
        """
