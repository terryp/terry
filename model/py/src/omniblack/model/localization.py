from string import Template

from public import public


@public
class TXT(Template):
    def __init__(self, template_str, local_ns=None, global_ns=None):
        if global_ns is None:
            global_ns = {}

        if local_ns is None:
            local_ns = {}

        super().__init__(template_str)
        self.vars = local_ns | global_ns
        self.substitute(self.vars)

    def localize(self, _lang):
        return self.substitute(self.vars)
