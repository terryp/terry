from datetime import datetime

from .dot_path import DotPath
from .field import Field
from .model import Model
from .struct import StructBase


class Diff:
    path: DotPath
    timestamp: datetime

    before: object
    after: object

    field: Field
    struct: StructBase
    model: Model


def _get_diffs[S: StructBase](old: S, _new: S):
    for field in old.meta.fields:
        pass


def get_diffs[S: StructBase](old: S, new: S) -> tuple[Diff]:
    if old.meta.name != new.meta.name:
        msg = (
            'A diff can only be generated from'
            + ' two instances of the same struct.'
        )
        raise ValueError(msg)
