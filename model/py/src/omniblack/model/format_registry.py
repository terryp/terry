from public import public

from .abc import Registry
from .format import Format
from .format_classes import JSONFormat, TomlFormat, YamlFormat

builtin_formats = tuple(
    format_cls
    for format_cls in (JSONFormat, TomlFormat, YamlFormat)
    if format_cls is not None
)


@public
class FormatRegistry(Registry):
    def __init__(self, model):
        super().__init__(model)
        self.by_suffix = {}
        self.by_mime_type = {}
        for plugin in model.plugins:
            for plugin_format in plugin.formats or []:
                self._add(plugin_format())

        for builtin_format in builtin_formats:
            self._add(builtin_format())

    def _add(self, new_format: Format | str):
        if isinstance(new_format, str):
            return self[new_format]
        else:
            self.by_mime_type |= {
                mime_type: new_format
                for mime_type in new_format.mime_types
            }

            self.by_suffix[new_format.file_suffix] = new_format

            return super()._add(new_format.name, new_format)
