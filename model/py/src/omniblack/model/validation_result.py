from collections.abc import (
    Iterable,
    MutableMapping,
    MutableSequence,
)
from dataclasses import dataclass
from enum import Enum
from itertools import chain
from typing import TypedDict

from public import public

from .abc import Localizable
from .dot_path import DotPath

mutable_container = (MutableMapping, MutableSequence)


@public
class ErrorTypes(Enum):
    model_error = 'The model contains an invalid setup.'
    invalid_value = 'The provided value is invalid for this type.'
    coercion_error = ('The value provided could not be coerced to/from the'
                      'specified format.')
    constraint_error = 'A constraint was violated.'


DotPathOrStr = DotPath | str


def coerce_str(path):
    if isinstance(path, DotPath):
        return path
    else:
        return DotPath.fromString(path)


@public
class ValidationMessageLike(TypedDict):
    type: ErrorTypes
    message: Localizable
    paths: Iterable[DotPath]
    suggestions: Iterable[Localizable]


@public
@dataclass(frozen=True)
class ValidationMessage:
    type: ErrorTypes
    message: Localizable
    paths: tuple[DotPath]
    suggestions: tuple[Localizable] = ()

    def __init__(
        self,
        error_type: ErrorTypes,
        message: Localizable,
        paths: Iterable[DotPathOrStr] | DotPathOrStr,
        suggestions: Iterable[Localizable] = (),
    ):
        if isinstance(paths, str | DotPath):
            paths = (paths, )

        paths = tuple(coerce_str(path) for path in paths)

        object.__setattr__(self, 'paths', paths)

        object.__setattr__(self, 'suggestions', tuple(suggestions))

        object.__setattr__(self, 'type', error_type)

        object.__setattr__(self, 'message', message)


def coerce_msg(message):
    if not isinstance(message, ValidationMessage):
        return ValidationMessage(**message)

    return message


ValidationMsgLike = ValidationMessage | ValidationMessageLike
public(ValidationMsgLike=ValidationMsgLike)

messages_type = Iterable[ValidationMsgLike] | ValidationMsgLike


@public
@dataclass(frozen=True)
class ValidationResult:
    messages: tuple[ValidationMessage]
    valid: bool

    def __init__(self, messages: messages_type = (), *, valid: bool):
        if not isinstance(messages, Iterable):
            messages = (messages, )

        messages_coerced = tuple(map(coerce_msg, messages))

        object.__setattr__(self, 'valid', valid)
        object.__setattr__(self, 'messages', messages_coerced)

    def merge(self, *other_results):
        final_valid = all(
            result.valid
            for result in chain((self, ), other_results)
        )

        all_messages = chain.from_iterable(
            result.messages
            for result in chain((self,), other_results)
        )

        return self.__class__(valid=final_valid, messages=all_messages)

    def __bool__(self):
        return self.valid

    def __iter__(self):
        return iter(self.messages)

    def __add__(self, other):
        return self.merge(other)

    def __or__(self, other):
        return self.merge(other)
