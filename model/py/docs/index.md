# Omniblack Model

`omniblack.model` provides a universal way of describing the shape of data.


```{eval-rst}
.. automodule:: omniblack.model
    :members:
```
