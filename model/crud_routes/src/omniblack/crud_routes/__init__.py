from werkzeug.exceptions import NotFound

from omniblack.app import config
from omniblack.server import route


def crud_route(_server, _storage):
    @route.get('/<struct_name>/<id>')
    def read(struct_name: str, id: str):
        try:
            struct_cls = config.model.structs[struct_name]
        except KeyError:
            msg = f'"{struct_name}" is not a known structure.'
            raise NotFound(msg)

        return config.storage.read(id, struct_cls)

    @read.put('/<struct_name>')
    def save(
        struct_name: str,
        new_record: dict,
        *,
        return_saved: bool = False,
        exclusive_creation: bool = False,
        exclusive_update: bool = False,
    ):
        try:
            struct_cls = config.model.structs[struct_name]
        except KeyError:
            msg = f'"{struct_name}" is not a known structure.'
            raise NotFound(msg)

        indiv = struct_cls.hydrate(new_record)

        return config.storage.save(
            indiv,
            return_saved=return_saved,
            exclusive_update=exclusive_update,
            exclusive_creation=exclusive_creation,
        )
