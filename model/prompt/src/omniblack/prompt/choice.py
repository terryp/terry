from datrie import Trie
from textual.suggester import Suggester


def build_alphabet(enum_cls):
    chars = set()
    for member in enum_cls:
        choice = member.choice
        chars.update(choice.name)
        chars.update(choice.display.en)

        if hasattr(choice, 'internal'):
            chars.update(choice.internal)

    return ''.join(sorted(chars))


class ChoiceSuggester(Suggester):
    def __init__(self, choice_enum):
        super().__init__(case_sensitive=False, use_cache=False)

        self.__trie = Trie(build_alphabet(choice_enum))

        for member in choice_enum:
            choice = member.choice
            self.__trie[choice.name.casefold()] = choice
            self.__trie[choice.display.en.casefold()] = choice
            if hasattr(choice, 'internal'):
                self.__trie[choice.internal.casefold()] = choice

    async def get_suggestion(self, value):
        suggestions = [
            choice.name
            for choice in self.__trie.values(value)
        ]

        return suggestions[0] if suggestions else None
