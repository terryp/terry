from textual import on
from textual.containers import VerticalScroll
from textual.reactive import reactive
from textual.screen import Screen
from textual.widgets import Button, Header, Input

from omniblack.model import validate
from omniblack.prompt import field


class AllSet(set):
    def __contains__(self, k):
        if not self:
            return True
        else:
            return super().__contains__(k)


class EditChildButton(field.PromptInput, Button):
    def prep_args(self):
        return (f'Edit {self.field.display.en}',)

    @property
    def struct(self):
        return self.model.structs[self.is_a.child_attrs.struct]

    def on_button_pressed(self):
        self.app.push_screen(
            FieldSet(
                struct_cls=self.struct,
                fields=self.fields,
                value=self.model_value,
                path=self.path,
                sub_id=self.sub_id,
            ),
            callback=self.child_result,
        )

    def child_result(self, value):
        msg = field.ValueChanged(self.sub_id, value, self)
        self.post_message(msg)


class FieldSet(Screen):
    validation_result = reactive(None)

    def __init__(
        self,
        *,
        struct_cls,
        fields=(),
        value,
        path,
        root=False,
        sub_id,
        **kwargs,
    ):
        self.value = value
        self.model = struct_cls.meta.model
        self.Struct = struct_cls
        self.path = path
        self.fields = AllSet(fields)
        self.root = root
        self.sub_id = sub_id

        super().__init__(**kwargs)

    def _sub_input(self, child_field):
        yield field.FieldControl(
            field=child_field,
            fields=self.fields,
            value=getattr(self.value, child_field.name, None),
            is_a=child_field.is_a,
            sub_id=child_field.name,
            model=self.model,
            path=self.path | child_field.name,
        )

    def compose(self):
        header = Header()
        header.tall = True
        yield header
        with VerticalScroll(classes='centered_child'):
            for struct_field in self.value.meta.fields:
                field_path = self.path | struct_field.name

                if field_path not in self.fields:
                    continue

                yield from self._sub_input(struct_field)

    @on(field.ValueChanged)
    def handle_change(self, event: field.ValueChanged):
        if event.control is self:
            return

        event.stop()

        setattr(self.value, event.sub_id, event.value)

        self.value_changed()

    def value_changed(self):
        msg = field.ValueChanged(self.sub_id, self.value, self)

        self.post_message(msg)

    @on(Input.Submitted)
    def attempt_submit(self):
        if self.root:
            result = validate(self.value)
            if not result.valid:
                return

        self.dismiss(self.value)
