from textual import on
from textual.screen import Screen
from textual.widgets import (
    Button,
    Footer,
    Header,
    Input,
    ListItem,
    ListView,
)

from omniblack.prompt import field


class EditListButton(field.PromptInput, Button):
    def prep_args(self):
        return (f'Edit {self.field.display.en}',)

    def on_button_pressed(self):
        self.app.push_screen(
            ListScreen(
                value=self.model_value,
                path=self.path,
                sub_id=self.sub_id,
                field=self.field,
                is_a=self.is_a,
                fields=self.fields,
                model=self.model,
            ),
            callback=self.child_result,
        )

    def child_result(self, value):
        msg = field.ValueChanged(self.sub_id, value, self)
        self.post_message(msg)


class ListScreen(Screen):
    CSS_PATH = 'list.tcss'

    BINDINGS = (
        ('I', 'new_item'),
    )

    def __init__(self, value, field, is_a, path, sub_id, fields, model):
        self.field = field
        self.is_a = is_a
        self.child_is_a = is_a.metadata.type.item_is_a
        self.path = path

        self.value = value

        self.fields = fields
        self.model = model
        self.sub_id = sub_id

        super().__init__()

    def compose(self):
        header = Header()
        header.tall = True
        yield header

        self.list_view = ListView()
        yield self.list_view
        self.call_next(self.rebuild_list_controls)

        yield Footer()

    async def rebuild_list_controls(self):
        async with self.batch(), self.lock:
            await self.list_view.clear()
            await self.list_view.extend(
                ListItem(field.FieldControl(
                    field=self.field,
                    is_a=self.child_is_a,
                    value=v,
                    model=self.model,
                    fields=self.fields,
                    path=self.path | i,
                    sub_id=i,
                ))
                for i, v in enumerate(self.value)
            )

    def action_new_item(self):
        self.value.insert(self.list_view.index or 0, None)
        self.call_next(self.rebuild_list_controls)

    @on(field.ValueChanged)
    def handle_change(self, change: field.ValueChanged):
        if change.control is self:
            return

        change.stop()

        self.value[change.sub_id] = change.value

    @on(Input.Submitted)
    def attempt_submit(self, _: Input.Submitted):
        self.dismiss(self.value)
