from asyncio import gather

from public import public
from textual import on, work
from textual.app import App

from omniblack.model import DotPath, validate

from . import child, field


@public
class Form(App):
    CSS_PATH = 'form.tcss'
    BINDINGS = [
        ('ctrl+q', 'quit'),
    ]

    def __init__(self, *args, Struct, fields=(), **kwargs):  # noqa: N803
        super().__init__(*args, **kwargs)

        fields = tuple(
            DotPath.from_string(field) if isinstance(field, str) else field
            for field in fields
        )

        self.field_controls = {}
        self.validation_messages = {}
        self.Struct = Struct
        self.fields = fields
        self.__root_field_set = child.FieldSet(
            struct_cls=Struct,
            fields=fields,
            value=Struct(),
            path=DotPath(),
            root=True,
            sub_id=None,
        )

    @work
    async def push_root_screen(self):
        v = await self.push_screen_wait(self.__root_field_set)
        self.exit(v)

    def on_mount(self):
        self.push_root_screen()

    @on(field.ValueChanged)
    async def handle_change(self, change):
        self.validation_messages = {}
        validation_results = validate(change.value)

        for msg in validation_results.messages:
            for path in msg.paths:
                path_msgs = self.validation_messages.setdefault(path, [])
                path_msgs.append(msg)

        with self.batch_update():
            updates = []
            for path, control in self.field_controls.items():
                messages = self.validation_messages.get(path, [])
                updates.append(control.update_validation_messages(messages))

            await gather(*updates)
