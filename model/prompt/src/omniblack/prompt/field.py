from __future__ import annotations

from textual import on, work
from textual.containers import Center
from textual.message import Message
from textual.reactive import reactive
from textual.widget import Widget
from textual.widgets import Input, Label, ListItem, ListView

from omniblack.model import ValidationResult


class ValueChanged(Message):
    def __init__(self, sub_id, value, control):
        self.sub_id = sub_id
        self.value = value
        self.widget = control
        super().__init__()
        self.bubble = True

    @property
    def control(self) -> Widget:
        return self.widget


class PromptInput:
    validation_messages = reactive(None)

    def __init__(
        self,
        *,
        field,
        model_value,
        sub_id,
        is_a,
        model,
        fields,
        path,
        **kwargs,
    ):
        self.field = field
        self.fields = fields
        self.sub_id = sub_id
        self.is_a = is_a
        self.model = model
        self.path = path
        self.__model_value = model_value

        self.enhance_kwargs(kwargs)
        args = self.prep_args()

        super().__init__(*args, **kwargs)

        self.value_changed()

    def enhance_kwargs(self, _kwargs):
        return

    def prep_args(self):
        return ()

    @property
    def model_value(self):
        return self.__model_value

    @model_value.setter
    def model_value(self, new_value):
        self.__model_value = new_value

        self.value_changed()

    def _set_model_value(self, new_value):
        """Set the current model value without update notifications.

        It is the callers responsibility to call `self.value_changed()`
        notify parent widgets of the change.
        """
        self.__model_value = new_value

    def value_changed(self):
        msg = ValueChanged(self.sub_id, self.model_value, self)

        self.post_message(msg)


class FieldInput(PromptInput, Input):
    def enhance_kwargs(self, kwargs):
        from omniblack.prompt import choice

        if self.is_a.type == 'choice':
            kwargs['suggester'] = choice.ChoiceSuggester(
                self.is_a.metadata.type.implementation
            )

        if self.model_value is not None:
            kwargs['value'] = self.model.types.to_format(
                format_name='cli',
                value=self.model_value,
                field=self.field,
                is_a=self.is_a,
            )

        kwargs['placeholder'] = self.field.desc.en
        kwargs['password'] = self.is_a.metadata.type.sensitive

    @on(Input.Changed)
    def handle_change(self, change: Input.Changed):
        self.log.error(f'{self.field.name} changed {change.value}')
        self.raw_value = change.value

    @property
    def raw_value(self):
        return self.__raw_value

    @raw_value.setter
    def raw_value(self, new_value):
        self.__raw_value = new_value

        if new_value == '':
            self._set_model_value(None)
        else:
            try:
                self._set_model_value(self.model.types.from_format(
                    format_name='cli',
                    value=new_value,
                    field=self.field,
                    is_a=self.is_a,
                ))
            except ValueError:
                self._set_model_value(None)

        self.value_changed()

    @property
    def model_value(self):
        return super().model_value

    @model_value.setter
    def model_value(self, new_value):
        if new_value is None:
            self.__raw_value = ''
        else:
            self.__raw_value = self.model.types.to_format(
                format_name='cli',
                value=new_value,
                field=self.field,
                is_a=self.is_a,
            )

        super().model_value = new_value


class FieldControl(Widget):
    root_validation_messages = reactive(None)
    validation_messages = reactive(None)

    def __init__(self, field, fields, sub_id, is_a, value, model, path):
        super().__init__()
        self.value = value

        self.field = field
        self.is_a = is_a
        self.model = model

        self.fields = fields
        self.sub_id = sub_id
        self.path = path

    def _list_input(self):
        from omniblack.prompt import list as list_mod

        if self.value is None:
            self.value = []
            msg = ValueChanged(self.sub_id, self.value, self)
            self.post_message(msg)

        with Center(classes='child_edit_button'):
            yield list_mod.EditListButton(
                model_value=self.value,

                field=self.field,
                is_a=self.is_a,
                model=self.model,

                path=self.path,
                fields=self.fields,
                sub_id=self.sub_id,
            )

    def _child_input(self):
        from omniblack.prompt import child

        Struct = self.model.structs[self.is_a.child_attrs.struct]

        if self.value is None:
            self.value = Struct()
            msg = ValueChanged(self.sub_id, self.value, self)
            self.post_message(msg)

        with Center(classes='child_edit_button'):
            yield child.EditChildButton(
                model_value=self.value,

                field=self.field,
                is_a=self.is_a,
                model=self.model,

                path=self.path,
                fields=self.fields,
                sub_id=self.sub_id,
            )

    def _general_input(self):
        yield Label(
            self.field.display.en,
            classes=f'{self.field.name}-label input-label',
        )

        yield FieldInput(
            model_value=self.value,

            field=self.field,
            is_a=self.is_a,
            model=self.model,

            fields=self.fields,
            path=self.path,
            sub_id=self.sub_id,

            classes=f'{self.field.name}-input',
        )

    def compose(self):
        match self.is_a.type:
            case 'child':
                yield from self._child_input()
            case 'list':
                yield from self._list_input()
            case _:
                yield from self._general_input()

        results = ListView(classes='validation_messages_list')
        results.styles.display = 'none'
        yield results

    @on(ValueChanged)
    def handle_change(self, event: ValueChanged):
        self.value = event.value
        validation_results = self.is_a.metadata.type.validate(
            self.value,
            self.path,
        )
        self.update_validation_messages(validation_results)

    @work
    async def update_validation_messages(self, result: ValidationResult):
        async with self.lock:
            if result.valid:
                self.add_class('valid')
                self.remove_class('invalid')
            else:
                self.add_class('invalid')
                self.remove_class('valid')

            results = self.query_one('ListView')
            await results.clear()

            # Updating the UI to show the reasons why validation failed
            if not result.valid:
                items = [
                    ListItem(Label(msg.message), classes='validation_message')
                    for msg in result.messages
                ]
                await results.extend(items)
                results.styles.display = 'block'
            else:
                results.styles.display = 'none'
