from public import private, public

from omniblack.model import Model

struct_def = {
    'name': 'mongo',
    'display': {'en': 'Mongo Connection Options'},
    'desc': {'en': 'Options to configure the connection to mongo.'},
    'fields': [
        {
            'name': 'mongo_username',
            'display': {'en': 'Mongo Username'},
            'desc': {
                'en': 'The username that should be used to authenticate with mongo.'  # noqa E501
            },
            'required': True,
            'is_a': {'type': 'string'},
        },
        {
            'name': 'mongo_password',
            'display': {'en': 'Mongo Password'},
            'desc': {
                'en': 'The password that should be used to authenticate with mongo.',  # noqa E501
            },
            'required': True,
            'is_a': {'type': 'secret'},
        },
        {
            'name': 'mongo_url',
            'display': {'en': 'Mongo URL'},
            'desc': {
                'en': 'The url used to talk to mongo.',
            },
            'required': True,
            'is_a': {'type': 'string'},
        },
    ],
}

options_model = Model(name='Mongo Options', struct_defs=[struct_def])

MongoOptions = mongo  # noqa:F821
public(MongoOptions=MongoOptions)
private(mongo)  # noqa:F821
