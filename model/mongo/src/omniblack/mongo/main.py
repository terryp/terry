from functools import partial
from logging import getLogger

from bson import ObjectId
from public import public
from pymongo import MongoClient

from omniblack.app import Storage
from omniblack.model import (
    Plugin,
    TypeExt,
    ValidationResult,
    coerce_from,
    coerce_to,
    native_adapter,
)

log = getLogger(__name__)


class Id(str):
    __slots__ = ()


IdImpl = Id


class Id(TypeExt):
    name = 'id'

    implementation = Id

    from_mongo = native_adapter
    to_mongo = native_adapter
    from_string = native_adapter
    to_string = native_adapter

    def json_schema(self):
        return {
            'type': 'string',
        }

    def validate(self, _value, _path):
        return ValidationResult((), valid=True)


class MongoStructBase:
    def __init__(self, values=None, **kwargs):
        no_id_values = not values or '_id' not in values
        no_id_kwargs = '_id' not in kwargs

        set_id = no_id_values and no_id_kwargs

        if set_id:
            kwargs['_id'] = str(ObjectId())

        super().__init__(values, **kwargs)


@public
class MongoStorage(Storage):
    def __init__(self, *mongo_args, model, **mongo_kwargs):
        self.client = MongoClient(*mongo_args, **mongo_kwargs)
        self.db = self.client.get_database()
        self.model = model
        log.info('Connection to mongo opened')

    def save(
        self,
        indiv,
        *,
        return_saved=False,
        return_metadata=False,  # noqa ARG002
        exclusive_creation=False,
        exclusive_update=False,
    ):
        struct_name = indiv.__class__.meta.name
        col = self.db[struct_name]
        rec = coerce_to(indiv, 'mongo')
        if exclusive_creation:
            write_result = col.insert_one(rec)
        else:
            mongo_filter = {'_id': rec['_id']}
            write_result = col.replace_one(  # noqa F841
                mongo_filter,
                rec,
                upsert=not exclusive_update,
            )

        if return_saved:
            doc = col.find_one({'_id': indiv._id})
            return coerce_from(self.model, doc, 'mongo', struct_name)

        return None

    def read(self, id, struct_cls):
        struct_name = struct_cls.meta.name
        doc = self.db[struct_name].find_one({'_id': id})
        return coerce_from(self.model, doc, 'mongo', struct_name)

    def search(self, struct_cls, skip=0, limit=0, mongo_filter=None):
        struct_name = struct_cls.meta.name
        docs = self.db[struct_name].find(mongo_filter, skip=skip, limit=limit)

        return (
            coerce_from(self.model, doc, 'mongo', struct_name)
            for doc in docs
        )

    def delete(self, indiv):
        struct_name = indiv.__class__.meta.name
        col = self.db[struct_name]
        result = col.delete_one({'_id': indiv._id})
        return result.acknowledged and result.deleted_count == 1


@public
class MongoPlugin(Plugin):
    type_exts = [Id]

    def on_create_class(self, _name, bases, _attrs, _struct_def):
        bases.append(MongoStructBase)


@public
def mongo(*mongo_args, db, **mongo_kwargs):
    return partial(MongoPlugin, *mongo_args, db=db, **mongo_kwargs)
