import astring from 'astring';
import { Path } from '@omniblack/pathlib';

import {
    ImportDeclaration,
    ImportDefaultSpecifier,
    Literal,
    Identifier,
    Property,
    ObjectExpression,
    MemberExpression,
    ArrayExpression,
    ExportDefaultDeclaration,
    Program,
    ChainExpression,
    Generator,
    LogicalExpression,
} from '@omniblack/estree';

const { generate } = astring;

export async function *pagesFinder(pagesDir) {
    for await (const entry of await pagesDir.opendir()) {
        if (await entry.isDir()) {
            yield* pagesFinder(entry);
        } else if (await entry.isFile() || await entry.isSymLink()) {
            const isVue = entry.suffix === 'vue';
            const isPrivate = entry.name.startsWith('__')
                && entry.name.endsWith('__');

            if (isVue && !isPrivate) {
                yield entry;
            }
        }
    }
}

export function staticImporter(pagesDir) {
    const imports = [];
    const routesExports = [];

    function addFiles(paths) {
        for (const path of paths) {
            const name = path.name;
            const relative = path.relativeTo(pagesDir);

            const identifier = new Identifier({ name });
            const specifier = new ImportDefaultSpecifier({ local: identifier });
            const importSource = new Literal({ value: String(path) });
            const importDec = new ImportDeclaration({
                specifiers: [specifier],
                source: importSource,
            });

            imports.push(importDec);


            const routePath = new Path('/')
                .join(relative)
                .withExt('')
                .toString();

            const pathProperty = new Property({
                key: new Literal({ value: 'path' }),
                value: new Literal({ value: routePath }),
            });

            const componentProperty = new Property({
                key: new Literal({ value: 'component' }),
                value: identifier,
            });

            const metaProperty = new Property({
                key: new Literal({ value: 'meta' }),
                value: new MemberExpression({
                    object: identifier,
                    property: new Identifier({ name: 'routeMeta' }),
                }),
            });

            const aliasProperty = new Property({
                key: new Literal({ value: 'alias' }),
                value: new LogicalExpression({
                    operator: '??',
                    left: new ChainExpression({
                        expression: new MemberExpression({
                            object: new MemberExpression({
                                object: identifier,
                                property: new Identifier({ name: 'routeMeta' }),
                            }),
                            optional: true,
                            property: new Identifier({ name: 'alias' }),
                        }),
                    }),
                    right: new ArrayExpression({ elements: [] }),
                }),
            });

            const routeObj = new ObjectExpression({
                properties: [
                    aliasProperty,
                    pathProperty,
                    componentProperty,
                    metaProperty,
                ],
            });

            routesExports.push(routeObj);
        }
    }

    function toString() {
        const exportArray = new ArrayExpression({
            elements: routesExports,
        });

        const exportDec = new ExportDefaultDeclaration({
            declaration: exportArray,
        });

        const program = new Program({
            body: [
                ...imports,
                [exportDec],
            ],
        });
        return generate(program, {
            indent: ' '.repeat(4),
            generator: Generator,
        });
    }

    return {
        addFiles,
        toString,
    };
}

