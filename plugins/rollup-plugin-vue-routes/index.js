import { createFilter } from 'rollup-pluginutils';
import YAML from 'js-yaml';

import { pagesFinder, staticImporter } from './generator.js';
import { Path } from '@omniblack/pathlib';

export function routes({ pagesDir }) {
    const blockFilter = createFilter([/vue&type=route/]);
    const name = '$routes$';

    pagesDir = new Path(pagesDir);
    pagesDir = pagesDir.resolve();

    // A cache of all known files
    const cache = new Set();

    function watch(context) {
        if (context.meta.watchMode) {
            context.addWatchFile(String(pagesDir) + '/');
        }
    }

    return {
        name: 'vue-routes',
        async buildStart() {
            watch(this);
            if (cache.size === 0) {
                for await (const file of pagesFinder(pagesDir)) {
                    cache.add(file);
                }
            }
        },
        resolveId(source) {
            watch(this);
            if (source === name) {
                return source;
            }

            return null;
        },
        async load(id) {
            watch(this);
            if (id === name) {
                const file = staticImporter(pagesDir);
                file.addFiles(cache);
                const str = file.toString();
                return str;
            }

            return null;
        },

        transform(code, id) {
            if (blockFilter(id)) {
                const outCode = JSON.stringify(
                    YAML.load(code.trim()),
                );

                return {
                    code: `
                        export default function route(component) {
                            component.routeMeta = ${outCode};
                        }
                    `,
                    map: null,
                };
            }
        },

        watchChange(path, { event }) {
            const pathObj = new Path(path);
            if (event === 'delete') {
                cache.delete(pathObj);
            }
        },
    };
}

