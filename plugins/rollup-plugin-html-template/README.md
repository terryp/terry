[npm]: https://img.shields.io/npm/v/@omniblack/rollup-plugin-html-template
[npm-url]: https://www.npmjs.com/package/@omniblack/rollup-plugin-html-template
[size]: https://packagephobia.now.sh/badge?p=@omniblack/rollup-plugin-html-template
[size-url]: https://packagephobia.now.sh/result?p=@omniblack/rollup-plugin-html-template

[![npm][npm]][npm-url]
[![size][size]][size-url]
[![libera manifesto](https://img.shields.io/badge/libera-manifesto-lightgrey.svg)](https://liberamanifesto.com)

# @omniblack/rollup-plugin-html-template

🍣 A Rollup plugin which creates HTML files to serve Rollup bundles.

Please see [Supported Output Formats](#supported-output-formats) for
information about using this plugin with output formats other than
`esm` (`es`), `iife`, and `umd`.

## Requirements

This plugin requires an ~~[LTS] Node version (v12.0.0+)~~ and Rollup v1.20.0+.
At the moment this module requires a Node version greater than or equal to 14.
We are using the native EcmaScript module implementation.
Once Node 16 enters long term support we will resume the normal support of
all Node [LTS] versions.

## Install

Using pnpm:

``` console
pnpm install --save-dev @omniblack/rollup-plugin-html-template
```

Using npm:

```console
npm install --save-dev @omniblack/rollup-plugin-html-template
```

Using yarn:

```console
yarn add --dev @omniblack/rollup-plugin-html-template
```

## Usage

Create a `rollup.config.js` [configuration file] and import the plugin:

```js
import html from '@omniblack/rollup-plugin-html-template';

export default {
  input: 'src/index.js',
  output: {
    dir: 'output',
    format: 'esm'
  },
  plugins: [
      html({
          templatePath: 'src/template.html',
      }),
  ]
};
```

Then call `rollup` either via the [CLI] or the [API].

Once run successfully, an HTML file should be written to the bundle
output destination.

## Options

### `attributes`

Type: `Object`<br>
Default: `{ html: { lang: 'en' }, link: null, script: null }`

Specifies additional attributes for `html`, `link`, and `script` elements. For
each property, provide an object with key-value pairs that represent an HTML
element attribute name and value. By default, the `html` element is rendered
with an attribute of `lang="en"`.

_Note: If using the `es` / `esm` output format, `{ type: 'module'}` is
automatically added to `attributes.script`._

### `fileName`

Type: `String`<br>
Default: `'index.html' || null`

The path to write the output html to. If `name` is provided then the default
is ignored and the output html will use `assetFileNames`.

### `name`

Type: `String`<br>
Default: `null`

The name rollup will use for the `[name]` substitutions with `assetFileNames`.
This is not used if `fileName` is provided.

### `meta`

Type: `Array[...object]`<br>
Default: `[{ charset: 'utf-8' }]`

Specifies attributes used to create `<meta>` elements. For each array item,
provide an object with key-value pairs that represent `<meta>` element
attribute names and values.

Specifies the name of the HTML to emit.

### `publicPath`

Type: `String`<br>
Default: `''`

Specifies a path to prepend to all bundle assets (files) in the HTML output.

### `title`

Type: `String`<br>
Default: `'Rollup Bundle'`

Specifies the HTML document title.
This title will not be used if the template file provides a title.

### `templatePath`
Type: `path`<br>
Required: `true`

A path, relative to the current working directory, to the ejs template file
to use.


## Supported Output Formats

By default, this plugin supports the `esm` (`es`), `iife`, and `umd`
[output formats], as those are most commonly used as browser bundles.
Other formats can be used, but will require using the [`template`](#template)
option to specify a custom template function which renders the unique
requirements of other formats.

### `amd`

Will likely require use of RequireJS semantics, which allows only for a single
entry `<script>` tag. If more entry chunks are emitted, these need to be loaded
via a proxy file. [RequireJS] would also need to be a dependency and added to
the build.

### `system`

Would require a separate `<script>` tag first that adds the `s.js` minimal
loader. Loading modules might then resemble:
`<script>System.import('./batman.js')</script>`.

## Attribution

This plugin was inspired by and is based upon [mini-html-webpack-plugin]
by Juho Vepsäläinen and Artem Sapegin, with permission.

## Meta

[LICENSE (MIT)](./LICENSE)

[mini-html-webpack-plugin]: https://github.com/styleguidist/mini-html-webpack-plugin

[RequireJS]: https://requirejs.org/docs/start.html

[output formats]: https://rollupjs.org/guide/en/#outputformat

[configuration file]: https://www.rollupjs.org/guide/en/#configuration-files
[LTS]: https://github.com/nodejs/Release
[CLI]:  https://www.rollupjs.org/guide/en/#command-line-reference
[API]:  https://www.rollupjs.org/guide/en/#javascript-api
