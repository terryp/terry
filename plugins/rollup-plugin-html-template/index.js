import { extname, resolve } from 'node:path';
import fs from 'node:fs/promises';

import jsdom from 'jsdom';
import ejs from 'ejs';
import minifyModule from 'html-minifier';

const htmlMinify = minifyModule.minify;

function getFiles(bundle) {
    const files = Object.values(bundle).filter(
        (file) => {
            return file.isEntry || file.type === 'asset';
        },
    );
    const result = {};
    for (const file of files) {
        const { fileName } = file;
        const extension = extname(fileName).slice(1);
        result[extension] = [
            ...result[extension] ?? [],
            ...file,
        ];
    }

    return result;
}

async function templateHTML({
    attributes,
    files,
    meta,
    publicPath,
    title,
    templateFunc,
    replacements,
    minify = false,

}) {
    const htmlStr = await templateFunc(replacements);
    const dom = new jsdom.JSDOM(htmlStr);
    const { document } = dom.window;
    for (const { fileName } of files.js || []) {
        const scriptNode = document.createElement('script');
        scriptNode.src = `${publicPath}${fileName}`;
        for (const [name, value] of entries(attributes.script)) {
            scriptNode[name] = value;
        }
        document.body.append(scriptNode);
    }

    for (const { fileName } of files.css ?? []) {
        const linkNode = document.createElement('link');
        linkNode.href = `${publicPath}${fileName}`;
        linkNode.rel = 'stylesheet';
        for (const [name, value] of entries(attributes.link)) {
            linkNode[name] = value;
        }
        document.head.append(linkNode);
    }

    for (const attrs of meta) {
        const metaNode = document.createElement('meta');
        for (const [name, value] of entries(attrs)) {
            metaNode[name] = value;
        }

        document.head.prepend(metaNode);
    }

    for (const [name, value] of entries(attributes.html)) {
        document.documentElement[name] = value;
    }

    if (!document.title) {
        document.title = title;
    }

    if (minify) {
        return htmlMinify(dom.serialize(), {
            collapseWhitespace: true,
            removeComments: true,
            removeRedundantAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true,
            useShortDoctype: true,
        });
    }

    return dom.serialize();
}

function entries(obj) {
    return Object.entries(obj ?? {});
}


const supportedFormats = new Set(['es', 'esm', 'iife', 'umd']);

const defaults = {
    attributes: {
        link: null,
        html: { lang: 'en' },
        script: null,
    },
    meta: [{ charset: 'utf8' }],
    publicPath: '',
    title: 'Rollup Bundle',
};

export function html(opts = {}) {
    if (!opts.fileName && !opts.name) {
        opts.fileName = 'index.html';
    }

    const {
        attributes,
        fileName,
        name,
        meta,
        publicPath,
        templatePath,
        title,
    } = Object.assign(
        {},
        defaults,
        opts,
    );

    const templateId = resolve(templatePath);

    let templateFunc;
    let reloadTemplate = true;
    return {
        name: 'html',

        async generateBundle(output, bundle) {
            if (!supportedFormats.has(output.format) && !opts.template) {
                this.warn(
                    `plugin-html: The output format '${output.format}'
                is not directly supported. A custom \`template\` is probably
                required. Supported formats include:
                ${supportedFormats.join(', ')}`,
                );
            }

            if (output.format === 'esm' || output.format === 'es') {
                attributes.script = Object.assign(
                    {},
                    attributes.script,
                    { type: 'module' },
                );
            }

            const files = getFiles(bundle);

            const source = await templateHTML({
                templateFunc,
                attributes,
                bundle,
                files,
                meta,
                publicPath,
                title,
            });

            const htmlFile = {
                type: 'asset',
                source,
                name,
                fileName,
            };

            this.emitFile(htmlFile);
        },
        async buildStart() {
            if (this.meta.watchMode) {
                this.addWatchFile(templateId);
            }

            if (reloadTemplate) {
                templateFunc = await createTemplateFunc(templateId);
                reloadTemplate = false;
            }
        },

        watchChange(id) {
            if (id === templateId) {
                reloadTemplate = true;
            }
        },
    };
}

async function createTemplateFunc(templatePath) {
    const fileStr = await fs.readFile(templatePath, { encoding: 'utf8' });
    return ejs.compile(fileStr, {
        async: true,
        strict: true,
        _with: false,
    });
}
