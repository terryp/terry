.PHONY: lint docs

lint:
	pnpx eslint . --format pretty '**/*.js'

docs:
	sphinx-build docs docs_build -b dirhtml -D html_baseurl='/docs'
