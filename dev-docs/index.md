# Welcome to Omniblack Dev Docs's documentation!

```{toctree}
:maxdepth: 2
:caption: Contents
```

## Indices and tables

* {ref}`genindex`
* {ref}`modindex`
* {ref}`search`

## Developer Introduction

### Repository Layout

| directory    | purpose               | notes       |
|--------------|-----------------------|-------------|
| tooling      | dev-tools             |             |
| lib          | libraries             | [^split_up] |
| spa          | websites              |             |
| desktop_apps | desktop apps          | [^electron] |
| extensions   | browser extensions    |             |
| model        | model implementations |             |
| plugins      | any plugins           |             |

### Package Structure

Each package has a top-level `package_config.yaml`, and either
a `package.json` (for packages containing javascript)
and or a `pyproject.toml` (for packages containing python).
Other configurations files may be kept next to the package_config.yaml.
Tests should be kept in a `test` directory, and docs should be kept
in a `docs` directory.  In packages that are intended to be published
as python packages should be kept under `omniblack/<package_name>`,
this allows namespace packaging. For packages that will not be published
as a python package source code files should be kept under a `src` directory.

[^split_up]: At some point this will probably need to be split up.
    I think js_lib, and py_lib might be good choices, but js_lib
    might need to be split as well.

[^electron]: Currently we use electron, but I am
    looking to alternatives. Electron works every hard to sandbox code
    running in the renderer processes. This slows the iteration of electron.
    I don't need this security, because I don't run untrusted code in my
    desktop apps.
