load('ext://uibutton', 'cmd_button', 'location')

docker_prune_settings(num_builds=10)

SRC = os.getenv('SRC')


def _run(*args, **kwds):
    """Return the output of a local command with out all the noise."""
    if 'quiet' not in kwds:
        kwds['quiet'] = True

    if 'echo_off' not in kwds:
        kwds['echo_off'] = True

    return local(*args, **kwds)


# tools the containers should watch to restart
tool_files = [
    '.node-version',
    'dev-tools/',
]

for fn in tool_files:
    watch_file(os.path.join(SRC, fn))


def sub_vars(file, **kwds):
    replacementStrs = [
        key + '=' + value
        for key, value in kwds.items()
        if value != None
    ]

    replacementStr = ' '.join(replacementStrs)

    return _run('_var_substitute ' + replacementStr + ' < ' + SRC + '/' + file)


def _port(name, port):
    return str(_run('get_port.mjs ' + name + ' ' + port))


def _make_port_forward(port, bind, name):
    return port_forward(
        int(port),
        int(port),
        host=bind,
        name=name + '-' + port,
    )


# utility function to run a prepackaged service
def service(
    task,
    labels,
    local_dir=None,
    after=[],
    port_name=None,
    port_forwards=None,
    links=None,
):
    if port_name != None:
        # handle the main port
        port = _port(port_name, 'service')
        ports = [port + ':' + port]
    else:
        port = None
        ports = []

    if local_dir:
        _run('mkdir -pv ' + local_dir)

    yaml_str = sub_vars('dev-tools/run_' + task + '.yaml', __PORT=port)

    # get our pod definition
    k8s_yaml(yaml_str)

    # map the port to the Tilt UI and forward from the outside
    k8s_resource(
        task,
        port_forwards=port_forwards or ports,
        labels=labels,
        resource_deps=after,
        links=links,
    )


def docs():
    local_resource(
        'build-docs',
        cmd='make docs',
        labels='z-docs',
        allow_parallel=True,
        deps=[
            SRC + '/docs',
            SRC + '/tooling',
            SRC + '/lib',
            SRC + '/model',
            SRC + '/spa',
            SRC + '/plugins',
        ],
    )


# ----------------------------------------------------------------------

# we use mongo for our database
service(
    'mongo',
    local_dir=SRC + '/.mongo-data',
    port_name='mongo',
    labels=['z-3rdParty'],
)

service(
    'caddy',
    labels=['z-3rdParty'],
    port_forwards=[
        port_forward(3000, name='Http Root'),
        port_forward(8000, name='Admin Api'),
    ],
)

local_resource(
    'Configure Caddy Routes',
    ['vulcan', 'configure-caddy'],
    resource_deps=['caddy'],
    deps=['**/package_config.yaml'],
    labels=['z-3rdParty'],
)

docs()
load_dynamic(SRC + '/.generated.tiltfile')
