---
author: Terry Patterson
email: <Terryp@wegrok.net>
date: 2021-11-14 09:29:58.819850
severity: patch
package: mithril
---
Improved NavLink and added routing utils
