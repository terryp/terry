---
author: Terry Patterson
email: <Terryp@wegrok.net>
date: 2021-10-05 19:16:47.551570
severity: major
package: localization
---
The `.localize` method will be passed three arguments.

1. lang - The user's preferred language.
1. locales - A list of the user's preferred locales.
1. localize - A function that can be called on sub-localizables.
