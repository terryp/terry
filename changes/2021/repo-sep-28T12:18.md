---
author: Terry Patterson
email: <Terryp@wegrok.net>
date: 2021-09-28 12:18:29.963356
severity: patch
package: repo
---
Add support for per package licenses.
This is helpful when forking a package.
