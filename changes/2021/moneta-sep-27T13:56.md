---
author: Terry Patterson
email: <Terryp@wegrok.net>
date: 2021-09-27 13:56:22.353063
severity: patch
package: moneta
---
Move to Apache License 2.0
