---
author: Terry Patterson
email: <Terryp@wegrok.net>
date: 2021-11-14 09:29:58.819850
severity: patch
package: stores
---
Split `DerviedStore` into `DerviedStore` and `AsyncDerivedStore`
