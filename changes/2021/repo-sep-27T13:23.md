---
author: Terry Patterson
email: <Terryp@wegrok.net>
date: 2021-09-27 13:23:34.651881
severity: minor
package: repo
---
Added new fields to `Package`:
    * `name`: The caconical name of the package.
    * `version`: The current version of the package.
    * `py`, `js`: The python and javascript package information.

Removed field from `Package`:
    * `python`
    * `javascript`
    * `py_path`
    * `js_path`

These information is now avalible in the respective package info, `js` or
`py`.
