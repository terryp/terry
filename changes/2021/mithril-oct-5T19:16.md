---
author: Terry Patterson
email: <Terryp@wegrok.net>
date: 2021-10-05 19:16:47.551570
severity: patch
package: mithril
---
Added new time component.

Exposed focused and color on Date, and Autocomplete.
