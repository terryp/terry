---
author: Terry Patterson
email: <Terryp@wegrok.net>
date: 2021-09-27 13:23:34.651881
severity: minor
package: async_itertools
---
*Breaking Change*: Renamed project to `@omniblack/async_itertools`
