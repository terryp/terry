---
author: Terry Patterson
email: Terryp@wegrok.net
date: 2024-01-23 18:40:45.693307
severity: minor
package: lib/py_indexes
---
Add the ability to fetch objects, or download files from an index.
