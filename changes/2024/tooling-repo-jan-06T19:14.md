---
author: Terry Patterson
email: Terryp@wegrok.net
date: 2024-01-06 19:14:24.567181
severity: minor
package: tooling/repo
---
Add better support for python c-extenstions written in rust
Add support for src layout python packages

