---
author: Terry Patterson
email: Terryp@wegrok.net
date: 2024-01-20 13:22:02.114702
severity: patch
package: lib/cli
---
If a non-required field has no assumed value, use `None`.
