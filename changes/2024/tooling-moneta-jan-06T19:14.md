---
author: Terry Patterson
email: Terryp@wegrok.net
date: 2024-01-06 19:14:24.567181
severity: patch
package: tooling/moneta
---
Switch from editing file to a TUI based on [textual](https://textual.textualize.io/)

