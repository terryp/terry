---
author: Terry Patterson
email: Terryp@wegrok.net
date: 2024-01-06 19:14:24.567181
severity: patch
package: lib/cli
---
Retrieve help text from argument descriptions

