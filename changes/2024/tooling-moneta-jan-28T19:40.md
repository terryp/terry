---
author: Terry Patterson
email: Terryp@wegrok.net
date: 2024-01-28 19:40:52.242527
severity: minor
package: tooling/moneta
---
Moneta now accepts `--ignore-no-changes` to allow programmatic
usage to not care if changes are present.
