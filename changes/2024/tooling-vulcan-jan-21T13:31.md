---
author: Terry Patterson
email: Terryp@wegrok.net
date: 2024-01-21 13:31:09.701078
severity: minor
package: tooling/vulcan
---
Remove the use of pathlib
