---
author: Terry Patterson
email: Terryp@wegrok.net
date: 2024-01-28 18:43:23.221464
severity: minor
package: tooling/moneta
---
Add timed publishing.

A package will only be published when a minor change is one week old,
or a patch change is two weeks old. When a change is old enough
all changes for the package will be published.
