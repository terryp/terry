---
author: Terry Patterson
email: Terryp@wegrok.net
date: 2024-01-28 19:20:41.355482
severity: minor
package: tooling/moneta
---
Moneta now creates annotated tags instead of lightweight tags.
