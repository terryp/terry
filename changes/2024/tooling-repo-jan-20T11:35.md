---
author: Terry Patterson
email: Terryp@wegrok.net
date: 2024-01-20 11:35:48.640671
severity: minor
package: tooling/repo
---
Remove the use of pathlib
