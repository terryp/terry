---
author: Terry Patterson
email: Terryp@wegrok.net
date: 2024-01-28 18:58:02.075221
severity: minor
package: tooling/moneta
---
Add dry run to moneta, allowing for checks of publishing state
without making actual changes.
