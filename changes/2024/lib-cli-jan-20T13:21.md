---
author: Terry Patterson
email: Terryp@wegrok.net
date: 2024-01-20 13:21:06.115108
severity: patch
package: lib/cli
---
Add support for list arguments
