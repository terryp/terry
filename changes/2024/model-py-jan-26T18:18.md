---
author: Terry Patterson
email: Terryp@wegrok.net
date: 2024-01-26 18:18:28.554465
severity: minor
package: model/py
---
Change `.struct_def` and `.field_def` to `.meta`
