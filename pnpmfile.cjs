/* eslint-env node */
const { resolutions } = JSON.parse(
    // eslint-disable-next-line n/no-sync
    require('node:fs').readFileSync('./package.json', 'utf8'),
);

if (resolutions) {
    module.exports = {
        hooks: {
            readPackage,
        },
    };

    function readPackage(pkg, context) {
        if (pkg.dependencies) {
            for (const k in resolutions) {
                if (pkg.dependencies[k]
                    && pkg.dependencies[k] !== resolutions[k]) {
                    context.log(
                        `'${k}@${pkg.dependencies[k]}' overridden in "${pkg.name}" to "${k}@${resolutions[k]}"`,
                    );
                    pkg.dependencies[k] = resolutions[k];
                }
            }
        }

        return pkg;
    }
} else {
    module.exports = {};
}
