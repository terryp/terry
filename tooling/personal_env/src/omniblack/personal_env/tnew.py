from sh import git as bg_git

from omniblack.cli import CLI
from omniblack.repo import get_remote, main_branch

from .model import model

git = bg_git.bake(_fg=True)

remotes = tuple(
    line.strip()
    for line in bg_git.remote(_iter=True)
)

for_reach_ref = bg_git.bake('for-each-ref')


branches = tuple(
    line.strip()
    for line in for_reach_ref('refs/heads/', format='%(refname:short)')
)

app = CLI('omniblack.personal_env', model=model)


@app.command
def tnew(new_branch_name, parent=None, tracking_remote=None):
    if parent is None:
        parent = main_branch()

    if tracking_remote is None:
        tracking_remote = get_remote()

    git.switch(f'{tracking_remote}/{parent}', create=new_branch_name)
    git.push(
        tracking_remote,
        f'{new_branch_name}:{new_branch_name}',
        set_upstream=True,
        _fg=True,
    )


if __name__ == '__main__':
    app()
