#! /usr/bin/python3

from collections import defaultdict
from logging import DEBUG, basicConfig, getLogger
from pathlib import Path
from shutil import rmtree
from sys import stdout

from git import NoSuchPathError, RemoteProgress, Repo

from omniblack.config_files import Config
from omniblack.run import entry

basicConfig(stream=stdout)

log = getLogger(__name__)

log.setLevel(DEBUG)


class OpDict(defaultdict):
    def __missing__(self, key):
        log.debug('unknown opcode', key)
        return super().__missing__(key)


def default():
    return 'unknown opcode'


op_table = OpDict(default)
op_table[RemoteProgress.BEGIN] = 'Begin'
op_table[RemoteProgress.CHECKING_OUT] = 'Checking out'
op_table[RemoteProgress.COMPRESSING] = 'Compressing'
op_table[RemoteProgress.COUNTING] = 'Counting'
op_table[RemoteProgress.DONE_TOKEN] = 'Done'
op_table[RemoteProgress.END] = 'End'
op_table[RemoteProgress.FINDING_SOURCES] = 'Finding sources'
op_table[RemoteProgress.OP_MASK] = 'Op mask'
op_table[RemoteProgress.RECEIVING] = 'Receiving'
op_table[RemoteProgress.RESOLVING] = 'Resolving'
op_table[RemoteProgress.STAGE_MASK] = 'Stage mask'
op_table[RemoteProgress.WRITING] = 'Writing'


class MyProgressPrinter(RemoteProgress):
    def __init__(self, *args, name, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = name

    def update(self, op_code, cur_count, max_count=None, message=''):
        op_msg = op_table[op_code]
        percent = int((cur_count / max_count) * 100) or 100
        log.debug(f'{op_msg} {self.name}: {percent} percent {message or ""}')


home = Path.home()
install_dir = home.joinpath('.gitDeps')


def up_to_date(repo):
    remote = repo.remotes[0]
    remote_head = remote.refs[0]
    head = repo.head
    return head.object == remote_head.object


def update_dep(name, dep):
    repo_dir = install_dir.joinpath(name)
    try:
        repo = Repo(repo_dir)
        remote = repo.remotes[0]
        log.debug(f'Fetching {name}')
        remote.fetch(depth=1, progress=MyProgressPrinter(name=name))
        if not up_to_date(repo):
            log.debug(f'{name} was not up to date, resetting')
            repo.head.reset(working_tree=True, index=True, hard=True)
            log.debug(f'Cleaning {name}')
            git = repo.git
            git.clean('-dfx')

    except NoSuchPathError:
        log.debug(f'Cloning {name}')
        repo = Repo.clone_from(
            dep['remote'],
            to_path=repo_dir,
            progress=MyProgressPrinter(name=name),
            depth=1,
        )


def uninstall_deps(deps):
    for directory in install_dir.iterdir():
        if directory.name not in deps:
            log.info(f'Removing {directory}')
            rmtree(directory)


@entry
def sync_git_deps():
    """
        Sync ~/.gitDeps to the state of ~/.config/dotfiles/deps.yaml
    """
    raw_deps = Config('dotfiles', 'deps')

    deps = {
        key: value
        for key, value in raw_deps.items()
        if value['type'] == 'git'
    }

    for name, dep in deps.items():
        update_dep(name, dep)

    uninstall_deps(deps)
