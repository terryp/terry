#! /usr/bin/python3
from logging import getLogger
from os import EX_CONFIG, environ
from os.path import expandvars
from sys import exit
from warnings import filterwarnings, warn

from omniblack import logging
from omniblack.run import entry

from ..config_files import Config, Data, DataTypes, Path

dotfiles = None

logging.config(systemd=True)
logger = getLogger(__name__)

fatal = logger.fatal
warning = logger.warning
info = logger.info

home = Path.home()


class ProtectedFile(UserWarning):
    """
    Attempted to delete a protected file.
    """


filterwarnings(action='always', category=ProtectedFile)


def add_to_existing(existing_links, file, link):
    existing_links[file] = link


def delete_existing(existing_links, file):
    del existing_links[file]


def process_link(file, link, existing_links):
    if link.exists():
        if link.is_symlink():
            if file.resolve() != link.resolve():
                warning(f'{link} already exists, and does not point'
                        f' to {file}')
            else:
                add_to_existing(
                    existing_links,
                    file,
                    link
                )
        else:
            info(f'{link} already exists and is not a symlink')
    elif file.exists():
        link.parent.mkdir(parents=True, exist_ok=True)
        link.symlink_to(file)
        add_to_existing(existing_links, file, link)
    else:
        warning(f'{file} does not exist')


def process_existing_link(file, link, existing_links, current_links):
    if link not in current_links and link.exists():
        if link.resolve() == file.resolve():
            if dotfiles in link.parents:
                warn(f'File {link} in the program dir was marked'
                     ' for deletion  ignoring',
                     category=ProtectedFile)
        else:
            info(f'{link} was in existing links,'
                 ' but has been changed ignoring.')
            delete_existing(existing_links, file)


def process_file_path(file_path):
    file_path = Path(expandvars(file_path))
    if not file_path.is_absolute():
        file_path = dotfiles / file_path

    return file_path


@entry
def ensure_links(_name: str):
    """
        Manage symlinks from dotfiles to destinations.
    """
    global dotfiles
    dotfiles = Path(environ['DOTFILES'])
    links = Config('dotfiles', 'links')

    existing_links = Data(DataTypes.state, 'dotfiles', 'existing_links')

    if not links:
        fatal('No links in links.yaml')
        exit(EX_CONFIG)

    links = {
        process_file_path(file_path): Path(expandvars(link_path))
        for file_path, link_path in links.items()
    }

    for file, link in links.items():
        process_link(file, link, existing_links)

    current_links = set(links.values())

    for file, link in existing_links.copy().items():
        process_existing_link(file, link, existing_links, current_links)

    existing_links.write()
