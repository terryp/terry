#! /usr/bin/python3
from omniblack.cli import CLI
from omniblack.repo import get_remote, git, main_branch as get_main_branch

from .model import model

app = CLI('omniblack.personal_env', model=model)


def rebase(main_branch, remote):
    git.rebase(f'{remote}/{main_branch}', autostash=True, _fg=True)


@app.command
def main():
    git.fetch(prune=True, all=True, _fg=True)
    remote = get_remote()
    main_branch = get_main_branch()
    rebase(main_branch, remote)
