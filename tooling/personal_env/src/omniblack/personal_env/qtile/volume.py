from asyncio import CancelledError, get_running_loop
from contextlib import ExitStack

import pulsectl_asyncio

from libqtile.widget import base
from pulsectl import (
    PulseEventFacilityEnum as FacilityType,
    PulseEventTypeEnum as EventType,
)


class Volume(base._TextBox):
    """
    Widget that display and change volume
    """

    orientations = base.ORIENTATION_HORIZONTAL
    defaults = []

    def __init__(self, **config):
        super().__init__('0', **config)
        self.add_defaults(Volume.defaults)
        self.surfaces = {}
        self.volume = None
        self.muted = None
        self._pulse = pulsectl_asyncio.PulseAsync('event-printer')
        self.stack = ExitStack()
        self.stack.enter_context(self._pulse)

    async def _config_async(self):
        loop = get_running_loop()
        await self._pulse.connect()
        self.__watcher = loop.create_task(self.watch_volume())
        self.stack.callback(self.__watcher.cancel)

    def finalize(self):
        self.stack.close()
        super().finalize()

    async def watch_volume(self):
        try:
            async with pulsectl_asyncio.PulseAsync('event-printer') as pulse:
                server_info = await pulse.server_info()
                await self.set_volume(server_info, pulse)
                async for event in pulse.subscribe_events('all'):
                    is_source = event.facility == FacilityType.source
                    is_change = event.t == EventType.change
                    if is_change and is_source:
                        await self.set_volume(server_info, pulse)
        except CancelledError:
            pass

    async def set_volume(self, server_info, pulse):
        default_name = server_info.default_sink_name
        default_sink = await pulse.get_sink_by_name(default_name)
        vol = default_sink.volume.values[0]
        muted = bool(default_sink.mute)

        update = False
        if vol != self.volume:
            self.volume = vol
            update = True

        if muted != self.muted:
            self.muted = muted
            update = True

        if update:
            self.update()

    def update(self):
        # Update the underlying canvas size before actually attempting
        # to figure out how big it is and draw it.
        self._update_drawer()
        self.bar.draw()

    def _update_drawer(self):
        if self.muted:
            self.text = ' '
        else:
            self.text = f'{self.volume:2.0%}'
