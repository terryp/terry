from os import environ

from sh import dbus_update_activation_environment as dbus, systemctl

usys = systemctl.bake('--user')

variables = (
    'DESKTOP_SESSION',
    'XDG_CURRENT_DESKTOP',
    'XDG_SESSION_DESKTOP',
    'XDG_SESSION_TYPE',
    'XDG_SESSION_ID',
    'DISPLAY',
    'PATH',
    'PYTHONPATH',
    'ENV',
    'HEADLESS',
    'DOTFILES',
    'WAYLAND_DISPLAY',
)


def start_session(wm, session_target=None):
    if session_target is None:
        session_target = f'{wm}-session.target'

    environ['XDG_CURRENT_DESKTOP'] = wm
    environ['XDG_SESSION_DESKTOP'] = wm

    dbus(variables, systemd=True)
    usys('reset-failed')

    usys('import-environment', variables)
    usys.start(session_target)
