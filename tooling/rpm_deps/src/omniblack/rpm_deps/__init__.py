from collections.abc import Iterator
from concurrent.futures import ThreadPoolExecutor
from importlib.metadata import Distribution
from os import environ, getcwd
from os.path import join
from pathlib import Path
from subprocess import DEVNULL, check_call
from sys import executable, stderr, stdout
from tempfile import TemporaryDirectory

from dnf import Base
from packaging.requirements import Requirement
from packaging.utils import canonicalize_name
from packaging.version import Version
from public import public
from pyproject_hooks import BuildBackendHookCaller
from rich import print
from rich.progress import Progress
from rich.table import Table, box
from rpm import expandMacro

from omniblack.cli import CLI
from omniblack.model import Field
from omniblack.repo import Package, find_nearest, find_packages

from .model import model

app = CLI(__package__, model=model)


def parse_req(req_specifier):
    req = Requirement(req_specifier)

    return req.name


def subprocess_runner(
    cmd: list[str],
    cwd: str | None = None,
    extra_environ: dict[str, str] | None = None,
) -> None:
    """The default method of calling the wrapper subprocess.

    This uses :func:`subprocess.check_call` under the hood.
    """
    env = environ.copy()

    if extra_environ:
        env.update(extra_environ)

    check_call(cmd, cwd=cwd, env=env, stdout=stderr, stderr=stderr)


def null_runner(
    cmd: list[str],
    cwd: str | None = None,
    extra_environ: dict[str, str] | None = None,
) -> None:
    env = environ.copy()

    if extra_environ:
        env.update(extra_environ)

    check_call(cmd, cwd=cwd, env=env, stdout=DEVNULL)


UiString = model.meta_structs.UIString
IsA = model.meta_structs.IsA
MetaField = model.meta_structs.Field
ChoiceAttrs = model.meta_structs.choice_attrs
ChoiceMember = model.meta_structs.ChoiceMember


mode_field_def = MetaField(
    name='mode',
    display=UiString(en='Id'),
    desc=UiString(
        en='The mode of extract deps for.',
    ),
    required=True,
    is_a=IsA(
        type='choice',
        choice_attrs=ChoiceAttrs(
            enum_name='mode',
            choices=[
                ChoiceMember(
                    name='build',
                    display=UiString(en='Build-time Dependencies'),
                ),
                ChoiceMember(
                    name='runtime',
                    display=UiString(en='Runtime Dependencies'),
                ),
            ]
        ),
    ),
)

mode_field = Field(meta=mode_field_def, model=model)

Mode = mode_field.is_a.metadata.enum_cls


@app.command
def get_rpm_requires(
    mode: mode_field,
    dir_path: Path = None,
    *,
    verbose=False,
):
    dir_path = getcwd() if dir_path is None else str(dir_path)
    pkg = find_nearest(dir_path)

    requirements = tuple(get_rpm_deps(mode=mode, pkg=pkg, verbose=verbose))

    stdout.writelines(
        req + '\n'
        for req in requirements
    )


def _get_dep_strings(
    mode: Mode,
    pkg: Package,
    *,
    verbose=False,
) -> Iterator[str]:
    builder = BuildBackendHookCaller(
        pkg.path,
        pkg.py.manifest['build-system']['build-backend'],
        python_executable=executable,
        runner=subprocess_runner if verbose else null_runner,
    )

    if mode == Mode.build:
        yield from pkg.py.manifest['build-system']['requires']
        yield from builder.get_requires_for_build_sdist()
        yield from builder.get_requires_for_build_wheel()

    with TemporaryDirectory() as temp_dir:
        sub_dir = builder.prepare_metadata_for_build_wheel(temp_dir)
        full_path = join(temp_dir, sub_dir)
        dist = Distribution.at(full_path)
        if dist.requires is not None:
            yield from dist.requires


@public
def get_py_deps(
    mode: Mode,
    pkg: Package,
    *,
    verbose=False,
) -> Iterator[Requirement]:
    seen = set()

    for req in _get_dep_strings(mode=mode, pkg=pkg, verbose=verbose):
        req = Requirement(req)
        if req not in seen:
            yield req
            seen.add(req)


def _get_py_deps(kwargs):
    deps = tuple(get_py_deps(
        mode=kwargs['mode'],
        pkg=kwargs['pkg'],
        verbose=kwargs['verbose'],
    ))

    return kwargs['pkg'], deps


@public
def get_rpm_deps(mode: Mode, pkg: Package, *, verbose=False) -> Iterator[str]:
    for req in get_py_deps(mode=mode, pkg=pkg, verbose=verbose):
        yield get_dep_provide(req)


def get_dep_provide(req: Requirement) -> str:
    return expandMacro('%{py3_dist ' + req.name + '}').strip()


eq_names = {'updates', 'fedora'}


def get_rpm_version(req: Requirement, base: Base):
    provide = get_dep_provide(req)
    q = base.sack.query()
    available = q.available()
    provider_packages = available.filter(provides=provide)

    found_version = None
    found_package = None

    for dnf_pkg in provider_packages:
        if found_version is not None and found_package.repo in eq_names:
            msg = f'Multiple rpm packages found for {req.name}'
            raise ValueError(msg)

        found_version = Version(dnf_pkg.version)
        found_package = dnf_pkg

    return found_version


@app.command
def check_dep_versions(mode: mode_field, *, verbose=False):
    pkgs = tuple(
        pkg
        for pkg in find_packages()
        if pkg.py is not None
    )

    workspace_names = {
        canonicalize_name(pkg.py.canonical_name)
        for pkg in pkgs
    }
    mismatched_pkgs = []

    with (
        Progress(transient=True) as prog,
        Base() as base,
        ThreadPoolExecutor(max_workers=4) as pool,
    ):
        base.conf.set_or_append_opt_value('skip_if_unavailable', 'true')
        base.read_all_repos()
        base.fill_sack(load_system_repo=False)

        get_deps_args = (
            {'pkg': pkg, 'mode': mode, 'verbose': verbose}
            for pkg in pkgs
        )
        pkgs_tracked = prog.track(
            pool.map(_get_py_deps, get_deps_args),
            total=len(pkgs),
            description='[blue]Checking package deps[/][red]...[/]',
        )

        for pkg, deps in pkgs_tracked:
            for req in deps:
                if canonicalize_name(req.name) not in workspace_names:
                    rpm_version = get_rpm_version(req, base)
                    if rpm_version is None:
                        mismatched_pkgs.append((pkg, req, None))
                    elif not req.specifier.contains(rpm_version):
                        mismatched_pkgs.append((pkg, req, rpm_version))

    table = Table(
        title='Mismatched Packages',
        box=box.HORIZONTALS,
        show_lines=True,
        row_styles=['on grey15', ''],
    )
    table.add_column('Workspace Package', justify='right', style='blue')
    table.add_column('Requirement', justify='center', style='red')
    table.add_column('Repository Version', justify='left', style='green')

    for pkg, req, ver in mismatched_pkgs:
        table.add_row(
            canonicalize_name(pkg.py.canonical_name),
            str(req),
            str(ver),
        )

    if mismatched_pkgs:
        print(table)
