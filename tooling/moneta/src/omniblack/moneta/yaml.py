from ruamel.yaml import YAML

yaml = YAML()
yaml.indent(4)
yaml.default_flow_style = False
