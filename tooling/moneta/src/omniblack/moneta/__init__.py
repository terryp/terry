from omniblack.utils import improve_module

from .app import app  # noqa: F401

improve_module(__name__)
