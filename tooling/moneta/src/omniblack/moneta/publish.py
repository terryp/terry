from contextlib import suppress
from datetime import date, timedelta
from glob import glob
from logging import getLogger
from os.path import join, relpath as relative_to
from shutil import rmtree

import sh

from sh import ErrorReturnCode_128, git, twine
from tomlkit import dump

from omniblack.repo import find_packages, find_root

from .app import app
from .parse_diff import Severity
from .read_change import read_change

log = getLogger(__name__)
pyproject_build = getattr(sh, 'pyproject-build')

info_git = git.bake('--no-pager', '--no-optional-locks')

minor_period = timedelta(weeks=1)
patch_period = timedelta(weeks=1)


def get_changes(pkg):
    changes_dir = join(pkg.root_dir, 'changes')
    newest_tag = get_newest_tag(pkg)

    if newest_tag is not None:
        new_changes = info_git(
            'diff-index',
            '--no-color',
            '--name-only',
            '--diff-filter=A',
            newest_tag,
            changes_dir,
            _cwd=pkg.root_dir,
        )

        change_files = new_changes.strip().split('\n')
    else:
        change_files = glob(join(changes_dir, '**', '*.md'), recursive=True)

    changes = (
        read_change(change_file)
        for change_file in change_files
    )

    return [
        change
        for change in changes
        if change.package == pkg.rel_path
    ]


def get_newest_tag(pkg):
    try:
        result = info_git.describe(
            tags=True,
            match=pkg.rel_path + '-*',
            abbrev=0,
            _cwd=pkg.root_dir,
        )

        return result.strip()
    except ErrorReturnCode_128:
        return None


def has_majors(pkgs_to_change):
    for pkg, changes in pkgs_to_change.items():
        for change in changes:
            if change.severity == Severity.major:
                return True

    return False


def change_is_old_enough(pkg, changes, today):
    if not changes:
        return False

    for change in changes:
        change_date = change.date.date()
        change_age = today - change_date
        match change.severity:
            case Severity.minor:
                if change_age >= minor_period:
                    return True
            case Severity.patch:
                if change_age >= patch_period:
                    return True
            case Severity.major:
                return True

    log.info(
        f'[green]"{pkg.name}"[/] [orange_red1]has no changes '
        'that are old enough to publish.[/]'
    )
    return False


@app.command
def publish(
    dry_run: bool = False,  # noqa FBT002
    allow_major: bool = False,  # noqa FBT002
    ignore_no_changes: bool = False,  # noqa FBT002
):
    """
    Create a release commit including pending changes to packages.

    :param dry_run: Run a dry run, no changes will actually occur.
    :type dry_run: :type:`bool`

    :param allow_major: Allow major changes to be published.
    :type allow_major: :type:`bool`

    :param ignore_no_changes: Moneta will not exit with
        an error in there are no changes.
    :type ignore_no_changes: :type:`bool`
    """
    root_dir = find_root()

    pkg_to_change = {
        pkg: get_changes(pkg)
        for pkg in find_packages(root_dir, lazy=True)
        if pkg.py
    }

    today = date.today()

    pkg_to_change = {
        pkg: changes
        for pkg, changes in pkg_to_change.items()
        if change_is_old_enough(pkg, changes, today)
    }

    if not allow_major and has_majors(pkg_to_change):
        log.error(
            '[red]When [bold]major[/bold] changes are pending'
            ' no automtic publishing will occur.[/red]',
        )
        return 2

    new_versions = tuple(
        publish_pkg(pkg, changes, dry_run)
        for pkg, changes in pkg_to_change.items()
    )

    if not new_versions:
        log.error('[red]No changes to publish.[/]')
        return 0 if ignore_no_changes else 1

    change_messages = {
        pkg: pkg_commit_message(pkg, new_version, changes)
        for pkg, new_version, changes in new_versions
    }

    commit_lines = list(change_messages.values())

    commit_lines[0:-1] = [
        line + '\n'
        for line in commit_lines[0:-1]
    ]

    # TODO: check if package has already been uploaded
    commit_message = 'Bump Packages:\n\n' + '\n'.join(commit_lines)

    if not dry_run:
        git.commit(
            message=commit_message,
            _cwd=root_dir,
            verbose=True,
            _fg=True,
        )

    all_dist_files = []
    for pkg, *_ in new_versions:
        with suppress(FileNotFoundError):
            rmtree(join(pkg.path, 'dist'))

        log.info(f'[blue]Rebuilding[/] [green]{pkg.rel_path}[/]')
        pyproject_build(_fg=True, _cwd=pkg.path)
        dist_files = glob(join(pkg.path, 'dist', '*'))
        all_dist_files.extend(dist_files)

    all_tags = [
        (pkg, f'{pkg.rel_path}-{version}')
        for pkg, version, _ in new_versions
    ]

    if not dry_run:
        for pkg, tag in all_tags:
            message = change_messages[pkg]
            git.tag(
                tag,
                message=message,
                annotate=True,
                _fg=True,
            )
            log.info(f'[blue]Created tag[/] [green]{tag}[/]')

        log.info('[blue]Pushing release commit[/]')
        git.push(verbose=True, _fg=True)

        for tag in all_tags:
            log.info(f'[blue]Pushing tag [/][green]{tag}[/]')
            git.push('origin', tag, _fg=True, verbose=True)

        log.info('[blue]Uploading[/]:')
        for file in all_dist_files:
            log.info(f'  [green]{file}[/]')

        twine.upload(
            *all_dist_files,
            non_interactive=True,
            _fg=True,
            verbose=True,
        )
        return None
    else:
        return None


def pkg_commit_message(pkg, new_version, changes):
    lines = [f'* {pkg.rel_path} {pkg.version} -> {new_version}']
    lines.extend([
        (' ' * 2) + f'* {relative_to(change.path, pkg.root_dir)}'
        for change in changes
    ])

    return '\n'.join(lines)


def prepare_message(msg: str):
    lines = msg.splitlines()
    lines[0] = f'* {lines[0]}'
    lines[1:] = (
        f'  {line}'
        for line in lines[1:]
    )

    return '\n'.join(lines)


def publish_pkg(pkg, changes, dry_run):
    if len(changes) != 1:
        severity = max(
            change.severity
            for change in changes
        )
    else:
        change = changes[0]
        severity = change.severity

    new_version = pkg.version.bump(severity.name)
    if pkg.py and not dry_run:
        man = pkg.py.manifest
        man['project']['version'] = str(new_version)
        with open(pkg.py.path, 'w') as man_file:
            dump(man, man_file)

        git.add(pkg.py.path, _cwd=pkg.root_dir, verbose=True, _fg=True)

    return pkg, new_version, changes


if __name__ == '__main__':
    publish()
