from enum import Enum

from msgspec import Struct


class TriggerMode(Enum):
    auto = 'TRIGGER_MODE_AUTO'
    manual = 'TRIGGER_MODE_MANUAL'


class Link(Struct, omit_defaults=True):
    url: str
    name: str | None = None


class LocalResource(Struct, omit_defaults=True):
    name: str
    cmd: list[str] = []
    serve_cmd: list[str] = []
    dir: str = None
    serve_dir: str = None

    deps: list[str] = []
    trigger_mode: TriggerMode | None = None
    resource_deps: list[str] = []
    ignore:  list[str] = []
    auto_init: bool = True
    allow_parallel: bool = False
    links: list[str | Link] = []
    labels: list[str] = []
    env: dict[str, str] = {}

    def __post_init__(self):
        if 'OMNIBLACK_MODE' not in self.env:
            self.env['OMNIBLACK_MODE'] = 'headless'
