import sys

from contextlib import suppress
from importlib.resources import path as resource_path
from logging import getLogger
from operator import attrgetter
from os import getcwd
from os.path import join
from shlex import quote

from msgspec.json import Encoder as JsonEncoder
from msgspec.yaml import encode as yaml_encode
from packaging.requirements import Requirement
from requests import RequestException
from rich import print as fancy_print
from rpm import expandMacro

from omniblack.app import config
from omniblack.caddy import (
    Caddy,
    FileServerHandler,
    HeadersHandler,
    MatcherList,
    PathMatcher,
    RewriteHandler,
    Route,
    StaticResponseHandler,
    VarsHandler,
    add_standard_headers,
)
from omniblack.repo import (
    build_tree,
    convert_path,
    find_packages,
    find_root,
)

from .app import app
from .dev import DevPackage

PORT = 3000

log = getLogger(__name__)


packages = []


caddy_instance = None


def create_caddy_config(packages):
    root = find_root()

    vars_handler = VarsHandler()
    vars_handler.vars['path_prefix'] = '/docs'
    vars_handler.vars['root'] = join(root, 'docs_build')

    docs_route = Route(
        match=MatcherList(PathMatcher('/docs/*', '/docs')),
        handle=[vars_handler],
        group='static_files',
    )

    pkg_configs = [
        config
        for pkg in packages
        for config in pkg.configs
    ]

    pkg_routes = [
        route
        for config in pkg_configs
        for route in config.routes()
    ] + [docs_route]

    rewriter = Route(
        match=MatcherList(),
        handle=[RewriteHandler(strip_path_prefix='{http.vars.path_prefix}')],
    )

    file_server = Route(
        match=MatcherList(),
        handle=[FileServerHandler(
            pass_thru=True,
            canonical_uris=False,
        )],
    )

    spa_rewrite = Route(
        match=MatcherList(),
        handle=[RewriteHandler(uri='/index.html')],
    )
    headers = HeadersHandler()
    add_standard_headers(headers)
    headers.response.add.add('Cache-Control', 'no-store, max-age=0')

    header_route = Route(
        match=MatcherList(),
        handle=[headers],
    )

    not_found_handler = StaticResponseHandler(
        handler='static_response',
        close=True,
        status_code=404,
        body='Page not found',
    )

    not_found = Route(
        match=MatcherList(),
        handle=[not_found_handler],
    )

    routes = pkg_routes + [
        header_route,
        rewriter,
        file_server,
        spa_rewrite,
        file_server,
        not_found,
    ]

    routes_json = (
        route.model_dump()
        for route in routes
    )

    routes_json = [
        route
        for route in routes_json
        if route is not None
    ]

    return routes_json


@app.command
def print_caddy():
    """
    Print the caddy configuration for this repository.

    Primarily intended for debugging vulcan.
    """

    found_packages = find_packages(Package=DevPackage, sort=True)

    routes = create_caddy_config(found_packages)

    fancy_print(routes)


@app.command
def configure_caddy():
    """Configure a caddy server to reverse proxy for dev server."""
    with resource_path(__package__, 'Caddyfile.json') as caddy_file_path:
        SRC_PATH = find_root(getcwd())

        found_packages = find_packages(Package=DevPackage, sort=True)

        routes = create_caddy_config(found_packages)

        caddy = Caddy(
            cwd=SRC_PATH,
            caddy_file=caddy_file_path,
        )

        with caddy, suppress(RequestException):
            caddy.patch('/id/server/routes', json=routes)

            resp = caddy.get('/id/server')
            data = resp.json()

            print('Caddy configured')
            fancy_print(data)


@app.command
def k8s_yaml(*, pretty: bool = False):
    """Print the k8s yaml to for this repository."""
    found_packages = sorted(
        find_packages(Package=DevPackage),
        key=attrgetter('path'),
    )

    config.buffer = bytearray(1024)
    config.encoder = JsonEncoder()

    for package in found_packages:
        for pkg in package.configs:
            kube_objects = pkg.create_kube_objects()
            for kube_object in kube_objects:
                write_out(kube_object, pretty=pretty)


@app.command
def list_images(*, pretty: bool = False):
    """List all the docker images for this repository."""
    found_packages = find_packages(Package=DevPackage, lazy=True)
    image_names = [
        (image, pkg.name)
        for pkg in found_packages
        for config in pkg.configs
        for image in config.image_names()
    ]

    config.buffer = bytearray(1024)
    config.encoder = JsonEncoder()

    write_out(image_names, pretty=pretty)


@app.command
def list_resources(*, pretty: bool = False):
    """List all tilt resources for this repository."""
    found_packages = find_packages(Package=DevPackage, lazy=True)

    resource_names = [
        resource
        for pkg in found_packages
        for config in pkg.configs
        for resource in config.resources()
    ]

    config.buffer = bytearray(1024)
    config.encoder = JsonEncoder()

    write_out(resource_names, pretty=pretty)


@app.command
def list_cmd():
    """List all packages in this repository."""
    for package in find_packages(Package=DevPackage, lazy=True):
        fancy_print(package.config)


list_cmd.__name__ = 'list'


@app.command
def files(*, pretty: bool = False):
    """List all depended upon by this package"""
    repo_root = find_root()
    fs = build_tree(repo_root)
    pkgs = tuple(find_packages(
        Package=DevPackage,
        lazy=True,
        root=repo_root,
        fs=fs,
    ))

    all_pkg_files = {}
    for pkg in pkgs:
        pkg.files(
            pkgs=pkgs,
            all_pkg_files=all_pkg_files,
            fs=fs,
        )

    all_pkg_files['js'] = [
        convert_path(repo_root, entry.path)
        for entry in fs.glob('**/*.js')
        if not entry.info.is_dir
    ]

    all_pkg_files['py'] = [
        convert_path(repo_root, entry.path)
        for entry in fs.glob('**/*.py')
        if not entry.info.is_dir
    ]

    config.buffer = bytearray(1024)
    config.encoder = JsonEncoder()

    write_out(all_pkg_files, pretty=pretty)


@app.command
def manifests():
    pkgs = find_packages(
        Package=DevPackage,
        lazy=True,
    )

    for pkg in pkgs:
        for lang in pkg.languages.values():
            print('- ' + repr(lang.path))


@app.command
def deps(*, pretty: bool = False):
    pkgs = find_packages(Package=DevPackage)

    workspace_names = {
        pkg.py.canonical_name
        for pkg in pkgs
        if pkg.py
    }

    requires = (
        Requirement(dep)
        for pkg in pkgs
        if pkg.py
        for dep in pkg.py.manifest['project'].get('dependencies', ())
        if dep not in workspace_names
    )

    dep_names = (
        dep.name
        for dep in requires
    )

    dnf_deps = sorted({
        quote(expandMacro('%{py3_dist ' + dep + '}').strip())
        for dep in dep_names
    })

    config.buffer = bytearray(1024)
    config.encoder = JsonEncoder()
    write_out(dnf_deps, pretty=pretty)


@app.command
def local_resources(*, pretty: bool = False):
    found_packages = find_packages(Package=DevPackage, lazy=True)

    config.buffer = bytearray(1024)
    config.encoder = JsonEncoder()
    resources = (
        resource
        for pkg in found_packages
        for config in pkg.configs
        for resource in config.create_local_resources()
    )

    for resource in resources:
        write_out(resource, pretty=pretty)


def write_out(obj, pretty):
    if not pretty:
        config.encoder.encode_into(obj, config.buffer, 5)
        config.buffer[:5] = b'\n---\n'
        sys.stdout.buffer.write(config.buffer)
    else:
        out = yaml_encode(obj)
        sys.stdout.buffer.write(b'\n---\n')
        sys.stdout.buffer.write(out)
