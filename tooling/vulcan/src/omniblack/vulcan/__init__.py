from omniblack.utils import improve_module

from .app import app

__all__ = ['app']

improve_module(__name__)
