/* eslint-env node */
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import replace from '@rollup/plugin-replace';
import json from '@rollup/plugin-json';
import yaml from '@rollup/plugin-yaml';
import toml from '@fbraem/rollup-plugin-toml';

import { configProvider } from '#src/appProvider.js';
import {
    getEnvs,
    getConditions,
} from '#src/configs/utils.js';
import { manifest } from '#src/manifest.js';

// eslint-disable-next-line n/no-process-env
const ENV = JSON.stringify(process.env.NODE_ENV);

export async function createBaseConfig(app, options) {
    const conditions = [...getConditions(app, options.dev)];
    const envs = getEnvs(app);
    const isNodeOnly = envs.has('node') && !envs.has('browser');
    const output_dir = String(
        app.resolvePath(app.config.output_dir ?? 'build'),
    );

    return {
        output: {
            format: 'es',
            dir: output_dir,
            sourcemap: app.config.sourcemap ?? true,
            sourcemapExcludeSources: app.config.sourcemapExcludeSources
                ?? false,
            generatedCode: 'es2015',
            assetFileNames: 'assets/[name]-[hash][extname]',
            chunkFileNames: '[name]-[hash].js',
            entryFileNames: '[name]-[hash].js',
        },
        beforePlugins: [
            replace({
                preventAssignment: true,
                values: {

                    /*
                     * process env is required by at lot of npm modules
                     * We should offer other env options eventually
                     */
                    'process.env.NODE_ENV': ENV,
                },
            }),
        ],
        plugins: [
            json({ namedExports: false, preferConst: true }),
            yaml(),
            toml(),
            configProvider(app),
            resolve({
                browser: envs.has('browser'),
                preferBuiltins: isNodeOnly,
                exportConditions: conditions,
            }),
            commonjs(),
            manifest(app),
        ],
        watch: {
            clearScreen: false,
        },
    };
}
