import { Path } from '@omniblack/pathlib';
import { style } from '#src/styles.js';

export async function createLibConfig(app, options) {
    const config = {
        input: await getLibInputs(app),
        preserveEntrySignatures: 'allow-extension',
        plugins: [style({ rootDir: app.src_dir })],
    };

    return config;
}

export async function getLibInputs(app) {
    return Object.fromEntries(app.config.exports.map((_export) => {
        const exportPath = new Path(_export);
        const fullPath = String(app.srcResolve(exportPath));
        const exportName = String(exportPath.withExt(''));

        return [exportName, fullPath];
    }));
}
