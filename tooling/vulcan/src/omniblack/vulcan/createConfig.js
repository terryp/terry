import { merge } from '#src/mergeRollup.js';

import { createBaseConfig } from './configs/base.js';
import { createDevConfig as createDevelopmentConfig } from './configs/dev.js';
import { createLibConfig as createLibraryConfig } from './configs/lib.js';

const possibleConfigs = ['mjs', 'js', 'cjs']
    .map((extension) => `rollup.config.${extension}`);

const notFoundErrors = new Set([
    'ERR_MODULE_NOT_FOUND',
    'UNRESOLVED_ENTRY',
    'MODULE_NOT_FOUND',
]);

export async function createConfig(app, options) {
    const config = await createBaseConfig(app, options);

    if (app.config.type === 'lib') {
        merge(config, await createLibraryConfig(app, options));
    }

    if (options.dev) {
        merge(config, await createDevelopmentConfig(app, options));
    }


    for (const configPath of possibleConfigs) {
        try {
            const fullPath = app.path.join(configPath);

            const { default: userConfig } = await import(String(fullPath));

            merge(config, userConfig);
        } catch (err) {
            if (!notFoundErrors.has(err.code)) {
                throw err;
            }
        }
    }

    const { beforePlugins = [] } = config;
    config.plugins = [
        ...beforePlugins,
        ...config.plugins,
    ];

    delete config.beforePlugins;

    return config;
}
