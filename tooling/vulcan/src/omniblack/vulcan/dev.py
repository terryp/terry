import contextlib

from dataclasses import dataclass
from functools import cached_property
from logging import getLogger
from os import environ, getgid, getuid
from os.path import (
    abspath,
    dirname,
    join,
    relpath as relative_path,
)
from typing import ClassVar

from omniblack.app import LibKind, PkgKind
from omniblack.caddy import (
    CaddyHeader,
    HeaderDict,
    MatcherList,
    PathMatcher,
    ReverseProxyHandler,
    ReverseProxyHeaders,
    Rewrite,
    Route,
    Upstream,
    VarsHandler,
    add_standard_headers,
)
from omniblack.repo import Package, convert_path, get_unscoped_name
from omniblack.string_case import Cases

from .kube import (
    Container,
    ContainerPort,
    ContainerSecurityContext,
    DeploymentSpec,
    EnvVar,
    HostPath,
    JobSpec,
    KubeMetadata,
    KubeObject,
    KubeObjectKind,
    KubeSelector,
    PathTypes,
    PodSecurityContext,
    PodSpec,
    PodTemplate,
    ReplacementStrategy,
    ReplacementStrategyTypes,
    RestartPolicy,
    ServicePort,
    ServiceSpec,
    TemplateMetadata,
    Volume,
    VolumeMount,
)
from .tilt import LocalResource

log = getLogger(__name__)

PORT = int(environ.get('PORT', 3000))

next_range_start = PORT + 10


def next_range():
    global next_range_start

    range_start = next_range_start
    next_range_start += 10

    return range(range_start, next_range_start)


class DevPackage(Package):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.configs = tuple(self._create_dev_configs())

    def _create_dev_configs(self):
        for lang in self.languages:
            if lang in Config.lang_configs:
                yield Config(self, lang)

    def files(self, pkgs, all_pkg_files, fs):
        if self.name in all_pkg_files:
            if all_pkg_files[self.name] == 'pending':
                msg = 'Circular Dep!'
                raise RuntimeError(msg)
            else:
                return all_pkg_files[self.name]

        all_pkg_files[self.name] = 'pending'
        our_files = tuple(
            file
            for config in self.configs
            for file in config.files(pkgs, all_pkg_files, fs)
        )
        all_pkg_files[self.name] = our_files
        return our_files


@dataclass
class Config:
    lang_configs: ClassVar = {}
    pkg: DevPackage
    lang: str

    def __new__(cls, _pkg, lang):
        cls = cls.lang_configs[lang]
        return super().__new__(cls)

    def __post_init__(self, *args, **kwargs):
        if hasattr(self, 'assign_ports'):
            self.port_range = iter(next_range())
            self.assign_ports()

    def __init_subclass__(cls, lang):
        Config.lang_configs[lang] = cls

    @property
    def base_image_name(self):
        rel_path = relative_path(self.pkg.path, self.pkg.root_dir)
        return rel_path.replace('/', '_').replace('.', '_').lower()

    def image_name(self, task=None):
        image_name = self.base_image_name

        if task is not None:
            image_name += '_' + task

        return image_name

    def resource_name(self, task=None):
        resource_name = self.base_image_name

        if task is not None:
            resource_name += '-' + task

        resource_name = resource_name.replace('_', '-')
        return resource_name

    def create_volume_mounts(self):
        return [
            VolumeMount(
                mountPath=self.pkg.root_dir,
                name='source-code',
                readOnly=False,
            ),
            VolumeMount(
                mountPath='/etc/localtime',
                name='localtime',
                readOnly=True,
            ),
        ]

    def create_volumes(self):
        return [
            Volume(
                name='source-code',
                hostPath=HostPath(
                    path=self.pkg.root_dir,
                    type=PathTypes.directory
                ),
            ),
            Volume(
                name='localtime',
                hostPath=HostPath(
                    path='/etc/localtime',
                    type=PathTypes.file,
                ),
            ),
        ]

    def to_k8s_env(self, *envs):
        final_env = {}
        for env in envs:
            final_env |= env

        final_env.setdefault('OMNIBLACK_MODE', 'docker')

        return [
            EnvVar(name=name, value=str(value))
            for name, value in final_env.items()
        ]

    def to_tilt_env(self, *envs):
        final_env = {}
        for env in envs:
            final_env |= env

        final_env.setdefault('OMNIBLACK_MODE', 'headless')

        return {
            key: str(value)
            for key, value in final_env.items()
        }

    def pod_spec(self, restart_policy, containers):
        return PodSpec(
            restartPolicy=restart_policy,
            backoffLimit=0,
            workingDir=self.pkg.path,
            volumes=self.create_volumes(),
            containers=containers,
            securityContext=PodSecurityContext(
                runAsUser=getuid(),
                runAsGroup=getgid(),
            ),
        )

    def create_kube_object(self, task, kube_kind, sub_spec):
        match kube_kind:
            case KubeObjectKind.job:
                api_version = 'batch/v1'
            case KubeObjectKind.deployment:
                api_version = 'apps/v1'
            case KubeObjectKind.service:
                api_version = 'v1'

        return KubeObject(
            apiVersion=api_version,
            kind=kube_kind,
            metadata=KubeMetadata(
                name=self.resource_name(task),
            ),
            spec=sub_spec,
        )

    def container(self, task, env, command, port=None):
        ports = []

        if port is not None:
            ports.append(ContainerPort(containerPort=port))

        return Container(
            image=self.image_name(task),
            name=self.resource_name(task),
            command=command,
            env=env,
            workingDir=self.pkg.path,
            volumeMounts=self.create_volume_mounts(),
            securityContext=ContainerSecurityContext(
                allowPrivilegeEscalation=False,
            ),
            ports=ports,
        )

    def job(self, name, env, command):
        task = f'{self.lang}_{name}'
        cont = self.container(task, env, command)
        pod_spec = self.pod_spec(RestartPolicy.never, [cont])
        metadata = TemplateMetadata(labels={'app': self.resource_name(task)})

        pod_template = PodTemplate(
            spec=pod_spec,
            metadata=metadata,
        )

        job = JobSpec(backoffLimit=0, template=pod_template)

        return self.create_kube_object(task, KubeObjectKind.job, job)

    def local_job(self, name, env, command):
        task = f'{self.lang}_{name}'
        return LocalResource(
            name=self.resource_name(task),
            env=env,
            cmd=command,
            labels=[self.pkg.name.title()],
            dir=str(self.pkg.path)
        )

    def deployment(self, name, env, command, port=None):
        task = f'{self.lang}_{name}'

        cont = self.container(task, env, command, port=port)
        pod_spec = self.pod_spec(RestartPolicy.always, [cont])
        metadata = TemplateMetadata(labels={'app': self.resource_name(task)})

        pod_template = PodTemplate(
            spec=pod_spec,
            metadata=metadata,
        )

        deployment = DeploymentSpec(
            replicas=1,
            template=pod_template,
            strategy=ReplacementStrategy(
                type=ReplacementStrategyTypes.recreate,
            ),
            selector=KubeSelector(
                matchLabels={'app': self.resource_name(task)},
            ),
        )

        return self.create_kube_object(
            task,
            KubeObjectKind.deployment,
            deployment,
        )

    def service(self, name, ports):
        task = f'{self.lang}_{name}'

        ports = [
            ServicePort(name=name, port=port)
            for name, port in ports.items()
        ]

        service = ServiceSpec(
            selector={'app': self.resource_name(task)},
            ports=ports,
        )

        return self.create_kube_object(
            task,
            KubeObjectKind.service,
            service,
        )

    @cached_property
    def public_path(self):
        if 'public_path' not in self.pkg.config:
            return None

        public_path = self.pkg.config.public_path

        if not public_path.startswith('/'):
            return '/' + public_path

        return public_path

    # method stub for languages where we don't have file retrieval
    def files(self, pkgs, all_pkg_files, fs):  # noqa ARG002
        return ()


class JsConfig(Config, lang='js'):
    def assign_ports(self):
        self.reload_port = next(self.port_range)
        self.debug_port = next(self.port_range)

    def is_spa(self):
        is_comp_lib = False

        if self.pkg.config.type == PkgKind.lib:
            is_comp_lib = LibKind.svelte is self.pkg.config.lib_type

        return self.pkg.config.type == PkgKind.spa or is_comp_lib

    @property
    def pnpm_store(self):
        SRC = self.pkg.root_dir
        return abspath(join(SRC, '../.pnpm-store'))

    def create_volume_mounts(self):
        mounts = super().create_volume_mounts()
        mounts.append(VolumeMount(
            mountPath=self.pnpm_store,
            name='pnpm-store',
            readOnly=False,
        ))

        return mounts

    def create_volumes(self):
        volumes = super().create_volumes()
        volumes.append(Volume(
            name='pnpm-store',
            hostPath=HostPath(
                path=self.pnpm_store,
                type=PathTypes.directory,
            ),
        ))

        return volumes

    def js_env(self):
        return self.pkg.proc_env | {
            'CONFIG_PATH': self.pkg.config_path,
            'DEBUG_PORT': str(self.debug_port),
            'FORCE_COLOR': '3',
            'JS_PATH': self.pkg.js.path,
            'NODE_OPTIONS': '--experimental-import-meta-resolve',
            'PKG_PATH': self.pkg.path,
            'RELOAD_PORT': str(self.reload_port),
        }

    def image_names(self):
        return []

    def create_kube_objects(self):
        return []

    def resources(self):
        return []

    def create_local_resources(self):
        objects = []

        pkg_json = self.pkg.js.manifest
        scripts = pkg_json.get('scripts', {})

        if 'test' in scripts:
            objects.append(self.local_job(
                'test',
                self.to_tilt_env(self.js_env()),
                ['pnpm', 'run', 'test'],
            ))

        if self.is_spa():
            objects.append(self.local_job(
                'spa',
                self.to_tilt_env(self.js_env()),
                [
                    'node',
                    '--unhandled-rejections=strict',
                    join(dirname(__file__), 'dev.js'),
                ],
            ))

        return objects

    def routes(self):
        routes = [
            self.spa_route(),
        ]

        return [
            route
            for route in routes
            if route is not None
        ]

    def spa_route(self):
        if not self.is_spa():
            return None

        vars_handler = VarsHandler()
        vars_handler.vars['path_prefix'] = self.public_path
        vars_handler.vars['root'] = self.pkg.resolve_path(
            self.pkg.config.output_dir,
        )

        matcher = PathMatcher(f'{self.public_path}/*', self.public_path)
        match = MatcherList(matcher)

        return Route(
            handle=[vars_handler],
            match=match,
            group='static_files',

        )

    def files(self, pkgs, all_pkg_files, fs):
        our_files = [
            convert_path(self.pkg.root_dir, entry.path)
            for entry in fs.glob('**/*', self.pkg.rel_path)
            if not entry.info.is_dir
            if not entry.path.endswith('.py')
            if not entry.path.endswith('.jinja')
        ]

        pkg_deps = set()
        for dep in self.pkg.js.dependencies:
            with contextlib.suppress(TypeError):
                pkg_deps.add(get_unscoped_name(dep.name))

        for other_pkg in pkgs:
            if other_pkg.name in pkg_deps:
                other_files = other_pkg.files(
                    pkgs,
                    all_pkg_files=all_pkg_files,
                    fs=fs,
                )

                our_files.extend(other_files)

        return tuple(our_files)


class PyConfig(Config, lang='py'):
    @property
    def dev_proc_env(self):
        var_prefix = Cases.CapSnake.to(self.pkg.name)
        SRC = self.pkg.root_dir

        return {
            f'{var_prefix}_MONGO_URL': f'mongodb://mongo:3270/{self.pkg.name}',
            f'{var_prefix}_MONGO_USERNAME': f'{self.pkg.name}_user',
            f'{var_prefix}_MONGO_PASSWORD': self.pkg.name,
            f'{var_prefix}_DEPLOYMENT_PATH': join(SRC, 'dev-deployment.yaml'),
        } | {
            key: value
            for key, value in environ.items()
            if key.startswith(var_prefix)
        }

    def assign_ports(self):
        self.server_port = 8000

    def resources(self):
        resources = []

        if self.pkg.config.type == PkgKind.server:
            resources.append(
                {
                    'name': self.resource_name('py_server'),
                    'labels': [self.pkg.name.title()],
                },
            )

        return resources

    def image_names(self):
        image_names = []

        if self.pkg.config.type == PkgKind.server:
            image_names.append(self.image_name('py_server'))

        return image_names

    def create_kube_objects(self):
        objects = []

        if self.pkg.config.type == PkgKind.server:
            mod_name = self.pkg.py.manifest['project']['name']

            objects.append(self.deployment(
                'server',
                self.to_k8s_env(self.pkg.proc_env, self.dev_proc_env),
                [
                    'gunicorn',
                    '--config=python:omniblack.server.gunicorn_conf',
                    '--bind', f'0.0.0.0:{self.server_port}',
                    f'{mod_name}:app',
                ],
                self.server_port,
            ))

            objects.append(
                self.service('server', {'primary': self.server_port}),
            )

        return objects

    def create_local_resources(self):
        objects = []
        return objects

    def server_route(self):
        if self.pkg.config.type != PkgKind.server or not self.public_path:
            return None

        resource_name = self.resource_name('py-server')
        upstream = f'{resource_name}:{self.server_port}'
        proxy = ReverseProxyHandler(
            headers=ReverseProxyHeaders(
                request=CaddyHeader(
                    set=HeaderDict({'X-Forwarded-Prefix': [self.public_path]}),
                ),
            ),
            upstreams=[
                Upstream(dial=upstream),
            ],
            rewrite=Rewrite(strip_path_prefix=self.public_path),
        )

        add_standard_headers(proxy.headers)

        matcher = PathMatcher(f'{self.public_path}/*', self.public_path)

        return Route(
            match=MatcherList(matcher),
            handle=[
                proxy,
            ],
        )

    def routes(self):
        routes = [
            self.server_route()
        ]

        return [
            route
            for route in routes
            if route is not None
        ]

    def files(self, pkgs, all_pkg_files, fs):
        our_files = [
            convert_path(self.pkg.root_dir, entry.path)
            for entry in fs.glob('**/*', self.pkg.rel_path)
            if not entry.info.is_dir
            if not entry.path.endswith('.js')
            if not entry.path.endswith('.css')
            if not entry.path.endswith('.jinja')
        ]

        pkg_deps = set()
        for dep in self.pkg.py.dependencies:
            with contextlib.suppress(TypeError):
                pkg_deps.add(get_unscoped_name(dep.name))

        for other_pkg in pkgs:
            if other_pkg.name in pkg_deps:
                other_files = other_pkg.files(
                    pkgs,
                    all_pkg_files=all_pkg_files,
                    fs=fs,
                )

                our_files.extend(other_files)

        return tuple(our_files)
