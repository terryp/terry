import { basename } from 'node:path';
import postcss from 'postcss';
import atImport from 'postcss-import';
import { createFilter } from '@rollup/pluginutils';

async function postcssProcess(plugins, code, filePath) {
    const processor = postcss(plugins);
    const result = await processor.process(code, { from: filePath });

    return result.css;
}

export function style(options = {}) {
    const {
        plugins = [],
    } = options;
    const filter = createFilter(
        options.include ?? ['**/*.css'],
        options.exclude,
    );

    function handle(id) {
        return filter(id);
    }

    return {
        name: '@omniblack/style',
        async transform(code, id) {
            if (!handle(id)) {
                return null;
            }

            const boundResolve = this.resolve.bind(this);


            const allPlugins = [
                atImport({
                    async resolve(cssId) {
                        const resolved = await boundResolve(cssId, id);
                        return resolved.id;
                    },
                }),
                ...plugins,
            ];

            this.emitFile({
                type: 'asset',
                name: basename(id),
                originalFileName: id,
                source: await postcssProcess(allPlugins, code, id),
            });

            return { code: '', moduleSideEffects: false };
        },
    };
}

