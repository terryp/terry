# ruff: noqa: N815
# This names map to external structures.
from msgspec import Struct

from omniblack.utils import Enum


class EnvVar(Struct, omit_defaults=True):
    name: str
    value: str


class VolumeMount(Struct, omit_defaults=True):
    name: str
    mountPath: str
    readOnly: bool


class PortProtocol(Enum):
    TCP = 'TCP'
    UDP = 'UDP'
    SCTP = 'SCTP'


class ContainerPort(Struct, omit_defaults=True):
    containerPort: int
    hostIP: str | None = None
    hostPort: int | None = None
    name: str | None = None
    protocol: PortProtocol = PortProtocol.TCP


class PathTypes(Enum):
    directory_or_create = 'DirectoryOrCreate'
    directory = 'Directory'
    file_or_create = 'FileOrCreate'
    file = 'File'
    socket = 'Socket'
    char_device = 'CharDevice'
    block_device = 'BlockDevice'


class HostPath(Struct, omit_defaults=True):
    path: str
    type: PathTypes


class Volume(Struct, omit_defaults=True):
    name: str
    hostPath: HostPath


class ContainerSecurityContext(Struct, omit_defaults=True):
    allowPrivilegeEscalation: bool


class Container(Struct, omit_defaults=True):
    name: str
    image: str
    command: list[str] = None
    env: list[EnvVar] = None
    securityContext: ContainerSecurityContext = None
    volumeMounts: list[VolumeMount] = None
    workingDir: str = None
    ports: list[ContainerPort] = []


class PodSecurityContext(Struct, omit_defaults=True):
    runAsUser: int
    runAsGroup: int


class RestartPolicy(Enum):
    always = 'Always'
    on_failure = 'OnFailure'
    never = 'Never'


class PodSpec(Struct, omit_defaults=True):
    securityContext: PodSecurityContext
    restartPolicy: RestartPolicy
    backoffLimit: int
    workingDir: str
    volumes: list[Volume] = []
    containers: list[Container] = []


class TemplateMetadata(Struct, omit_defaults=True):
    labels: dict[str, str] = {}


class PodTemplate(Struct, omit_defaults=True):
    metadata: TemplateMetadata
    spec: PodSpec


class KubeMetadata(Struct, omit_defaults=True):
    name: str


class KubeObjectKind(Enum):
    deployment = 'Deployment'
    job = 'Job'
    service = 'Service'


class KubeSelector(Struct, omit_defaults=True):
    matchLabels: dict[str, str] = {}


class ReplacementStrategyTypes(Enum):
    recreate = 'Recreate'
    rolling_update = 'RollingUpdate'


class ReplacementStrategy(Struct, omit_defaults=True):
    type: ReplacementStrategyTypes


class DeploymentSpec(Struct, omit_defaults=True):
    replicas: int
    selector: KubeSelector
    strategy: ReplacementStrategy
    template: PodTemplate


class JobSpec(Struct, omit_defaults=True):
    backoffLimit: int
    template: PodTemplate


class ServicePort(Struct, omit_defaults=True):
    port: int
    name: str = None
    targetPort: int = None
    protocol: PortProtocol = None
    nodePort: int = None
    appProtocol: str = None


class ServiceType(Enum):
    cluster_ip = 'ClusterIp'
    node_port = 'NodePort'
    load_balancer = 'LoadBalancer'


class ServiceSpec(Struct, omit_defaults=True):
    selector: dict[str, str]
    ports: list[ServicePort] = []
    type: ServiceType = None


class KubeObject(Struct, omit_defaults=True):
    kind: KubeObjectKind
    apiVersion: str
    metadata: KubeMetadata
    spec: DeploymentSpec | JobSpec | ServiceSpec
