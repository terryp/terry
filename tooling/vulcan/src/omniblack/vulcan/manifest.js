import { Path } from '@omniblack/pathlib';

function *_prepareAssets(assets, app) {
    for (const [realPrefix, outPrefix] of Object.entries(assets)) {
        yield [app.resolvePath(realPrefix), new Path(outPrefix)];
    }
}

function prepareAssets(assets, app) {
    return new Map(_prepareAssets(assets, app));
}

export function manifest(app) {
    return {
        name: '@omniblack/vulcan/manifest',
        generateBundle(options, bundle) {
            const manifest = {};
            const assets = prepareAssets(app.config.assets, app);

            for (const [outPath, outInfo] of Object.entries(bundle)) {
                if (outInfo.type === 'asset') {
                    processAsset(outPath, outInfo, manifest, app, assets);
                } else {
                    processChunk(outPath, outInfo, app, manifest);
                }
            }

            this.emitFile({
                type: 'asset',
                fileName: 'manifest.json',
                source: JSON.stringify(manifest, null, 4),
            });
        },
    };
}

function processAsset(outPath, assetInfo, manifest, app, assets) {
    if (assetInfo.originalFileName === null) {
        // Files like source maps don't have an original file
        return;
    }

    const fullSourcePath = new Path(assetInfo.originalFileName);

    for (const path of [fullSourcePath, ...fullSourcePath.parents]) {
        if (assets.has(path)) {
            const assetPrefix = assets.get(path);
            const relPath = fullSourcePath.relativeTo(path);
            const assetPath = assetPrefix.join(relPath);
            manifest[String(assetPath)] = outPath;
            return;
        }
    }

    const appRel = app.srcRelative(assetInfo.originalFileName);
    manifest[appRel] = outPath;
}

function processChunk(outPath, chunkInfo, app, manifest) {
    if (!chunkInfo.isEntry) {
        return;
    }

    const appRel = app.srcRelative(chunkInfo.facadeModuleId);
    manifest[appRel] = outPath;
}
