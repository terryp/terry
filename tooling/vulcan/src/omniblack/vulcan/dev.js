/* eslint-env node */
import { fileURLToPath } from 'node:url';
import { readFile, writeFile, mkdir } from 'node:fs/promises';
import { basename, join, dirname } from 'node:path';
import { availableParallelism } from 'node:os';

import { rollup, VERSION as ROLLUP_VERSION } from 'rollup';
import ms from 'pretty-ms';
import { bold, underline, green, blue } from 'colorette';
import signale from 'signale-logger';
import postcss from 'postcss';
import atImport from 'postcss-import';
import pLimit from 'p-limit';

import { App } from '#src/findApps.js';
import { createConfig } from '#src/createConfig.js';
import { log } from '#src/logger.js';

import { Path } from '@omniblack/pathlib';
import { collapseWhitespace } from '@omniblack/utils';

/* eslint-disable n/no-process-env */
const {
    CONFIG_PATH,
    JS_PATH,
    PKG_PATH,
    RELOAD_PORT,
} = process.env;
/* eslint-enable n/no-process-env */


async function processCss(
    cssFiles,
    resolve,
    emitFile,
    watchFile,
    rollupConfig,
) {
    const plugins = [
        atImport({
            async resolve(cssId, _, __, node) {
                const resolved = await resolve(
                    cssId,
                    node?.source?.input?.from ?? '',
                );
                if (!resolved) {
                    const err = new Error(`'${cssId}' file not found.`);
                    err.code = 'MODULE_NOT_FOUND';
                    throw err;
                }
                return resolved?.id;
            },
        }),
    ];

    const processor = postcss(plugins);
    const referencesFiles = [];

    for (const filePath of cssFiles) {
        const code = await readFile(filePath, { encoding: 'utf8' });

        watchFile(filePath);

        const cssOpts = {
            from: filePath,
            map: {
                annotation: true,
                sourceContent: !rollupConfig.sourcemapExcludeSources,
                inline: false,
            },
        };
        const result = await processor.process(code, cssOpts);

        emitFile({
            type: 'asset',
            name: basename(filePath),
            originalFileName: filePath,
            source: result.css,
            needCodeReference: false,
        });

        if (rollupConfig.sourcemap && result.map) {
            emitFile({
                type: 'asset',
                name: basename(filePath) + '.map',
                source: result.map.toString(),
            });
        }

        const fileReferences = result.messages
            .filter((msg) => msg.type === 'dependency')
            .map((msg) => msg.file);

        for (const file of fileReferences) {
            watchFile(file);
        }


        referencesFiles.push(...fileReferences);
    }

    return referencesFiles;
}

async function writeOutput(fullPath, content) {
    await mkdir(dirname(fullPath), { recursive: true });
    await writeFile(fullPath, content, { encoding: 'utf8' });
}

const limit = pLimit(availableParallelism());

function getCssFiles(input) {
    const array = Array.isArray(input)
        ? input
        : Object.values(input);

    return array.filter((file) => file.endsWith('.css'));
}

async function processAssets(app, emitFile, addWatchFile) {
    const assetPaths = Object.entries(app.config.assets ?? {});
    if (assetPaths.length === 0) {
        return [];
    }

    const allAssetsDeep = await Promise.all(
        assetPaths.map(async ([src, dest]) => await limit(
            _getAssetFile,
            app,
            src,
            dest,
        )),
    );

    const allAssets = allAssetsDeep.flat();

    for (const [directory, assetPath] of allAssets) {
        emitFile({
            type: 'asset',
            name: basename(assetPath),
            source: await readFile(assetPath),
            originalFileName: assetPath,
            needCodeReference: false,
        });
        addWatchFile(assetPath);
        addWatchFile(directory);
    }
}

async function *onlyFiles(paths) {
    for await (const path of paths) {
        if (await path.isFile()) {
            yield path;
        }
    }
}

async function _getAssetFile(app, src, dest) {
    const appPath = new Path(app.path);

    const assets = await Array.fromAsync(
        onlyFiles(appPath.glob(join(src, '**/*'))),
    );

    const srcPath = String(appPath.resolve(src));
    return assets
        .map((path) => [srcPath, String(path), dest]);
}

async function build(cache) {
    try {
        const startTime = process.hrtime.bigint();

        const app = await App.create({
            configPath: new Path(CONFIG_PATH),
            path: new Path(PKG_PATH),
            jsPath: new Path(JS_PATH),
        });

        const options = {
            reload_port: RELOAD_PORT,
            dev: true,
            log,
        };

        const config = await createConfig(app, options);
        config.cache = cache;

        config.onLog = (level, logObj) => {
            const logFunc = log[level];

            logFunc({
                message: logObj.message,
            });
            delete logObj.message;

            signale.log(logObj);
        };

        const cssFiles = getCssFiles(config.input);
        config.input = Array.isArray(config.input)
            ? config.input.filter((file) => !file.endsWith('.css'))
            : Object.fromEntries(
                Object.entries(config.input)
                    .filter(([_, file]) => !file.endsWith('.css')),
            );

        config.plugins.unshift({
            name: 'emitCss',
            async generateBundle(opts) {
                await processCss(
                    cssFiles,
                    this.resolve.bind(this),
                    this.emitFile.bind(this),
                    this.addWatchFile.bind(this),
                    opts,
                );
            },
        });

        config.plugins.unshift({
            name: 'emitAssets',
            async generateBundle() {
                await processAssets(
                    app,
                    this.emitFile.bind(this),
                    this.addWatchFile.bind(this),
                );
            },
        });

        const inputStr = [
            ...Object.values(config.input),
            ...cssFiles,
        ]
            .map((path) => app.relative(path))
            .join(', ');

        const outputStr = app.relative(config.output.dir);

        log.pending(
            collapseWhitespace(
                `${green('Bundling')} ${bold(inputStr)}
            ${green('->')} ${bold(outputStr)}`,
            ),
        );

        const { output: outputConfig } = config;
        const bundle = await rollup(config);
        const output = await bundle.generate(outputConfig);
        const watchedFiles = [
            ...bundle.watchFiles,
            CONFIG_PATH,
            JS_PATH,
        ];

        await bundle.close();

        const allOutFiles = output.output.flatMap((chunk) => {
            return chunk.type === 'asset'
                ? [[join(config.output.dir, chunk.fileName), chunk.source]]
                : [
                    [join(config.output.dir, chunk.fileName), chunk.code],
                    [
                        join(config.output.dir, chunk.sourcemapFileName),
                        chunk.map.toString(),
                    ],
                ];
        });

        await Promise.all(allOutFiles.map(async (fileArgs) => await limit(
            writeOutput,
            ...fileArgs,
        )));

        const endTime = process.hrtime.bigint();

        const duration = (endTime - startTime) / 1_000_000n;


        log.success(green('Bundle Built'));
        log.info(`Build took ${bold(ms(duration))}`);

        return [watchedFiles, bundle.cache];
    } catch (err) {
        reportError(err);
        return [err.watchFiles, null];
    }
}

function reportError(err) {
    log.error('Build error Occurred.');
    log.error(err);
    const props = Object.assign({}, err);
    delete props.stack;

    /*
     * Use the root logger so we don't get a timestamp or scope
     * this make the props look connected to the previous message
     */
    signale.log(props);
}


async function main() {
    log.start(underline(`Started rollup: ${blue(ROLLUP_VERSION)}`));

    const app = await App.create({
        configPath: new Path(CONFIG_PATH),
        path: new Path(PKG_PATH),
        jsPath: new Path(JS_PATH),
    });
    const cacheFile = String(app.path.join('.rollup_cache.json'));
    let prevCache;
    try {
        const prevCacheStr = await readFile(cacheFile);
        prevCache = JSON.parse(prevCacheStr);
    } catch (err) {
        if (err.code !== 'ENOENT') {
            reportError(err);
            throw err;
        }
    }
    const [_, cache] = await build(prevCache);
    const str = JSON.stringify(cache);
    await writeFile(cacheFile, str);
}

const __name__ = fileURLToPath(import.meta.url);
const __main__ = process.argv[1];

if (__name__ === __main__) {
    await main();
}
