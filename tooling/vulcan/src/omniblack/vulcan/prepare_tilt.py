from logging import getLogger
from operator import attrgetter
from os.path import join

from msgspec import to_builtins
from msgspec.json import Encoder as JsonEncoder
from watchdog.events import FileSystemEventHandler

from omniblack.repo import (
    build_tree,
    convert_path,
    find_packages,
    find_root,
    get_git_dir,
)

from .app import app
from .dev import DevPackage
from .tilt import LocalResource

log = getLogger(__name__)


@app.command
def generate_tiltfile():
    repo_root = find_root()
    fs = build_tree(repo_root)
    found_packages = sorted(
        find_packages(
            Package=DevPackage,
            lazy=True,
            root=repo_root,
            fs=fs,
        ),
        key=attrgetter('path'),
    )
    all_pkg_files = get_files(found_packages, fs, repo_root)

    tiltfile_path = join(repo_root, '.generated.tiltfile')
    k8s_yaml_prefix = 'k8s_yaml(blob('
    k8s_yaml_suffix = '))\n'
    encoding_buffer = bytearray(2048)
    encoder = JsonEncoder()

    manifests = [
        lang.path
        for pkg in found_packages
        for lang in pkg.languages.values()
    ]

    with open(tiltfile_path, 'w') as file:
        write_self(file, all_pkg_files)
        write_lints(file, all_pkg_files)

        for vulcan_file in all_pkg_files['vulcan']:
            file.write(f'watch_file({vulcan_file!r})\n')

        for manifest_path in manifests:
            file.write(f'watch_file({manifest_path!r})\n')

        for package in found_packages:
            for pkg_cfg in package.configs:
                kube_objects = pkg_cfg.create_kube_objects()
                for kube_object in kube_objects:
                    encoder.encode_into(
                        kube_object,
                        encoding_buffer,
                    )
                    file.write(k8s_yaml_prefix)
                    file.write(repr(encoding_buffer.decode('utf-8')))
                    file.write(k8s_yaml_suffix)

                for image in pkg_cfg.image_names():
                    deps = all_pkg_files[package.name]
                    file.write(
                        f"custom_build({image!r},command='_build_image',ignore=['**/build'],deps={deps!r})\n",  # noqa: E501
                    )

                for resource in pkg_cfg.resources():
                    labels = resource['labels']
                    name = resource['name']
                    file.write(
                        f'k8s_resource({name!r}, labels={labels!r})\n',
                    )

                for resource in pkg_cfg.create_local_resources():
                    write_local_resource(file, resource)

    log.debug('Tiltfile has been regenerated.')


class IndexChange(FileSystemEventHandler):
    def __init__(self, on_change, target_path):
        super().__init__()
        self.on_change = on_change
        self.target_path = target_path

    def on_moved(self, e):
        matching = (
            e.src_path == self.target_path
            or e.dest_path == self.target_path
        )
        if matching:
            self.on_change()


def write_local_resource(file, local_resource):
    data = to_builtins(local_resource)

    args_str = ','.join(
        f'{key}={value!r}'
        for key, value in data.items()
    )
    file.write(f'local_resource({args_str})\n')


def write_lints(file, all_pkg_files):
    py_lint = LocalResource(
        'ruff',
        cmd='ruff check --preview --no-cache',
        deps=all_pkg_files['py'],
        labels=['a-Lint'],
        allow_parallel=True,
    )
    write_local_resource(file, py_lint)

    js_lint = LocalResource(
        'eslint',
        cmd='eslint --max-warnings 0 --format mo',
        deps=all_pkg_files['js'],
        labels=['a-Lint'],
        allow_parallel=True,
    )

    write_local_resource(file, js_lint)


def write_self(file, all_pkg_files):
    self_resource = LocalResource(
        'Rebuild Tiltfile',
        cmd=['vulcan', 'generate-tiltfile'],
        deps=(
            join(get_git_dir(), 'index'),
            *all_pkg_files['vulcan'],
        ),
        labels=['Tiltfile'],
        allow_parallel=True,
        env={
            'OMNIBLACK_MODE': 'headless',
        },
    )

    write_local_resource(file, self_resource)


def get_files(pkgs, fs, repo_root):
    """List all depended upon by this package"""
    all_pkg_files = {}
    _build_image = join(repo_root, 'dev-tools/bin/_build_image')
    for pkg in pkgs:
        pkg.files(
            pkgs=pkgs,
            all_pkg_files=all_pkg_files,
            fs=fs,
        )

        all_pkg_files[pkg.name] = (
            *all_pkg_files[pkg.name],
            _build_image,
        )

    all_pkg_files['js'] = [
        convert_path(repo_root, entry.path)
        for entry in fs.glob('**/*.js')
        if not entry.info.is_dir
    ]

    all_pkg_files['js'].append(_build_image)

    all_pkg_files['py'] = [
        convert_path(repo_root, entry.path)
        for entry in fs.glob('**/*.py')
        if not entry.info.is_dir
    ]
    all_pkg_files['py'].append(_build_image)

    return all_pkg_files
