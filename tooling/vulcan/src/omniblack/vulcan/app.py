from omniblack.cli import CLI
from omniblack.vulcan.model import model

app = CLI('omniblack.vulcan', model=model, module='omniblack.vulcan')
