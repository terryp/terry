# Omniblack Vulcan

Omniblack Vulcan offers a simple, low configuration
system for the development and deployment
of applications and programming libraries.

During development Vulcan uses [Tilt](https://tilt.dev/)
to support:

* Process Reloading of applications
* Automtic Running of Tests
* Automtic Linting of Code


Vulcan also uses [Caddy](https://caddyserver.com/)
serve static files and offer a reverse proxy to services.
