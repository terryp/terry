# @omniblack/vulcan

## 0.0.1
### Patch Changes

- 3d34507: Update lodash and internal deps
- Updated dependencies [3d34507]
  - @omniblack/utils@0.0.1
  - @omniblack/rollup-plugin-vue-routes@0.0.2
