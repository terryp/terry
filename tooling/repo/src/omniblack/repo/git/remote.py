from public import public
from sh import ErrorReturnCode_1

from . import branch as branch_mod
from .wrapper import git


@public
def get_remote(branch=None):
    if branch is None:
        branch = branch_mod.current_branch()

    try:
        return str(git.config('--get', f'branch.{branch}.remote'))
    except ErrorReturnCode_1:
        return 'origin'


@public
def current_branch_remote():
    return get_remote()
