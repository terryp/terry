from public import public
from sh import Command as _Command, RunningCommand as _RunningCommand


class RunningCommand(_RunningCommand):
    def __str__(self):
        v = super().__str__()
        return v.strip()


class Command(_Command):
    RunningCommandCls = RunningCommand


git = Command('git')
public(git=git)

rev_parse = git.bake('rev-parse')
