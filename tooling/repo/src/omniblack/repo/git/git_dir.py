from public import public

from .wrapper import rev_parse


@public
def get_git_dir():
    return rev_parse(absolute_git_dir=True)
