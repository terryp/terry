from logging import getLogger
from os import EX_CONFIG, environ
from sys import exit

from fs.path import combine
from public import public

from . import remote
from .wrapper import git, rev_parse

log = getLogger()


def get_env():
    try:
        return environ['WRK_ENV']
    except KeyError:
        log.error('[red][bold]`WRK_ENV`[/bold] is not set.[/]')
        log.error('Are you in a wrk environment?')
        exit(EX_CONFIG)


@public
def current_branch():
    return str(rev_parse('--abbrev-ref', 'HEAD'))


@public
def main_branch(branch=None):
    if branch is None:
        branch = current_branch()

    current_remote = remote.get_remote(branch)

    remote_path = f'refs/remotes/{current_remote}/'
    sym_ref = git('symbolic-ref', combine(remote_path, 'HEAD'))
    return sym_ref.removeprefix(remote_path)
