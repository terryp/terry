from omniblack.cli import CLI
from omniblack.repo.model import model

app = CLI('omniblack.repo', model=model)
