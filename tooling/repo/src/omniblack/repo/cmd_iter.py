from itertools import chain, takewhile


def null_separated(stream):
    stream = chain.from_iterable(stream)

    while item := ''.join(takewhile(lambda c: c != '\0', stream)):
        yield item
