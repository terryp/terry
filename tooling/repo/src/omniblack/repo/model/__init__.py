from omniblack.model import Model

from .version import Version

model = Model(
    'Omniblack Repo',
    struct_packages=['omniblack.repo.model'],
    types=[Version],
)
