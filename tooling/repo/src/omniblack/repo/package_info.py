from collections.abc import Mapping
from itertools import chain
from logging import getLogger
from os.path import dirname
from typing import Protocol, runtime_checkable
from warnings import warn

from packaging.requirements import Requirement
from packaging.version import VERSION_PATTERN
from public import public
from regex import IGNORECASE, VERBOSE, compile
from rich.repr import auto

from .languages import Languages
from .metadata_warning import InvalidMetadata
from .names import canonicalize_name
from .semantic_version import parse_npm
from .version import Version

log = getLogger(__name__)


@public
@runtime_checkable
@auto
class PackageInfo(Protocol):
    name: str
    version: Version
    path: str
    canonical_name: str
    manifest: Mapping
    language: Languages
    private: bool = False

    @property
    def proc_env(self):
        return {
            f'{self.language.upper()}_NAME': self.name,
            f'{self.language.upper()}_MANIFEST': self.path,
        }

    @property
    def __id_keys(self):
        return (
            self.version,
            self.canonical_name,
            self.language,
            self.path,
        )

    def __eq__(self, other):
        if not isinstance(other, PackageInfo):
            return NotImplemented

        return self.__id_keys == other.__id_keys

    def __hash__(self):
        return hash(self.__id_keys)

    def __rich_repr__(self):
        yield self.name
        yield 'path', self.path
        yield 'version', self.version

        if self.canonical_name != self.name:
            yield 'canonical_name', self.canonical_name

        yield 'manifest', self.manifest


class JsPackageInfo(PackageInfo):
    language = Languages.js

    def __init__(self, pkg_json: Mapping, path: str):
        self.manifest = pkg_json
        self.path = path
        self.version = Version(pkg_json['version'])
        self.name = pkg_json['name']
        self.private = pkg_json.get('private', False)

        try:
            self.canonical_name = canonicalize_name(self.name, self.language)
        except ValueError as err:
            log.exception(err)
            self.canonical_name = self.name
            pkg_path = dirname(path)
            warn(
                f'Package at {pkg_path} does not have a '
                + 'valid name in package.json.',
                category=InvalidMetadata,
            )

    @property
    def dependencies(self):
        all_deps = (
            self.manifest.get('dependencies', {}),
            self.manifest.get('devDependencies', {}),
            self.manifest.get('peerDependencies', {}),
            self.manifest.get('bundleDependencies', {}),
            self.manifest.get('optionalDependencies', {}),
        )

        all_dep_items = chain.from_iterable(deps.items() for deps in all_deps)

        return [
            combine_req(key, value)
            for key, value in all_dep_items
        ]


SPECIFIER_RE = None


def get_specifier_re():
    global SPECIFIER_RE
    if SPECIFIER_RE is not None:
        return SPECIFIER_RE

    operators = r'(?P<operator>\^|>|<|=|>=|<=)'

    specifier_pattern = rf'{operators}?\s*(?P<version>{VERSION_PATTERN})'

    range_pattern = rf'(?P<range> {specifier_pattern} - {specifier_pattern})'

    full_pattern = rf'{range_pattern} | {specifier_pattern}'

    SPECIFIER_RE = compile(full_pattern, IGNORECASE | VERBOSE)

    return SPECIFIER_RE


def combine_req(name, specifier):
    name = name.removeprefix('@').replace('/', '_')
    specifier = specifier.removeprefix('workspace:')
    req = Requirement(name)
    req.specifier = parse_npm(specifier)
    return req


private_classifier = 'Private :: Do Not Upload'


class PyPackageInfo(PackageInfo):
    language = Languages.py

    def __init__(self, pyproject, path: str):
        self.manifest = pyproject
        project = pyproject['project']
        self.version = Version(project['version'])
        self.name = project['name']
        self.path = path
        self.private = private_classifier in project.get('classifiers', [])

        try:
            self.canonical_name = canonicalize_name(self.name, self.language)
        except ValueError as err:
            log.exception(err)
            self.canonical_name = self.name
            pkg_path = dirname(path)
            warn(
                f'Package at {pkg_path} does not have a '
                + 'valid name in pyproject.toml.',
                category=InvalidMetadata,
            )

    @property
    def dependencies(self):
        return [
            Requirement(dep_str)
            for dep_str in self.manifest['project'].get('dependencies', ())
        ]

    @property
    def is_extension(self):
        return (
            self.pkg.rs
            and 'pyo3' in self.pkg.rs.manifest.get('dependencies', {})
        )


class RsPackageInfo(PackageInfo):
    language = Languages.rs

    def __init__(self, cargo_toml, path):
        self.manifest = cargo_toml
        pkg = cargo_toml['package']

        self.version = Version(pkg['version'])
        self.name = pkg['name']
        self.path = path
        self.private = not pkg.get('publish', True)

        try:
            self.canonical_name = canonicalize_name(self.name, self.language)
        except ValueError:
            log.exception()
            self.canonical_name = self.name
            pkg_path = dirname(path)
            warn(
                f'Package at {pkg_path} does not have a '
                + 'valid nam in Cargo.toml',
                category=InvalidMetadata,
            )
