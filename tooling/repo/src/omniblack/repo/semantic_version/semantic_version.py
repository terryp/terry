from lark import Lark, Transformer
from msgspec import Struct
from packaging.version import Version


class SemanticVersion(Struct):
    major: str
    minor: str
    patch: str
    pre_release: str = None
    build: str = None

    def to_dict(self):
        return {f: getattr(self, f) for f in self.__struct_fields__}

    def to_py(self):
        s = f'{self.major}.{self.minor}.{self.patch}'

        # TODO validate against python's restrictions?
        if self.pre_release is not None:
            s += f'.{self.pre_release}'

        if self.build is not None:
            s += f'+{self.build}'

        return Version(s)


def get(lst, index, default=None):
    try:
        return lst[index]
    except IndexError:
        return default


class Build(str):
    __slots__ = ()


class Prerelease(str):
    __slots__ = ()


class SemVerTransformer(Transformer):
    POSITIVE_DIGIT = str
    ZERO = str

    def version(self, args):
        core = args[0]
        args_dict = core.to_dict()

        for arg in args[1:]:
            match arg:
                case Build() as build:
                    args_dict['build'] = build
                case Prerelease() as pre_release:
                    args_dict['pre_release'] = pre_release

        return SemanticVersion(**args_dict)

    def version_core(self, args):
        return SemanticVersion(
            major=get(args, 0),
            minor=get(args, 1),
            patch=get(args, 2),
        )

    def build(self, args):
        return Build(''.join(args))

    def pre_release(self, args):
        return Prerelease(''.join(args))


sem_ver_grammar = None


def get_sem_ver_grammar():
    global sem_ver_grammar
    if sem_ver_grammar is not None:
        return sem_ver_grammar

    sem_ver_grammar = Lark.open_from_package(
        __package__,
        'semantic_version.lark',
        start='version',
        parser='earley',
    )

    return sem_ver_grammar


def parse_sem_ver(string):
    grammar = get_sem_ver_grammar()
    tree = grammar.parse(string)
    transformer = SemVerTransformer()
    return transformer.transform(tree)
