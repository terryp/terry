from enum import Enum
from itertools import repeat

from lark import Lark, Transformer
from msgspec import Struct
from packaging.specifiers import Specifier, SpecifierSet
from packaging.version import Version


class NpmToPy:
    def to_py(self):
        return tuple(
            self.packaging_cls(thing)
            for thing in self.to_py_strings()
        )


class SpecifierVersion(Struct, NpmToPy):
    packaging_cls = Version

    major: str = None
    minor: str = None
    patch: str = None
    pre_release: str = None
    build: str = None

    def to_dict(self):
        return {f: getattr(self, f) for f in self.__struct_fields__}

    def __str__(self):
        if self.major is None:
            string = '*'
        elif self.minor is None:
            string = f'{self.major}.*'
        elif self.patch is None:
            string = f'{self.major}.{self.minor}.*'
        else:
            string = f'{self.major}.{self.minor}.{self.patch}'

        # TODO validate against python's restrictions?
        if self.pre_release is not None:
            string += f'.{self.pre_release}'

        if self.build is not None:
            string += f'+{self.build}'

        return string

    def to_py_strings(self):
        if str(self) == '*':
            yield
        else:
            yield '==' + str(self)


class Operator(Enum):
    LT = '<'
    LTE = '<='
    GT = '>'
    GTE = '>='
    EQ = '=='

    def __str__(self):
        return self.value


class Comparator(Struct, NpmToPy):
    packaging_cls = Specifier

    op: Operator
    version: SpecifierVersion

    def to_py_strings(self):
        if str(self.version) == '*':
            yield None
        else:
            yield f'{self.op} {self.version}'


class InclusiveRange(Struct, NpmToPy):
    packaging_cls = Specifier

    left: SpecifierVersion
    right: SpecifierVersion

    def to_py_strings(self):
        left_op = Operator(
            op=Operator.GTE,
            version=self.left,
        )

        right_op = Operator(
            op=Operator.LTE,
            version=self.right,
        )
        yield from left_op.to_py_strings()
        yield from right_op.to_py_strings()


class Tilde(SpecifierVersion):
    packaging_cls = Specifier

    def to_py_strings(self):
        if self.major is None:
            yield None
            return

        elif self.minor is not None:
            yield f'>={int(self.major)}.{int(self.minor)}.{self.patch or 0}'
            next_minor = int(self.minor) + 1
            yield f'<{int(self.major)}.{next_minor}.0'
        else:
            yield f'>={int(self.major)}.0.0'
            next_major = int(self.major) + 1
            yield f'<{next_major}.0.0'


class Caret(SpecifierVersion):
    packaging_cls = Specifier

    def to_py_strings(self):
        if self.major is None:
            yield None
            return

        pieces = (self.major or 0, self.minor or 0, self.patch or 0)
        first_non_zero = None
        for index, piece in enumerate(pieces):
            if piece != 0:
                first_non_zero = index
                break
        else:
            yield str(self)
            return

        yield f'>= {'.'.join(str(piece) for piece in pieces)}'

        new_pieces = list(pieces)
        new_pieces[first_non_zero] += 1
        remaining_pieces = len(new_pieces) - (first_non_zero + 1)

        new_pieces[first_non_zero + 1:] = tuple(repeat(0, remaining_pieces))

        yield f'< {'.'.join(str(piece) for piece in new_pieces)}'


type simple = Comparator | SpecifierVersion | Tilde | Caret


class Range(tuple[simple]):
    def to_py_string(self):
        specifiers = []
        for thing in self:
            specifiers.extend(
                spec
                for spec in thing.to_py_strings()
                if spec is not None
            )

        if not specifiers:
            return ''
        else:
            return ', '.join(specifiers)

    def to_py(self):
        return SpecifierSet(self.to_py_string())


class RangeSet(tuple[Range]):
    def to_py(self):
        msg = 'Python specifiers do not support a logic or'
        raise NotImplementedError(msg)


class Qualifiers(Struct):
    pre_release: str = None
    build: str = None


npm_grammar = None


def get_npm_grammar():
    global npm_grammar
    if npm_grammar is not None:
        return npm_grammar

    npm_grammar = Lark.open_from_package(
        __package__,
        'npm_specifiers.lark',
        start='range_set',
        parser='earley',
    )

    return npm_grammar


def parse_npm(string):
    grammar = get_npm_grammar()
    transformer = NpmSpecifierTransformer()
    tree = grammar.parse(string)
    parsed = transformer.transform(tree)
    return parsed.to_py()


def get(lst, index, default=None):
    try:
        return lst[index]
    except IndexError:
        return default


class Build(str):
    __slots__ = ()


class Prerelease(str):
    __slots__ = ()


class NpmSpecifierTransformer(Transformer):
    POSITIVE_DIGIT = str
    ZERO = str

    def inclusive_range(self, args):
        return InclusiveRange(left=args[0], right=args[1])

    def version(self, args):
        pre_release = None
        build = None
        if q := get(args, 3):
            pre_release = q.pre_release
            build = q.build

        return SpecifierVersion(
            major=get(args, 0, None),
            minor=get(args, 1, None),
            patch=get(args, 2, None),
            pre_release=pre_release,
            build=build,
        )

    def tilde(self, args):
        return Tilde(**args[0].to_dict())

    def range_set(self, args):
        return RangeSet(args)

    def caret(self, args):
        return Caret(**args[0].to_dict())

    def range(self, args):
        return Range(args)

    def OPERATOR(self, args):  # noqa: N802
        match args[0]:
            case '>':
                return Operator.GT
            case '>=':
                return Operator.GTE
            case '<':
                return Operator.LT
            case '<=':
                return Operator.LTE
            case '=':
                return Operator.EQ

    def comparator(self, args):
        match args:
            case [Operator() as op, SpecifierVersion() as ver]:
                return Comparator(
                    op=op,
                    version=ver,
                )
            case [SpecifierVersion() as ver]:
                return Comparator(
                    op=Operator.EQ,
                    version=ver,
                )

    def build(self, args):
        return Build(''.join(args))

    def pre_release(self, args):
        return Prerelease(''.join(args))

    def qualifiers(self, args):
        match args:
            case [Prerelease() as p, Build() as b]:
                return Qualifiers(
                    pre_release=p,
                    build=b,
                )
            case [Prerelease() as p]:
                return Qualifiers(pre_release=p)
            case [Build() as b]:
                return Qualifiers(build=b)

    def numeric_identifier(self, args):
        return int(''.join(args))

    def WILDCARD(self, _args):  # noqa: N802
        return None
