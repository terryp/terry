from contextlib import suppress
from os.path import dirname, join

from fs.errors import DirectoryExists
from fs.memoryfs import MemoryFS
from public import public

from .find_root import find_root
from .list_all_files import list_files


@public
def build_tree(root=None):
    fs = MemoryFS()
    if root is None:
        root = find_root()

    for file in list_files(root):
        file = join('/', file)
        with suppress(DirectoryExists):
            fs.makedirs(dirname(file))

        fs.touch(file)

    return fs
