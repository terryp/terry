from os import EX_OK, getcwd
from os.path import join

from textual.app import App
from textual.widgets import SelectionList

from omniblack.app import model
from omniblack.model import coerce_to
from omniblack.prompt import Form

from ..app import app
from ..base_package import find_nearest
from ..git import git
from ..languages import Languages
from ..package_info import JsPackageInfo, PyPackageInfo
from ..yaml import yaml
from .sync_metadata import create_syncable_cls


class LanguageSelect(App):
    BINDINGS = (
        ('shift+enter', 'accept'),
        ('ctrl+q', 'quit'),
    )

    def compose(self):
        self.selection_list = SelectionList(*(
            (lang.value, lang)
            for lang in Languages
        ))
        yield self.selection_list

    def on_mount(self) -> None:
        opts_list = self.query_one(SelectionList)
        opts_list.border_title = 'What languages will this package use?'

    def action_accept(self):
        if self.selection_list.selected:
            self.exit(self.selection_list.selected)


def prep_python(pkg_root, pkg):
    python_manifest = {
        'project': {
            'name': f'omniblack.{pkg.name}',
            'version': '0.0.1',
        },
    }

    return PyPackageInfo(
        python_manifest,
        join(pkg_root, 'pyproject.toml'),
    )


def prep_js(pkg_root, pkg):
    pkg_json = {
        'name': f'@omniblack/{pkg.name}',
        'version': '0.0.1',
    }

    return JsPackageInfo(
        pkg_json,
        join(pkg_root, 'package.json'),
    )


@app.command
def create_pkg():
    pkg_root = getcwd()

    lang_select = LanguageSelect()
    selected_langs = lang_select.run()
    langs = set(selected_langs) if selected_langs is not None else set()

    if not langs:
        return 1

    form = Form(Struct=model.structs.package_config)

    pkg = form.run()

    if pkg is None:
        return

    raw_pkg = coerce_to(pkg, 'yaml')

    with open(join(pkg_root, 'package_config.yaml'), 'x') as file:
        yaml.dump(raw_pkg, file)

    if Languages.py in langs:
        py_info = prep_python(pkg_root, pkg)
        with open(py_info.path, 'x') as file:
            model.formats.toml.dump(py_info.manifest, file)

    if Languages.js in langs:
        js_info = prep_js(pkg_root, pkg)
        with open(js_info.path, 'x') as file:
            model.formats.json.dump(js_info.manifest, file)

    git.add(pkg_root)
    Sync = create_syncable_cls()
    pkg = find_nearest(pkg_root, Sync)
    pkg.sync()

    return EX_OK
