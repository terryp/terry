#!/usr/bin/env node
/* eslint-env node */

import { createRequire } from 'node:module';
import { generate } from 'astring';
import { sorted } from 'itertools';
import { NodePath as Path } from '@omniblack/pathlib';
import {
    ExportNamedDeclaration,
    Identifier,
    Literal,
    ExportSpecifier,
    Program,
} from '@omniblack/estree';


const dataSuffixes = new Set(['json', 'toml', 'yaml']);

const args = process.argv.slice(2);

if (args.length === 0) {
    await stderr(
        'Please provide a glob of files to be included in the index file',
    );
    // eslint-disable-next-line n/no-process-exit
    process.exit(14);
}

const cwd = Path.cwd();

const indexFile = cwd.join('index.js');

const files = [];
for await (const file of filesOnly(cwd.glob(args))) {
    if (dataSuffixes.has(file.suffix)) {
        files.push(file);
    }
}

if (files.length === 0) {
    await stderr('No files found.');
    // eslint-disable-next-line n/no-process-exit
    process.exit(15);
}


const sortedFiles = sorted(files, (path) => String(path));

const exports = sortedFiles.map((path) => {
    const relPath = path.relativeTo(cwd);
    const specifier = new ExportSpecifier({
        local: new Identifier({ name: 'default' }),
        exported: new Identifier({ name: relPath.name }),
    });
    const source = new Literal({ value: `./${relPath}` });

    return new ExportNamedDeclaration({
        specifiers: [specifier],
        source,
    });
});


const program = new Program({
    body: exports,
});

const programTxt = generate(program);

const require = createRequire(import.meta.url);
const preludePath = new Path(require.resolve('./prelude.txt'));

const prelude = await preludePath.readFile();


const indexText = prelude + '\n\n' + programTxt;

await indexFile.writeFile(indexText);

console.log(`Index updated ${indexFile}`);

async function *filesOnly(iter) {
    for await (const path of iter) {
        if (await path.isFile()) {
            yield path;
        }
    }
}

function stderr(data) {
    // eslint-disable-next-line promise/avoid-new
    return new Promise((resolve, reject) => {
        process.stderr.write(data, (err) => {
            if (err !== null) {
                reject(err);
            }

            resolve();
        });
    });
}
