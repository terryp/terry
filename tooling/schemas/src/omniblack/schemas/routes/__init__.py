from pathlib import PurePosixPath as Path

from werkzeug.exceptions import abort
from werkzeug.wrappers import Response

from omniblack.model import Model
from omniblack.model.json_schema import to_json_schema
from omniblack.server import route

model = Model('', no_expose=True).load_meta_model(
    name='Meta Model',
    no_expose=True,
)


@route.get
def meta(struct_name: Path):
    assert len(struct_name.parts) == 1
    struct_name = struct_name.name.removesuffix('.schema.json')

    if struct_name not in model.structs:
        resp = Response(
            f'Struct "{struct_name}" not found.',
            404,
        )

        abort(resp)

    cls = model.structs[struct_name]
    return to_json_schema(cls)
