# Configuration file for the Sphinx documentation builder.
from datetime import date
from os.path import dirname as dir_name, exists, join, relpath as relative_path

from sphinx import roles

from omniblack.repo import find_packages, find_root

_today = date.today()


# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Omniblack'
copyright = f'{_today.year}, Terry Patterson'
author = 'Terry Patterson'
release = '0.0.1'

source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown',
}


docs_dir = dir_name(__file__)
SRC = find_root()

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'enum_tools.autoenum',
    'myst_parser',
    'omniblack.sphinx_type_role',

    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.extlinks',
    'sphinx.ext.intersphinx',
    'sphinx.ext.napoleon',
    'sphinx.ext.todo',
    'sphinx.ext.viewcode',

    'sphinx_copybutton',
    'sphinx_jinja',
    'sphinx_links',
    'sphinx_prompt',

    'sphinx_toolbox.more_autodoc.generic_bases',
    'sphinx_toolbox.more_autodoc.genericalias',
    'sphinx_toolbox.more_autodoc.typehints',
]

napoleon_preprocess_types = True
napoleon_numpy_docstring = True
napoleon_google_docstring = True

myst_enable_extensions = [
    'colon_fence',
    'deflist',
    'attrs_block',
    'attrs_inline',
]

links_collection = {
    'pydantic.parse_obj_as': 'https://docs.pydantic.dev/1.10/usage/models/#parsing-data-into-a-specified-type',  # noqa E501
}

extlinks = {
    'HTTP_METHOD': ('https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/%s', '%s'),  # noqa E501
    'packaging=spec': ('https://packaging.python.org/en/latest/specifications/%s/', '%s'),  # noqa E501
    'core-metadata': ('https://packaging.python.org/en/latest/specifications/core-metadata/#%s', '%s'),  # noqa E501
}

intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None),
    'werkzeug': ('https://werkzeug.palletsprojects.com/en/2.3.x', None),
    'packaging': ('https://packaging.pypa.io/en/stable/', None),
    'requests': ('https://requests.readthedocs.io/en/latest/', None),
    'pydantic': ('https://docs.pydantic.dev/latest/', None),
}

autodoc_typehints = 'signature'

autodoc_default_options = {
    'autodoc_default_options': True,
    'show-inheritance': True,
    'private-members': False,
}

autoclass_content = 'both'

autodoc_member_order = 'alphabetical'


templates_path = ['_templates']
exclude_patterns = []

todo_include_todos = True


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_book_theme'
html_static_path = ['_static']
html_baseurl = '/docs'
html_use_smartypants = False
smartquotes = False

pkgs = find_packages(SRC, sort=True)

pkgs = [
    pkg
    for pkg in pkgs
    if pkg.py
    if exists(join(pkg.path, 'docs'))
]

paths = [
    join(relative_path(pkg.path, docs_dir), 'index')
    for pkg in pkgs
]


jinja_contexts = {
    'toc': {'toc_entries': paths},
    'SRC': {'SRC': SRC},
}
_rst_prolog = """
.. role:: python(code)
   :language: python
"""


def code_role(
    _role,
    rawtext,
    text,
    lineno,
    inliner,
    options=None,
    content=None,
):

    if options is None:
        options = {'language': 'python'}
    else:
        options['language'] = 'python'

    return roles.code_role(
        'code',
        rawtext,
        text,
        lineno,
        inliner,
        options,
        content,
    )


def setup(app):
    app.add_role('python', code_role)
