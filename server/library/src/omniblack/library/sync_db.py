from logging import getLogger
from os import fspath

from fs.osfs import OSFS
from fs.path import basename, combine, splitext
from fs.wrap import cache_directory

from omniblack.app import config
from omniblack.server import sort_by_deps

from .model import model

log = getLogger(__name__)

_entry_types = None


def entry_types():
    global _entry_types
    if _entry_types is None:
        _entry_types = {
            struct.meta.name.lower(): struct
            for struct in model.structs.values()
            if struct.meta.top_level_struct
        }

    return _entry_types


def prepare_paths():
    for entry_type in entry_types():
        for suffix in model.formats.by_suffix:
            yield entry_type + suffix


def find_entries():
    path = fspath(config.library_options.base_path)

    fs = cache_directory(OSFS(path))
    file_filter = tuple(prepare_paths())
    for file in fs.walk.files(filter=file_filter):
        full_path = combine(path, file)
        stem, _ = splitext(basename(file))
        struct = entry_types()[stem]
        indiv = struct.load_file(full_path)
        indiv._id = file
        yield indiv


def sync_lib():
    all_entries = tuple(find_entries())
    ordered_entries = sorted(all_entries, key=sort_by_deps)

    for entry in ordered_entries:
        log.debug(f"Loading '{entry._id}'")
        config.storage.save(entry)
