from omniblack.model import Model
from omniblack.server import ServerModelPlugin

model = Model(
    "Omniblack Library's Model",
    struct_packages=['omniblack.library.model'],
    plugins=[ServerModelPlugin],
)
