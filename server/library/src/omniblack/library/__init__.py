from omniblack import security
from omniblack.server import Server
from omniblack.utils import improve_module

from .model import model
from .sync_db import sync_lib


class LibraryServer(Server):
    def startup(self):
        sync_lib()


app = LibraryServer(
    'omniblack.library',
    model=model,
    option_structs=(model.structs.library_options,),
    plugins=(security.SecurityPlugin,)
)

improve_module(__name__, ('routes',))
