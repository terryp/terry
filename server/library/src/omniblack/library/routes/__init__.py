from omniblack import security
from omniblack.app import config
from omniblack.library.model import Literature
from omniblack.server import route


@route.get(template='index')
def root():
    works = config.storage.search(Literature, limit=10)
    return {'works': works}


def library():
    return 'library'


@security.public
@route.get(template='loading.svg')
def loading():
    return {}


@security.public
@route.get(template='assets/icon.svg')
def icon(color: str = 'black'):
    return {'color': color}
