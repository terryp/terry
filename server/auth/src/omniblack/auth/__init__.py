from omniblack.security import SecurityPlugin
from omniblack.server import Server
from omniblack.utils import improve_module

from .model import model

app = Server(
    'omniblack.auth',
    model=model,
    plugins=(SecurityPlugin,),
)

improve_module(__name__, ('routes',))
