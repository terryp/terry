from datetime import datetime, timedelta
from logging import getLogger

from werkzeug.exceptions import Unauthorized
from werkzeug.utils import redirect

from omniblack import security
from omniblack.app import config
from omniblack.secret import Password, Secret
from omniblack.server import route

log = getLogger(__name__)


@route.get(template='index')
def root():
    return {}


max_age = timedelta(days=356)


@security.public
@route.post
def logout():
    if not config.user:
        return redirect('/', 307)

    user = config.user
    user.sessions = [
        session
        for session in user.sessions
        if session.token.reveal() != config.session.token.reveal()
    ]
    config.storage.save(user)

    resp = redirect('/', 307)

    resp.delete_cookie(
        'omniblack-session',
        secure=True,
        httponly=True,
        samesite='Strict',
    )

    return resp


@security.public
@route.get(template='login')
def login():
    if config.user:
        return redirect('/', 307)
    else:
        return None


@security.public
@login.post
def login(email, password: Password):
    if config.user:
        config.user.sessions = [
            session
            for session in config.user.sessions
            if session is not config.session
        ]

    users = tuple(config.storage.search(
        config.model.structs.User,
        mongo_filter={'email': email},
    ))

    if not users:
        raise Unauthorized

    user, = users

    if user._id == getattr(config.user, '_id', False):
        user = config.user

    if not password.verify_password_against(user.password):
        raise Unauthorized

    token = Secret.random_secret(128)
    expires = datetime.now() + max_age

    Session = config.model.structs.Session
    session = Session(token=token, expiration=expires)

    if not hasattr(user, 'sessions'):
        user.sessions = []

    user.sessions.append(session)
    config.storage.save(user)

    resp = redirect('/', 307)
    resp.set_cookie(
        key='omniblack-session',
        value=token.reveal(),
        secure=True,
        httponly=True,
        max_age=max_age,
        expires=expires,
        samesite='Strict',
    )
    return resp


@route.get(template='user')
def user():
    return {
        'user': config.user,
    }


@route.get
def verify_user():
    return {
        'display_name': config.user.display_name,
        'email': config.user.email,
    }


@security.public
@route.get(template='assets/icon.svg')
def icon(color: str = 'black'):
    return {'color': color}
