from omniblack.model import Model
from omniblack.server import ServerModelPlugin

model = Model(
    "Omniblack Auth's Model",
    struct_packages=['omniblack.auth.model', 'omniblack.security.model'],
    plugins=[ServerModelPlugin],
)
