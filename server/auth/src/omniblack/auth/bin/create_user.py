from omniblack.app import config
from omniblack.auth import app, model
from omniblack.cli import CLI
from omniblack.prompt import Form

cli = CLI('omniblack.auth', model=model)


@cli.command
@app.helper
def main():
    form = Form(
        Struct=model.structs.User,
        fields=('display_name', 'email', 'password'),
    )

    user = form.run()

    user.password = user.password.hash()

    if user is None:
        return

    config.storage.save(user)
